-- Table structure for table `ci_admins` 

CREATE TABLE `ci_admins` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `active` int(1) DEFAULT '0',
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `last_login` datetime DEFAULT NULL,
  `last_login_ip` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  UNIQUE KEY `username` (`username`)
);

-- Table structure for table `ci_admins_access` 

CREATE TABLE `ci_admins_access` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `admin_id` bigint(20) NOT NULL,
  `controller` varchar(100) NOT NULL,
  `add` int(1) DEFAULT '0',
  `edit` int(1) DEFAULT '0',
  `delete` int(1) DEFAULT '0',
  PRIMARY KEY (`id`)
);

-- Table structure for table `ci_admins_sessions` 

CREATE TABLE `ci_admins_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`),
  KEY `last_activity_idx` (`last_activity`)
);

-- Table structure for table `ci_attributes` 

CREATE TABLE `ci_attributes` (
  `attr_id` int(10) NOT NULL AUTO_INCREMENT,
  `attr_controller` varchar(50) NOT NULL,
  `attr_name` varchar(50) NOT NULL,
  `attr_label` varchar(50) NOT NULL,
  `attr_description` varchar(160) DEFAULT NULL,
  PRIMARY KEY (`attr_id`)
);

-- Table structure for table `ci_goals` 

CREATE TABLE `ci_goals` (
  `goal_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `goal_name` varchar(200) NOT NULL,
  `goal_slug` varchar(200) NOT NULL,
  `grade_level` int(10) NOT NULL,
  `goal_public` int(1) NOT NULL DEFAULT '0',
  `creator` bigint(20) NOT NULL,
  PRIMARY KEY (`goal_id`),
  KEY `creator` (`creator`),
  CONSTRAINT `GOALCREATOR` FOREIGN KEY (`creator`) REFERENCES `ci_users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
);

-- Table structure for table `ci_lessons` 

CREATE TABLE `ci_lessons` (
  `lesson_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `lesson_title` varchar(100) NOT NULL,
  `lesson_slug` varchar(100) NOT NULL,
  `lesson_level` bigint(20) NOT NULL,
  `lesson_chapter` bigint(20) NOT NULL,
  `lesson_video_url` text,
  `lesson_video_duration` varchar(10) DEFAULT NULL,
  `lesson_number` int(10) NOT NULL,
  `lesson_active` int(1) DEFAULT '0',
  `lesson_quiz` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`lesson_id`),
  UNIQUE KEY `lesson_id` (`lesson_id`)
);

-- Table structure for table `ci_lessons_meta` 

CREATE TABLE `ci_lessons_meta` (
  `lmeta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `lesson_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext,
  PRIMARY KEY (`lmeta_id`)
);

-- Table structure for table `ci_lessons_quiz` 

CREATE TABLE `ci_lessons_quiz` (
  `quiz_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `lesson_id` bigint(20) NOT NULL,
  `quiz_number` int(10) NOT NULL,
  `quiz_time` varchar(10) NOT NULL,
  `quiz_question` text NOT NULL,
  `quiz_type` varchar(100) DEFAULT NULL,
  `quiz_details` text,
  `quiz_correct` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`quiz_id`)
);

-- Table structure for table `ci_lessons_subscriptions` 

CREATE TABLE `ci_lessons_subscriptions` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `lesson_id` bigint(20) NOT NULL,
  `s_plan_id` int(10) NOT NULL,
  PRIMARY KEY (`id`)
);

-- Table structure for table `ci_lessons_tags` 

CREATE TABLE `ci_lessons_tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tag_id` int(11) NOT NULL,
  `lesson_id` int(11) NOT NULL,
  `active` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `tag_id` (`tag_id`),
  KEY `lesson_id` (`lesson_id`)
);

-- Table structure for table `ci_level_chapters` 

CREATE TABLE `ci_level_chapters` (
  `level_chapter_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `level_id` bigint(20) NOT NULL,
  `chapter_id` bigint(20) NOT NULL,
  `chapter_title` varchar(200) NOT NULL,
  PRIMARY KEY (`level_chapter_id`),
  UNIQUE KEY `level_chapter_id` (`level_chapter_id`)
);

-- Table structure for table `ci_media_uploads` 

CREATE TABLE `ci_media_uploads` (
  `media_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `file_name` varchar(255) NOT NULL,
  `file_type` varchar(100) DEFAULT NULL,
  `file_path` varchar(255) DEFAULT NULL,
  `full_path` varchar(255) DEFAULT NULL,
  `raw_name` varchar(255) DEFAULT NULL,
  `orig_name` varchar(255) DEFAULT NULL,
  `client_name` varchar(255) DEFAULT NULL,
  `file_ext` varchar(100) DEFAULT NULL,
  `file_size` float DEFAULT NULL,
  `is_image` int(1) DEFAULT NULL,
  `image_width` int(10) DEFAULT NULL,
  `image_height` int(10) DEFAULT NULL,
  `image_type` varchar(100) DEFAULT NULL,
  `image_size_str` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`media_id`)
);

-- Table structure for table `ci_settings` 

CREATE TABLE `ci_settings` (
  `setting_id` int(20) NOT NULL AUTO_INCREMENT,
  `setting_controller` varchar(50) NOT NULL,
  `setting_name` varchar(50) NOT NULL,
  `setting_description` varchar(200) NOT NULL,
  `setting_value` text NOT NULL,
  PRIMARY KEY (`setting_id`)
);

-- Table structure for table `ci_subscription_attributes` 

CREATE TABLE `ci_subscription_attributes` (
  `s_attr_id` int(10) NOT NULL AUTO_INCREMENT,
  `s_plan_id` int(10) NOT NULL,
  `attr_id` int(10) NOT NULL,
  `s_attr_value` varchar(100) NOT NULL,
  PRIMARY KEY (`s_attr_id`)
);

-- Table structure for table `ci_subscription_plans` 

CREATE TABLE `ci_subscription_plans` (
  `s_plan_id` int(10) NOT NULL AUTO_INCREMENT,
  `s_plan_name` varchar(100) NOT NULL,
  `s_plan_price` float DEFAULT '0',
  `s_plan_active` int(1) DEFAULT '0',
  `s_plan_order` int(2) DEFAULT '0',
  PRIMARY KEY (`s_plan_id`)
);

-- Table structure for table `ci_tax_chapters` 

CREATE TABLE `ci_tax_chapters` (
  `chapter_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `chapter_name` varchar(100) NOT NULL,
  `chapter_slug` varchar(100) NOT NULL,
  `chapter_description` text,
  `chapter_active` int(1) NOT NULL DEFAULT '0',
  `chapter_order` int(3) DEFAULT '0',
  PRIMARY KEY (`chapter_id`),
  UNIQUE KEY `chapter_id` (`chapter_id`)
);

-- Table structure for table `ci_tax_levels` 

CREATE TABLE `ci_tax_levels` (
  `level_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `level_name` varchar(100) NOT NULL,
  `level_slug` varchar(100) NOT NULL,
  `level_description` text,
  `level_active` int(1) NOT NULL DEFAULT '0',
  `level_order` int(3) DEFAULT '0',
  PRIMARY KEY (`level_id`),
  UNIQUE KEY `level_id` (`level_id`)
);

-- Table structure for table `ci_tax_tags` 

CREATE TABLE `ci_tax_tags` (
  `tag_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tag_name` varchar(200) NOT NULL,
  `tag_slug` varchar(200) DEFAULT NULL,
  `tag_count` int(10) DEFAULT '0',
  `tag_active` int(11) DEFAULT '0',
  PRIMARY KEY (`tag_id`)
);

-- Table structure for table `ci_users` 

CREATE TABLE `ci_users` (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_username` varchar(100) NOT NULL,
  `user_password` varchar(100) NOT NULL,
  `user_type` varchar(10) DEFAULT NULL,
  `user_level` bigint(20) DEFAULT NULL,
  `user_firstname` varchar(100) NOT NULL,
  `user_lastname` varchar(100) DEFAULT NULL,
  `user_email` varchar(100) DEFAULT NULL,
  `user_plan` varchar(100) DEFAULT 'free',
  `user_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `user_expiry` timestamp NULL DEFAULT NULL,
  `user_active` int(1) DEFAULT '0',
  `last_login` timestamp NULL DEFAULT NULL,
  `last_login_ip` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
);

-- Table structure for table `ci_users_goals` 

CREATE TABLE `ci_users_goals` (
  `ug_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `goal_id` bigint(20) NOT NULL,
  PRIMARY KEY (`ug_id`),
  KEY `user_id` (`user_id`,`goal_id`),
  KEY `goal_id` (`goal_id`),
  CONSTRAINT `ci_users_goals_ibfk_2` FOREIGN KEY (`goal_id`) REFERENCES `ci_goals` (`goal_id`) ON DELETE CASCADE,
  CONSTRAINT `ci_users_goals_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `ci_users` (`user_id`) ON DELETE CASCADE
);

-- Table structure for table `ci_users_lessons_quizzes` 

CREATE TABLE `ci_users_lessons_quizzes` (
  `ulq_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `session_id` bigint(20) NOT NULL,
  `lesson_id` bigint(20) NOT NULL,
  `quiz_number` int(10) NOT NULL,
  `time_taken` int(10) NOT NULL DEFAULT '0',
  `mistakes` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ulq_id`),
  KEY `user_id` (`user_id`,`session_id`)
);

-- Table structure for table `ci_users_lessons_sessions` 

CREATE TABLE `ci_users_lessons_sessions` (
  `uls_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `lesson_id` bigint(20) NOT NULL,
  `started` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `ended` timestamp NULL DEFAULT NULL,
  `time_taken` int(10) DEFAULT NULL,
  `mistakes` int(10) DEFAULT NULL,
  PRIMARY KEY (`uls_id`),
  KEY `user_id` (`user_id`)
);

-- Table structure for table `ci_users_logs` 

CREATE TABLE `ci_users_logs` (
  `log_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `log_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_id` bigint(20) NOT NULL,
  `log_code` varchar(10) NOT NULL,
  `log_msg` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`log_id`)
);

-- Table structure for table `ci_users_meta` 

CREATE TABLE `ci_users_meta` (
  `umeta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext,
  PRIMARY KEY (`umeta_id`)
);

-- Table structure for table `ci_users_profiles` 

CREATE TABLE `ci_users_profiles` (
  `profile_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `attr_id` int(10) NOT NULL,
  `profile_value` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`profile_id`)
);

-- Table structure for table `ci_users_sessions` 

CREATE TABLE `ci_users_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`),
  KEY `last_activity_idx` (`last_activity`)
);

-- Table structure for table `ci_users_sponsors` 

CREATE TABLE `ci_users_sponsors` (
  `sponsorship_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `sponsor_id` bigint(20) NOT NULL,
  `date_sponsored` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `active` int(1) DEFAULT '0',
  PRIMARY KEY (`sponsorship_id`)
);

