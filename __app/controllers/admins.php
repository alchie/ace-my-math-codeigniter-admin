<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admins extends MY_Controller {

    function __construct() {
        parent::__construct();

        $this->load->library('template_data');
        $this->lang->load('admins');
        $this->load->model( array('Admins_model') );
        
        $this->template_data->set('main_page', 'accounts' ); 
        $this->template_data->set('sub_page', 'admins' ); 
        $this->template_data->set('page_title', 'Admins' ); 

    }
    
    public function index()
	{
        $this->load->view('admins', $this->template_data->get() );
	}
	
	public function ajax($required=NULL, $value=NULL) {

		switch($this->input->post('action')) {
			case 'list':
				$list_limit = ( $this->input->post('limit') != '') ? (int) $this->input->post('limit') : 20;
				$order_by = ( $this->input->post('order_by') != '') ? $this->input->post('order_by') : 'id';
				$order_sort = ( $this->input->post('order_sort') != '') ? $this->input->post('order_sort') : 'DESC';
				$list_start = ( $this->input->post('list_start') != '') ? $this->input->post('list_start') : 0;
				
				$list = new $this->Admins_model;
				$pagination = new $this->Admins_model;
				
				$pagination->setSelect('COUNT(*) as total_items');

				
				if( is_array($this->input->post('filter')) && count($this->input->post('filter')) > 0 ) {
					foreach( $this->input->post('filter') as $filter ) {
						$list->setFilter($filter['key'],$filter['value'],$filter['table']);
						$pagination->setFilter($filter['key'],$filter['value'],$filter['table']);
					}
				}
				
				$list->setOrder($order_by, $order_sort);
				$list->setStart($list_start);
				$list->setLimit($list_limit);
				
				echo json_encode( array(
							'table' => 'admins',
							'total_items' => (int) $pagination->get()->total_items,
							'start' => $list_start,
							'limit' => $list_limit,
							'error' => false,
							'list' => true,
							'results' => $list->populate()
						) );
				exit;
			break;
			case 'get':
				$item = $this->Admins_model;
				$item->setId( $this->input->post('id'), true );

				echo json_encode( array(
							'id' => $this->input->post('id'),
							'table' => 'admins',
							'error' => false,
							'get' => true,
							'results' => $item->get()
						) );
						exit;
			break;
			case 'update_field':
				if(  ! $this->template_data->get('admin_access')->controller_admins ) {
					return 0;
				}
				$results = array(
							'id' => $this->input->post('id'),
							'table' => 'admins',
							'error' => true,
							'update_field' => false,
							'message' => 'Unable to update field!'
						);
				$item = $this->Admins_model;
				$item->setId( $this->input->post('id'), true );
				$item->setFieldValue( $this->input->post('field'), $this->input->post('value'), FALSE, TRUE );
				if( $item->updateById() ) {
					$results['error'] = false;
					$results['update_field'] = true;
					$results['message'] = 'Successfully Updated!';
				}
				echo json_encode( $results );
				exit;
			break;
			case 'add':
				if(  ! $this->template_data->get('admin_access')->controller_admins ) {
					return 0;
				}
				echo json_encode($this->submission('add'));
				exit;
			break;
			case 'edit':
				if(  ! $this->template_data->get('admin_access')->controller_admins ) {
					return 0;
				}
				echo json_encode($this->submission('edit'));
				exit;
			break;
			case 'delete':
				if(  ! $this->template_data->get('admin_access')->controller_admins ) {
					return 0;
				}
				$results = array(
							'id' => $this->input->post('id'),
							'table' => 'admins',
							'error' => true,
							'removed' => false,
							'message' => 'Unable to delete!'
						);
				$this->Admins_model->setId( $this->input->post('id') );
				$data = $this->Admins_model->getById();
		
				
				if( $this->Admins_model->deleteById() ) {
					$results['error'] = false;
					$results['removed'] = true;
					$results['message'] = 'Successfully Deleted!';
				}
		
				echo json_encode( $results );
				exit;
			break;
			case "upload":
				if(  ! $this->template_data->get('admin_access')->controller_admins ) {
					return 0;
				}
				$results = array(
					'group' => $this->input->post('group'),
					'key' => $this->input->post('key'),
					'table' => 'media_uploads',
					'error' => true,
					'removed' => false,
					'message' => 'Unable to upload!'
				);
				
				$config['upload_path'] = 'None';
				$config['allowed_types'] = '';
				$config['max_size']	= 'None';
				$config['max_width']  = 'None';
				$config['max_height']  = 'None';

				$this->load->library('upload', $config);

				if ( ! $this->upload->do_upload())
				{
					$results['error'] = true;
					$results['message'] = $this->upload->display_errors();
					
				}
				else
				{
					$results['error'] = false;
					$results['message'] = "Success!";
					$upload_data = $this->upload->data();
					$results['upload_data'] = $upload_data;
					
					$container = new $this->Admins_model;

					if( $container->insert() ) {
						$results['id'] = $container->getId();
						$results['results'] = $container->getById();
					}
				}
				echo json_encode( $results );
				exit;
			break;
		
		}
		echo 0;
		exit;
	}
	
	private function submission($action) {
	    $this->load->library('form_validation');
	    $results = array(
			'error' => true,
			'message' => 'No Response!',
			'table' => 'admins',
	    );

		if( $action == 'add' ) {
			$this->form_validation->set_rules('name', 'lang:admins_name', 'required');
			$this->form_validation->set_rules('email', 'lang:admins_email', 'required|valid_email|is_unique[admins.email]');
			$this->form_validation->set_rules('password', 'lang:admins_password', 'required|matches[confirm_password]|sha1|md5');
			$this->form_validation->set_rules('username', 'lang:admins_username', 'required|is_unique[admins.username]');
		}
		elseif( $action == 'edit' ) {
			$this->form_validation->set_rules('id', 'lang:admins_id', 'required');
			$this->form_validation->set_rules('name', 'lang:admins_name', 'required');
			$this->form_validation->set_rules('email', 'lang:admins_email', 'required|valid_email');
			$this->form_validation->set_rules('password', 'lang:admins_password', 'matches[confirm_password]|sha1|md5');
			$this->form_validation->set_rules('username', 'lang:admins_username', 'required');
			$this->form_validation->set_rules('last_login_ip', 'lang:admins_last_login_ip', 'valid_ip');
		}

		if ($this->form_validation->run() == FALSE)
		{
			if( $this->input->post() ) {
				$this->template_data->alert( validation_errors(), 'danger');
				$results['message'] = validation_errors();
			}
		} 
		else 
		{
			$container = new $this->Admins_model;
			if( $this->input->post('id') !== FALSE ) {
				$container->setId( $this->input->post('id'), FALSE, TRUE );
			}

			$container->setName( $this->input->post('name'), FALSE, TRUE );

			$container->setEmail( $this->input->post('email'), FALSE, TRUE );

			if( $this->input->post('password') !== FALSE ) {
				$container->setPassword( $this->input->post('password'), FALSE, TRUE );
			}

			$container->setUsername( $this->input->post('username'), FALSE, TRUE );

			if( $this->input->post('active') !== FALSE ) {
				$container->setActive( $this->input->post('active'), FALSE, TRUE );
			}
			else {
				$container->setActive( '0', FALSE, TRUE );
			}

			if( $this->input->post('date_created') !== FALSE ) {
				$container->setDateCreated( $this->input->post('date_created'), FALSE, TRUE );
			}

			if( $this->input->post('last_login') !== FALSE ) {
				$container->setLastLogin( $this->input->post('last_login'), FALSE, TRUE );
			}

			if( $this->input->post('last_login_ip') !== FALSE ) {
				$container->setLastLoginIp( $this->input->post('last_login_ip'), FALSE, TRUE );
			}

			if( $action == 'add' ) { 
			

				if( $container->replace() ) {
					$this->template_data->alert( 'Successfully Added!', 'success');
					$results['added'] = true;
					$results['error'] = false;
					$results['message'] = 'Successfully Added!';
				} else {
					$results['added'] = false;
					$results['error'] = true;
					$results['message'] = 'Unable to add data!';
				}
			} 
			elseif( $action == 'edit' ) { 
				if( $container->updateById() ) {
					$this->template_data->alert( 'Successfully Updated!', 'success');
					$results['updated'] = true;
					$results['error'] = false;
					$results['message'] = 'Successfully Updated!';
				} else {
					$results['updated'] = false;
					$results['error'] = true;
					$results['message'] = 'Unable to update data!';
				}
			}

			$results['id'] = $container->getId();
			$results['results'] = $container->getById();
		}

	    return $results;
	}
	
}
/* End of file admins.php */
/* Location: ./application/controllers/admins.php */
