<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Lessons_subscriptions extends MY_Controller {

    function __construct() {
        parent::__construct();

        $this->load->library('template_data');
        $this->lang->load('lessons_subscriptions');
        $this->load->model( array('Lessons_subscriptions_model') );
        
        $this->template_data->set('main_page', 'lessons_subscriptions' ); 
        $this->template_data->set('sub_page', 'lessons_subscriptions' ); 
        $this->template_data->set('page_title', 'Lessons Subscriptions' ); 

    }
    
    public function index()
	{
        $this->load->view('404', $this->template_data->get() );
	}
	
	public function ajax($required=NULL, $value=NULL) {

		switch($this->input->post('action')) {
			case 'list':
				$list_limit = ( $this->input->post('limit') != '') ? (int) $this->input->post('limit') : 20;
				$order_by = ( $this->input->post('order_by') != '') ? $this->input->post('order_by') : 'lesson_id';
				$order_sort = ( $this->input->post('order_sort') != '') ? $this->input->post('order_sort') : 'ASC';
				$list_start = ( $this->input->post('list_start') != '') ? $this->input->post('list_start') : 0;
				
				$list = new $this->Lessons_subscriptions_model;
				$pagination = new $this->Lessons_subscriptions_model;
				
				$list->setJoin('ci_lessons ci_lessons_c','ci_lessons_subscriptions.lesson_id = ci_lessons_c.lesson_id');
				$list->setSelect('ci_lessons_subscriptions.*');
				$list->setSelect('ci_lessons_c.lesson_title as lesson_title_c');

				$list->setJoin('ci_subscription_plans ci_subscription_plans_c','ci_lessons_subscriptions.s_plan_id = ci_subscription_plans_c.s_plan_id');
				$list->setSelect('ci_lessons_subscriptions.*');
				$list->setSelect('ci_subscription_plans_c.s_plan_name as s_plan_name_c');

				$pagination->setSelect('COUNT(*) as total_items');
				$pagination->setJoin('ci_lessons ci_lessons_c','ci_lessons_subscriptions.lesson_id = ci_lessons_c.lesson_id');
				$pagination->setJoin('ci_subscription_plans ci_subscription_plans_c','ci_lessons_subscriptions.s_plan_id = ci_subscription_plans_c.s_plan_id');

				
				if( is_array($this->input->post('filter')) && count($this->input->post('filter')) > 0 ) {
					foreach( $this->input->post('filter') as $filter ) {
						$list->setFilter($filter['key'],$filter['value'],$filter['table']);
						$pagination->setFilter($filter['key'],$filter['value'],$filter['table']);
					}
				}
				
				$list->setOrder($order_by, $order_sort);
				$list->setStart($list_start);
				$list->setLimit($list_limit);
				
				echo json_encode( array(
							'table' => 'lessons_subscriptions',
							'total_items' => (int) $pagination->get()->total_items,
							'start' => $list_start,
							'limit' => $list_limit,
							'error' => false,
							'list' => true,
							'results' => $list->populate()
						) );
				exit;
			break;
			case 'get':
				$item = $this->Lessons_subscriptions_model;
				$item->setId( $this->input->post('id'), true );

				$item->setJoin('ci_lessons ci_lessons_c','ci_lessons_subscriptions.lesson_id = ci_lessons_c.lesson_id');
				$item->setSelect('ci_lessons_subscriptions.*');
				$item->setSelect('ci_lessons_c.lesson_title as lesson_title_c');

				$item->setJoin('ci_subscription_plans ci_subscription_plans_c','ci_lessons_subscriptions.s_plan_id = ci_subscription_plans_c.s_plan_id');
				$item->setSelect('ci_lessons_subscriptions.*');
				$item->setSelect('ci_subscription_plans_c.s_plan_name as s_plan_name_c');

				echo json_encode( array(
							'id' => $this->input->post('id'),
							'table' => 'lessons_subscriptions',
							'error' => false,
							'get' => true,
							'results' => $item->get()
						) );
						exit;
			break;
			case 'update_field':
				if(  ! $this->template_data->get('admin_access')->controller_lessons_subscriptions ) {
					return 0;
				}
				$results = array(
							'id' => $this->input->post('id'),
							'table' => 'lessons_subscriptions',
							'error' => true,
							'update_field' => false,
							'message' => 'Unable to update field!'
						);
				$item = $this->Lessons_subscriptions_model;
				$item->setId( $this->input->post('id'), true );
				$item->setFieldValue( $this->input->post('field'), $this->input->post('value'), FALSE, TRUE );
				if( $item->updateById() ) {
					$results['error'] = false;
					$results['update_field'] = true;
					$results['message'] = 'Successfully Updated!';
				}
				echo json_encode( $results );
				exit;
			break;
			case 'add':
				if(  ! $this->template_data->get('admin_access')->controller_lessons_subscriptions ) {
					return 0;
				}
				echo json_encode($this->submission('add'));
				exit;
			break;
			case 'edit':
				if(  ! $this->template_data->get('admin_access')->controller_lessons_subscriptions ) {
					return 0;
				}
				echo json_encode($this->submission('edit'));
				exit;
			break;
			case 'delete':
				if(  ! $this->template_data->get('admin_access')->controller_lessons_subscriptions ) {
					return 0;
				}
				$results = array(
							'id' => $this->input->post('id'),
							'table' => 'lessons_subscriptions',
							'error' => true,
							'removed' => false,
							'message' => 'Unable to delete!'
						);
				$this->Lessons_subscriptions_model->setId( $this->input->post('id') );
				$data = $this->Lessons_subscriptions_model->getById();
		
				
				if( $this->Lessons_subscriptions_model->deleteById() ) {
					$results['error'] = false;
					$results['removed'] = true;
					$results['message'] = 'Successfully Deleted!';
				}
		
				echo json_encode( $results );
				exit;
			break;
			case "upload":
				if(  ! $this->template_data->get('admin_access')->controller_lessons_subscriptions ) {
					return 0;
				}
				$results = array(
					'group' => $this->input->post('group'),
					'key' => $this->input->post('key'),
					'table' => 'media_uploads',
					'error' => true,
					'removed' => false,
					'message' => 'Unable to upload!'
				);
				
				$config['upload_path'] = 'None';
				$config['allowed_types'] = '';
				$config['max_size']	= 'None';
				$config['max_width']  = 'None';
				$config['max_height']  = 'None';

				$this->load->library('upload', $config);

				if ( ! $this->upload->do_upload())
				{
					$results['error'] = true;
					$results['message'] = $this->upload->display_errors();
					
				}
				else
				{
					$results['error'] = false;
					$results['message'] = "Success!";
					$upload_data = $this->upload->data();
					$results['upload_data'] = $upload_data;
					
					$container = new $this->Lessons_subscriptions_model;

					if( $container->insert() ) {
						$results['id'] = $container->getId();
						$results['results'] = $container->getById();
					}
				}
				echo json_encode( $results );
				exit;
			break;
		
		}
		echo 0;
		exit;
	}
	
	private function submission($action) {
	    $this->load->library('form_validation');
	    $results = array(
			'error' => true,
			'message' => 'No Response!',
			'table' => 'lessons_subscriptions',
	    );

		if( $action == 'add' ) {
			$this->form_validation->set_rules('lesson_id', 'lang:lessons_subscriptions_lesson_id', 'required');
			$this->form_validation->set_rules('s_plan_id', 'lang:lessons_subscriptions_s_plan_id', 'required');
		}
		elseif( $action == 'edit' ) {
			$this->form_validation->set_rules('lesson_id', 'lang:lessons_subscriptions_lesson_id', 'required');
			$this->form_validation->set_rules('s_plan_id', 'lang:lessons_subscriptions_s_plan_id', 'required');
		}

		if ($this->form_validation->run() == FALSE)
		{
			if( $this->input->post() ) {
				$this->template_data->alert( validation_errors(), 'danger');
				$results['message'] = validation_errors();
			}
		} 
		else 
		{
			$container = new $this->Lessons_subscriptions_model;
			if( $this->input->post('id') !== FALSE ) {
				$container->setId( $this->input->post('id'), FALSE, TRUE );
			}

			$container->setLessonId( $this->input->post('lesson_id'), FALSE, TRUE );

			$container->setSPlanId( $this->input->post('s_plan_id'), FALSE, TRUE );

			if( $action == 'add' ) { 
			

				if( $container->replace() ) {
					$this->template_data->alert( 'Successfully Added!', 'success');
					$results['added'] = true;
					$results['error'] = false;
					$results['message'] = 'Successfully Added!';
				} else {
					$results['added'] = false;
					$results['error'] = true;
					$results['message'] = 'Unable to add data!';
				}
			} 
			elseif( $action == 'edit' ) { 
				if( $container->updateById() ) {
					$this->template_data->alert( 'Successfully Updated!', 'success');
					$results['updated'] = true;
					$results['error'] = false;
					$results['message'] = 'Successfully Updated!';
				} else {
					$results['updated'] = false;
					$results['error'] = true;
					$results['message'] = 'Unable to update data!';
				}
			}
				$container->setJoin('ci_lessons ci_lessons_c','ci_lessons_subscriptions.lesson_id = ci_lessons_c.lesson_id');
				$container->setSelect('ci_lessons_subscriptions.*');
				$container->setSelect('ci_lessons_c.lesson_title as lesson_title_c');

				$container->setJoin('ci_subscription_plans ci_subscription_plans_c','ci_lessons_subscriptions.s_plan_id = ci_subscription_plans_c.s_plan_id');
				$container->setSelect('ci_lessons_subscriptions.*');
				$container->setSelect('ci_subscription_plans_c.s_plan_name as s_plan_name_c');


			$results['id'] = $container->getId();
			$results['results'] = $container->getById();
		}

	    return $results;
	}
	
}
/* End of file lessons_subscriptions.php */
/* Location: ./application/controllers/lessons_subscriptions.php */
