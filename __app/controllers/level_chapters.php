<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Level_chapters extends MY_Controller {

    function __construct() {
        parent::__construct();

        $this->load->library('template_data');
        $this->lang->load('level_chapters');
        $this->load->model( array('Level_chapters_model') );
        
        $this->template_data->set('main_page', 'level_chapters' ); 
        $this->template_data->set('sub_page', 'level_chapters' ); 
        $this->template_data->set('page_title', 'Level Chapters' ); 

    }
    
    public function index()
	{
        $this->load->view('404', $this->template_data->get() );
	}
	
	public function ajax($required=NULL, $value=NULL) {

		switch($this->input->post('action')) {
			case 'list':
				$list_limit = ( $this->input->post('limit') != '') ? (int) $this->input->post('limit') : 20;
				$order_by = ( $this->input->post('order_by') != '') ? $this->input->post('order_by') : 'chapter_id';
				$order_sort = ( $this->input->post('order_sort') != '') ? $this->input->post('order_sort') : 'ASC';
				$list_start = ( $this->input->post('list_start') != '') ? $this->input->post('list_start') : 0;
				
				$list = new $this->Level_chapters_model;
				$pagination = new $this->Level_chapters_model;
				
				$list->setJoin('ci_tax_levels ci_tax_levels','ci_level_chapters.level_id = ci_tax_levels.level_id');
				$list->setSelect('ci_level_chapters.*');
				$list->setSelect('ci_tax_levels.level_name as level_name');

				$list->setJoin('ci_tax_chapters ci_tax_chapters','ci_level_chapters.chapter_id = ci_tax_chapters.chapter_id');
				$list->setSelect('ci_level_chapters.*');
				$list->setSelect('ci_tax_chapters.chapter_name as chapter_name');

				$pagination->setSelect('COUNT(*) as total_items');
				$pagination->setJoin('ci_tax_levels ci_tax_levels','ci_level_chapters.level_id = ci_tax_levels.level_id');
				$pagination->setJoin('ci_tax_chapters ci_tax_chapters','ci_level_chapters.chapter_id = ci_tax_chapters.chapter_id');

				
				if( is_array($this->input->post('filter')) && count($this->input->post('filter')) > 0 ) {
					foreach( $this->input->post('filter') as $filter ) {
						$list->setFilter($filter['key'],$filter['value'],$filter['table']);
						$pagination->setFilter($filter['key'],$filter['value'],$filter['table']);
					}
				}
				
				$list->setOrder($order_by, $order_sort);
				$list->setStart($list_start);
				$list->setLimit($list_limit);
				
				echo json_encode( array(
							'table' => 'level_chapters',
							'total_items' => (int) $pagination->get()->total_items,
							'start' => $list_start,
							'limit' => $list_limit,
							'error' => false,
							'list' => true,
							'results' => $list->populate()
						) );
				exit;
			break;
			case 'get':
				$item = $this->Level_chapters_model;
				$item->setLevelChapterId( $this->input->post('level_chapter_id'), true );

				$item->setJoin('ci_tax_levels ci_tax_levels','ci_level_chapters.level_id = ci_tax_levels.level_id');
				$item->setSelect('ci_level_chapters.*');
				$item->setSelect('ci_tax_levels.level_name as level_name');

				$item->setJoin('ci_tax_chapters ci_tax_chapters','ci_level_chapters.chapter_id = ci_tax_chapters.chapter_id');
				$item->setSelect('ci_level_chapters.*');
				$item->setSelect('ci_tax_chapters.chapter_name as chapter_name');

				echo json_encode( array(
							'id' => $this->input->post('level_chapter_id'),
							'table' => 'level_chapters',
							'error' => false,
							'get' => true,
							'results' => $item->get()
						) );
						exit;
			break;
			case 'update_field':
				if(  ! $this->template_data->get('admin_access')->controller_level_chapters ) {
					return 0;
				}
				$results = array(
							'id' => $this->input->post('level_chapter_id'),
							'table' => 'level_chapters',
							'error' => true,
							'update_field' => false,
							'message' => 'Unable to update field!'
						);
				$item = $this->Level_chapters_model;
				$item->setLevelChapterId( $this->input->post('level_chapter_id'), true );
				$item->setFieldValue( $this->input->post('field'), $this->input->post('value'), FALSE, TRUE );
				if( $item->updateByLevelChapterId() ) {
					$results['error'] = false;
					$results['update_field'] = true;
					$results['message'] = 'Successfully Updated!';
				}
				echo json_encode( $results );
				exit;
			break;
			case 'add':
				if(  ! $this->template_data->get('admin_access')->controller_level_chapters ) {
					return 0;
				}
				echo json_encode($this->submission('add'));
				exit;
			break;
			case 'edit':
				if(  ! $this->template_data->get('admin_access')->controller_level_chapters ) {
					return 0;
				}
				echo json_encode($this->submission('edit'));
				exit;
			break;
			case 'delete':
				if(  ! $this->template_data->get('admin_access')->controller_level_chapters ) {
					return 0;
				}
				$results = array(
							'id' => $this->input->post('level_chapter_id'),
							'table' => 'level_chapters',
							'error' => true,
							'removed' => false,
							'message' => 'Unable to delete!'
						);
				$this->Level_chapters_model->setLevelChapterId( $this->input->post('level_chapter_id') );
				$data = $this->Level_chapters_model->getByLevelChapterId();
		
				
				if( $this->Level_chapters_model->deleteByLevelChapterId() ) {
					$results['error'] = false;
					$results['removed'] = true;
					$results['message'] = 'Successfully Deleted!';
				}
		
				echo json_encode( $results );
				exit;
			break;
			case "upload":
				if(  ! $this->template_data->get('admin_access')->controller_level_chapters ) {
					return 0;
				}
				$results = array(
					'group' => $this->input->post('group'),
					'key' => $this->input->post('key'),
					'table' => 'media_uploads',
					'error' => true,
					'removed' => false,
					'message' => 'Unable to upload!'
				);
				
				$config['upload_path'] = 'None';
				$config['allowed_types'] = '';
				$config['max_size']	= 'None';
				$config['max_width']  = 'None';
				$config['max_height']  = 'None';

				$this->load->library('upload', $config);

				if ( ! $this->upload->do_upload())
				{
					$results['error'] = true;
					$results['message'] = $this->upload->display_errors();
					
				}
				else
				{
					$results['error'] = false;
					$results['message'] = "Success!";
					$upload_data = $this->upload->data();
					$results['upload_data'] = $upload_data;
					
					$container = new $this->Level_chapters_model;

					if( $container->insert() ) {
						$results['id'] = $container->getLevelChapterId();
						$results['results'] = $container->getByLevelChapterId();
					}
				}
				echo json_encode( $results );
				exit;
			break;
		
		}
		echo 0;
		exit;
	}
	
	private function submission($action) {
	    $this->load->library('form_validation');
	    $results = array(
			'error' => true,
			'message' => 'No Response!',
			'table' => 'level_chapters',
	    );

		if( $action == 'add' ) {
			$this->form_validation->set_rules('level_id', 'lang:level_chapters_level_id', 'required');
			$this->form_validation->set_rules('chapter_id', 'lang:level_chapters_chapter_id', 'required');
			$this->form_validation->set_rules('chapter_title', 'lang:level_chapters_chapter_title', 'required');
		}
		elseif( $action == 'edit' ) {
			$this->form_validation->set_rules('level_id', 'lang:level_chapters_level_id', 'required');
			$this->form_validation->set_rules('chapter_id', 'lang:level_chapters_chapter_id', 'required');
			$this->form_validation->set_rules('chapter_title', 'lang:level_chapters_chapter_title', 'required');
		}

		if ($this->form_validation->run() == FALSE)
		{
			if( $this->input->post() ) {
				$this->template_data->alert( validation_errors(), 'danger');
				$results['message'] = validation_errors();
			}
		} 
		else 
		{
			$container = new $this->Level_chapters_model;
			if( $this->input->post('level_chapter_id') !== FALSE ) {
				$container->setLevelChapterId( $this->input->post('level_chapter_id'), FALSE, TRUE );
			}

			if( $this->input->post('level_id') !== FALSE ) {
				$container->setLevelId( $this->input->post('level_id'), FALSE, TRUE );
			}

			if( $this->input->post('chapter_id') !== FALSE ) {
				$container->setChapterId( $this->input->post('chapter_id'), FALSE, TRUE );
			}

			if( $this->input->post('chapter_title') !== FALSE ) {
				$container->setChapterTitle( $this->input->post('chapter_title'), FALSE, TRUE );
			}

			if( $action == 'add' ) { 
			

				if( $container->replace() ) {
					$this->template_data->alert( 'Successfully Added!', 'success');
					$results['added'] = true;
					$results['error'] = false;
					$results['message'] = 'Successfully Added!';
				} else {
					$results['added'] = false;
					$results['error'] = true;
					$results['message'] = 'Unable to add data!';
				}
			} 
			elseif( $action == 'edit' ) { 
				if( $container->updateByLevelChapterId() ) {
					$this->template_data->alert( 'Successfully Updated!', 'success');
					$results['updated'] = true;
					$results['error'] = false;
					$results['message'] = 'Successfully Updated!';
				} else {
					$results['updated'] = false;
					$results['error'] = true;
					$results['message'] = 'Unable to update data!';
				}
			}
				$container->setJoin('ci_tax_levels ci_tax_levels','ci_level_chapters.level_id = ci_tax_levels.level_id');
				$container->setSelect('ci_level_chapters.*');
				$container->setSelect('ci_tax_levels.level_name as level_name');

				$container->setJoin('ci_tax_chapters ci_tax_chapters','ci_level_chapters.chapter_id = ci_tax_chapters.chapter_id');
				$container->setSelect('ci_level_chapters.*');
				$container->setSelect('ci_tax_chapters.chapter_name as chapter_name');


			$results['id'] = $container->getLevelChapterId();
			$results['results'] = $container->getByLevelChapterId();
		}

	    return $results;
	}
	
}
/* End of file level_chapters.php */
/* Location: ./application/controllers/level_chapters.php */
