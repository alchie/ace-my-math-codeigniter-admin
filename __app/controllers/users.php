<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends MY_Controller {

    function __construct() {
        parent::__construct();

        $this->load->library('template_data');
        $this->lang->load('users');
        $this->load->model( array('Users_model') );
        
        $this->template_data->set('main_page', 'accounts' ); 
        $this->template_data->set('sub_page', 'users' ); 
        $this->template_data->set('page_title', 'Users' ); 

    }
    
    public function index()
	{
        $this->load->view('users', $this->template_data->get() );
	}
	
	public function ajax($required=NULL, $value=NULL) {

		switch($this->input->post('action')) {
			case 'list':
				$list_limit = ( $this->input->post('limit') != '') ? (int) $this->input->post('limit') : 20;
				$order_by = ( $this->input->post('order_by') != '') ? $this->input->post('order_by') : 'user_id';
				$order_sort = ( $this->input->post('order_sort') != '') ? $this->input->post('order_sort') : 'DESC';
				$list_start = ( $this->input->post('list_start') != '') ? $this->input->post('list_start') : 0;
				
				$list = new $this->Users_model;
				$pagination = new $this->Users_model;
				
				$list->setJoin('ci_subscription_plans ci_subscription_plans','ci_users.user_plan = ci_subscription_plans.s_plan_id');
				$list->setSelect('ci_users.*');
				$list->setSelect('ci_subscription_plans.s_plan_name as s_plan_name_s');

				$pagination->setSelect('COUNT(*) as total_items');
				$pagination->setJoin('ci_subscription_plans ci_subscription_plans','ci_users.user_plan = ci_subscription_plans.s_plan_id');

				
				if( is_array($this->input->post('filter')) && count($this->input->post('filter')) > 0 ) {
					foreach( $this->input->post('filter') as $filter ) {
						$list->setFilter($filter['key'],$filter['value'],$filter['table']);
						$pagination->setFilter($filter['key'],$filter['value'],$filter['table']);
					}
				}
				
				$list->setOrder($order_by, $order_sort);
				$list->setStart($list_start);
				$list->setLimit($list_limit);
				
				echo json_encode( array(
							'table' => 'users',
							'total_items' => (int) $pagination->get()->total_items,
							'start' => $list_start,
							'limit' => $list_limit,
							'error' => false,
							'list' => true,
							'results' => $list->populate()
						) );
				exit;
			break;
			case 'get':
				$item = $this->Users_model;
				$item->setUserId( $this->input->post('user_id'), true );

				$item->setJoin('ci_subscription_plans ci_subscription_plans','ci_users.user_plan = ci_subscription_plans.s_plan_id');
				$item->setSelect('ci_users.*');
				$item->setSelect('ci_subscription_plans.s_plan_name as s_plan_name_s');

				echo json_encode( array(
							'id' => $this->input->post('user_id'),
							'table' => 'users',
							'error' => false,
							'get' => true,
							'results' => $item->get()
						) );
						exit;
			break;
			case 'update_field':
				if(  ! $this->template_data->get('admin_access')->controller_users ) {
					return 0;
				}
				$results = array(
							'id' => $this->input->post('user_id'),
							'table' => 'users',
							'error' => true,
							'update_field' => false,
							'message' => 'Unable to update field!'
						);
				$item = $this->Users_model;
				$item->setUserId( $this->input->post('user_id'), true );
				$item->setFieldValue( $this->input->post('field'), $this->input->post('value'), FALSE, TRUE );
				if( $item->updateByUserId() ) {
					$results['error'] = false;
					$results['update_field'] = true;
					$results['message'] = 'Successfully Updated!';
				}
				echo json_encode( $results );
				exit;
			break;
			case 'add':
				if(  ! $this->template_data->get('admin_access')->controller_users ) {
					return 0;
				}
				echo json_encode($this->submission('add'));
				exit;
			break;
			case 'edit':
				if(  ! $this->template_data->get('admin_access')->controller_users ) {
					return 0;
				}
				echo json_encode($this->submission('edit'));
				exit;
			break;
			case 'delete':
				if(  ! $this->template_data->get('admin_access')->controller_users ) {
					return 0;
				}
				$results = array(
							'id' => $this->input->post('user_id'),
							'table' => 'users',
							'error' => true,
							'removed' => false,
							'message' => 'Unable to delete!'
						);
				$this->Users_model->setUserId( $this->input->post('user_id') );
				$data = $this->Users_model->getByUserId();
		
				
				if( $this->Users_model->deleteByUserId() ) {
					$results['error'] = false;
					$results['removed'] = true;
					$results['message'] = 'Successfully Deleted!';
				}
		
				echo json_encode( $results );
				exit;
			break;
			case "upload":
				if(  ! $this->template_data->get('admin_access')->controller_users ) {
					return 0;
				}
				$results = array(
					'group' => $this->input->post('group'),
					'key' => $this->input->post('key'),
					'table' => 'media_uploads',
					'error' => true,
					'removed' => false,
					'message' => 'Unable to upload!'
				);
				
				$config['upload_path'] = 'None';
				$config['allowed_types'] = '';
				$config['max_size']	= 'None';
				$config['max_width']  = 'None';
				$config['max_height']  = 'None';

				$this->load->library('upload', $config);

				if ( ! $this->upload->do_upload())
				{
					$results['error'] = true;
					$results['message'] = $this->upload->display_errors();
					
				}
				else
				{
					$results['error'] = false;
					$results['message'] = "Success!";
					$upload_data = $this->upload->data();
					$results['upload_data'] = $upload_data;
					
					$container = new $this->Users_model;

					if( $container->insert() ) {
						$results['id'] = $container->getUserId();
						$results['results'] = $container->getByUserId();
					}
				}
				echo json_encode( $results );
				exit;
			break;
		
		}
		echo 0;
		exit;
	}
	
	private function submission($action) {
	    $this->load->library('form_validation');
	    $results = array(
			'error' => true,
			'message' => 'No Response!',
			'table' => 'users',
	    );

		if( $action == 'add' ) {
			$this->form_validation->set_rules('user_username', 'lang:users_user_username', 'required|is_unique[users.user_username]');
			$this->form_validation->set_rules('user_password', 'lang:users_user_password', 'required|matches[confirm_password]|sha1|md5');
			$this->form_validation->set_rules('user_firstname', 'lang:users_user_firstname', 'required');
			$this->form_validation->set_rules('user_email', 'lang:users_user_email', 'valid_email|is_unique[users.user_email]|required');
		}
		elseif( $action == 'edit' ) {
			$this->form_validation->set_rules('user_id', 'lang:users_user_id', 'required');
			$this->form_validation->set_rules('user_username', 'lang:users_user_username', 'required');
			$this->form_validation->set_rules('user_password', 'lang:users_user_password', 'matches[confirm_password]|sha1|md5');
			$this->form_validation->set_rules('user_firstname', 'lang:users_user_firstname', 'required');
			$this->form_validation->set_rules('user_email', 'lang:users_user_email', 'valid_email|required');
		}

		if ($this->form_validation->run() == FALSE)
		{
			if( $this->input->post() ) {
				$this->template_data->alert( validation_errors(), 'danger');
				$results['message'] = validation_errors();
			}
		} 
		else 
		{
			$container = new $this->Users_model;
			if( $this->input->post('user_id') !== FALSE ) {
				$container->setUserId( $this->input->post('user_id'), FALSE, TRUE );
			}

			$container->setUserUsername( $this->input->post('user_username'), FALSE, TRUE );

			if( $this->input->post('user_password') !== FALSE ) {
				$container->setUserPassword( $this->input->post('user_password'), FALSE, TRUE );
			}

			if( $this->input->post('user_type') !== FALSE ) {
				$container->setUserType( $this->input->post('user_type'), FALSE, TRUE );
			}

			if( $this->input->post('user_level') !== FALSE ) {
				$container->setUserLevel( $this->input->post('user_level'), FALSE, TRUE );
			}

			if( $this->input->post('user_firstname') !== FALSE ) {
				$container->setUserFirstname( $this->input->post('user_firstname'), FALSE, TRUE );
			}

			if( $this->input->post('user_lastname') !== FALSE ) {
				$container->setUserLastname( $this->input->post('user_lastname'), FALSE, TRUE );
			}

			$container->setUserEmail( $this->input->post('user_email'), FALSE, TRUE );

			if( $this->input->post('user_plan') !== FALSE ) {
				$container->setUserPlan( $this->input->post('user_plan'), FALSE, TRUE );
			}

			if( $this->input->post('user_created') !== FALSE ) {
				$container->setUserCreated( $this->input->post('user_created'), FALSE, TRUE );
			}

			if( $this->input->post('user_expiry') !== FALSE ) {
				$container->setUserExpiry( $this->input->post('user_expiry'), FALSE, TRUE );
			}

			if( $this->input->post('user_active') !== FALSE ) {
				$container->setUserActive( $this->input->post('user_active'), FALSE, TRUE );
			}
			else {
				$container->setUserActive( '0', FALSE, TRUE );
			}

			if( $this->input->post('last_login') !== FALSE ) {
				$container->setLastLogin( $this->input->post('last_login'), FALSE, TRUE );
			}

			if( $this->input->post('last_login_ip') !== FALSE ) {
				$container->setLastLoginIp( $this->input->post('last_login_ip'), FALSE, TRUE );
			}

			if( $action == 'add' ) { 
			

				if( $container->replace() ) {
					$this->template_data->alert( 'Successfully Added!', 'success');
					$results['added'] = true;
					$results['error'] = false;
					$results['message'] = 'Successfully Added!';
				} else {
					$results['added'] = false;
					$results['error'] = true;
					$results['message'] = 'Unable to add data!';
				}
			} 
			elseif( $action == 'edit' ) { 
				if( $container->updateByUserId() ) {
					$this->template_data->alert( 'Successfully Updated!', 'success');
					$results['updated'] = true;
					$results['error'] = false;
					$results['message'] = 'Successfully Updated!';
				} else {
					$results['updated'] = false;
					$results['error'] = true;
					$results['message'] = 'Unable to update data!';
				}
			}
				$container->setJoin('ci_subscription_plans ci_subscription_plans','ci_users.user_plan = ci_subscription_plans.s_plan_id');
				$container->setSelect('ci_users.*');
				$container->setSelect('ci_subscription_plans.s_plan_name as s_plan_name_s');


			$results['id'] = $container->getUserId();
			$results['results'] = $container->getByUserId();
		}

	    return $results;
	}
	
}
/* End of file users.php */
/* Location: ./application/controllers/users.php */
