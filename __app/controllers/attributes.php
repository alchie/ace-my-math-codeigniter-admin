<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Attributes extends MY_Controller {

    function __construct() {
        parent::__construct();

        $this->load->library('template_data');
        $this->lang->load('attributes');
        $this->load->model( array('Attributes_model') );
        
        $this->template_data->set('main_page', 'settings' ); 
        $this->template_data->set('sub_page', 'attributes' ); 
        $this->template_data->set('page_title', 'Attributes' ); 

    }
    
    public function index()
	{
        $this->load->view('attributes', $this->template_data->get() );
	}
	
	public function ajax($required=NULL, $value=NULL) {

		switch($this->input->post('action')) {
			case 'list':
				$list_limit = ( $this->input->post('limit') != '') ? (int) $this->input->post('limit') : 20;
				$order_by = ( $this->input->post('order_by') != '') ? $this->input->post('order_by') : 'attr_label';
				$order_sort = ( $this->input->post('order_sort') != '') ? $this->input->post('order_sort') : 'ASC';
				$list_start = ( $this->input->post('list_start') != '') ? $this->input->post('list_start') : 0;
				
				$list = new $this->Attributes_model;
				$pagination = new $this->Attributes_model;
				
				$pagination->setSelect('COUNT(*) as total_items');

				
				if( is_array($this->input->post('filter')) && count($this->input->post('filter')) > 0 ) {
					foreach( $this->input->post('filter') as $filter ) {
						$list->setFilter($filter['key'],$filter['value'],$filter['table']);
						$pagination->setFilter($filter['key'],$filter['value'],$filter['table']);
					}
				}
				
				$list->setOrder($order_by, $order_sort);
				$list->setStart($list_start);
				$list->setLimit($list_limit);
				
				echo json_encode( array(
							'table' => 'attributes',
							'total_items' => (int) $pagination->get()->total_items,
							'start' => $list_start,
							'limit' => $list_limit,
							'error' => false,
							'list' => true,
							'results' => $list->populate()
						) );
				exit;
			break;
			case 'get':
				$item = $this->Attributes_model;
				$item->setAttrId( $this->input->post('attr_id'), true );

				echo json_encode( array(
							'id' => $this->input->post('attr_id'),
							'table' => 'attributes',
							'error' => false,
							'get' => true,
							'results' => $item->get()
						) );
						exit;
			break;
			case 'update_field':
				if(  ! $this->template_data->get('admin_access')->controller_attributes ) {
					return 0;
				}
				$results = array(
							'id' => $this->input->post('attr_id'),
							'table' => 'attributes',
							'error' => true,
							'update_field' => false,
							'message' => 'Unable to update field!'
						);
				$item = $this->Attributes_model;
				$item->setAttrId( $this->input->post('attr_id'), true );
				$item->setFieldValue( $this->input->post('field'), $this->input->post('value'), FALSE, TRUE );
				if( $item->updateByAttrId() ) {
					$results['error'] = false;
					$results['update_field'] = true;
					$results['message'] = 'Successfully Updated!';
				}
				echo json_encode( $results );
				exit;
			break;
			case 'add':
				if(  ! $this->template_data->get('admin_access')->controller_attributes ) {
					return 0;
				}
				echo json_encode($this->submission('add'));
				exit;
			break;
			case 'edit':
				if(  ! $this->template_data->get('admin_access')->controller_attributes ) {
					return 0;
				}
				echo json_encode($this->submission('edit'));
				exit;
			break;
			case 'delete':
				if(  ! $this->template_data->get('admin_access')->controller_attributes ) {
					return 0;
				}
				$results = array(
							'id' => $this->input->post('attr_id'),
							'table' => 'attributes',
							'error' => true,
							'removed' => false,
							'message' => 'Unable to delete!'
						);
				$this->Attributes_model->setAttrId( $this->input->post('attr_id') );
				$data = $this->Attributes_model->getByAttrId();
		
				
				if( $this->Attributes_model->deleteByAttrId() ) {
					$results['error'] = false;
					$results['removed'] = true;
					$results['message'] = 'Successfully Deleted!';
				}
		
				echo json_encode( $results );
				exit;
			break;
			case "upload":
				if(  ! $this->template_data->get('admin_access')->controller_attributes ) {
					return 0;
				}
				$results = array(
					'group' => $this->input->post('group'),
					'key' => $this->input->post('key'),
					'table' => 'media_uploads',
					'error' => true,
					'removed' => false,
					'message' => 'Unable to upload!'
				);
				
				$config['upload_path'] = 'None';
				$config['allowed_types'] = '';
				$config['max_size']	= 'None';
				$config['max_width']  = 'None';
				$config['max_height']  = 'None';

				$this->load->library('upload', $config);

				if ( ! $this->upload->do_upload())
				{
					$results['error'] = true;
					$results['message'] = $this->upload->display_errors();
					
				}
				else
				{
					$results['error'] = false;
					$results['message'] = "Success!";
					$upload_data = $this->upload->data();
					$results['upload_data'] = $upload_data;
					
					$container = new $this->Attributes_model;

					if( $container->insert() ) {
						$results['id'] = $container->getAttrId();
						$results['results'] = $container->getByAttrId();
					}
				}
				echo json_encode( $results );
				exit;
			break;
		
		}
		echo 0;
		exit;
	}
	
	private function submission($action) {
	    $this->load->library('form_validation');
	    $results = array(
			'error' => true,
			'message' => 'No Response!',
			'table' => 'attributes',
	    );

		if( $action == 'add' ) {
			$this->form_validation->set_rules('attr_controller', 'lang:attributes_attr_controller', 'required');
		}
		elseif( $action == 'edit' ) {
			$this->form_validation->set_rules('attr_controller', 'lang:attributes_attr_controller', 'required');
		}

		if ($this->form_validation->run() == FALSE)
		{
			if( $this->input->post() ) {
				$this->template_data->alert( validation_errors(), 'danger');
				$results['message'] = validation_errors();
			}
		} 
		else 
		{
			$container = new $this->Attributes_model;
			if( $this->input->post('attr_id') !== FALSE ) {
				$container->setAttrId( $this->input->post('attr_id'), FALSE, TRUE );
			}

			if( $this->input->post('attr_controller') !== FALSE ) {
				$container->setAttrController( $this->input->post('attr_controller'), FALSE, TRUE );
			}

			if( $this->input->post('attr_name') !== FALSE ) {
				$container->setAttrName( $this->input->post('attr_name'), FALSE, TRUE );
			}

			if( $this->input->post('attr_label') !== FALSE ) {
				$container->setAttrLabel( $this->input->post('attr_label'), FALSE, TRUE );
			}

			if( $this->input->post('attr_description') !== FALSE ) {
				$container->setAttrDescription( $this->input->post('attr_description'), FALSE, TRUE );
			}

			if( $action == 'add' ) { 
			

				if( $container->replace() ) {
					$this->template_data->alert( 'Successfully Added!', 'success');
					$results['added'] = true;
					$results['error'] = false;
					$results['message'] = 'Successfully Added!';
				} else {
					$results['added'] = false;
					$results['error'] = true;
					$results['message'] = 'Unable to add data!';
				}
			} 
			elseif( $action == 'edit' ) { 
				if( $container->updateByAttrId() ) {
					$this->template_data->alert( 'Successfully Updated!', 'success');
					$results['updated'] = true;
					$results['error'] = false;
					$results['message'] = 'Successfully Updated!';
				} else {
					$results['updated'] = false;
					$results['error'] = true;
					$results['message'] = 'Unable to update data!';
				}
			}

			$results['id'] = $container->getAttrId();
			$results['results'] = $container->getByAttrId();
		}

	    return $results;
	}
	
}
/* End of file attributes.php */
/* Location: ./application/controllers/attributes.php */
