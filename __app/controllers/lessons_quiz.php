<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Lessons_quiz extends MY_Controller {

    function __construct() {
        parent::__construct();

        $this->load->library('template_data');
        $this->lang->load('lessons_quiz');
        $this->load->model( array('Lessons_quiz_model') );
        
        $this->template_data->set('main_page', 'lessons_quiz' ); 
        $this->template_data->set('sub_page', 'lessons_quiz' ); 
        $this->template_data->set('page_title', 'Lesson Quizzes' ); 

    }
    
    public function index()
	{
        $this->load->view('lessons_quiz', $this->template_data->get() );
	}
	
	public function ajax($required=NULL, $value=NULL) {

		switch($this->input->post('action')) {
			case 'list':
				$list_limit = ( $this->input->post('limit') != '') ? (int) $this->input->post('limit') : 20;
				$order_by = ( $this->input->post('order_by') != '') ? $this->input->post('order_by') : 'quiz_time';
				$order_sort = ( $this->input->post('order_sort') != '') ? $this->input->post('order_sort') : 'ASC';
				$list_start = ( $this->input->post('list_start') != '') ? $this->input->post('list_start') : 0;
				
				$list = new $this->Lessons_quiz_model;
				$pagination = new $this->Lessons_quiz_model;
				
				$list->setJoin('ci_lessons ci_lessons','ci_lessons_quiz.lesson_id = ci_lessons.lesson_id');
				$list->setSelect('ci_lessons_quiz.*');
				$list->setSelect('ci_lessons.lesson_title as lesson_title');

				$pagination->setSelect('COUNT(*) as total_items');
				$pagination->setJoin('ci_lessons ci_lessons','ci_lessons_quiz.lesson_id = ci_lessons.lesson_id');

				
				if( is_array($this->input->post('filter')) && count($this->input->post('filter')) > 0 ) {
					foreach( $this->input->post('filter') as $filter ) {
						$list->setFilter($filter['key'],$filter['value'],$filter['table']);
						$pagination->setFilter($filter['key'],$filter['value'],$filter['table']);
					}
				}
				
				$list->setOrder($order_by, $order_sort);
				$list->setStart($list_start);
				$list->setLimit($list_limit);
				
				echo json_encode( array(
							'table' => 'lessons_quiz',
							'total_items' => (int) $pagination->get()->total_items,
							'start' => $list_start,
							'limit' => $list_limit,
							'error' => false,
							'list' => true,
							'results' => $list->populate()
						) );
				exit;
			break;
			case 'get':
				$item = $this->Lessons_quiz_model;
				$item->setQuizId( $this->input->post('quiz_id'), true );

				$item->setJoin('ci_lessons ci_lessons','ci_lessons_quiz.lesson_id = ci_lessons.lesson_id');
				$item->setSelect('ci_lessons_quiz.*');
				$item->setSelect('ci_lessons.lesson_title as lesson_title');

				echo json_encode( array(
							'id' => $this->input->post('quiz_id'),
							'table' => 'lessons_quiz',
							'error' => false,
							'get' => true,
							'results' => $item->get()
						) );
						exit;
			break;
			case 'update_field':
				if(  ! $this->template_data->get('admin_access')->controller_lessons_quiz ) {
					return 0;
				}
				$results = array(
							'id' => $this->input->post('quiz_id'),
							'table' => 'lessons_quiz',
							'error' => true,
							'update_field' => false,
							'message' => 'Unable to update field!'
						);
				$item = $this->Lessons_quiz_model;
				$item->setQuizId( $this->input->post('quiz_id'), true );
				$item->setFieldValue( $this->input->post('field'), $this->input->post('value'), FALSE, TRUE );
				if( $item->updateByQuizId() ) {
					$results['error'] = false;
					$results['update_field'] = true;
					$results['message'] = 'Successfully Updated!';
				}
				echo json_encode( $results );
				exit;
			break;
			case 'add':
				if(  ! $this->template_data->get('admin_access')->controller_lessons_quiz ) {
					return 0;
				}
				echo json_encode($this->submission('add'));
				exit;
			break;
			case 'edit':
				if(  ! $this->template_data->get('admin_access')->controller_lessons_quiz ) {
					return 0;
				}
				echo json_encode($this->submission('edit'));
				exit;
			break;
			case 'delete':
				if(  ! $this->template_data->get('admin_access')->controller_lessons_quiz ) {
					return 0;
				}
				$results = array(
							'id' => $this->input->post('quiz_id'),
							'table' => 'lessons_quiz',
							'error' => true,
							'removed' => false,
							'message' => 'Unable to delete!'
						);
				$this->Lessons_quiz_model->setQuizId( $this->input->post('quiz_id') );
				$data = $this->Lessons_quiz_model->getByQuizId();
		
				
				if( $this->Lessons_quiz_model->deleteByQuizId() ) {
					$results['error'] = false;
					$results['removed'] = true;
					$results['message'] = 'Successfully Deleted!';
				}
		
				echo json_encode( $results );
				exit;
			break;
			case "upload":
				if(  ! $this->template_data->get('admin_access')->controller_lessons_quiz ) {
					return 0;
				}
				$results = array(
					'group' => $this->input->post('group'),
					'key' => $this->input->post('key'),
					'table' => 'media_uploads',
					'error' => true,
					'removed' => false,
					'message' => 'Unable to upload!'
				);
				
				$config['upload_path'] = 'None';
				$config['allowed_types'] = '';
				$config['max_size']	= 'None';
				$config['max_width']  = 'None';
				$config['max_height']  = 'None';

				$this->load->library('upload', $config);

				if ( ! $this->upload->do_upload())
				{
					$results['error'] = true;
					$results['message'] = $this->upload->display_errors();
					
				}
				else
				{
					$results['error'] = false;
					$results['message'] = "Success!";
					$upload_data = $this->upload->data();
					$results['upload_data'] = $upload_data;
					
					$container = new $this->Lessons_quiz_model;

					if( $container->insert() ) {
						$results['id'] = $container->getQuizId();
						$results['results'] = $container->getByQuizId();
					}
				}
				echo json_encode( $results );
				exit;
			break;
		
		}
		echo 0;
		exit;
	}
	
	private function submission($action) {
	    $this->load->library('form_validation');
	    $results = array(
			'error' => true,
			'message' => 'No Response!',
			'table' => 'lessons_quiz',
	    );

		if( $action == 'add' ) {
			$this->form_validation->set_rules('lesson_id', 'lang:lessons_quiz_lesson_id', 'required');
		}
		elseif( $action == 'edit' ) {
			$this->form_validation->set_rules('quiz_id', 'lang:lessons_quiz_quiz_id', 'required');
			$this->form_validation->set_rules('lesson_id', 'lang:lessons_quiz_lesson_id', 'required');
		}

		if ($this->form_validation->run() == FALSE)
		{
			if( $this->input->post() ) {
				$this->template_data->alert( validation_errors(), 'danger');
				$results['message'] = validation_errors();
			}
		} 
		else 
		{
			$container = new $this->Lessons_quiz_model;
			if( $this->input->post('quiz_id') !== FALSE ) {
				$container->setQuizId( $this->input->post('quiz_id'), FALSE, TRUE );
			}

			if( $this->input->post('lesson_id') !== FALSE ) {
				$container->setLessonId( $this->input->post('lesson_id'), FALSE, TRUE );
			}

			if( $this->input->post('quiz_number') !== FALSE ) {
				$container->setQuizNumber( $this->input->post('quiz_number'), FALSE, TRUE );
			}

			if( $this->input->post('quiz_time') !== FALSE ) {
				$container->setQuizTime( $this->input->post('quiz_time'), FALSE, TRUE );
			}

			if( $this->input->post('quiz_question') !== FALSE ) {
				$container->setQuizQuestion( $this->input->post('quiz_question'), FALSE, TRUE );
			}

			if( $this->input->post('quiz_type') !== FALSE ) {
				$container->setQuizType( $this->input->post('quiz_type'), FALSE, TRUE );
			}

			if( $this->input->post('quiz_details') !== FALSE ) {
				$container->setQuizDetails( $this->input->post('quiz_details'), FALSE, TRUE );
			}

			if( $this->input->post('quiz_correct') !== FALSE ) {
				$container->setQuizCorrect( $this->input->post('quiz_correct'), FALSE, TRUE );
			}

			if( $action == 'add' ) { 
			

				if( $container->replace() ) {
					$this->template_data->alert( 'Successfully Added!', 'success');
					$results['added'] = true;
					$results['error'] = false;
					$results['message'] = 'Successfully Added!';
				} else {
					$results['added'] = false;
					$results['error'] = true;
					$results['message'] = 'Unable to add data!';
				}
			} 
			elseif( $action == 'edit' ) { 
				if( $container->updateByQuizId() ) {
					$this->template_data->alert( 'Successfully Updated!', 'success');
					$results['updated'] = true;
					$results['error'] = false;
					$results['message'] = 'Successfully Updated!';
				} else {
					$results['updated'] = false;
					$results['error'] = true;
					$results['message'] = 'Unable to update data!';
				}
			}
				$container->setJoin('ci_lessons ci_lessons','ci_lessons_quiz.lesson_id = ci_lessons.lesson_id');
				$container->setSelect('ci_lessons_quiz.*');
				$container->setSelect('ci_lessons.lesson_title as lesson_title');


			$results['id'] = $container->getQuizId();
			$results['results'] = $container->getByQuizId();
		}

	    return $results;
	}
	
}
/* End of file lessons_quiz.php */
/* Location: ./application/controllers/lessons_quiz.php */
