<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Media_uploads extends MY_Controller {

    function __construct() {
        parent::__construct();

        $this->load->library('template_data');
        $this->lang->load('media_uploads');
        $this->load->model( array('Media_uploads_model') );
        
        $this->template_data->set('main_page', 'content' ); 
        $this->template_data->set('sub_page', 'media_uploads' ); 
        $this->template_data->set('page_title', 'Media Uploads' ); 

    }
    
    public function index()
	{
        $this->load->view('media_uploads', $this->template_data->get() );
	}
	
	public function ajax($required=NULL, $value=NULL) {

		switch($this->input->post('action')) {
			case 'list':
				$list_limit = ( $this->input->post('limit') != '') ? (int) $this->input->post('limit') : 20;
				$order_by = ( $this->input->post('order_by') != '') ? $this->input->post('order_by') : 'media_id';
				$order_sort = ( $this->input->post('order_sort') != '') ? $this->input->post('order_sort') : 'DESC';
				$list_start = ( $this->input->post('list_start') != '') ? $this->input->post('list_start') : 0;
				
				$list = new $this->Media_uploads_model;
				$pagination = new $this->Media_uploads_model;
				
				$pagination->setSelect('COUNT(*) as total_items');

				
				if( is_array($this->input->post('filter')) && count($this->input->post('filter')) > 0 ) {
					foreach( $this->input->post('filter') as $filter ) {
						$list->setFilter($filter['key'],$filter['value'],$filter['table']);
						$pagination->setFilter($filter['key'],$filter['value'],$filter['table']);
					}
				}
				
				$list->setOrder($order_by, $order_sort);
				$list->setStart($list_start);
				$list->setLimit($list_limit);
				
				echo json_encode( array(
							'table' => 'media_uploads',
							'total_items' => (int) $pagination->get()->total_items,
							'start' => $list_start,
							'limit' => $list_limit,
							'error' => false,
							'list' => true,
							'results' => $list->populate()
						) );
				exit;
			break;
			case 'get':
				$item = $this->Media_uploads_model;
				$item->setMediaId( $this->input->post('media_id'), true );

				echo json_encode( array(
							'id' => $this->input->post('media_id'),
							'table' => 'media_uploads',
							'error' => false,
							'get' => true,
							'results' => $item->get()
						) );
						exit;
			break;
			case 'update_field':
				if(  ! $this->template_data->get('admin_access')->controller_media_uploads ) {
					return 0;
				}
				$results = array(
							'id' => $this->input->post('media_id'),
							'table' => 'media_uploads',
							'error' => true,
							'update_field' => false,
							'message' => 'Unable to update field!'
						);
				$item = $this->Media_uploads_model;
				$item->setMediaId( $this->input->post('media_id'), true );
				$item->setFieldValue( $this->input->post('field'), $this->input->post('value'), FALSE, TRUE );
				if( $item->updateByMediaId() ) {
					$results['error'] = false;
					$results['update_field'] = true;
					$results['message'] = 'Successfully Updated!';
				}
				echo json_encode( $results );
				exit;
			break;
			case 'add':
				if(  ! $this->template_data->get('admin_access')->controller_media_uploads ) {
					return 0;
				}
				echo json_encode($this->submission('add'));
				exit;
			break;
			case 'edit':
				if(  ! $this->template_data->get('admin_access')->controller_media_uploads ) {
					return 0;
				}
				echo json_encode($this->submission('edit'));
				exit;
			break;
			case 'delete':
				if(  ! $this->template_data->get('admin_access')->controller_media_uploads ) {
					return 0;
				}
				$results = array(
							'id' => $this->input->post('media_id'),
							'table' => 'media_uploads',
							'error' => true,
							'removed' => false,
							'message' => 'Unable to delete!'
						);
				$this->Media_uploads_model->setMediaId( $this->input->post('media_id') );
				$data = $this->Media_uploads_model->getByMediaId();
		
			if( unlink( $data->full_path ) ) {
				
				if( $this->Media_uploads_model->deleteByMediaId() ) {
					$results['error'] = false;
					$results['removed'] = true;
					$results['message'] = 'Successfully Deleted!';
				}
		
			}
			
				echo json_encode( $results );
				exit;
			break;
			case "upload":
				if(  ! $this->template_data->get('admin_access')->controller_media_uploads ) {
					return 0;
				}
				$results = array(
					'group' => $this->input->post('group'),
					'key' => $this->input->post('key'),
					'table' => 'media_uploads',
					'error' => true,
					'removed' => false,
					'message' => 'Unable to upload!'
				);
				
				$config['upload_path'] = '../uploads/';
				$config['allowed_types'] = 'jpg|jpeg|gif|png';
				$config['max_size']	= '100000';
				$config['max_width']  = '1024';
				$config['max_height']  = '768';

				$this->load->library('upload', $config);

				if ( ! $this->upload->do_upload())
				{
					$results['error'] = true;
					$results['message'] = $this->upload->display_errors();
					
				}
				else
				{
					$results['error'] = false;
					$results['message'] = "Success!";
					$upload_data = $this->upload->data();
					$results['upload_data'] = $upload_data;
					
					$container = new $this->Media_uploads_model;
					$container->setFileName( $upload_data['file_name'], FALSE, TRUE );
					$container->setFileType( $upload_data['file_type'], FALSE, TRUE );
					$container->setFilePath( $upload_data['file_path'], FALSE, TRUE );
					$container->setFullPath( $upload_data['full_path'], FALSE, TRUE );
					$container->setRawName( $upload_data['raw_name'], FALSE, TRUE );
					$container->setOrigName( $upload_data['orig_name'], FALSE, TRUE );
					$container->setClientName( $upload_data['client_name'], FALSE, TRUE );
					$container->setFileExt( $upload_data['file_ext'], FALSE, TRUE );
					$container->setFileSize( $upload_data['file_size'], FALSE, TRUE );
					$container->setIsImage( $upload_data['is_image'], FALSE, TRUE );
					$container->setImageWidth( $upload_data['image_width'], FALSE, TRUE );
					$container->setImageHeight( $upload_data['image_height'], FALSE, TRUE );
					$container->setImageType( $upload_data['image_type'], FALSE, TRUE );
					$container->setImageSizeStr( $upload_data['image_size_str'], FALSE, TRUE );

					if( $container->insert() ) {
						$results['id'] = $container->getMediaId();
						$results['results'] = $container->getByMediaId();
					}
				}
				echo json_encode( $results );
				exit;
			break;
		
		}
		echo 0;
		exit;
	}
	
	private function submission($action) {
	    $this->load->library('form_validation');
	    $results = array(
			'error' => true,
			'message' => 'No Response!',
			'table' => 'media_uploads',
	    );

		if( $action == 'add' ) {
			$this->form_validation->set_rules('file_name', 'lang:media_uploads_file_name', 'required');
		}
		elseif( $action == 'edit' ) {
			$this->form_validation->set_rules('media_id', 'lang:media_uploads_media_id', 'required');
			$this->form_validation->set_rules('file_name', 'lang:media_uploads_file_name', 'required');
		}

		if ($this->form_validation->run() == FALSE)
		{
			if( $this->input->post() ) {
				$this->template_data->alert( validation_errors(), 'danger');
				$results['message'] = validation_errors();
			}
		} 
		else 
		{
			$container = new $this->Media_uploads_model;
			if( $this->input->post('media_id') !== FALSE ) {
				$container->setMediaId( $this->input->post('media_id'), FALSE, TRUE );
			}

			if( $this->input->post('file_name') !== FALSE ) {
				$container->setFileName( $this->input->post('file_name'), FALSE, TRUE );
			}

			if( $this->input->post('file_type') !== FALSE ) {
				$container->setFileType( $this->input->post('file_type'), FALSE, TRUE );
			}

			if( $this->input->post('file_path') !== FALSE ) {
				$container->setFilePath( $this->input->post('file_path'), FALSE, TRUE );
			}

			if( $this->input->post('full_path') !== FALSE ) {
				$container->setFullPath( $this->input->post('full_path'), FALSE, TRUE );
			}

			if( $this->input->post('raw_name') !== FALSE ) {
				$container->setRawName( $this->input->post('raw_name'), FALSE, TRUE );
			}

			if( $this->input->post('orig_name') !== FALSE ) {
				$container->setOrigName( $this->input->post('orig_name'), FALSE, TRUE );
			}

			if( $this->input->post('client_name') !== FALSE ) {
				$container->setClientName( $this->input->post('client_name'), FALSE, TRUE );
			}

			if( $this->input->post('file_ext') !== FALSE ) {
				$container->setFileExt( $this->input->post('file_ext'), FALSE, TRUE );
			}

			if( $this->input->post('file_size') !== FALSE ) {
				$container->setFileSize( $this->input->post('file_size'), FALSE, TRUE );
			}

			if( $this->input->post('is_image') !== FALSE ) {
				$container->setIsImage( $this->input->post('is_image'), FALSE, TRUE );
			}
			else {
				$container->setIsImage( '0', FALSE, TRUE );
			}

			if( $this->input->post('image_width') !== FALSE ) {
				$container->setImageWidth( $this->input->post('image_width'), FALSE, TRUE );
			}

			if( $this->input->post('image_height') !== FALSE ) {
				$container->setImageHeight( $this->input->post('image_height'), FALSE, TRUE );
			}

			if( $this->input->post('image_type') !== FALSE ) {
				$container->setImageType( $this->input->post('image_type'), FALSE, TRUE );
			}

			if( $this->input->post('image_size_str') !== FALSE ) {
				$container->setImageSizeStr( $this->input->post('image_size_str'), FALSE, TRUE );
			}

			if( $action == 'add' ) { 
			

				if( $container->replace() ) {
					$this->template_data->alert( 'Successfully Added!', 'success');
					$results['added'] = true;
					$results['error'] = false;
					$results['message'] = 'Successfully Added!';
				} else {
					$results['added'] = false;
					$results['error'] = true;
					$results['message'] = 'Unable to add data!';
				}
			} 
			elseif( $action == 'edit' ) { 
				if( $container->updateByMediaId() ) {
					$this->template_data->alert( 'Successfully Updated!', 'success');
					$results['updated'] = true;
					$results['error'] = false;
					$results['message'] = 'Successfully Updated!';
				} else {
					$results['updated'] = false;
					$results['error'] = true;
					$results['message'] = 'Unable to update data!';
				}
			}

			$results['id'] = $container->getMediaId();
			$results['results'] = $container->getByMediaId();
		}

	    return $results;
	}
	
}
/* End of file media_uploads.php */
/* Location: ./application/controllers/media_uploads.php */
