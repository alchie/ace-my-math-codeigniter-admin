<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users_meta extends MY_Controller {

    function __construct() {
        parent::__construct();

        $this->load->library('template_data');
        $this->lang->load('users_meta');
        $this->load->model( array('Users_meta_model') );
        
        $this->template_data->set('main_page', 'users_meta' ); 
        $this->template_data->set('sub_page', 'users_meta' ); 
        $this->template_data->set('page_title', 'User Meta' ); 

    }
    
    public function index()
	{
        $this->load->view('404', $this->template_data->get() );
	}
	
	public function ajax($required=NULL, $value=NULL) {

		switch($this->input->post('action')) {
			case 'list':
				$list_limit = ( $this->input->post('limit') != '') ? (int) $this->input->post('limit') : 20;
				$order_by = ( $this->input->post('order_by') != '') ? $this->input->post('order_by') : 'umeta_id';
				$order_sort = ( $this->input->post('order_sort') != '') ? $this->input->post('order_sort') : 'DESC';
				$list_start = ( $this->input->post('list_start') != '') ? $this->input->post('list_start') : 0;
				
				$list = new $this->Users_meta_model;
				$pagination = new $this->Users_meta_model;
				
				$list->setJoin('ci_users ci_users','ci_users_meta.user_id = ci_users.user_id');
				$list->setSelect('ci_users_meta.*');
				$list->setSelect('ci_users.user_username as user_username');

				$pagination->setSelect('COUNT(*) as total_items');
				$pagination->setJoin('ci_users ci_users','ci_users_meta.user_id = ci_users.user_id');

				
				if( is_array($this->input->post('filter')) && count($this->input->post('filter')) > 0 ) {
					foreach( $this->input->post('filter') as $filter ) {
						$list->setFilter($filter['key'],$filter['value'],$filter['table']);
						$pagination->setFilter($filter['key'],$filter['value'],$filter['table']);
					}
				}
				
				$list->setOrder($order_by, $order_sort);
				$list->setStart($list_start);
				$list->setLimit($list_limit);
				
				echo json_encode( array(
							'table' => 'users_meta',
							'total_items' => (int) $pagination->get()->total_items,
							'start' => $list_start,
							'limit' => $list_limit,
							'error' => false,
							'list' => true,
							'results' => $list->populate()
						) );
				exit;
			break;
			case 'get':
				$item = $this->Users_meta_model;
				$item->setUmetaId( $this->input->post('umeta_id'), true );

				$item->setJoin('ci_users ci_users','ci_users_meta.user_id = ci_users.user_id');
				$item->setSelect('ci_users_meta.*');
				$item->setSelect('ci_users.user_username as user_username');

				echo json_encode( array(
							'id' => $this->input->post('umeta_id'),
							'table' => 'users_meta',
							'error' => false,
							'get' => true,
							'results' => $item->get()
						) );
						exit;
			break;
			case 'update_field':
				if(  ! $this->template_data->get('admin_access')->controller_users_meta ) {
					return 0;
				}
				$results = array(
							'id' => $this->input->post('umeta_id'),
							'table' => 'users_meta',
							'error' => true,
							'update_field' => false,
							'message' => 'Unable to update field!'
						);
				$item = $this->Users_meta_model;
				$item->setUmetaId( $this->input->post('umeta_id'), true );
				$item->setFieldValue( $this->input->post('field'), $this->input->post('value'), FALSE, TRUE );
				if( $item->updateByUmetaId() ) {
					$results['error'] = false;
					$results['update_field'] = true;
					$results['message'] = 'Successfully Updated!';
				}
				echo json_encode( $results );
				exit;
			break;
			case 'add':
				if(  ! $this->template_data->get('admin_access')->controller_users_meta ) {
					return 0;
				}
				echo json_encode($this->submission('add'));
				exit;
			break;
			case 'edit':
				if(  ! $this->template_data->get('admin_access')->controller_users_meta ) {
					return 0;
				}
				echo json_encode($this->submission('edit'));
				exit;
			break;
			case 'delete':
				if(  ! $this->template_data->get('admin_access')->controller_users_meta ) {
					return 0;
				}
				$results = array(
							'id' => $this->input->post('umeta_id'),
							'table' => 'users_meta',
							'error' => true,
							'removed' => false,
							'message' => 'Unable to delete!'
						);
				$this->Users_meta_model->setUmetaId( $this->input->post('umeta_id') );
				$data = $this->Users_meta_model->getByUmetaId();
		
				
				if( $this->Users_meta_model->deleteByUmetaId() ) {
					$results['error'] = false;
					$results['removed'] = true;
					$results['message'] = 'Successfully Deleted!';
				}
		
				echo json_encode( $results );
				exit;
			break;
			case "upload":
				if(  ! $this->template_data->get('admin_access')->controller_users_meta ) {
					return 0;
				}
				$results = array(
					'group' => $this->input->post('group'),
					'key' => $this->input->post('key'),
					'table' => 'media_uploads',
					'error' => true,
					'removed' => false,
					'message' => 'Unable to upload!'
				);
				
				$config['upload_path'] = 'None';
				$config['allowed_types'] = '';
				$config['max_size']	= 'None';
				$config['max_width']  = 'None';
				$config['max_height']  = 'None';

				$this->load->library('upload', $config);

				if ( ! $this->upload->do_upload())
				{
					$results['error'] = true;
					$results['message'] = $this->upload->display_errors();
					
				}
				else
				{
					$results['error'] = false;
					$results['message'] = "Success!";
					$upload_data = $this->upload->data();
					$results['upload_data'] = $upload_data;
					
					$container = new $this->Users_meta_model;

					if( $container->insert() ) {
						$results['id'] = $container->getUmetaId();
						$results['results'] = $container->getByUmetaId();
					}
				}
				echo json_encode( $results );
				exit;
			break;
		
		}
		echo 0;
		exit;
	}
	
	private function submission($action) {
	    $this->load->library('form_validation');
	    $results = array(
			'error' => true,
			'message' => 'No Response!',
			'table' => 'users_meta',
	    );

		if( $action == 'add' ) {
			$this->form_validation->set_rules('meta_key', 'lang:users_meta_meta_key', 'required');
		}
		elseif( $action == 'edit' ) {
			$this->form_validation->set_rules('meta_key', 'lang:users_meta_meta_key', 'required');
		}

		if ($this->form_validation->run() == FALSE)
		{
			if( $this->input->post() ) {
				$this->template_data->alert( validation_errors(), 'danger');
				$results['message'] = validation_errors();
			}
		} 
		else 
		{
			$container = new $this->Users_meta_model;
			if( $this->input->post('umeta_id') !== FALSE ) {
				$container->setUmetaId( $this->input->post('umeta_id'), FALSE, TRUE );
			}

			if( $this->input->post('user_id') !== FALSE ) {
				$container->setUserId( $this->input->post('user_id'), FALSE, TRUE );
			}

			if( $this->input->post('meta_key') !== FALSE ) {
				$container->setMetaKey( $this->input->post('meta_key'), FALSE, TRUE );
			}

			if( $this->input->post('meta_value') !== FALSE ) {
				$container->setMetaValue( $this->input->post('meta_value'), FALSE, TRUE );
			}

			if( $action == 'add' ) { 
			

				if( $container->replace() ) {
					$this->template_data->alert( 'Successfully Added!', 'success');
					$results['added'] = true;
					$results['error'] = false;
					$results['message'] = 'Successfully Added!';
				} else {
					$results['added'] = false;
					$results['error'] = true;
					$results['message'] = 'Unable to add data!';
				}
			} 
			elseif( $action == 'edit' ) { 
				if( $container->updateByUmetaId() ) {
					$this->template_data->alert( 'Successfully Updated!', 'success');
					$results['updated'] = true;
					$results['error'] = false;
					$results['message'] = 'Successfully Updated!';
				} else {
					$results['updated'] = false;
					$results['error'] = true;
					$results['message'] = 'Unable to update data!';
				}
			}
				$container->setJoin('ci_users ci_users','ci_users_meta.user_id = ci_users.user_id');
				$container->setSelect('ci_users_meta.*');
				$container->setSelect('ci_users.user_username as user_username');


			$results['id'] = $container->getUmetaId();
			$results['results'] = $container->getByUmetaId();
		}

	    return $results;
	}
	
}
/* End of file users_meta.php */
/* Location: ./application/controllers/users_meta.php */
