<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Settings extends MY_Controller {

    function __construct() {
        parent::__construct();

        $this->load->library('template_data');
        $this->lang->load('settings');
        $this->load->model( array('Settings_model') );
        
        $this->template_data->set('main_page', 'settings' ); 
        $this->template_data->set('sub_page', 'settings' ); 
        $this->template_data->set('page_title', 'Settings' ); 

    }
    
    public function index()
	{
        $this->load->view('settings', $this->template_data->get() );
	}
	
	public function ajax($required=NULL, $value=NULL) {

		switch($this->input->post('action')) {
			case 'list':
				$list_limit = ( $this->input->post('limit') != '') ? (int) $this->input->post('limit') : 20;
				$order_by = ( $this->input->post('order_by') != '') ? $this->input->post('order_by') : 'setting_name';
				$order_sort = ( $this->input->post('order_sort') != '') ? $this->input->post('order_sort') : 'ASC';
				$list_start = ( $this->input->post('list_start') != '') ? $this->input->post('list_start') : 0;
				
				$list = new $this->Settings_model;
				$pagination = new $this->Settings_model;
				
				$pagination->setSelect('COUNT(*) as total_items');

				
				if( is_array($this->input->post('filter')) && count($this->input->post('filter')) > 0 ) {
					foreach( $this->input->post('filter') as $filter ) {
						$list->setFilter($filter['key'],$filter['value'],$filter['table']);
						$pagination->setFilter($filter['key'],$filter['value'],$filter['table']);
					}
				}
				
				$list->setOrder($order_by, $order_sort);
				$list->setStart($list_start);
				$list->setLimit($list_limit);
				
				echo json_encode( array(
							'table' => 'settings',
							'total_items' => (int) $pagination->get()->total_items,
							'start' => $list_start,
							'limit' => $list_limit,
							'error' => false,
							'list' => true,
							'results' => $list->populate()
						) );
				exit;
			break;
			case 'get':
				$item = $this->Settings_model;
				$item->setSettingId( $this->input->post('setting_id'), true );

				echo json_encode( array(
							'id' => $this->input->post('setting_id'),
							'table' => 'settings',
							'error' => false,
							'get' => true,
							'results' => $item->get()
						) );
						exit;
			break;
			case 'update_field':
				if(  ! $this->template_data->get('admin_access')->controller_settings ) {
					return 0;
				}
				$results = array(
							'id' => $this->input->post('setting_id'),
							'table' => 'settings',
							'error' => true,
							'update_field' => false,
							'message' => 'Unable to update field!'
						);
				$item = $this->Settings_model;
				$item->setSettingId( $this->input->post('setting_id'), true );
				$item->setFieldValue( $this->input->post('field'), $this->input->post('value'), FALSE, TRUE );
				if( $item->updateBySettingId() ) {
					$results['error'] = false;
					$results['update_field'] = true;
					$results['message'] = 'Successfully Updated!';
				}
				echo json_encode( $results );
				exit;
			break;
			case 'add':
				if(  ! $this->template_data->get('admin_access')->controller_settings ) {
					return 0;
				}
				echo json_encode($this->submission('add'));
				exit;
			break;
			case 'edit':
				if(  ! $this->template_data->get('admin_access')->controller_settings ) {
					return 0;
				}
				echo json_encode($this->submission('edit'));
				exit;
			break;
			case 'delete':
				if(  ! $this->template_data->get('admin_access')->controller_settings ) {
					return 0;
				}
				$results = array(
							'id' => $this->input->post('setting_id'),
							'table' => 'settings',
							'error' => true,
							'removed' => false,
							'message' => 'Unable to delete!'
						);
				$this->Settings_model->setSettingId( $this->input->post('setting_id') );
				$data = $this->Settings_model->getBySettingId();
		
				
				if( $this->Settings_model->deleteBySettingId() ) {
					$results['error'] = false;
					$results['removed'] = true;
					$results['message'] = 'Successfully Deleted!';
				}
		
				echo json_encode( $results );
				exit;
			break;
			case "upload":
				if(  ! $this->template_data->get('admin_access')->controller_settings ) {
					return 0;
				}
				$results = array(
					'group' => $this->input->post('group'),
					'key' => $this->input->post('key'),
					'table' => 'media_uploads',
					'error' => true,
					'removed' => false,
					'message' => 'Unable to upload!'
				);
				
				$config['upload_path'] = 'None';
				$config['allowed_types'] = '';
				$config['max_size']	= 'None';
				$config['max_width']  = 'None';
				$config['max_height']  = 'None';

				$this->load->library('upload', $config);

				if ( ! $this->upload->do_upload())
				{
					$results['error'] = true;
					$results['message'] = $this->upload->display_errors();
					
				}
				else
				{
					$results['error'] = false;
					$results['message'] = "Success!";
					$upload_data = $this->upload->data();
					$results['upload_data'] = $upload_data;
					
					$container = new $this->Settings_model;

					if( $container->insert() ) {
						$results['id'] = $container->getSettingId();
						$results['results'] = $container->getBySettingId();
					}
				}
				echo json_encode( $results );
				exit;
			break;
		
		}
		echo 0;
		exit;
	}
	
	private function submission($action) {
	    $this->load->library('form_validation');
	    $results = array(
			'error' => true,
			'message' => 'No Response!',
			'table' => 'settings',
	    );

		if( $action == 'add' ) {
			$this->form_validation->set_rules('setting_controller', 'lang:settings_setting_controller', 'required');
			$this->form_validation->set_rules('setting_name', 'lang:settings_setting_name', 'required');
			$this->form_validation->set_rules('setting_value', 'lang:settings_setting_value', 'required');
		}
		elseif( $action == 'edit' ) {
			$this->form_validation->set_rules('setting_controller', 'lang:settings_setting_controller', 'required');
			$this->form_validation->set_rules('setting_name', 'lang:settings_setting_name', 'required');
			$this->form_validation->set_rules('setting_value', 'lang:settings_setting_value', 'required');
		}

		if ($this->form_validation->run() == FALSE)
		{
			if( $this->input->post() ) {
				$this->template_data->alert( validation_errors(), 'danger');
				$results['message'] = validation_errors();
			}
		} 
		else 
		{
			$container = new $this->Settings_model;
			if( $this->input->post('setting_id') !== FALSE ) {
				$container->setSettingId( $this->input->post('setting_id'), FALSE, TRUE );
			}

			$container->setSettingController( $this->input->post('setting_controller'), FALSE, TRUE );

			$container->setSettingName( $this->input->post('setting_name'), FALSE, TRUE );

			if( $this->input->post('setting_description') !== FALSE ) {
				$container->setSettingDescription( $this->input->post('setting_description'), FALSE, TRUE );
			}

			$container->setSettingValue( $this->input->post('setting_value'), FALSE, TRUE );

			if( $action == 'add' ) { 
			

				if( $container->replace() ) {
					$this->template_data->alert( 'Successfully Added!', 'success');
					$results['added'] = true;
					$results['error'] = false;
					$results['message'] = 'Successfully Added!';
				} else {
					$results['added'] = false;
					$results['error'] = true;
					$results['message'] = 'Unable to add data!';
				}
			} 
			elseif( $action == 'edit' ) { 
				if( $container->updateBySettingId() ) {
					$this->template_data->alert( 'Successfully Updated!', 'success');
					$results['updated'] = true;
					$results['error'] = false;
					$results['message'] = 'Successfully Updated!';
				} else {
					$results['updated'] = false;
					$results['error'] = true;
					$results['message'] = 'Unable to update data!';
				}
			}

			$results['id'] = $container->getSettingId();
			$results['results'] = $container->getBySettingId();
		}

	    return $results;
	}
	
}
/* End of file settings.php */
/* Location: ./application/controllers/settings.php */
