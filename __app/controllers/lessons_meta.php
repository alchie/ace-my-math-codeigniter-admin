<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Lessons_meta extends MY_Controller {

    function __construct() {
        parent::__construct();

        $this->load->library('template_data');
        $this->lang->load('lessons_meta');
        $this->load->model( array('Lessons_meta_model') );
        
        $this->template_data->set('main_page', 'lessons_meta' ); 
        $this->template_data->set('sub_page', 'lessons_meta' ); 
        $this->template_data->set('page_title', 'Lesson Meta' ); 

    }
    
    public function index()
	{
        $this->load->view('404', $this->template_data->get() );
	}
	
	public function ajax($required=NULL, $value=NULL) {

		switch($this->input->post('action')) {
			case 'list':
				$list_limit = ( $this->input->post('limit') != '') ? (int) $this->input->post('limit') : 20;
				$order_by = ( $this->input->post('order_by') != '') ? $this->input->post('order_by') : 'lmeta_id';
				$order_sort = ( $this->input->post('order_sort') != '') ? $this->input->post('order_sort') : 'DESC';
				$list_start = ( $this->input->post('list_start') != '') ? $this->input->post('list_start') : 0;
				
				$list = new $this->Lessons_meta_model;
				$pagination = new $this->Lessons_meta_model;
				
				$list->setJoin('ci_lessons ci_lessons_m','ci_lessons_meta.lesson_id = ci_lessons_m.lesson_id');
				$list->setSelect('ci_lessons_meta.*');
				$list->setSelect('ci_lessons_m.lesson_title as lesson_title_m');

				$pagination->setSelect('COUNT(*) as total_items');
				$pagination->setJoin('ci_lessons ci_lessons_m','ci_lessons_meta.lesson_id = ci_lessons_m.lesson_id');

				
				if( is_array($this->input->post('filter')) && count($this->input->post('filter')) > 0 ) {
					foreach( $this->input->post('filter') as $filter ) {
						$list->setFilter($filter['key'],$filter['value'],$filter['table']);
						$pagination->setFilter($filter['key'],$filter['value'],$filter['table']);
					}
				}
				
				$list->setOrder($order_by, $order_sort);
				$list->setStart($list_start);
				$list->setLimit($list_limit);
				
				echo json_encode( array(
							'table' => 'lessons_meta',
							'total_items' => (int) $pagination->get()->total_items,
							'start' => $list_start,
							'limit' => $list_limit,
							'error' => false,
							'list' => true,
							'results' => $list->populate()
						) );
				exit;
			break;
			case 'get':
				$item = $this->Lessons_meta_model;
				$item->setLmetaId( $this->input->post('lmeta_id'), true );

				$item->setJoin('ci_lessons ci_lessons_m','ci_lessons_meta.lesson_id = ci_lessons_m.lesson_id');
				$item->setSelect('ci_lessons_meta.*');
				$item->setSelect('ci_lessons_m.lesson_title as lesson_title_m');

				echo json_encode( array(
							'id' => $this->input->post('lmeta_id'),
							'table' => 'lessons_meta',
							'error' => false,
							'get' => true,
							'results' => $item->get()
						) );
						exit;
			break;
			case 'update_field':
				if(  ! $this->template_data->get('admin_access')->controller_lessons_meta ) {
					return 0;
				}
				$results = array(
							'id' => $this->input->post('lmeta_id'),
							'table' => 'lessons_meta',
							'error' => true,
							'update_field' => false,
							'message' => 'Unable to update field!'
						);
				$item = $this->Lessons_meta_model;
				$item->setLmetaId( $this->input->post('lmeta_id'), true );
				$item->setFieldValue( $this->input->post('field'), $this->input->post('value'), FALSE, TRUE );
				if( $item->updateByLmetaId() ) {
					$results['error'] = false;
					$results['update_field'] = true;
					$results['message'] = 'Successfully Updated!';
				}
				echo json_encode( $results );
				exit;
			break;
			case 'add':
				if(  ! $this->template_data->get('admin_access')->controller_lessons_meta ) {
					return 0;
				}
				echo json_encode($this->submission('add'));
				exit;
			break;
			case 'edit':
				if(  ! $this->template_data->get('admin_access')->controller_lessons_meta ) {
					return 0;
				}
				echo json_encode($this->submission('edit'));
				exit;
			break;
			case 'delete':
				if(  ! $this->template_data->get('admin_access')->controller_lessons_meta ) {
					return 0;
				}
				$results = array(
							'id' => $this->input->post('lmeta_id'),
							'table' => 'lessons_meta',
							'error' => true,
							'removed' => false,
							'message' => 'Unable to delete!'
						);
				$this->Lessons_meta_model->setLmetaId( $this->input->post('lmeta_id') );
				$data = $this->Lessons_meta_model->getByLmetaId();
		
				
				if( $this->Lessons_meta_model->deleteByLmetaId() ) {
					$results['error'] = false;
					$results['removed'] = true;
					$results['message'] = 'Successfully Deleted!';
				}
		
				echo json_encode( $results );
				exit;
			break;
			case "upload":
				if(  ! $this->template_data->get('admin_access')->controller_lessons_meta ) {
					return 0;
				}
				$results = array(
					'group' => $this->input->post('group'),
					'key' => $this->input->post('key'),
					'table' => 'media_uploads',
					'error' => true,
					'removed' => false,
					'message' => 'Unable to upload!'
				);
				
				$config['upload_path'] = 'None';
				$config['allowed_types'] = '';
				$config['max_size']	= 'None';
				$config['max_width']  = 'None';
				$config['max_height']  = 'None';

				$this->load->library('upload', $config);

				if ( ! $this->upload->do_upload())
				{
					$results['error'] = true;
					$results['message'] = $this->upload->display_errors();
					
				}
				else
				{
					$results['error'] = false;
					$results['message'] = "Success!";
					$upload_data = $this->upload->data();
					$results['upload_data'] = $upload_data;
					
					$container = new $this->Lessons_meta_model;

					if( $container->insert() ) {
						$results['id'] = $container->getLmetaId();
						$results['results'] = $container->getByLmetaId();
					}
				}
				echo json_encode( $results );
				exit;
			break;
		
		}
		echo 0;
		exit;
	}
	
	private function submission($action) {
	    $this->load->library('form_validation');
	    $results = array(
			'error' => true,
			'message' => 'No Response!',
			'table' => 'lessons_meta',
	    );

		if( $action == 'add' ) {
			$this->form_validation->set_rules('lesson_id', 'lang:lessons_meta_lesson_id', 'required');
			$this->form_validation->set_rules('meta_key', 'lang:lessons_meta_meta_key', 'required');
		}
		elseif( $action == 'edit' ) {
			$this->form_validation->set_rules('lesson_id', 'lang:lessons_meta_lesson_id', 'required');
			$this->form_validation->set_rules('meta_key', 'lang:lessons_meta_meta_key', 'required');
		}

		if ($this->form_validation->run() == FALSE)
		{
			if( $this->input->post() ) {
				$this->template_data->alert( validation_errors(), 'danger');
				$results['message'] = validation_errors();
			}
		} 
		else 
		{
			$container = new $this->Lessons_meta_model;
			if( $this->input->post('lmeta_id') !== FALSE ) {
				$container->setLmetaId( $this->input->post('lmeta_id'), FALSE, TRUE );
			}

			$container->setLessonId( $this->input->post('lesson_id'), FALSE, TRUE );

			$container->setMetaKey( $this->input->post('meta_key'), FALSE, TRUE );

			if( $this->input->post('meta_value') !== FALSE ) {
				$container->setMetaValue( $this->input->post('meta_value'), FALSE, TRUE );
			}

			if( $action == 'add' ) { 
			

				if( $container->replace() ) {
					$this->template_data->alert( 'Successfully Added!', 'success');
					$results['added'] = true;
					$results['error'] = false;
					$results['message'] = 'Successfully Added!';
				} else {
					$results['added'] = false;
					$results['error'] = true;
					$results['message'] = 'Unable to add data!';
				}
			} 
			elseif( $action == 'edit' ) { 
				if( $container->updateByLmetaId() ) {
					$this->template_data->alert( 'Successfully Updated!', 'success');
					$results['updated'] = true;
					$results['error'] = false;
					$results['message'] = 'Successfully Updated!';
				} else {
					$results['updated'] = false;
					$results['error'] = true;
					$results['message'] = 'Unable to update data!';
				}
			}
				$container->setJoin('ci_lessons ci_lessons_m','ci_lessons_meta.lesson_id = ci_lessons_m.lesson_id');
				$container->setSelect('ci_lessons_meta.*');
				$container->setSelect('ci_lessons_m.lesson_title as lesson_title_m');


			$results['id'] = $container->getLmetaId();
			$results['results'] = $container->getByLmetaId();
		}

	    return $results;
	}
	
}
/* End of file lessons_meta.php */
/* Location: ./application/controllers/lessons_meta.php */
