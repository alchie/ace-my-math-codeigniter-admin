<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Goals extends MY_Controller {

    function __construct() {
        parent::__construct();

        $this->load->library('template_data');
        $this->lang->load('goals');
        $this->load->model( array('Goals_model') );
        
        $this->template_data->set('main_page', 'content' ); 
        $this->template_data->set('sub_page', 'goals' ); 
        $this->template_data->set('page_title', 'Goals' ); 

    }
    
    public function index()
	{
        $this->load->view('goals', $this->template_data->get() );
	}
	
	public function ajax($required=NULL, $value=NULL) {

		switch($this->input->post('action')) {
			case 'list':
				$list_limit = ( $this->input->post('limit') != '') ? (int) $this->input->post('limit') : 20;
				$order_by = ( $this->input->post('order_by') != '') ? $this->input->post('order_by') : 'goal_name';
				$order_sort = ( $this->input->post('order_sort') != '') ? $this->input->post('order_sort') : 'ASC';
				$list_start = ( $this->input->post('list_start') != '') ? $this->input->post('list_start') : 0;
				
				$list = new $this->Goals_model;
				$pagination = new $this->Goals_model;
				
				$list->setJoin('ci_users ci_tax_levels','ci_goals.creator = ci_tax_levels.user_id');
				$list->setSelect('ci_goals.*');
				$list->setSelect('ci_tax_levels.user_username as user_username');

				$pagination->setSelect('COUNT(*) as total_items');
				$pagination->setJoin('ci_users ci_tax_levels','ci_goals.creator = ci_tax_levels.user_id');

				
				if( is_array($this->input->post('filter')) && count($this->input->post('filter')) > 0 ) {
					foreach( $this->input->post('filter') as $filter ) {
						$list->setFilter($filter['key'],$filter['value'],$filter['table']);
						$pagination->setFilter($filter['key'],$filter['value'],$filter['table']);
					}
				}
				
				$list->setOrder($order_by, $order_sort);
				$list->setStart($list_start);
				$list->setLimit($list_limit);
				
				echo json_encode( array(
							'table' => 'goals',
							'total_items' => (int) $pagination->get()->total_items,
							'start' => $list_start,
							'limit' => $list_limit,
							'error' => false,
							'list' => true,
							'results' => $list->populate()
						) );
				exit;
			break;
			case 'get':
				$item = $this->Goals_model;
				$item->setGoalId( $this->input->post('goal_id'), true );

				$item->setJoin('ci_users ci_tax_levels','ci_goals.creator = ci_tax_levels.user_id');
				$item->setSelect('ci_goals.*');
				$item->setSelect('ci_tax_levels.user_username as user_username');

				echo json_encode( array(
							'id' => $this->input->post('goal_id'),
							'table' => 'goals',
							'error' => false,
							'get' => true,
							'results' => $item->get()
						) );
						exit;
			break;
			case 'update_field':
				if(  ! $this->template_data->get('admin_access')->controller_goals ) {
					return 0;
				}
				$results = array(
							'id' => $this->input->post('goal_id'),
							'table' => 'goals',
							'error' => true,
							'update_field' => false,
							'message' => 'Unable to update field!'
						);
				$item = $this->Goals_model;
				$item->setGoalId( $this->input->post('goal_id'), true );
				$item->setFieldValue( $this->input->post('field'), $this->input->post('value'), FALSE, TRUE );
				if( $item->updateByGoalId() ) {
					$results['error'] = false;
					$results['update_field'] = true;
					$results['message'] = 'Successfully Updated!';
				}
				echo json_encode( $results );
				exit;
			break;
			case 'add':
				if(  ! $this->template_data->get('admin_access')->controller_goals ) {
					return 0;
				}
				echo json_encode($this->submission('add'));
				exit;
			break;
			case 'edit':
				if(  ! $this->template_data->get('admin_access')->controller_goals ) {
					return 0;
				}
				echo json_encode($this->submission('edit'));
				exit;
			break;
			case 'delete':
				if(  ! $this->template_data->get('admin_access')->controller_goals ) {
					return 0;
				}
				$results = array(
							'id' => $this->input->post('goal_id'),
							'table' => 'goals',
							'error' => true,
							'removed' => false,
							'message' => 'Unable to delete!'
						);
				$this->Goals_model->setGoalId( $this->input->post('goal_id') );
				$data = $this->Goals_model->getByGoalId();
		
				
				if( $this->Goals_model->deleteByGoalId() ) {
					$results['error'] = false;
					$results['removed'] = true;
					$results['message'] = 'Successfully Deleted!';
				}
		
				echo json_encode( $results );
				exit;
			break;
			case "upload":
				if(  ! $this->template_data->get('admin_access')->controller_goals ) {
					return 0;
				}
				$results = array(
					'group' => $this->input->post('group'),
					'key' => $this->input->post('key'),
					'table' => 'media_uploads',
					'error' => true,
					'removed' => false,
					'message' => 'Unable to upload!'
				);
				
				$config['upload_path'] = 'None';
				$config['allowed_types'] = '';
				$config['max_size']	= 'None';
				$config['max_width']  = 'None';
				$config['max_height']  = 'None';

				$this->load->library('upload', $config);

				if ( ! $this->upload->do_upload())
				{
					$results['error'] = true;
					$results['message'] = $this->upload->display_errors();
					
				}
				else
				{
					$results['error'] = false;
					$results['message'] = "Success!";
					$upload_data = $this->upload->data();
					$results['upload_data'] = $upload_data;
					
					$container = new $this->Goals_model;

					if( $container->insert() ) {
						$results['id'] = $container->getGoalId();
						$results['results'] = $container->getByGoalId();
					}
				}
				echo json_encode( $results );
				exit;
			break;
		
		}
		echo 0;
		exit;
	}
	
	private function submission($action) {
	    $this->load->library('form_validation');
	    $results = array(
			'error' => true,
			'message' => 'No Response!',
			'table' => 'goals',
	    );

		if( $action == 'add' ) {
			$this->form_validation->set_rules('goal_name', 'lang:goals_goal_name', 'trim|required');
		}
		elseif( $action == 'edit' ) {
			$this->form_validation->set_rules('goal_id', 'lang:goals_goal_id', 'required');
			$this->form_validation->set_rules('goal_name', 'lang:goals_goal_name', 'trim|required');
			$this->form_validation->set_rules('goal_slug', 'lang:goals_goal_slug', 'required');
		}

		if ($this->form_validation->run() == FALSE)
		{
			if( $this->input->post() ) {
				$this->template_data->alert( validation_errors(), 'danger');
				$results['message'] = validation_errors();
			}
		} 
		else 
		{
			$container = new $this->Goals_model;
			if( $this->input->post('goal_id') !== FALSE ) {
				$container->setGoalId( $this->input->post('goal_id'), FALSE, TRUE );
			}

			if( $this->input->post('goal_name') !== FALSE ) {
				$container->setGoalName( $this->input->post('goal_name'), FALSE, TRUE );
			}

			if( $this->input->post('goal_slug') !== FALSE ) {
				$container->setGoalSlug( url_title( $this->input->post('goal_slug'), '-', TRUE ), FALSE, TRUE );
				if($container->getGoalSlug() == '') {
					$container->setGoalSlug( url_title( $this->input->post('goal_name') , '-', TRUE), FALSE, TRUE );
				}
			}

			$container->setGradeLevel( $this->input->post('grade_level'), FALSE, TRUE );

			if( $this->input->post('goal_public') !== FALSE ) {
				$container->setGoalPublic( $this->input->post('goal_public'), FALSE, TRUE );
			}
			else {
				$container->setGoalPublic( '0', FALSE, TRUE );
			}

			if( $this->input->post('creator') !== FALSE ) {
				$container->setCreator( $this->input->post('creator'), FALSE, TRUE );
			}

			if( $action == 'add' ) { 
			
				if($container->getGoalSlug() == '') {
					$container->setGoalSlug( url_title( $this->input->post('goal_name') , '-', TRUE), FALSE, TRUE );
				}

				if( $container->replace() ) {
					$this->template_data->alert( 'Successfully Added!', 'success');
					$results['added'] = true;
					$results['error'] = false;
					$results['message'] = 'Successfully Added!';
				} else {
					$results['added'] = false;
					$results['error'] = true;
					$results['message'] = 'Unable to add data!';
				}
			} 
			elseif( $action == 'edit' ) { 
				if( $container->updateByGoalId() ) {
					$this->template_data->alert( 'Successfully Updated!', 'success');
					$results['updated'] = true;
					$results['error'] = false;
					$results['message'] = 'Successfully Updated!';
				} else {
					$results['updated'] = false;
					$results['error'] = true;
					$results['message'] = 'Unable to update data!';
				}
			}
				$container->setJoin('ci_users ci_tax_levels','ci_goals.creator = ci_tax_levels.user_id');
				$container->setSelect('ci_goals.*');
				$container->setSelect('ci_tax_levels.user_username as user_username');


			$results['id'] = $container->getGoalId();
			$results['results'] = $container->getByGoalId();
		}

	    return $results;
	}
	
}
/* End of file goals.php */
/* Location: ./application/controllers/goals.php */
