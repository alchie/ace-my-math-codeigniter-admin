<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('template_data');
        
        $this->template_data->set('main_page', 'dashboard' ); 
        $this->template_data->set('sub_page', 'dashboard' ); 
        $this->template_data->set('page_title', 'Dashboard' ); 
    }
    
	public function index()
	{ 
		$this->load->view('dashboard', $this->template_data->get() );
		
	}
	
	public function unauthorized()
	{
	    
	    $this->template_data->set('unauthorized', true );
		$this->load->view('dashboard', $this->template_data->get() );

	}
	
	    
	public function change_password()
	{
	    
	    $this->load->library(array('form_validation') );
	    
	    $this->form_validation->set_rules('old_password', 'Old Password', 'trim|required|xss_clean|sha1|md5');
	    $this->form_validation->set_rules('new_password', 'New Password', 'trim|required|xss_clean|sha1|md5');
	    $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'trim|required|xss_clean|matches[new_password]|sha1|md5');
		
		if ($this->form_validation->run() == FALSE)
		{
		     if( $this->input->post() ) {
		         $this->template_data->alert( validation_errors(), 'danger');
		    }
		} 
		else 
		{
		    
		    $this->load->model(array('Admins_model'));
		    $this->Admins_model->setId( $this->session->userdata('admin_id'), true );
            $this->Admins_model->setPassword( $this->input->post( 'old_password' ), true );
            
		    if ( $this->Admins_model->nonempty() === TRUE ) { 
				$this->Admins_model->setPassword( $this->input->post( 'new_password' ), false, true );
				$this->Admins_model->update();
				$this->template_data->alert( 'Password Changed!', 'success');       
		    } else {
				$this->template_data->alert( 'Password Incorrect!', 'danger'); 
			}
		}
	    
		$this->load->view('change_password', $this->template_data->get() );
		
	}
	
}


