<?php $this->load->view('common_header'); ?>
<div class="row"><div class="col-lg-12">
	<h1 class="page-header"><?php echo $page_title; ?></h1>
	
	<div id="list-view-goals" class="list-view">
<div class="panel panel-default panel-goals">
<div class="panel-heading">

			<?php if( isset($admin_access->controller_goals->can_add) && ($admin_access->controller_goals->can_add == 1) ) { ?>
			<a href="javascript:void(0);" class="btn btn-default btn-sm pull-right" id="add-button-goals">Add Goal</a>
			<?php } ?>
<div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">
<div class="table-responsive">
<table id="dataTables" class="table table-striped table-bordered table-hover">
<thead>
<tr>

<th width="">Name<span  data-key="goal_name" data-table="goals" id="list_search_button_goal_name" class="btn btn-primary btn-xs pull-right btn-search list-search-goals" title="Search Name">
		<i class="fa fa-search"></i></span></th><th width="10%"><div class="dropdown-filter"><a href="javascript:void(0);" data-filter="grade_level" data-table="goals">Grade Level <span class="glyphicon glyphicon-chevron-down pull-right"></span></a></div></th><th width="130">Actions</th>
</tr>
</thead>
<tbody>
</tbody>
</table>
</div>
</div> <!-- .panel-body -->
</div> <!-- .panel .panel-goals -->
</div>
		<?php if( isset($admin_access->controller_goals->can_add) && ($admin_access->controller_goals->can_add == 1) ) { ?>
		<div id="add-view-goals" style="display:none">
<div class="panel panel-default add-panel-goals">
                        <div class="panel-heading"><h3 class="panel-title">Add Goal</h3><div class="clearfix"></div>
                        </div>
                        <!-- /.panel-heading -->
<div class="panel-body">

<div class="form-group">
<label for="add_goals_goal_name">Name</label> 
<input data-type="text" type="text" name="goal_name" id="add_goals_goal_name" class="form-control add_goals_goal_name goals-input  table-goals add-table-goals text text" placeholder="Name" value=""/>
</div>
<div class="form-group">
<label for="add_lessons_grade_level">Grade Level</label> 
			<select name="grade_level" id="add_goals_grade_level" class="selectpicker form-control add_goals_grade_level goals-input  table-goals add-table-goals dropdown text dropdown-table" placeholder="Grade Level" data-live-search="true"  data-type="dropdown" data-label="Grade Level" data-field="grade_level" data-table="tax_levels" data-key="level_id" data-value="level_name" data-filter="0" data-filter-key="" data-filter-value="" data-order="1" data-order-by="level_name" data-order-sort="ASC">
			<option value="">- - Select Grade Level - -</option>
</select></div>
<div class="form-group">
<label for="add_goals_creator">Created By</label> 
<input data-type="text" type="hidden" name="creator" id="add_goals_creator" class="form-control add_goals_creator goals-input  table-goals add-table-goals text text text-searchable-key-creator  add text-searchable-key" />
<a href="javascript:void(0)" data-field="creator"  data-table="users" data-key="user_id" data-value="user_username" data-display="user_username" data-action="add"  class="text-searchable-list creator" data-toggle="modal" data-target="#add-text-searchable-box-creator"><span class="glyphicon glyphicon-list"></span></a>
				<input data-type="text" type="text" name="creator" class="form-control add text-searchable creator" placeholder="Search Created By" data-field="creator"  data-table="users" data-key="user_id" data-value="user_username" data-display="user_username" data-action="add" />
				<div class="modal fade add" id="add-text-searchable-box-creator" tabindex="-1" role="dialog" aria-labelledby="Created By" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Created By List</h4>
      </div>
      <div class="modal-body">
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
				
</div>
<div class="form-group"><strong>Public</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="goal_public" id="add_goals_goal_public" class="add_goals_goal_public goals-input  table-goals add-table-goals checkbox text" placeholder="Public" value="1" />Public</label></div></div>
</div> <!-- .panel-body -->

<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="add returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="add-action-goals">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm add-back-button" id="add-back-goals">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-goals -->
</div>
<?php } ?><?php if( isset($admin_access->controller_goals->can_edit) && ($admin_access->controller_goals->can_edit == 1) ) { ?>
		<div id="edit-view-goals" style="display:none">
		
		<div class="tab-content tab-content-goals parent active"><div class="panel panel-default edit-panel-goals">
<div class="panel-heading">
	 <h3 class="panel-title">Edit Goal</h3>
	 <div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="goal_id" id="edit_goals_goal_id" class="edit_goals_goal_id goals-input  table-goals edit-table-goals hidden text" placeholder="ID" value="" />
<div class="form-group">
<label for="edit_goals_goal_name">Name</label> 
<input data-type="text" type="text" name="goal_name" id="edit_goals_goal_name" class="form-control edit_goals_goal_name goals-input  table-goals edit-table-goals text text" placeholder="Name" value=""/>
</div>
<div class="form-group change-deliberately button goals goal_slug">
<label>Slug</label>
			<a href="javascript:void(0);" class="btn btn-xs btn-success pull-right update goals goal_slug" data-table="goals" data-field="goal_slug">Update Slug</a>
			<p class="text-value goals goal_slug edit_goals_goal_slug goals-input  table-goals edit-table-goals text " data-type="text"></p>
			</div>
			<div class="panel panel-info change-deliberately form goals goal_slug" style="display:none;">
			<div class="panel-heading">
			<a href="javascript:void(0);" class="btn btn-xs btn-danger pull-right cancel goals goal_slug" data-table="goals" data-field="goal_slug">Cancel</a>
			Update Slug</div>
			<div class="panel-body">
			
<div class="form-group">
<label for="edit_goals_goal_slug">Slug</label> 
<input data-type="text" type="text" name="goal_slug" id="edit_goals_goal_slug" class="form-control edit_goals_goal_slug goals-input  table-goals edit-table-goals text " placeholder="Slug" value=""/>
</div></div>
</div>

<div class="form-group">
<label for="add_lessons_grade_level">Grade Level</label> 
			<select name="grade_level" id="edit_goals_grade_level" class="selectpicker form-control edit_goals_grade_level goals-input  table-goals edit-table-goals dropdown text dropdown-table" placeholder="Grade Level" data-live-search="true"  data-type="dropdown" data-label="Grade Level" data-field="grade_level" data-table="tax_levels" data-key="level_id" data-value="level_name" data-filter="0" data-filter-key="" data-filter-value="" data-order="1" data-order-by="level_name" data-order-sort="ASC">
			<option value="">- - Select Grade Level - -</option>
</select></div>
<div class="form-group">
<label for="edit_goals_creator">Created By</label> 
<input data-type="text" type="hidden" name="creator" id="edit_goals_creator" class="form-control edit_goals_creator goals-input  table-goals edit-table-goals text text text-searchable-key-creator  edit text-searchable-key" />
<a href="javascript:void(0)" data-field="creator"  data-table="users" data-key="user_id" data-value="user_username" data-display="user_username" data-action="edit"  class="text-searchable-list creator" data-toggle="modal" data-target="#edit-text-searchable-box-creator"><span class="glyphicon glyphicon-list"></span></a>
				<input data-type="text" type="text" name="creator" class="form-control edit text-searchable creator" placeholder="Search Created By" data-field="creator"  data-table="users" data-key="user_id" data-value="user_username" data-display="user_username" data-action="edit" />
				<div class="modal fade edit" id="edit-text-searchable-box-creator" tabindex="-1" role="dialog" aria-labelledby="Created By" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Created By List</h4>
      </div>
      <div class="modal-body">
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
				
</div>
<div class="form-group"><strong>Public</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="goal_public" id="edit_goals_goal_public" class="edit_goals_goal_public goals-input  table-goals edit-table-goals checkbox text" placeholder="Public" value="1" />Public</label></div></div>
</div> <!-- .panel-body -->
<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="update-action-goals">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm update-back-goals" id="update-back-goals">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-goals -->
</div><!-- .tab-content .tab-content-goals --></div>
<?php } ?>
	
	<script>
jQuery(document).ready(function($) {
	$(document).AdminActions({
		baseURL : '<?php echo base_url(); ?>',
		current_table : 'goals',
		tables : { 
		<?php if( isset($admin_access->controller_goals) ) { ?>
		
'goals' : { label : 'Goal',
fields : ["goal_id","goal_name","goal_slug","grade_level","creator","goal_public"],
add_fields : ["goal_name","grade_level","creator","goal_public"],
edit_fields : ["goal_id","goal_name","grade_level","creator","goal_public"],
list_limit : 20,
list_fields : ["goal_name","grade_level"],
order_by : 'goal_name',
order_sort : 'ASC',
filters : {"grade_level":{"type":"table","anchor":0,"table":"tax_levels","key":"level_id","value":"level_name", "filter" : 0, "filter_key" : "", "filter_value" : "", "order" : 1, "order_by" : "level_name", "order_sort" : "ASC" }},
primary_key : 'goal_id',
primary_title : 'goal_name',
actual_values : {"grade_level" : "None"},
actions_edit : <?php echo ($admin_access->controller_goals->can_edit) ? 1 : 0; ?>,
actions_delete : <?php echo ($admin_access->controller_goals->can_delete) ? 1 : 0; ?> },

		<?php } ?>
		 },
		filters_data : {"goal_public": {"1": "Public"}},
	});
});
</script>

</div></div><!-- .row . col-lg-12 -->
<?php $this->load->view('common_footer'); ?>