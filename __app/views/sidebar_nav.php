<li <?php echo ($main_page == 'dashboard') ? 'class="active"' : ''; ?>>
    <a id="menu-dashboard" href="<?php echo site_url('dashboard'); ?>" title="Dashboard"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
</li>
<?php if( isset($admin_access->controller_lessons) || isset($admin_access->controller_goals) || isset($admin_access->controller_media_uploads) ) { ?>

<li <?php echo ($main_page == 'content') ? 'class="active"' : ''; ?>>
    <a id="menu-content" href="javascript:void(0);" title="content" class=""><i class="fa fa-book fa-fw"></i> Content<span class="fa arrow"></span></a>
<ul class="nav nav-second-level" style="height: auto;"> 
					<?php if( isset($admin_access->controller_lessons) ) { ?>
					<li <?php echo ($sub_page == 'lessons') ? 'class="active"' : ''; ?>>
		<a id="menu-lessons" href="<?php echo site_url('lessons'); ?>" title="Lessons" class=""><i class="fa fa-bullhorn fa-fw"></i> Lessons</a>
	</li>
	<?php } ?>
					<?php if( isset($admin_access->controller_goals) ) { ?>
					<li <?php echo ($sub_page == 'goals') ? 'class="active"' : ''; ?>>
		<a id="menu-goals" href="<?php echo site_url('goals'); ?>" title="Goals" class=""><i class="glyphicon glyphicon-screenshot"></i> Goals</a>
	</li>
	<?php } ?>
					<?php if( isset($admin_access->controller_media_uploads) ) { ?>
					<li <?php echo ($sub_page == 'media_uploads') ? 'class="active"' : ''; ?>>
		<a id="menu-media_uploads" href="<?php echo site_url('media_uploads'); ?>" title="Media Uploads" class=""><i class="fa fa-file-image-o fa-fw"></i> Media Uploads</a>
	</li>
	<?php } ?></ul></li>
<?php } ?>
<?php if( isset($admin_access->controller_tax_levels) || isset($admin_access->controller_tax_chapters) || isset($admin_access->controller_tax_tags) ) { ?>

<li <?php echo ($main_page == 'taxonomy') ? 'class="active"' : ''; ?>>
    <a id="menu-taxonomy" href="javascript:void(0);" title="taxonomy" class=""><i class="fa fa-tasks fa-fw"></i> Taxonomies<span class="fa arrow"></span></a>
<ul class="nav nav-second-level" style="height: auto;"> 
					<?php if( isset($admin_access->controller_tax_levels) ) { ?>
					<li <?php echo ($sub_page == 'tax_levels') ? 'class="active"' : ''; ?>>
		<a id="menu-tax_levels" href="<?php echo site_url('tax_levels'); ?>" title="Grade Levels" class=""><i class="fa fa-signal fa-fw"></i> Grade Levels</a>
	</li>
	<?php } ?>
					<?php if( isset($admin_access->controller_tax_chapters) ) { ?>
					<li <?php echo ($sub_page == 'tax_chapters') ? 'class="active"' : ''; ?>>
		<a id="menu-tax_chapters" href="<?php echo site_url('tax_chapters'); ?>" title="Lesson Chapters" class=""><i class="fa fa-bookmark fa-fw"></i> Lesson Chapters</a>
	</li>
	<?php } ?>
					<?php if( isset($admin_access->controller_tax_tags) ) { ?>
					<li <?php echo ($sub_page == 'tax_tags') ? 'class="active"' : ''; ?>>
		<a id="menu-tax_tags" href="<?php echo site_url('tax_tags'); ?>" title="Lesson Topics" class=""><i class="fa fa-tags fa-fw"></i> Lesson Topics</a>
	</li>
	<?php } ?></ul></li>
<?php } ?>
<?php if( isset($admin_access->controller_users) || isset($admin_access->controller_admins) ) { ?>

<li <?php echo ($main_page == 'accounts') ? 'class="active"' : ''; ?>>
    <a id="menu-accounts" href="javascript:void(0);" title="accounts" class=""><i class="fa fa-group fa-fw"></i> Accounts<span class="fa arrow"></span></a>
<ul class="nav nav-second-level" style="height: auto;"> 
					<?php if( isset($admin_access->controller_users) ) { ?>
					<li <?php echo ($sub_page == 'users') ? 'class="active"' : ''; ?>>
		<a id="menu-users" href="<?php echo site_url('users'); ?>" title="Users" class=""><i class="fa fa-user fa-fw"></i> Users</a>
	</li>
	<?php } ?>
					<?php if( isset($admin_access->controller_admins) ) { ?>
					<li <?php echo ($sub_page == 'admins') ? 'class="active"' : ''; ?>>
		<a id="menu-admins" href="<?php echo site_url('admins'); ?>" title="Admins" class=""><i class="fa fa-bug fa-fw"></i> Admins</a>
	</li>
	<?php } ?></ul></li>
<?php } ?>
<?php if( isset($admin_access->controller_settings) || isset($admin_access->controller_attributes) || isset($admin_access->controller_subscription_plans) ) { ?>

<li <?php echo ($main_page == 'settings') ? 'class="active"' : ''; ?>>
    <a id="menu-settings" href="javascript:void(0);" title="settings" class=""><i class="fa fa-cogs fa-fw"></i> Settings<span class="fa arrow"></span></a>
<ul class="nav nav-second-level" style="height: auto;"> 
					<?php if( isset($admin_access->controller_settings) ) { ?>
					<li <?php echo ($sub_page == 'settings') ? 'class="active"' : ''; ?>>
		<a id="menu-settings" href="<?php echo site_url('settings'); ?>" title="Settings" class=""><i class="fa fa-wrench fa-fw"></i> Settings</a>
	</li>
	<?php } ?>
					<?php if( isset($admin_access->controller_attributes) ) { ?>
					<li <?php echo ($sub_page == 'attributes') ? 'class="active"' : ''; ?>>
		<a id="menu-attributes" href="<?php echo site_url('attributes'); ?>" title="Attributes" class=""><i class="fa fa-info fa-fw"></i> Attributes</a>
	</li>
	<?php } ?>
					<?php if( isset($admin_access->controller_subscription_plans) ) { ?>
					<li <?php echo ($sub_page == 'subscription_plans') ? 'class="active"' : ''; ?>>
		<a id="menu-subscription_plans" href="<?php echo site_url('subscription_plans'); ?>" title="Membership Plans" class=""><i class="fa fa-star fa-fw"></i> Membership Plans</a>
	</li>
	<?php } ?></ul></li>
<?php } ?>