<?php $this->load->view('common_header'); ?>
<div class="row"><div class="col-lg-12">
	<h1 class="page-header"><?php echo $page_title; ?></h1>
	
	<div id="list-view-media_uploads" class="list-view">
<div class="panel panel-default panel-media_uploads">
<div class="panel-heading">

			<?php if( isset($admin_access->controller_media_uploads->can_add) && ($admin_access->controller_media_uploads->can_add == 1) ) { ?>
			<a href="javascript:void(0);" class="btn btn-default btn-sm pull-right" id="add-button-media_uploads">Upload Media File</a>
			<?php } ?>
<div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">
<div class="table-responsive">
<table id="dataTables" class="table table-striped table-bordered table-hover">
<thead>
<tr>

<th width="100px">Media ID</th><th width="">Filename<span  data-key="file_name" data-table="media_uploads" id="list_search_button_file_name" class="btn btn-primary btn-xs pull-right btn-search list-search-media_uploads" title="Search Filename">
		<i class="fa fa-search"></i></span></th><th width="">File Type<span  data-key="file_type" data-table="media_uploads" id="list_search_button_file_type" class="btn btn-primary btn-xs pull-right btn-search list-search-media_uploads" title="Search File Type">
		<i class="fa fa-search"></i></span></th><th width="130">Actions</th>
</tr>
</thead>
<tbody>
</tbody>
</table>
</div>
</div> <!-- .panel-body -->
</div> <!-- .panel .panel-media_uploads -->
</div>
		<?php if( isset($admin_access->controller_media_uploads->can_add) && ($admin_access->controller_media_uploads->can_add == 1) ) { ?>
		<div id="add-view-media_uploads" style="display:none">
<div class="panel panel-default add-panel-media_uploads">
                        <div class="panel-heading">
 <div class="btn btn-success btn-xs btn-file pull-right" id="input-file-media_uploads">
    Select Media Files<input type="file" multiple>
  </div>
<h3 class="panel-title">Upload Media Files</h3><div class="clearfix"></div>
                        </div>
                        <!-- /.panel-heading -->
<div class="panel-body">

			<div class="row" id="fileupload-media_uploads-preview" style="margin-top:20px;min-height:200px;"></div>
			
</div> <!-- .panel-body -->

<div class="panel-footer">
<button class="btn btn-success btn-sm action-button" id="upload-action-media_uploads" data-table="media_uploads">Upload All</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm upload-back-button" id="upload-back-media_uploads">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-media_uploads -->
</div>
<?php } ?><?php if( isset($admin_access->controller_media_uploads->can_edit) && ($admin_access->controller_media_uploads->can_edit == 1) ) { ?>
		<div id="edit-view-media_uploads" style="display:none">
		
		<div class="tab-content tab-content-media_uploads parent active"><div class="panel panel-default edit-panel-media_uploads">
<div class="panel-heading">
	 <h3 class="panel-title">Edit Media File</h3>
	 <div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="media_id" id="edit_media_uploads_media_id" class="edit_media_uploads_media_id media_uploads-input  table-media_uploads edit-table-media_uploads hidden text" placeholder="Media ID" value="" />
<div class="form-group">
<label for="edit_media_uploads_file_name">Filename</label> 
<input data-type="text" type="text" name="file_name" id="edit_media_uploads_file_name" class="form-control edit_media_uploads_file_name media_uploads-input  table-media_uploads edit-table-media_uploads text text" placeholder="Filename" value=""/>
</div>
<div class="form-group">
<label for="edit_media_uploads_file_type">File Type</label> 
<input data-type="text" type="text" name="file_type" id="edit_media_uploads_file_type" class="form-control edit_media_uploads_file_type media_uploads-input  table-media_uploads edit-table-media_uploads text " placeholder="File Type" value=""/>
</div>
<div class="form-group">
<label for="edit_media_uploads_file_path">File Path</label> 
<input data-type="text" type="text" name="file_path" id="edit_media_uploads_file_path" class="form-control edit_media_uploads_file_path media_uploads-input  table-media_uploads edit-table-media_uploads text text" placeholder="File Path" value=""/>
</div>
<div class="form-group">
<label for="edit_media_uploads_full_path">Full Path</label> 
<input data-type="text" type="text" name="full_path" id="edit_media_uploads_full_path" class="form-control edit_media_uploads_full_path media_uploads-input  table-media_uploads edit-table-media_uploads text text" placeholder="Full Path" value=""/>
</div>
<div class="form-group">
<label for="edit_media_uploads_raw_name">Raw Name</label> 
<input data-type="text" type="text" name="raw_name" id="edit_media_uploads_raw_name" class="form-control edit_media_uploads_raw_name media_uploads-input  table-media_uploads edit-table-media_uploads text text" placeholder="Raw Name" value=""/>
</div>
<div class="form-group">
<label for="edit_media_uploads_orig_name">Original Name</label> 
<input data-type="text" type="text" name="orig_name" id="edit_media_uploads_orig_name" class="form-control edit_media_uploads_orig_name media_uploads-input  table-media_uploads edit-table-media_uploads text text" placeholder="Original Name" value=""/>
</div>
<div class="form-group">
<label for="edit_media_uploads_client_name">Client Name</label> 
<input data-type="text" type="text" name="client_name" id="edit_media_uploads_client_name" class="form-control edit_media_uploads_client_name media_uploads-input  table-media_uploads edit-table-media_uploads text text" placeholder="Client Name" value=""/>
</div>
<div class="form-group">
<label for="edit_media_uploads_file_ext">File Extension</label> 
<input data-type="text" type="text" name="file_ext" id="edit_media_uploads_file_ext" class="form-control edit_media_uploads_file_ext media_uploads-input  table-media_uploads edit-table-media_uploads text text" placeholder="File Extension" value=""/>
</div>
<div class="form-group">
<label for="edit_media_uploads_file_size">File Size</label> 
<input data-type="text" type="text" name="file_size" id="edit_media_uploads_file_size" class="form-control edit_media_uploads_file_size media_uploads-input  table-media_uploads edit-table-media_uploads text text" placeholder="File Size" value=""/>
</div>
<div class="form-group"><strong>Is Image</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="is_image" id="edit_media_uploads_is_image" class="edit_media_uploads_is_image media_uploads-input  table-media_uploads edit-table-media_uploads checkbox text" placeholder="Is Image" value="1" />Yes</label></div></div>
<div class="form-group">
<label for="edit_media_uploads_image_width">Image Width</label> 
<input data-type="text" type="text" name="image_width" id="edit_media_uploads_image_width" class="form-control edit_media_uploads_image_width media_uploads-input  table-media_uploads edit-table-media_uploads text text" placeholder="Image Width" value=""/>
</div>
<div class="form-group">
<label for="edit_media_uploads_image_height">Image Height</label> 
<input data-type="text" type="text" name="image_height" id="edit_media_uploads_image_height" class="form-control edit_media_uploads_image_height media_uploads-input  table-media_uploads edit-table-media_uploads text text" placeholder="Image Height" value=""/>
</div>
<div class="form-group">
<label for="edit_media_uploads_image_type">Image Type</label> 
<input data-type="text" type="text" name="image_type" id="edit_media_uploads_image_type" class="form-control edit_media_uploads_image_type media_uploads-input  table-media_uploads edit-table-media_uploads text text" placeholder="Image Type" value=""/>
</div>
<div class="form-group">
<label for="edit_media_uploads_image_size_str">Image Size String</label> 
<input data-type="text" type="text" name="image_size_str" id="edit_media_uploads_image_size_str" class="form-control edit_media_uploads_image_size_str media_uploads-input  table-media_uploads edit-table-media_uploads text text" placeholder="Image Size String" value=""/>
</div>
</div> <!-- .panel-body -->
<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="update-action-media_uploads">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm update-back-media_uploads" id="update-back-media_uploads">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-media_uploads -->
</div><!-- .tab-content .tab-content-media_uploads --></div>
<?php } ?>
	
	<script>
jQuery(document).ready(function($) {
	$(document).AdminActions({
		baseURL : '<?php echo base_url(); ?>',
		current_table : 'media_uploads',
		tables : { 
		<?php if( isset($admin_access->controller_media_uploads) ) { ?>
		
'media_uploads' : { label : 'Media File',
fields : ["media_id","file_name","file_type","file_path","full_path","raw_name","orig_name","client_name","file_ext","file_size","is_image","image_width","image_height","image_type","image_size_str"],
add_fields : ["media_id"],
edit_fields : ["media_id","file_name","file_type","file_path","full_path","raw_name","orig_name","client_name","file_ext","file_size","is_image","image_width","image_height","image_type","image_size_str"],
list_limit : 20,
list_fields : ["media_id","file_name","file_type"],
order_by : 'media_id',
order_sort : 'DESC',
primary_key : 'media_id',
primary_title : 'file_name',
actions_edit : <?php echo ($admin_access->controller_media_uploads->can_edit) ? 1 : 0; ?>,
actions_delete : <?php echo ($admin_access->controller_media_uploads->can_delete) ? 1 : 0; ?>,
upload_settings : {
				allowed_types : 'jpg,jpeg,gif,png',
				max_size : '100000', 
				upload_url : 'http://dev2.acemymath.com/uploads/',
				is_image : 'is_image',
				file_name : 'file_name',
			} },

		<?php } ?>
		 },
		filters_data : {"is_image": {"1": "Yes"}},
	});
});
</script>

</div></div><!-- .row . col-lg-12 -->
<?php $this->load->view('common_footer'); ?>