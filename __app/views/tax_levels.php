<?php $this->load->view('common_header'); ?>
<div class="row"><div class="col-lg-12">
	<h1 class="page-header"><?php echo $page_title; ?></h1>
	
	<div id="list-view-tax_levels" class="list-view">
<div class="panel panel-default panel-tax_levels">
<div class="panel-heading">

			<?php if( isset($admin_access->controller_tax_levels->can_add) && ($admin_access->controller_tax_levels->can_add == 1) ) { ?>
			<a href="javascript:void(0);" class="btn btn-default btn-sm pull-right" id="add-button-tax_levels">Add Grade Level</a>
			<?php } ?>
<div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">
<div class="table-responsive">
<table id="dataTables" class="table table-striped table-bordered table-hover">
<thead>
<tr>
<th width='1%'></th>
<th width="50px">ID</th><th width="">Name<span  data-key="level_name" data-table="tax_levels" id="list_search_button_level_name" class="btn btn-primary btn-xs pull-right btn-search list-search-tax_levels" title="Search Name">
		<i class="fa fa-search"></i></span></th><th width="">Description<span  data-key="level_description" data-table="tax_levels" id="list_search_button_level_description" class="btn btn-primary btn-xs pull-right btn-search list-search-tax_levels" title="Search Description">
		<i class="fa fa-search"></i></span></th><th width="">Order</th><th width="130">Actions</th>
</tr>
</thead>
<tbody>
</tbody>
</table>
</div>
</div> <!-- .panel-body -->
</div> <!-- .panel .panel-tax_levels -->
</div>
		<?php if( isset($admin_access->controller_tax_levels->can_add) && ($admin_access->controller_tax_levels->can_add == 1) ) { ?>
		<div id="add-view-tax_levels" style="display:none">
<div class="panel panel-default add-panel-tax_levels">
                        <div class="panel-heading"><h3 class="panel-title">Add Grade Level</h3><div class="clearfix"></div>
                        </div>
                        <!-- /.panel-heading -->
<div class="panel-body">

<div class="form-group">
<label for="add_tax_levels_level_name">Name</label> 
<input data-type="text" type="text" name="level_name" id="add_tax_levels_level_name" class="form-control add_tax_levels_level_name tax_levels-input  table-tax_levels add-table-tax_levels text text" placeholder="Name" value=""/>
</div>
<div class="form-group">
<label for="add_tax_levels_level_description">Description</label>
<textarea rows="5" data-type="textarea" data-wysiwyg="0" type="text" name="level_description" id="add_tax_levels_level_description" class="form-control add_tax_levels_level_description tax_levels-input  table-tax_levels add-table-tax_levels textarea text" placeholder="Description" value="" /></textarea>
</div>
<div class="form-group">
<label for="add_tax_levels_level_order">Order</label> 
<input data-type="text" type="text" name="level_order" id="add_tax_levels_level_order" class="form-control add_tax_levels_level_order tax_levels-input  table-tax_levels add-table-tax_levels text text" placeholder="Order" value="0"/>
</div>
<div class="form-group"><strong>Active</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="level_active" id="add_tax_levels_level_active" class="add_tax_levels_level_active tax_levels-input  table-tax_levels add-table-tax_levels checkbox text" placeholder="Active" value="1" />Active</label></div></div>
</div> <!-- .panel-body -->

<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="add returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="add-action-tax_levels">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm add-back-button" id="add-back-tax_levels">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-tax_levels -->
</div>
<?php } ?><?php if( isset($admin_access->controller_tax_levels->can_edit) && ($admin_access->controller_tax_levels->can_edit == 1) ) { ?>
		<div id="edit-view-tax_levels" style="display:none">
		<ul class="nav nav-tabs">
<li>
		<a href="javascript:void(0);" class="update-back-tax_levels" id="update-back-tax_levels">
		<span class="glyphicon glyphicon-arrow-left"></span>
		</a>
		</li><li class="parent-tab active">
		<a class="tab-item" href="javascript:void(0);" data-table="tax_levels" data-child="0">Edit Grade Level</a>
		</li>
			<?php if( isset($admin_access->controller_level_chapters) ) { ?>
			<li>
			<a class="tab-item" href="javascript:void(0);"  data-table="level_chapters" data-child="1">Chapter Titles</a>
			</li>
			<?php } ?>
</ul><br>
		<div class="tab-content tab-content-tax_levels parent active"><div class="panel panel-default edit-panel-tax_levels">
<div class="panel-heading">
	 <h3 class="panel-title">Edit Grade Level</h3>
	 <div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="level_id" id="edit_tax_levels_level_id" class="edit_tax_levels_level_id tax_levels-input  table-tax_levels edit-table-tax_levels hidden text" placeholder="ID" value="" />
<div class="form-group">
<label for="edit_tax_levels_level_name">Name</label> 
<input data-type="text" type="text" name="level_name" id="edit_tax_levels_level_name" class="form-control edit_tax_levels_level_name tax_levels-input  table-tax_levels edit-table-tax_levels text text" placeholder="Name" value=""/>
</div>
<div class="form-group">
<label for="edit_tax_levels_level_slug">Slug</label> 
<input data-type="text" type="text" name="level_slug" id="edit_tax_levels_level_slug" class="form-control edit_tax_levels_level_slug tax_levels-input  table-tax_levels edit-table-tax_levels text text" placeholder="Slug" value=""/>
</div>
<div class="form-group">
<label for="edit_tax_levels_level_description">Description</label>
<textarea rows="5" data-type="textarea" data-wysiwyg="0" type="text" name="level_description" id="edit_tax_levels_level_description" class="form-control edit_tax_levels_level_description tax_levels-input  table-tax_levels edit-table-tax_levels textarea text" placeholder="Description" value="" /></textarea>
</div>
<div class="form-group">
<label for="edit_tax_levels_level_order">Order</label> 
<input data-type="text" type="text" name="level_order" id="edit_tax_levels_level_order" class="form-control edit_tax_levels_level_order tax_levels-input  table-tax_levels edit-table-tax_levels text text" placeholder="Order" value="0"/>
</div>
<div class="form-group"><strong>Active</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="level_active" id="edit_tax_levels_level_active" class="edit_tax_levels_level_active tax_levels-input  table-tax_levels edit-table-tax_levels checkbox text" placeholder="Active" value="1" />Active</label></div></div>
</div> <!-- .panel-body -->
<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="update-action-tax_levels">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm update-back-tax_levels" id="update-back-tax_levels">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-tax_levels -->
</div><!-- .tab-content .tab-content-tax_levels -->
			<?php if( isset($admin_access->controller_level_chapters) ) { ?>
			<div class="tab-content tab-content-level_chapters" style="display:none"><div id="list-view-level_chapters" class="list-view">
<div class="panel panel-default panel-level_chapters">
<div class="panel-heading">

			<?php if( isset($admin_access->controller_level_chapters->can_add) && ($admin_access->controller_level_chapters->can_add == 1) ) { ?>
			<a href="javascript:void(0);" class="btn btn-default btn-sm pull-right" id="add-button-level_chapters">Add Title</a>
			<?php } ?>
<div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">
<div class="table-responsive">
<table id="dataTables" class="table table-striped table-bordered table-hover">
<thead>
<tr>

<th width=""><div class="dropdown-filter"><a href="javascript:void(0);" data-filter="chapter_id" data-table="level_chapters">Chapter <span class="glyphicon glyphicon-chevron-down pull-right"></span></a></div></th><th width="">Chapter Title<span  data-key="chapter_title" data-table="level_chapters" id="list_search_button_chapter_title" class="btn btn-primary btn-xs pull-right btn-search list-search-level_chapters" title="Search Chapter Title">
		<i class="fa fa-search"></i></span></th><th width="130">Actions</th>
</tr>
</thead>
<tbody>
</tbody>
</table>
</div>
</div> <!-- .panel-body -->
</div> <!-- .panel .panel-level_chapters -->
</div>
		<?php if( isset($admin_access->controller_level_chapters->can_add) && ($admin_access->controller_level_chapters->can_add == 1) ) { ?>
		<div id="add-view-level_chapters" style="display:none">
<div class="panel panel-default add-panel-level_chapters">
                        <div class="panel-heading"><h3 class="panel-title">Add Title</h3><div class="clearfix"></div>
                        </div>
                        <!-- /.panel-heading -->
<div class="panel-body">

<div class="form-group">
<label for="add_lessons_chapter_id">Chapter</label> 
			<select name="chapter_id" id="add_level_chapters_chapter_id" class="selectpicker form-control add_level_chapters_chapter_id level_chapters-input  table-level_chapters add-table-level_chapters dropdown text dropdown-table" placeholder="Chapter" data-live-search="true"  data-type="dropdown" data-label="Chapter" data-field="chapter_id" data-table="tax_chapters" data-key="chapter_id" data-value="chapter_name" data-filter="0" data-filter-key="" data-filter-value="" data-order="1" data-order-by="chapter_name" data-order-sort="ASC">
			<option value="">- - Select Chapter - -</option>
</select></div>
<input data-type="hidden" type="hidden" name="level_id" id="add_level_chapters_level_id" class="add_level_chapters_level_id level_chapters-input  table-level_chapters add-table-level_chapters hidden text" placeholder="Level" value="" />
<div class="form-group">
<label for="add_level_chapters_chapter_title">Chapter Title</label> 
<input data-type="text" type="text" name="chapter_title" id="add_level_chapters_chapter_title" class="form-control add_level_chapters_chapter_title level_chapters-input  table-level_chapters add-table-level_chapters text text" placeholder="Chapter Title" value=""/>
</div>
</div> <!-- .panel-body -->

<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="add returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="add-action-level_chapters">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm add-back-button" id="add-back-level_chapters">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-level_chapters -->
</div>
<?php } ?><?php if( isset($admin_access->controller_level_chapters->can_edit) && ($admin_access->controller_level_chapters->can_edit == 1) ) { ?>
		<div id="edit-view-level_chapters" style="display:none">
		
		<div class="panel panel-default edit-panel-level_chapters">
<div class="panel-heading">
	 <h3 class="panel-title">Edit Title</h3>
	 <div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">

<div class="form-group">
<label for="add_lessons_chapter_id">Chapter</label> 
			<select name="chapter_id" id="edit_level_chapters_chapter_id" class="selectpicker form-control edit_level_chapters_chapter_id level_chapters-input  table-level_chapters edit-table-level_chapters dropdown text dropdown-table" placeholder="Chapter" data-live-search="true"  data-type="dropdown" data-label="Chapter" data-field="chapter_id" data-table="tax_chapters" data-key="chapter_id" data-value="chapter_name" data-filter="0" data-filter-key="" data-filter-value="" data-order="1" data-order-by="chapter_name" data-order-sort="ASC">
			<option value="">- - Select Chapter - -</option>
</select></div>
<input data-type="hidden" type="hidden" name="level_id" id="edit_level_chapters_level_id" class="edit_level_chapters_level_id level_chapters-input  table-level_chapters edit-table-level_chapters hidden text" placeholder="Level" value="" />
<div class="form-group">
<label for="edit_level_chapters_chapter_title">Chapter Title</label> 
<input data-type="text" type="text" name="chapter_title" id="edit_level_chapters_chapter_title" class="form-control edit_level_chapters_chapter_title level_chapters-input  table-level_chapters edit-table-level_chapters text text" placeholder="Chapter Title" value=""/>
</div>
</div> <!-- .panel-body -->
<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="update-action-level_chapters">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm update-back-level_chapters" id="update-back-level_chapters">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-level_chapters -->
</div>
<?php } ?></div><!-- .tab-content .tab-content-level_chapters -->
			<?php } ?></div>
<?php } ?>
	
	<script>
jQuery(document).ready(function($) {
	$(document).AdminActions({
		baseURL : '<?php echo base_url(); ?>',
		current_table : 'tax_levels',
		tables : { 
		<?php if( isset($admin_access->controller_tax_levels) ) { ?>
		
'tax_levels' : { label : 'Grade Level',
fields : ["level_id","level_name","level_slug","level_description","level_order","level_active"],
add_fields : ["level_name","level_description","level_order","level_active"],
edit_fields : ["level_id","level_name","level_slug","level_description","level_order","level_active"],
list_limit : 20,
list_fields : ["level_id","level_name","level_description","level_order"],
order_by : 'level_order',
order_sort : 'DESC',
primary_key : 'level_id',
primary_title : 'level_name',
active_key : 'level_active',
actions_edit : <?php echo ($admin_access->controller_tax_levels->can_edit) ? 1 : 0; ?>,
actions_delete : <?php echo ($admin_access->controller_tax_levels->can_delete) ? 1 : 0; ?> },

		<?php } ?>
		
		<?php if( isset($admin_access->controller_level_chapters) ) { ?>
		
'level_chapters' : { label : 'Level Chapter',
fields : ["level_chapter_id","chapter_id","level_id","chapter_title"],
add_fields : ["chapter_id","level_id","chapter_title"],
edit_fields : ["chapter_id","level_id","chapter_title"],
list_limit : 20,
list_fields : ["chapter_id","chapter_title"],
order_by : 'chapter_id',
order_sort : 'ASC',
filters : {"chapter_id":{"type":"table","anchor":0,"table":"tax_chapters","key":"chapter_id","value":"chapter_name", "filter" : 0, "filter_key" : "", "filter_value" : "", "order" : 1, "order_by" : "chapter_name", "order_sort" : "ASC" },"level_id":{"type":"table","anchor":0,"table":"tax_levels","key":"level_id","value":"level_name", "filter" : 0, "filter_key" : "", "filter_value" : "", "order" : 1, "order_by" : "level_name", "order_sort" : "ASC" }},
primary_key : 'level_chapter_id',
primary_title : 'chapter_title',
actual_values : {"chapter_id" : "chapter_name","level_id" : "level_name"},
required_key : 'level_id',
required_value : 'level_id',
actions_edit : <?php echo ($admin_access->controller_level_chapters->can_edit) ? 1 : 0; ?>,
actions_delete : <?php echo ($admin_access->controller_level_chapters->can_delete) ? 1 : 0; ?> },

		<?php } ?>
		 },
		filters_data : {"level_active": {"1": "Active"}},
	});
});
</script>

</div></div><!-- .row . col-lg-12 -->
<?php $this->load->view('common_footer'); ?>