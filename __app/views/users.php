<?php $this->load->view('common_header'); ?>
<div class="row"><div class="col-lg-12">
	<h1 class="page-header"><?php echo $page_title; ?></h1>
	
	<div id="list-view-users" class="list-view">
<div class="panel panel-default panel-users">
<div class="panel-heading">

			<?php if( isset($admin_access->controller_users->can_add) && ($admin_access->controller_users->can_add == 1) ) { ?>
			<a href="javascript:void(0);" class="btn btn-default btn-sm pull-right" id="add-button-users">Add User</a>
			<?php } ?>
<div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">
<div class="table-responsive">
<table id="dataTables" class="table table-striped table-bordered table-hover">
<thead>
<tr>
<th width='1%'></th>
<th width="50px">ID</th><th width="">Firstname<span  data-key="user_firstname" data-table="users" id="list_search_button_user_firstname" class="btn btn-primary btn-xs pull-right btn-search list-search-users" title="Search Firstname">
		<i class="fa fa-search"></i></span></th><th width="">Username<span  data-key="user_username" data-table="users" id="list_search_button_user_username" class="btn btn-primary btn-xs pull-right btn-search list-search-users" title="Search Username">
		<i class="fa fa-search"></i></span></th><th width=""><div class="dropdown-filter"><a href="javascript:void(0);" data-filter="user_type" data-table="users">User Type <span class="glyphicon glyphicon-chevron-down pull-right"></span></a></div></th><th width=""><div class="dropdown-filter"><a href="javascript:void(0);" data-filter="user_plan" data-table="users">Membership Plan <span class="glyphicon glyphicon-chevron-down pull-right"></span></a></div></th><th width="130">Actions</th>
</tr>
</thead>
<tbody>
</tbody>
</table>
</div>
</div> <!-- .panel-body -->
</div> <!-- .panel .panel-users -->
</div>
		<?php if( isset($admin_access->controller_users->can_add) && ($admin_access->controller_users->can_add == 1) ) { ?>
		<div id="add-view-users" style="display:none">
<div class="panel panel-default add-panel-users">
                        <div class="panel-heading"><h3 class="panel-title">Add User</h3><div class="clearfix"></div>
                        </div>
                        <!-- /.panel-heading -->
<div class="panel-body">

<div class="form-group">
<label for="add_users_user_firstname">Firstname</label> 
<input data-type="text" type="text" name="user_firstname" id="add_users_user_firstname" class="form-control add_users_user_firstname users-input  table-users add-table-users text text" placeholder="Firstname" value=""/>
</div>
<div class="form-group">
<label for="add_users_user_lastname">Lastname</label> 
<input data-type="text" type="text" name="user_lastname" id="add_users_user_lastname" class="form-control add_users_user_lastname users-input  table-users add-table-users text text" placeholder="Lastname" value=""/>
</div>
<div class="form-group">
<label for="add_users_user_email">Email</label> 
<input data-type="text" type="text" name="user_email" id="add_users_user_email" class="form-control add_users_user_email users-input  table-users add-table-users text text" placeholder="Email" value=""/>
</div>
<div class="form-group">
<label for="add_users_user_username">Username</label> 
<input data-type="text" type="text" name="user_username" id="add_users_user_username" class="form-control add_users_user_username users-input  table-users add-table-users text text" placeholder="Username" value=""/>
</div>
<div class="form-group">
<label for="add_users_user_password">Password</label>

<input data-type="password" type="password" name="user_password" id="add_users_user_password" class="form-control add_users_user_password users-input  table-users add-table-users password text" placeholder="Password" value="" />
</div>
<div class="form-group">
<label for="add_users_confirm_password">Confirm Password</label>

<input data-type="password" type="password" name="confirm_password" id="add_users_confirm_password" class="form-control add_users_confirm_password users-input  table-users add-table-users password text" placeholder="Confirm Password" value="" />
</div>
<div class="form-group">
<label for="add_lessons_user_type">User Type</label> 
			<select name="user_type" id="add_users_user_type" class="selectpicker form-control add_users_user_type users-input  table-users add-table-users dropdown text" placeholder="User Type" data-live-search="true" >
			<option value="">- - Select User Type - -</option>
<option value="parent">Parent</option>
<option value="student">Student</option>
</select></div>
<div class="form-group">
<label for="add_lessons_user_level">Grade Level</label> 
			<select name="user_level" id="add_users_user_level" class="selectpicker form-control add_users_user_level users-input  table-users add-table-users dropdown text dropdown-table" placeholder="Grade Level" data-live-search="true"  data-type="dropdown" data-label="Grade Level" data-field="user_level" data-table="tax_levels" data-key="level_id" data-value="level_name" data-filter="0" data-filter-key="" data-filter-value="" data-order="1" data-order-by="level_name" data-order-sort="ASC">
			<option value="">- - Select Grade Level - -</option>
</select></div>
<div class="form-group">
<label for="add_lessons_user_plan">Membership Plan</label> 
			<select name="user_plan" id="add_users_user_plan" class="selectpicker form-control add_users_user_plan users-input  table-users add-table-users dropdown text dropdown-table" placeholder="Membership Plan" data-live-search="true"  data-type="dropdown" data-label="Membership Plan" data-field="user_plan" data-table="subscription_plans" data-key="s_plan_id" data-value="s_plan_name" data-filter="0" data-filter-key="" data-filter-value="" data-order="0" data-order-by="" data-order-sort="">
			<option value="">- - Select Membership Plan - -</option>
</select></div>
<div class="form-group">
<label for="add_users_user_expiry">Expiry</label> 
<input data-type="text" type="text" name="user_expiry" id="add_users_user_expiry" class="form-control add_users_user_expiry users-input  table-users add-table-users text text datetimepicker" placeholder="Expiry" value=""/>
</div>
<div class="form-group"><strong>Active</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="user_active" id="add_users_user_active" class="add_users_user_active users-input  table-users add-table-users checkbox text" placeholder="Active" value="1" />Active</label></div></div>
</div> <!-- .panel-body -->

<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="add returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="add-action-users">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm add-back-button" id="add-back-users">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-users -->
</div>
<?php } ?><?php if( isset($admin_access->controller_users->can_edit) && ($admin_access->controller_users->can_edit == 1) ) { ?>
		<div id="edit-view-users" style="display:none">
		<ul class="nav nav-tabs">
<li>
		<a href="javascript:void(0);" class="update-back-users" id="update-back-users">
		<span class="glyphicon glyphicon-arrow-left"></span>
		</a>
		</li><li class="parent-tab active">
		<a class="tab-item" href="javascript:void(0);" data-table="users" data-child="0">Edit User</a>
		</li>
			<?php if( isset($admin_access->controller_users_profiles) ) { ?>
			<li>
			<a class="tab-item" href="javascript:void(0);"  data-table="users_profiles" data-child="1">Profile Attributes</a>
			</li>
			<?php } ?>
			<?php if( isset($admin_access->controller_users_sponsors) ) { ?>
			<li>
			<a class="tab-item" href="javascript:void(0);"  data-table="users_sponsors" data-child="1">Sponsors</a>
			</li>
			<?php } ?>
			<?php if( isset($admin_access->controller_users_logs) ) { ?>
			<li>
			<a class="tab-item" href="javascript:void(0);"  data-table="users_logs" data-child="1">Activities</a>
			</li>
			<?php } ?>
			<?php if( isset($admin_access->controller_users_meta) ) { ?>
			<li>
			<a class="tab-item" href="javascript:void(0);"  data-table="users_meta" data-child="1">Meta</a>
			</li>
			<?php } ?>
			<?php if( isset($admin_access->controller_users_goals) ) { ?>
			<li>
			<a class="tab-item" href="javascript:void(0);"  data-table="users_goals" data-child="1">User Goals</a>
			</li>
			<?php } ?>
</ul><br>
		<div class="tab-content tab-content-users parent active"><div class="panel panel-default edit-panel-users">
<div class="panel-heading">
	 <h3 class="panel-title">Edit User</h3>
	 <div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="user_id" id="edit_users_user_id" class="edit_users_user_id users-input  table-users edit-table-users hidden text" placeholder="ID" value="" />
<div class="form-group">
<label for="edit_users_user_firstname">Firstname</label> 
<input data-type="text" type="text" name="user_firstname" id="edit_users_user_firstname" class="form-control edit_users_user_firstname users-input  table-users edit-table-users text text" placeholder="Firstname" value=""/>
</div>
<div class="form-group">
<label for="edit_users_user_lastname">Lastname</label> 
<input data-type="text" type="text" name="user_lastname" id="edit_users_user_lastname" class="form-control edit_users_user_lastname users-input  table-users edit-table-users text text" placeholder="Lastname" value=""/>
</div>
<div class="form-group change-deliberately button users user_email">
<label>Email</label>
			<a href="javascript:void(0);" class="btn btn-xs btn-success pull-right update users user_email" data-table="users" data-field="user_email">Update Email</a>
			<p class="text-value users user_email edit_users_user_email users-input  table-users edit-table-users text " data-type="text"></p>
			
<input data-type="text" type="hidden" name="user_email" id="edit_users_user_email" class="edit_users_user_email users-input  table-users edit-table-users text " placeholder="Email" value="" /></div>
			<div class="panel panel-info change-deliberately form users user_email" style="display:none;">
			<div class="panel-heading">
			<a href="javascript:void(0);" class="btn btn-xs btn-danger pull-right cancel users user_email" data-table="users" data-field="user_email">Cancel</a>
			Update Email</div>
			<div class="panel-body">
			
<div class="form-group">
<label for="edit_users_user_email">Email</label> 
<input data-type="text" type="text" name="user_email" id="edit_users_user_email" class="form-control edit_users_user_email users-input  table-users edit-table-users text " placeholder="Email" value=""/>
</div></div>
</div>

<div class="form-group change-deliberately button users user_username">
<label>Username</label>
			<a href="javascript:void(0);" class="btn btn-xs btn-success pull-right update users user_username" data-table="users" data-field="user_username">Update Username</a>
			<p class="text-value users user_username edit_users_user_username users-input  table-users edit-table-users text " data-type="text"></p>
			
<input data-type="text" type="hidden" name="user_username" id="edit_users_user_username" class="edit_users_user_username users-input  table-users edit-table-users text " placeholder="Username" value="" /></div>
			<div class="panel panel-info change-deliberately form users user_username" style="display:none;">
			<div class="panel-heading">
			<a href="javascript:void(0);" class="btn btn-xs btn-danger pull-right cancel users user_username" data-table="users" data-field="user_username">Cancel</a>
			Update Username</div>
			<div class="panel-body">
			
<div class="form-group">
<label for="edit_users_user_username">Username</label> 
<input data-type="text" type="text" name="user_username" id="edit_users_user_username" class="form-control edit_users_user_username users-input  table-users edit-table-users text " placeholder="Username" value=""/>
</div></div>
</div>

<div class="form-group change-deliberately button users user_password">
<label>Password</label>
			<a href="javascript:void(0);" class="btn btn-xs btn-success pull-right update users user_password" data-table="users" data-field="user_password|confirm_password">Update Password</a>
			<p class="text-value users user_password edit_users_user_password users-input  table-users edit-table-users password " data-type="password"></p>
			</div>
			<div class="panel panel-info change-deliberately form users user_password" style="display:none;">
			<div class="panel-heading">
			<a href="javascript:void(0);" class="btn btn-xs btn-danger pull-right cancel users user_password" data-table="users" data-field="user_password">Cancel</a>
			Update Password</div>
			<div class="panel-body">
			
<div class="form-group">
<label for="edit_users_user_password">Password</label>

<input data-type="password" type="password" name="user_password" id="edit_users_user_password" class="form-control edit_users_user_password users-input  table-users edit-table-users password " placeholder="Password" value="" />
</div>
<div class="form-group">
<label for="edit_users_confirm_password">Confirm Password</label>

<input data-type="password" type="password" name="confirm_password" id="edit_users_confirm_password" class="form-control edit_users_confirm_password users-input  table-users edit-table-users password " placeholder="Confirm Password" value="" />
</div></div>
</div>

<div class="form-group">
<label for="add_lessons_user_type">User Type</label> 
			<select name="user_type" id="edit_users_user_type" class="selectpicker form-control edit_users_user_type users-input  table-users edit-table-users dropdown text" placeholder="User Type" data-live-search="true" >
			<option value="">- - Select User Type - -</option>
<option value="parent">Parent</option>
<option value="student">Student</option>
</select></div>
<div class="form-group">
<label for="add_lessons_user_level">Grade Level</label> 
			<select name="user_level" id="edit_users_user_level" class="selectpicker form-control edit_users_user_level users-input  table-users edit-table-users dropdown text dropdown-table" placeholder="Grade Level" data-live-search="true"  data-type="dropdown" data-label="Grade Level" data-field="user_level" data-table="tax_levels" data-key="level_id" data-value="level_name" data-filter="0" data-filter-key="" data-filter-value="" data-order="1" data-order-by="level_name" data-order-sort="ASC">
			<option value="">- - Select Grade Level - -</option>
</select></div>
<div class="form-group">
<label for="add_lessons_user_plan">Membership Plan</label> 
			<select name="user_plan" id="edit_users_user_plan" class="selectpicker form-control edit_users_user_plan users-input  table-users edit-table-users dropdown text dropdown-table" placeholder="Membership Plan" data-live-search="true"  data-type="dropdown" data-label="Membership Plan" data-field="user_plan" data-table="subscription_plans" data-key="s_plan_id" data-value="s_plan_name" data-filter="0" data-filter-key="" data-filter-value="" data-order="0" data-order-by="" data-order-sort="">
			<option value="">- - Select Membership Plan - -</option>
</select></div>
<div class="form-group">
<label for="edit_users_user_created">Created</label> 
<p class="text-value users user_created" data-type="view_only"></p>
</div>
<div class="form-group change-deliberately button users user_expiry">
<label>Expiry</label>
			<a href="javascript:void(0);" class="btn btn-xs btn-success pull-right update users user_expiry" data-table="users" data-field="user_expiry">Update Expiry</a>
			<p class="text-value users user_expiry edit_users_user_expiry users-input  table-users edit-table-users text " data-type="text"></p>
			</div>
			<div class="panel panel-info change-deliberately form users user_expiry" style="display:none;">
			<div class="panel-heading">
			<a href="javascript:void(0);" class="btn btn-xs btn-danger pull-right cancel users user_expiry" data-table="users" data-field="user_expiry">Cancel</a>
			Update Expiry</div>
			<div class="panel-body">
			
<div class="form-group">
<label for="edit_users_user_expiry">Expiry</label> 
<input data-type="text" type="text" name="user_expiry" id="edit_users_user_expiry" class="form-control edit_users_user_expiry users-input  table-users edit-table-users text  datetimepicker" placeholder="Expiry" value=""/>
</div></div>
</div>

<div class="form-group"><strong>Active</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="user_active" id="edit_users_user_active" class="edit_users_user_active users-input  table-users edit-table-users checkbox text" placeholder="Active" value="1" />Active</label></div></div>
<div class="form-group">
<label for="edit_users_last_login">Last Login Date</label> 
<p class="text-value users last_login" data-type="view_only"></p>
</div>
<div class="form-group">
<label for="edit_users_last_login_ip">Last Login IP</label> 
<p class="text-value users last_login_ip" data-type="view_only"></p>
</div>
</div> <!-- .panel-body -->
<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="update-action-users">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm update-back-users" id="update-back-users">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-users -->
</div><!-- .tab-content .tab-content-users -->
			<?php if( isset($admin_access->controller_users_profiles) ) { ?>
			<div class="tab-content tab-content-users_profiles" style="display:none"><div id="list-view-users_profiles" class="list-view">
<div class="panel panel-default panel-users_profiles">
<div class="panel-heading">

			<?php if( isset($admin_access->controller_users_profiles->can_add) && ($admin_access->controller_users_profiles->can_add == 1) ) { ?>
			<a href="javascript:void(0);" class="btn btn-default btn-sm pull-right" id="add-button-users_profiles">Add Attribute</a>
			<?php } ?>
<div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">
<div class="table-responsive">
<table id="dataTables" class="table table-striped table-bordered table-hover">
<thead>
<tr>

<th width="">Profile Attribute<span data-linked='ci_attributes' data-key="attr_name_c" data-table="users_profiles" id="list_search_button_attr_name_c" class="btn btn-primary btn-xs pull-right btn-search list-search-users_profiles" title="Search Profile Attribute">
		<i class="fa fa-search"></i></span></th><th width="">Value<span  data-key="profile_value" data-table="users_profiles" id="list_search_button_profile_value" class="btn btn-primary btn-xs pull-right btn-search list-search-users_profiles" title="Search Value">
		<i class="fa fa-search"></i></span></th><th width="130">Actions</th>
</tr>
</thead>
<tbody>
</tbody>
</table>
</div>
</div> <!-- .panel-body -->
</div> <!-- .panel .panel-users_profiles -->
</div>
		<?php if( isset($admin_access->controller_users_profiles->can_add) && ($admin_access->controller_users_profiles->can_add == 1) ) { ?>
		<div id="add-view-users_profiles" style="display:none">
<div class="panel panel-default add-panel-users_profiles">
                        <div class="panel-heading"><h3 class="panel-title">Add Attribute</h3><div class="clearfix"></div>
                        </div>
                        <!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="user_id" id="add_users_profiles_user_id" class="add_users_profiles_user_id users_profiles-input  table-users_profiles add-table-users_profiles hidden text" placeholder="User" value="" />
<div class="form-group">
<label for="add_lessons_attr_id">Profile Attribute</label> 
			<select name="attr_id" id="add_users_profiles_attr_id" class="selectpicker form-control add_users_profiles_attr_id users_profiles-input  table-users_profiles add-table-users_profiles dropdown text dropdown-table" placeholder="Profile Attribute" data-live-search="true"  data-type="dropdown" data-label="Profile Attribute" data-field="attr_id" data-table="attributes" data-key="attr_id" data-value="attr_name" data-filter="1" data-filter-key="attr_controller" data-filter-value="users_profiles" data-order="1" data-order-by="attr_name" data-order-sort="ASC">
			<option value="">- - Select Profile Attribute - -</option>
</select></div>
<div class="form-group">
<label for="add_users_profiles_profile_value">Value</label>
<textarea rows="5" data-type="textarea" data-wysiwyg="0" type="text" name="profile_value" id="add_users_profiles_profile_value" class="form-control add_users_profiles_profile_value users_profiles-input  table-users_profiles add-table-users_profiles textarea text" placeholder="Value" value="" /></textarea>
</div>
</div> <!-- .panel-body -->

<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="add returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="add-action-users_profiles">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm add-back-button" id="add-back-users_profiles">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-users_profiles -->
</div>
<?php } ?><?php if( isset($admin_access->controller_users_profiles->can_edit) && ($admin_access->controller_users_profiles->can_edit == 1) ) { ?>
		<div id="edit-view-users_profiles" style="display:none">
		
		<div class="panel panel-default edit-panel-users_profiles">
<div class="panel-heading">
	 <h3 class="panel-title">Edit Attribute</h3>
	 <div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="profile_id" id="edit_users_profiles_profile_id" class="edit_users_profiles_profile_id users_profiles-input  table-users_profiles edit-table-users_profiles hidden text" placeholder="Profile ID" value="" />
<input data-type="hidden" type="hidden" name="user_id" id="edit_users_profiles_user_id" class="edit_users_profiles_user_id users_profiles-input  table-users_profiles edit-table-users_profiles hidden text" placeholder="User" value="" />
<div class="form-group">
<label for="add_lessons_attr_id">Profile Attribute</label> 
			<select name="attr_id" id="edit_users_profiles_attr_id" class="selectpicker form-control edit_users_profiles_attr_id users_profiles-input  table-users_profiles edit-table-users_profiles dropdown text dropdown-table" placeholder="Profile Attribute" data-live-search="true"  data-type="dropdown" data-label="Profile Attribute" data-field="attr_id" data-table="attributes" data-key="attr_id" data-value="attr_name" data-filter="1" data-filter-key="attr_controller" data-filter-value="users_profiles" data-order="1" data-order-by="attr_name" data-order-sort="ASC">
			<option value="">- - Select Profile Attribute - -</option>
</select></div>
<div class="form-group">
<label for="edit_users_profiles_profile_value">Value</label>
<textarea rows="5" data-type="textarea" data-wysiwyg="0" type="text" name="profile_value" id="edit_users_profiles_profile_value" class="form-control edit_users_profiles_profile_value users_profiles-input  table-users_profiles edit-table-users_profiles textarea text" placeholder="Value" value="" /></textarea>
</div>
</div> <!-- .panel-body -->
<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="update-action-users_profiles">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm update-back-users_profiles" id="update-back-users_profiles">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-users_profiles -->
</div>
<?php } ?></div><!-- .tab-content .tab-content-users_profiles -->
			<?php } ?>
			<?php if( isset($admin_access->controller_users_sponsors) ) { ?>
			<div class="tab-content tab-content-users_sponsors" style="display:none"><div id="list-view-users_sponsors" class="list-view">
<div class="panel panel-default panel-users_sponsors">
<div class="panel-heading">

			<?php if( isset($admin_access->controller_users_sponsors->can_add) && ($admin_access->controller_users_sponsors->can_add == 1) ) { ?>
			<a href="javascript:void(0);" class="btn btn-default btn-sm pull-right" id="add-button-users_sponsors">Add Sponsor</a>
			<?php } ?>
<div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">
<div class="table-responsive">
<table id="dataTables" class="table table-striped table-bordered table-hover">
<thead>
<tr>
<th width='1%'></th>
<th width="">Sponsor<span data-linked='ci_users' data-key="user_username_s" data-table="users_sponsors" id="list_search_button_user_username_s" class="btn btn-primary btn-xs pull-right btn-search list-search-users_sponsors" title="Search Sponsor">
		<i class="fa fa-search"></i></span></th><th width="None">Date Sponsored</th><th width="130">Actions</th>
</tr>
</thead>
<tbody>
</tbody>
</table>
</div>
</div> <!-- .panel-body -->
</div> <!-- .panel .panel-users_sponsors -->
</div>
		<?php if( isset($admin_access->controller_users_sponsors->can_add) && ($admin_access->controller_users_sponsors->can_add == 1) ) { ?>
		<div id="add-view-users_sponsors" style="display:none">
<div class="panel panel-default add-panel-users_sponsors">
                        <div class="panel-heading"><h3 class="panel-title">Add Sponsor</h3><div class="clearfix"></div>
                        </div>
                        <!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="user_id" id="add_users_sponsors_user_id" class="add_users_sponsors_user_id users_sponsors-input  table-users_sponsors add-table-users_sponsors hidden text" placeholder="User ID" value="" />
<div class="form-group">
<label for="add_users_sponsors_sponsor_id">Sponsor</label> 
<input data-type="text" type="hidden" name="sponsor_id" id="add_users_sponsors_sponsor_id" class="form-control add_users_sponsors_sponsor_id users_sponsors-input  table-users_sponsors add-table-users_sponsors text text text-searchable-key-sponsor_id  add text-searchable-key" />
<a href="javascript:void(0)" data-field="sponsor_id"  data-table="users" data-key="user_id" data-value="user_username" data-display="user_username_s" data-action="add"  class="text-searchable-list sponsor_id" data-toggle="modal" data-target="#add-text-searchable-box-sponsor_id"><span class="glyphicon glyphicon-list"></span></a>
				<input data-type="text" type="text" name="sponsor_id" class="form-control add text-searchable sponsor_id" placeholder="Search Sponsor" data-field="sponsor_id"  data-table="users" data-key="user_id" data-value="user_username" data-display="user_username_s" data-action="add" />
				<div class="modal fade add" id="add-text-searchable-box-sponsor_id" tabindex="-1" role="dialog" aria-labelledby="Sponsor" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Sponsor List</h4>
      </div>
      <div class="modal-body">
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
				
</div>
<div class="form-group"><strong>Active</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="active" id="add_users_sponsors_active" class="add_users_sponsors_active users_sponsors-input  table-users_sponsors add-table-users_sponsors checkbox text" placeholder="Active" value="1" />Active</label></div></div>
</div> <!-- .panel-body -->

<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="add returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="add-action-users_sponsors">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm add-back-button" id="add-back-users_sponsors">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-users_sponsors -->
</div>
<?php } ?><?php if( isset($admin_access->controller_users_sponsors->can_edit) && ($admin_access->controller_users_sponsors->can_edit == 1) ) { ?>
		<div id="edit-view-users_sponsors" style="display:none">
		
		<div class="panel panel-default edit-panel-users_sponsors">
<div class="panel-heading">
	 <h3 class="panel-title">Edit Sponsor</h3>
	 <div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="sponsorship_id" id="edit_users_sponsors_sponsorship_id" class="edit_users_sponsors_sponsorship_id users_sponsors-input  table-users_sponsors edit-table-users_sponsors hidden text" placeholder="Sponsorship ID" value="" />
<input data-type="hidden" type="hidden" name="user_id" id="edit_users_sponsors_user_id" class="edit_users_sponsors_user_id users_sponsors-input  table-users_sponsors edit-table-users_sponsors hidden text" placeholder="User ID" value="" />
<div class="form-group">
<label for="edit_users_sponsors_sponsor_id">Sponsor</label> 
<input data-type="text" type="hidden" name="sponsor_id" id="edit_users_sponsors_sponsor_id" class="form-control edit_users_sponsors_sponsor_id users_sponsors-input  table-users_sponsors edit-table-users_sponsors text  text-searchable-key-sponsor_id  edit text-searchable-key" />
<a href="javascript:void(0)" data-field="sponsor_id"  data-table="users" data-key="user_id" data-value="user_username" data-display="user_username_s" data-action="edit"  class="text-searchable-list sponsor_id" data-toggle="modal" data-target="#edit-text-searchable-box-sponsor_id"><span class="glyphicon glyphicon-list"></span></a>
				<input data-type="text" type="text" name="sponsor_id" class="form-control edit text-searchable sponsor_id" placeholder="Search Sponsor" data-field="sponsor_id"  data-table="users" data-key="user_id" data-value="user_username" data-display="user_username_s" data-action="edit" />
				<div class="modal fade edit" id="edit-text-searchable-box-sponsor_id" tabindex="-1" role="dialog" aria-labelledby="Sponsor" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Sponsor List</h4>
      </div>
      <div class="modal-body">
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
				
</div>
<div class="form-group"><strong>Active</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="active" id="edit_users_sponsors_active" class="edit_users_sponsors_active users_sponsors-input  table-users_sponsors edit-table-users_sponsors checkbox text" placeholder="Active" value="1" />Active</label></div></div>
</div> <!-- .panel-body -->
<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="update-action-users_sponsors">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm update-back-users_sponsors" id="update-back-users_sponsors">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-users_sponsors -->
</div>
<?php } ?></div><!-- .tab-content .tab-content-users_sponsors -->
			<?php } ?>
			<?php if( isset($admin_access->controller_users_logs) ) { ?>
			<div class="tab-content tab-content-users_logs" style="display:none"><div id="list-view-users_logs" class="list-view">
<div class="panel panel-default panel-users_logs">
<div class="panel-heading">
&nbsp;
<div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">
<div class="table-responsive">
<table id="dataTables" class="table table-striped table-bordered table-hover">
<thead>
<tr>

<th width="50px">ID</th><th width="200px">Date<span  data-key="log_date" data-table="users_logs" id="list_search_button_log_date" class="btn btn-primary btn-xs pull-right btn-search list-search-users_logs" title="Search Date">
		<i class="fa fa-search"></i></span></th><th width=""><div class="dropdown-filter"><a href="javascript:void(0);" data-filter="log_code" data-table="users_logs">Code <span class="glyphicon glyphicon-chevron-down pull-right"></span></a></div></th><th width="None">Content</th>
</tr>
</thead>
<tbody>
</tbody>
</table>
</div>
</div> <!-- .panel-body -->
</div> <!-- .panel .panel-users_logs -->
</div>
		<?php if( isset($admin_access->controller_users_logs->can_add) && ($admin_access->controller_users_logs->can_add == 1) ) { ?>
		<div id="add-view-users_logs" style="display:none">
<div class="panel panel-default add-panel-users_logs">
                        <div class="panel-heading"><h3 class="panel-title">Add Activity</h3><div class="clearfix"></div>
                        </div>
                        <!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="user_id" id="add_users_logs_user_id" class="add_users_logs_user_id users_logs-input  table-users_logs add-table-users_logs hidden text" placeholder="User" value="" />
<div class="form-group">
<label for="add_lessons_log_code">Code</label> 
			<select name="log_code" id="add_users_logs_log_code" class="selectpicker form-control add_users_logs_log_code users_logs-input  table-users_logs add-table-users_logs dropdown text" placeholder="Code" data-live-search="true" >
			<option value="">- - Select Code - -</option>
<option value="ACCTUPD">Account Update</option>
<option value="PWDCHG">Password Change</option>
<option value="STUADD">Student Add</option>
<option value="STUPWD">Student Password Change</option>
<option value="STUUPD">Student Update</option>
<option value="STUUPG">Student Upgrade</option>
<option value="LOGIN">User Login</option>
<option value="LOGOUT">User Logout</option>
</select></div>
<div class="form-group">
<label for="add_users_logs_log_msg">Content</label> 
<input data-type="text" type="text" name="log_msg" id="add_users_logs_log_msg" class="form-control add_users_logs_log_msg users_logs-input  table-users_logs add-table-users_logs text text" placeholder="Content" value=""/>
</div>
</div> <!-- .panel-body -->

<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="add returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="add-action-users_logs">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm add-back-button" id="add-back-users_logs">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-users_logs -->
</div>
<?php } ?><?php if( isset($admin_access->controller_users_logs->can_edit) && ($admin_access->controller_users_logs->can_edit == 1) ) { ?>
		<div id="edit-view-users_logs" style="display:none">
		
		<div class="panel panel-default edit-panel-users_logs">
<div class="panel-heading">
	 <h3 class="panel-title">Edit Activity</h3>
	 <div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="log_id" id="edit_users_logs_log_id" class="edit_users_logs_log_id users_logs-input  table-users_logs edit-table-users_logs hidden text" placeholder="ID" value="" />
<input data-type="hidden" type="hidden" name="user_id" id="edit_users_logs_user_id" class="edit_users_logs_user_id users_logs-input  table-users_logs edit-table-users_logs hidden text" placeholder="User" value="" />
<div class="form-group">
<label for="edit_users_logs_log_date">Date</label> 
<input data-type="text" type="text" name="log_date" id="edit_users_logs_log_date" class="form-control edit_users_logs_log_date users_logs-input  table-users_logs edit-table-users_logs text text datetimepicker" placeholder="Date" value=""/>
</div>
<div class="form-group">
<label for="add_lessons_log_code">Code</label> 
			<select name="log_code" id="edit_users_logs_log_code" class="selectpicker form-control edit_users_logs_log_code users_logs-input  table-users_logs edit-table-users_logs dropdown text" placeholder="Code" data-live-search="true" >
			<option value="">- - Select Code - -</option>
<option value="ACCTUPD">Account Update</option>
<option value="PWDCHG">Password Change</option>
<option value="STUADD">Student Add</option>
<option value="STUPWD">Student Password Change</option>
<option value="STUUPD">Student Update</option>
<option value="STUUPG">Student Upgrade</option>
<option value="LOGIN">User Login</option>
<option value="LOGOUT">User Logout</option>
</select></div>
<div class="form-group">
<label for="edit_users_logs_log_msg">Content</label> 
<input data-type="text" type="text" name="log_msg" id="edit_users_logs_log_msg" class="form-control edit_users_logs_log_msg users_logs-input  table-users_logs edit-table-users_logs text text" placeholder="Content" value=""/>
</div>
</div> <!-- .panel-body -->
<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="update-action-users_logs">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm update-back-users_logs" id="update-back-users_logs">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-users_logs -->
</div>
<?php } ?></div><!-- .tab-content .tab-content-users_logs -->
			<?php } ?>
			<?php if( isset($admin_access->controller_users_meta) ) { ?>
			<div class="tab-content tab-content-users_meta" style="display:none"><div id="list-view-users_meta" class="list-view">
<div class="panel panel-default panel-users_meta">
<div class="panel-heading">

			<?php if( isset($admin_access->controller_users_meta->can_add) && ($admin_access->controller_users_meta->can_add == 1) ) { ?>
			<a href="javascript:void(0);" class="btn btn-default btn-sm pull-right" id="add-button-users_meta">Add Meta</a>
			<?php } ?>
<div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">
<div class="table-responsive">
<table id="dataTables" class="table table-striped table-bordered table-hover">
<thead>
<tr>

<th width="">Meta Key<span  data-key="meta_key" data-table="users_meta" id="list_search_button_meta_key" class="btn btn-primary btn-xs pull-right btn-search list-search-users_meta" title="Search Meta Key">
		<i class="fa fa-search"></i></span></th><th width="">Meta Value<span  data-key="meta_value" data-table="users_meta" id="list_search_button_meta_value" class="btn btn-primary btn-xs pull-right btn-search list-search-users_meta" title="Search Meta Value">
		<i class="fa fa-search"></i></span></th><th width="130">Actions</th>
</tr>
</thead>
<tbody>
</tbody>
</table>
</div>
</div> <!-- .panel-body -->
</div> <!-- .panel .panel-users_meta -->
</div>
		<?php if( isset($admin_access->controller_users_meta->can_add) && ($admin_access->controller_users_meta->can_add == 1) ) { ?>
		<div id="add-view-users_meta" style="display:none">
<div class="panel panel-default add-panel-users_meta">
                        <div class="panel-heading"><h3 class="panel-title">Add Meta</h3><div class="clearfix"></div>
                        </div>
                        <!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="user_id" id="add_users_meta_user_id" class="add_users_meta_user_id users_meta-input  table-users_meta add-table-users_meta hidden text" placeholder="User" value="" />
<div class="form-group">
<label for="add_users_meta_meta_key">Meta Key</label> 
<input data-type="text" type="text" name="meta_key" id="add_users_meta_meta_key" class="form-control add_users_meta_meta_key users_meta-input  table-users_meta add-table-users_meta text text" placeholder="Meta Key" value=""/>
</div>
<div class="form-group">
<label for="add_users_meta_meta_value">Meta Value</label>
<textarea rows="5" data-type="textarea" data-wysiwyg="0" type="text" name="meta_value" id="add_users_meta_meta_value" class="form-control add_users_meta_meta_value users_meta-input  table-users_meta add-table-users_meta textarea text" placeholder="Meta Value" value="" /></textarea>
</div>
</div> <!-- .panel-body -->

<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="add returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="add-action-users_meta">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm add-back-button" id="add-back-users_meta">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-users_meta -->
</div>
<?php } ?><?php if( isset($admin_access->controller_users_meta->can_edit) && ($admin_access->controller_users_meta->can_edit == 1) ) { ?>
		<div id="edit-view-users_meta" style="display:none">
		
		<div class="panel panel-default edit-panel-users_meta">
<div class="panel-heading">
	 <h3 class="panel-title">Edit Meta</h3>
	 <div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="umeta_id" id="edit_users_meta_umeta_id" class="edit_users_meta_umeta_id users_meta-input  table-users_meta edit-table-users_meta hidden text" placeholder="Meta ID" value="" />
<div class="form-group change-deliberately button users_meta user_id">
<label>User</label>
			<a href="javascript:void(0);" class="btn btn-xs btn-success pull-right update users_meta user_id" data-table="users_meta" data-field="user_id">Update User</a>
			<p class="text-value users_meta user_id edit_users_meta_user_id users_meta-input  table-users_meta edit-table-users_meta hidden " data-type="hidden"></p>
			</div>
			<div class="panel panel-info change-deliberately form users_meta user_id" style="display:none;">
			<div class="panel-heading">
			<a href="javascript:void(0);" class="btn btn-xs btn-danger pull-right cancel users_meta user_id" data-table="users_meta" data-field="user_id">Cancel</a>
			Update User</div>
			<div class="panel-body">
			
<input data-type="hidden" type="hidden" name="user_id" id="edit_users_meta_user_id" class="edit_users_meta_user_id users_meta-input  table-users_meta edit-table-users_meta hidden " placeholder="User" value="" /></div>
</div>

<div class="form-group change-deliberately button users_meta meta_key">
<label>Meta Key</label>
			<a href="javascript:void(0);" class="btn btn-xs btn-success pull-right update users_meta meta_key" data-table="users_meta" data-field="meta_key">Update Meta Key</a>
			<p class="text-value users_meta meta_key edit_users_meta_meta_key users_meta-input  table-users_meta edit-table-users_meta text " data-type="text"></p>
			</div>
			<div class="panel panel-info change-deliberately form users_meta meta_key" style="display:none;">
			<div class="panel-heading">
			<a href="javascript:void(0);" class="btn btn-xs btn-danger pull-right cancel users_meta meta_key" data-table="users_meta" data-field="meta_key">Cancel</a>
			Update Meta Key</div>
			<div class="panel-body">
			
<div class="form-group">
<label for="edit_users_meta_meta_key">Meta Key</label> 
<input data-type="text" type="text" name="meta_key" id="edit_users_meta_meta_key" class="form-control edit_users_meta_meta_key users_meta-input  table-users_meta edit-table-users_meta text " placeholder="Meta Key" value=""/>
</div></div>
</div>

<div class="form-group">
<label for="edit_users_meta_meta_value">Meta Value</label>
<textarea rows="5" data-type="textarea" data-wysiwyg="0" type="text" name="meta_value" id="edit_users_meta_meta_value" class="form-control edit_users_meta_meta_value users_meta-input  table-users_meta edit-table-users_meta textarea text" placeholder="Meta Value" value="" /></textarea>
</div>
</div> <!-- .panel-body -->
<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="update-action-users_meta">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm update-back-users_meta" id="update-back-users_meta">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-users_meta -->
</div>
<?php } ?></div><!-- .tab-content .tab-content-users_meta -->
			<?php } ?>
			<?php if( isset($admin_access->controller_users_goals) ) { ?>
			<div class="tab-content tab-content-users_goals" style="display:none"><div id="list-view-users_goals" class="list-view">
<div class="panel panel-default panel-users_goals">
<div class="panel-heading">

			<?php if( isset($admin_access->controller_users_goals->can_add) && ($admin_access->controller_users_goals->can_add == 1) ) { ?>
			<a href="javascript:void(0);" class="btn btn-default btn-sm pull-right" id="add-button-users_goals">Add Goal</a>
			<?php } ?>
<div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">
<div class="table-responsive">
<table id="dataTables" class="table table-striped table-bordered table-hover">
<thead>
<tr>

<th width="None">Goal</th><th width="130">Actions</th>
</tr>
</thead>
<tbody>
</tbody>
</table>
</div>
</div> <!-- .panel-body -->
</div> <!-- .panel .panel-users_goals -->
</div>
		<?php if( isset($admin_access->controller_users_goals->can_add) && ($admin_access->controller_users_goals->can_add == 1) ) { ?>
		<div id="add-view-users_goals" style="display:none">
<div class="panel panel-default add-panel-users_goals">
                        <div class="panel-heading"><h3 class="panel-title">Add Goal</h3><div class="clearfix"></div>
                        </div>
                        <!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="user_id" id="add_users_goals_user_id" class="add_users_goals_user_id users_goals-input  table-users_goals add-table-users_goals hidden text" placeholder="User" value="" />
<div class="form-group">
<label for="add_users_goals_goal_id">Goal</label> 
<input data-type="text" type="hidden" name="goal_id" id="add_users_goals_goal_id" class="form-control add_users_goals_goal_id users_goals-input  table-users_goals add-table-users_goals text text text-searchable-key-goal_id  add text-searchable-key" />
<a href="javascript:void(0)" data-field="goal_id"  data-table="goals" data-key="goal_id" data-value="goal_name" data-display="goal_name" data-action="add"  class="text-searchable-list goal_id" data-toggle="modal" data-target="#add-text-searchable-box-goal_id"><span class="glyphicon glyphicon-list"></span></a>
				<input data-type="text" type="text" name="goal_id" class="form-control add text-searchable goal_id" placeholder="Search Goal" data-field="goal_id"  data-table="goals" data-key="goal_id" data-value="goal_name" data-display="goal_name" data-action="add" />
				<div class="modal fade add" id="add-text-searchable-box-goal_id" tabindex="-1" role="dialog" aria-labelledby="Goal" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Goal List</h4>
      </div>
      <div class="modal-body">
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
				
</div>
</div> <!-- .panel-body -->

<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="add returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="add-action-users_goals">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm add-back-button" id="add-back-users_goals">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-users_goals -->
</div>
<?php } ?><?php if( isset($admin_access->controller_users_goals->can_edit) && ($admin_access->controller_users_goals->can_edit == 1) ) { ?>
		<div id="edit-view-users_goals" style="display:none">
		
		<div class="panel panel-default edit-panel-users_goals">
<div class="panel-heading">
	 <h3 class="panel-title">Edit Goal</h3>
	 <div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="ug_id" id="edit_users_goals_ug_id" class="edit_users_goals_ug_id users_goals-input  table-users_goals edit-table-users_goals hidden text" placeholder="ID" value="" />
<input data-type="hidden" type="hidden" name="user_id" id="edit_users_goals_user_id" class="edit_users_goals_user_id users_goals-input  table-users_goals edit-table-users_goals hidden text" placeholder="User" value="" />
<div class="form-group">
<label for="edit_users_goals_goal_id">Goal</label> 
<input data-type="text" type="hidden" name="goal_id" id="edit_users_goals_goal_id" class="form-control edit_users_goals_goal_id users_goals-input  table-users_goals edit-table-users_goals text text text-searchable-key-goal_id  edit text-searchable-key" />
<a href="javascript:void(0)" data-field="goal_id"  data-table="goals" data-key="goal_id" data-value="goal_name" data-display="goal_name" data-action="edit"  class="text-searchable-list goal_id" data-toggle="modal" data-target="#edit-text-searchable-box-goal_id"><span class="glyphicon glyphicon-list"></span></a>
				<input data-type="text" type="text" name="goal_id" class="form-control edit text-searchable goal_id" placeholder="Search Goal" data-field="goal_id"  data-table="goals" data-key="goal_id" data-value="goal_name" data-display="goal_name" data-action="edit" />
				<div class="modal fade edit" id="edit-text-searchable-box-goal_id" tabindex="-1" role="dialog" aria-labelledby="Goal" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Goal List</h4>
      </div>
      <div class="modal-body">
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
				
</div>
</div> <!-- .panel-body -->
<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="update-action-users_goals">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm update-back-users_goals" id="update-back-users_goals">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-users_goals -->
</div>
<?php } ?></div><!-- .tab-content .tab-content-users_goals -->
			<?php } ?></div>
<?php } ?>
	
	<script>
jQuery(document).ready(function($) {
	$(document).AdminActions({
		baseURL : '<?php echo base_url(); ?>',
		current_table : 'users',
		tables : { 
		<?php if( isset($admin_access->controller_users) ) { ?>
		
'users' : { label : 'User',
fields : ["user_id","user_firstname","user_lastname","user_email","user_username","user_password","confirm_password","user_type","user_level","user_plan","user_created","user_expiry","user_active","last_login","last_login_ip"],
add_fields : ["user_firstname","user_lastname","user_email","user_username","user_password","confirm_password","user_type","user_level","user_plan","user_expiry","user_active"],
edit_fields : ["user_id","user_firstname","user_lastname","user_email","user_username","user_type","user_level","user_plan","user_active"],
list_limit : 20,
list_fields : ["user_id","user_firstname","user_username","user_type","user_plan"],
order_by : 'user_id',
order_sort : 'DESC',
filters : {"user_type":{"type":"manual","anchor":0},"user_level":{"type":"table","anchor":0,"table":"tax_levels","key":"level_id","value":"level_name", "filter" : 0, "filter_key" : "", "filter_value" : "", "order" : 1, "order_by" : "level_name", "order_sort" : "ASC" },"user_plan":{"type":"table","anchor":0,"table":"subscription_plans","key":"s_plan_id","value":"s_plan_name", "filter" : 0, "filter_key" : "", "filter_value" : "", "order" : 0, "order_by" : "", "order_sort" : "" }},
primary_key : 'user_id',
primary_title : 'user_firstname',
active_key : 'user_active',
actual_values : {"user_plan" : "s_plan_name_s"},
actions_edit : <?php echo ($admin_access->controller_users->can_edit) ? 1 : 0; ?>,
actions_delete : <?php echo ($admin_access->controller_users->can_delete) ? 1 : 0; ?> },

		<?php } ?>
		
		<?php if( isset($admin_access->controller_users_profiles) ) { ?>
		
'users_profiles' : { label : 'User Profile',
fields : ["profile_id","user_id","attr_id","profile_value"],
add_fields : ["user_id","attr_id","profile_value"],
edit_fields : ["profile_id","user_id","attr_id","profile_value"],
list_limit : 20,
list_fields : ["attr_name_c","profile_value"],
order_by : 'attr_id',
order_sort : 'ASC',
filters : {"attr_id":{"type":"table","anchor":0,"table":"attributes","key":"attr_id","value":"attr_name", "filter" : 1, "filter_key" : "attr_controller", "filter_value" : "users_profiles", "order" : 1, "order_by" : "attr_name", "order_sort" : "ASC" }},
primary_key : 'profile_id',
actual_values : {"user_id" : "user_username_c","attr_id" : "attr_name_c"},
required_key : 'user_id',
required_value : 'user_id',
required_table : '',
actions_edit : <?php echo ($admin_access->controller_users_profiles->can_edit) ? 1 : 0; ?>,
actions_delete : <?php echo ($admin_access->controller_users_profiles->can_delete) ? 1 : 0; ?> },

		<?php } ?>
		
		<?php if( isset($admin_access->controller_users_sponsors) ) { ?>
		
'users_sponsors' : { label : 'Users Sponsor',
fields : ["sponsorship_id","user_id","sponsor_id","date_sponsored","active"],
add_fields : ["user_id","sponsor_id","active"],
edit_fields : ["sponsorship_id","user_id","sponsor_id","active"],
list_limit : 20,
list_fields : ["user_username_s","date_sponsored"],
order_by : 'date_sponsored',
order_sort : 'DESC',
primary_key : 'sponsorship_id',
primary_title : 'sponsor_id',
active_key : 'active',
actual_values : {"user_id" : "user_username_c","sponsor_id" : "user_username_s"},
required_key : 'user_id',
required_value : 'user_id',
required_table : 'ci_users_c',
actions_edit : <?php echo ($admin_access->controller_users_sponsors->can_edit) ? 1 : 0; ?>,
actions_delete : <?php echo ($admin_access->controller_users_sponsors->can_delete) ? 1 : 0; ?> },

		<?php } ?>
		
		<?php if( isset($admin_access->controller_users_logs) ) { ?>
		
'users_logs' : { label : 'User Activity',
fields : ["log_id","user_id","log_date","log_code","log_msg"],
add_fields : ["user_id","log_code","log_msg"],
edit_fields : ["log_id","user_id","log_date","log_code","log_msg"],
list_limit : 20,
list_fields : ["log_id","log_date","log_code","log_msg"],
order_by : 'log_date',
order_sort : 'DESC',
filters : {"log_code":{"type":"manual","anchor":0}},
primary_key : 'log_id',
primary_title : 'log_msg',
required_key : 'user_id',
required_value : 'user_id',
required_table : '' },

		<?php } ?>
		
		<?php if( isset($admin_access->controller_users_meta) ) { ?>
		
'users_meta' : { label : 'User Meta',
fields : ["umeta_id","user_id","meta_key","meta_value"],
add_fields : ["user_id","meta_key","meta_value"],
edit_fields : ["umeta_id","meta_value"],
list_limit : 20,
list_fields : ["meta_key","meta_value"],
order_by : 'umeta_id',
order_sort : 'DESC',
primary_key : 'umeta_id',
primary_title : 'meta_key',
required_key : 'user_id',
required_value : 'user_id',
required_table : 'ci_users',
actions_edit : <?php echo ($admin_access->controller_users_meta->can_edit) ? 1 : 0; ?>,
actions_delete : <?php echo ($admin_access->controller_users_meta->can_delete) ? 1 : 0; ?> },

		<?php } ?>
		
		<?php if( isset($admin_access->controller_users_goals) ) { ?>
		
'users_goals' : { label : 'User Goal',
fields : ["ug_id","user_id","goal_id"],
add_fields : ["user_id","goal_id"],
edit_fields : ["ug_id","user_id","goal_id"],
list_limit : 20,
list_fields : ["goal_id"],
order_by : 'goal_id',
order_sort : 'ASC',
primary_key : 'ug_id',
required_key : 'user_id',
required_value : 'user_id',
required_table : 'ci_users',
actions_edit : <?php echo ($admin_access->controller_users_goals->can_edit) ? 1 : 0; ?>,
actions_delete : <?php echo ($admin_access->controller_users_goals->can_delete) ? 1 : 0; ?> },

		<?php } ?>
		 },
		filters_data : {"active": {"1": "Active"}, "user_active": {"1": "Active"}, "log_code": {"ACCTUPD": "Account Update", "STUADD": "Student Add", "STUUPD": "Student Update", "STUUPG": "Student Upgrade", "STUPWD": "Student Password Change", "LOGOUT": "User Logout", "PWDCHG": "Password Change", "LOGIN": "User Login"}, "user_type": {"student": "Student", "parent": "Parent"}},
	});
});
</script>

</div></div><!-- .row . col-lg-12 -->
<?php $this->load->view('common_footer'); ?>