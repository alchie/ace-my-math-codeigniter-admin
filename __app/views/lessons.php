<?php $this->load->view('common_header'); ?>
<div class="row"><div class="col-lg-12">
	<h1 class="page-header"><?php echo $page_title; ?></h1>
	
	<div id="list-view-lessons" class="list-view">
<div class="panel panel-default panel-lessons">
<div class="panel-heading">

			<?php if( isset($admin_access->controller_lessons->can_add) && ($admin_access->controller_lessons->can_add == 1) ) { ?>
			<a href="javascript:void(0);" class="btn btn-default btn-sm pull-right" id="add-button-lessons">Add Lesson</a>
			<?php } ?>
<div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">
<div class="table-responsive">
<table id="dataTables" class="table table-striped table-bordered table-hover">
<thead>
<tr>
<th width='1%'></th>
<th width="50px">ID</th><th width="5%">Number</th><th width="">Title<span  data-key="lesson_title" data-table="lessons" id="list_search_button_lesson_title" class="btn btn-primary btn-xs pull-right btn-search list-search-lessons" title="Search Title">
		<i class="fa fa-search"></i></span></th><th width="150"><div class="dropdown-filter"><a href="javascript:void(0);" data-filter="lesson_level" data-table="lessons">Grade Level <span class="glyphicon glyphicon-chevron-down pull-right"></span></a></div></th><th width="100"><div class="dropdown-filter"><a href="javascript:void(0);" data-filter="lesson_chapter" data-table="lessons">Chapter <span class="glyphicon glyphicon-chevron-down pull-right"></span></a></div></th><th width="130">Actions</th>
</tr>
</thead>
<tbody>
</tbody>
</table>
</div>
</div> <!-- .panel-body -->
</div> <!-- .panel .panel-lessons -->
</div>
		<?php if( isset($admin_access->controller_lessons->can_add) && ($admin_access->controller_lessons->can_add == 1) ) { ?>
		<div id="add-view-lessons" style="display:none">
<div class="panel panel-default add-panel-lessons">
                        <div class="panel-heading"><h3 class="panel-title">Add Lesson</h3><div class="clearfix"></div>
                        </div>
                        <!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="lesson_id" id="add_lessons_lesson_id" class="add_lessons_lesson_id lessons-input  table-lessons add-table-lessons hidden text" placeholder="ID" value="" />
<div class="form-group">
<label for="add_lessons_lesson_number">Number</label> 
<input data-type="text" type="text" name="lesson_number" id="add_lessons_lesson_number" class="form-control add_lessons_lesson_number lessons-input  table-lessons add-table-lessons text text" placeholder="Number" value="1"/>
</div>
<div class="form-group">
<label for="add_lessons_lesson_title">Title</label> 
<input data-type="text" type="text" name="lesson_title" id="add_lessons_lesson_title" class="form-control add_lessons_lesson_title lessons-input  table-lessons add-table-lessons text test1" placeholder="Title" value=""/>
</div>
<div class="form-group">
<label for="add_lessons_lesson_level">Grade Level</label> 
			<select name="lesson_level" id="add_lessons_lesson_level" class="selectpicker form-control add_lessons_lesson_level lessons-input  table-lessons add-table-lessons dropdown text dropdown-table" placeholder="Grade Level" data-live-search="true"  data-type="dropdown" data-label="Grade Level" data-field="lesson_level" data-table="tax_levels" data-key="level_id" data-value="level_name" data-filter="0" data-filter-key="" data-filter-value="" data-order="1" data-order-by="level_name" data-order-sort="ASC">
			<option value="">- - Select Grade Level - -</option>
</select></div>
<div class="form-group">
<label for="add_lessons_lesson_chapter">Chapter</label> 
			<select name="lesson_chapter" id="add_lessons_lesson_chapter" class="selectpicker form-control add_lessons_lesson_chapter lessons-input  table-lessons add-table-lessons dropdown text dropdown-table" placeholder="Chapter" data-live-search="true"  data-type="dropdown" data-label="Chapter" data-field="lesson_chapter" data-table="tax_chapters" data-key="chapter_id" data-value="chapter_name" data-filter="0" data-filter-key="" data-filter-value="" data-order="1" data-order-by="chapter_name" data-order-sort="ASC">
			<option value="">- - Select Chapter - -</option>
</select></div>
<div class="form-group">
<label for="add_lessons_lesson_video_url">Video URL</label> 
<input data-type="text" type="text" name="lesson_video_url" id="add_lessons_lesson_video_url" class="form-control add_lessons_lesson_video_url lessons-input  table-lessons add-table-lessons text text" placeholder="Video URL" value=""/>
</div>
<div class="form-group">
<label for="add_lessons_lesson_video_duration">Video Duration</label> 
<input data-type="text" type="text" name="lesson_video_duration" id="add_lessons_lesson_video_duration" class="form-control add_lessons_lesson_video_duration lessons-input  table-lessons add-table-lessons text text" placeholder="Video Duration" value=""/>
</div>
<div class="form-group">
<label for="add_lessons_lesson_quiz">Quiz JS File</label> 
<input data-type="text" type="text" name="lesson_quiz" id="add_lessons_lesson_quiz" class="form-control add_lessons_lesson_quiz lessons-input  table-lessons add-table-lessons text text" placeholder="Quiz JS File" value=""/>
</div>
<div class="form-group"><strong>Active</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="lesson_active" id="add_lessons_lesson_active" class="add_lessons_lesson_active lessons-input  table-lessons add-table-lessons checkbox text" placeholder="Active" value="1" />Active</label></div></div>
</div> <!-- .panel-body -->

<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="add returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="add-action-lessons">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm add-back-button" id="add-back-lessons">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-lessons -->
</div>
<?php } ?><?php if( isset($admin_access->controller_lessons->can_edit) && ($admin_access->controller_lessons->can_edit == 1) ) { ?>
		<div id="edit-view-lessons" style="display:none">
		<ul class="nav nav-tabs">
<li>
		<a href="javascript:void(0);" class="update-back-lessons" id="update-back-lessons">
		<span class="glyphicon glyphicon-arrow-left"></span>
		</a>
		</li><li class="parent-tab active">
		<a class="tab-item" href="javascript:void(0);" data-table="lessons" data-child="0">Edit Lesson</a>
		</li>
			<?php if( isset($admin_access->controller_lessons_tags) ) { ?>
			<li>
			<a class="tab-item" href="javascript:void(0);"  data-table="lessons_tags" data-child="1">Topics</a>
			</li>
			<?php } ?>
			<?php if( isset($admin_access->controller_lessons_subscriptions) ) { ?>
			<li>
			<a class="tab-item" href="javascript:void(0);"  data-table="lessons_subscriptions" data-child="1">Subscription Plans</a>
			</li>
			<?php } ?>
			<?php if( isset($admin_access->controller_lessons_meta) ) { ?>
			<li>
			<a class="tab-item" href="javascript:void(0);"  data-table="lessons_meta" data-child="1">Meta</a>
			</li>
			<?php } ?>
			<?php if( isset($admin_access->controller_lessons_quiz) ) { ?>
			<li>
			<a class="tab-item" href="javascript:void(0);"  data-table="lessons_quiz" data-child="1">Quizzes</a>
			</li>
			<?php } ?>
</ul><br>
		<div class="tab-content tab-content-lessons parent active"><div class="panel panel-default edit-panel-lessons">
<div class="panel-heading">
	 <h3 class="panel-title">Edit Lesson</h3>
	 <div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="lesson_id" id="edit_lessons_lesson_id" class="edit_lessons_lesson_id lessons-input  table-lessons edit-table-lessons hidden text" placeholder="ID" value="" />
<div class="form-group">
<label for="edit_lessons_lesson_number">Number</label> 
<input data-type="text" type="text" name="lesson_number" id="edit_lessons_lesson_number" class="form-control edit_lessons_lesson_number lessons-input  table-lessons edit-table-lessons text text" placeholder="Number" value="1"/>
</div>
<div class="form-group">
<label for="edit_lessons_lesson_title">Title</label> 
<input data-type="text" type="text" name="lesson_title" id="edit_lessons_lesson_title" class="form-control edit_lessons_lesson_title lessons-input  table-lessons edit-table-lessons text test2" placeholder="Title" value=""/>
</div>
<div class="form-group">
<label for="edit_lessons_lesson_slug">Slug</label> 
<input data-type="text" type="text" name="lesson_slug" id="edit_lessons_lesson_slug" class="form-control edit_lessons_lesson_slug lessons-input  table-lessons edit-table-lessons text text" placeholder="Slug" value=""/>
</div>
<div class="form-group">
<label for="add_lessons_lesson_level">Grade Level</label> 
			<select name="lesson_level" id="edit_lessons_lesson_level" class="selectpicker form-control edit_lessons_lesson_level lessons-input  table-lessons edit-table-lessons dropdown text dropdown-table" placeholder="Grade Level" data-live-search="true"  data-type="dropdown" data-label="Grade Level" data-field="lesson_level" data-table="tax_levels" data-key="level_id" data-value="level_name" data-filter="0" data-filter-key="" data-filter-value="" data-order="1" data-order-by="level_name" data-order-sort="ASC">
			<option value="">- - Select Grade Level - -</option>
</select></div>
<div class="form-group">
<label for="add_lessons_lesson_chapter">Chapter</label> 
			<select name="lesson_chapter" id="edit_lessons_lesson_chapter" class="selectpicker form-control edit_lessons_lesson_chapter lessons-input  table-lessons edit-table-lessons dropdown text dropdown-table" placeholder="Chapter" data-live-search="true"  data-type="dropdown" data-label="Chapter" data-field="lesson_chapter" data-table="tax_chapters" data-key="chapter_id" data-value="chapter_name" data-filter="0" data-filter-key="" data-filter-value="" data-order="1" data-order-by="chapter_name" data-order-sort="ASC">
			<option value="">- - Select Chapter - -</option>
</select></div>
<div class="form-group">
<label for="edit_lessons_lesson_video_url">Video URL</label> 
<input data-type="text" type="text" name="lesson_video_url" id="edit_lessons_lesson_video_url" class="form-control edit_lessons_lesson_video_url lessons-input  table-lessons edit-table-lessons text text" placeholder="Video URL" value=""/>
</div>
<div class="form-group">
<label for="edit_lessons_lesson_video_duration">Video Duration</label> 
<input data-type="text" type="text" name="lesson_video_duration" id="edit_lessons_lesson_video_duration" class="form-control edit_lessons_lesson_video_duration lessons-input  table-lessons edit-table-lessons text text" placeholder="Video Duration" value=""/>
</div>
<div class="form-group">
<label for="edit_lessons_lesson_quiz">Quiz JS File</label> 
<input data-type="text" type="text" name="lesson_quiz" id="edit_lessons_lesson_quiz" class="form-control edit_lessons_lesson_quiz lessons-input  table-lessons edit-table-lessons text text" placeholder="Quiz JS File" value=""/>
</div>
<div class="form-group"><strong>Active</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="lesson_active" id="edit_lessons_lesson_active" class="edit_lessons_lesson_active lessons-input  table-lessons edit-table-lessons checkbox text" placeholder="Active" value="1" />Active</label></div></div>
</div> <!-- .panel-body -->
<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="update-action-lessons">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm update-back-lessons" id="update-back-lessons">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-lessons -->
</div><!-- .tab-content .tab-content-lessons -->
			<?php if( isset($admin_access->controller_lessons_tags) ) { ?>
			<div class="tab-content tab-content-lessons_tags" style="display:none"><div id="list-view-lessons_tags" class="list-view">
<div class="panel panel-default panel-lessons_tags">
<div class="panel-heading">

			<?php if( isset($admin_access->controller_lessons_tags->can_add) && ($admin_access->controller_lessons_tags->can_add == 1) ) { ?>
			<a href="javascript:void(0);" class="btn btn-default btn-sm pull-right" id="add-button-lessons_tags">Add Topic</a>
			<?php } ?>
<div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">
<div class="table-responsive">
<table id="dataTables" class="table table-striped table-bordered table-hover">
<thead>
<tr>
<th width='1%'></th>
<th width="">Topic<span data-linked='ci_tax_tags' data-key="tag_name" data-table="lessons_tags" id="list_search_button_tag_name" class="btn btn-primary btn-xs pull-right btn-search list-search-lessons_tags" title="Search Topic">
		<i class="fa fa-search"></i></span></th><th width="66">Action</th>
</tr>
</thead>
<tbody>
</tbody>
</table>
</div>
</div> <!-- .panel-body -->
</div> <!-- .panel .panel-lessons_tags -->
</div>
		<?php if( isset($admin_access->controller_lessons_tags->can_add) && ($admin_access->controller_lessons_tags->can_add == 1) ) { ?>
		<div id="add-view-lessons_tags" style="display:none">
<div class="panel panel-default add-panel-lessons_tags">
                        <div class="panel-heading"><h3 class="panel-title">Add Topic</h3><div class="clearfix"></div>
                        </div>
                        <!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="lesson_id" id="add_lessons_tags_lesson_id" class="add_lessons_tags_lesson_id lessons_tags-input  table-lessons_tags add-table-lessons_tags hidden text" placeholder="Lesson" value="" />
<div class="form-group">
<label for="add_lessons_tags_tag_id">Topic</label> 
<input data-type="text" type="hidden" name="tag_id" id="add_lessons_tags_tag_id" class="form-control add_lessons_tags_tag_id lessons_tags-input  table-lessons_tags add-table-lessons_tags text text text-searchable-key-tag_id  add text-searchable-key" />
<a href="javascript:void(0)" data-field="tag_id"  data-table="tax_tags" data-key="tag_id" data-value="tag_name" data-display="tag_name" data-action="add"  class="text-searchable-list tag_id" data-toggle="modal" data-target="#add-text-searchable-box-tag_id"><span class="glyphicon glyphicon-list"></span></a>
				<input data-type="text" type="text" name="tag_id" class="form-control add text-searchable tag_id" placeholder="Search Topic" data-field="tag_id"  data-table="tax_tags" data-key="tag_id" data-value="tag_name" data-display="tag_name" data-action="add" />
				<div class="modal fade add" id="add-text-searchable-box-tag_id" tabindex="-1" role="dialog" aria-labelledby="Topic" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Topic List</h4>
      </div>
      <div class="modal-body">
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
				
</div>
<div class="form-group"><strong>Active</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="active" id="add_lessons_tags_active" class="add_lessons_tags_active lessons_tags-input  table-lessons_tags add-table-lessons_tags checkbox text" placeholder="Active" value="1" />Active</label></div></div>
</div> <!-- .panel-body -->

<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="add returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="add-action-lessons_tags">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm add-back-button" id="add-back-lessons_tags">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-lessons_tags -->
</div>
<?php } ?><?php if( isset($admin_access->controller_lessons_tags->can_edit) && ($admin_access->controller_lessons_tags->can_edit == 1) ) { ?>
		<div id="edit-view-lessons_tags" style="display:none">
		
		<div class="panel panel-default edit-panel-lessons_tags">
<div class="panel-heading">
	 <h3 class="panel-title">Edit Topic</h3>
	 <div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="id" id="edit_lessons_tags_id" class="edit_lessons_tags_id lessons_tags-input  table-lessons_tags edit-table-lessons_tags hidden text" placeholder="" value="" />
<input data-type="hidden" type="hidden" name="lesson_id" id="edit_lessons_tags_lesson_id" class="edit_lessons_tags_lesson_id lessons_tags-input  table-lessons_tags edit-table-lessons_tags hidden text" placeholder="Lesson" value="" />
<div class="form-group">
<label for="edit_lessons_tags_tag_id">Topic</label> 
<input data-type="text" type="hidden" name="tag_id" id="edit_lessons_tags_tag_id" class="form-control edit_lessons_tags_tag_id lessons_tags-input  table-lessons_tags edit-table-lessons_tags text text text-searchable-key-tag_id  edit text-searchable-key" />
<a href="javascript:void(0)" data-field="tag_id"  data-table="tax_tags" data-key="tag_id" data-value="tag_name" data-display="tag_name" data-action="edit"  class="text-searchable-list tag_id" data-toggle="modal" data-target="#edit-text-searchable-box-tag_id"><span class="glyphicon glyphicon-list"></span></a>
				<input data-type="text" type="text" name="tag_id" class="form-control edit text-searchable tag_id" placeholder="Search Topic" data-field="tag_id"  data-table="tax_tags" data-key="tag_id" data-value="tag_name" data-display="tag_name" data-action="edit" />
				<div class="modal fade edit" id="edit-text-searchable-box-tag_id" tabindex="-1" role="dialog" aria-labelledby="Topic" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Topic List</h4>
      </div>
      <div class="modal-body">
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
				
</div>
<div class="form-group"><strong>Active</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="active" id="edit_lessons_tags_active" class="edit_lessons_tags_active lessons_tags-input  table-lessons_tags edit-table-lessons_tags checkbox text" placeholder="Active" value="1" />Active</label></div></div>
</div> <!-- .panel-body -->
<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="update-action-lessons_tags">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm update-back-lessons_tags" id="update-back-lessons_tags">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-lessons_tags -->
</div>
<?php } ?></div><!-- .tab-content .tab-content-lessons_tags -->
			<?php } ?>
			<?php if( isset($admin_access->controller_lessons_subscriptions) ) { ?>
			<div class="tab-content tab-content-lessons_subscriptions" style="display:none"><div id="list-view-lessons_subscriptions" class="list-view">
<div class="panel panel-default panel-lessons_subscriptions">
<div class="panel-heading">

			<?php if( isset($admin_access->controller_lessons_subscriptions->can_add) && ($admin_access->controller_lessons_subscriptions->can_add == 1) ) { ?>
			<a href="javascript:void(0);" class="btn btn-default btn-sm pull-right" id="add-button-lessons_subscriptions">Add Subscription Plan</a>
			<?php } ?>
<div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">
<div class="table-responsive">
<table id="dataTables" class="table table-striped table-bordered table-hover">
<thead>
<tr>

<th width=""><div class="dropdown-filter"><a href="javascript:void(0);" data-filter="s_plan_id" data-table="lessons_subscriptions">Subscription Plans <span class="glyphicon glyphicon-chevron-down pull-right"></span></a></div></th><th width="66">Action</th>
</tr>
</thead>
<tbody>
</tbody>
</table>
</div>
</div> <!-- .panel-body -->
</div> <!-- .panel .panel-lessons_subscriptions -->
</div>
		<?php if( isset($admin_access->controller_lessons_subscriptions->can_add) && ($admin_access->controller_lessons_subscriptions->can_add == 1) ) { ?>
		<div id="add-view-lessons_subscriptions" style="display:none">
<div class="panel panel-default add-panel-lessons_subscriptions">
                        <div class="panel-heading"><h3 class="panel-title">Add Subscription Plan</h3><div class="clearfix"></div>
                        </div>
                        <!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="lesson_id" id="add_lessons_subscriptions_lesson_id" class="add_lessons_subscriptions_lesson_id lessons_subscriptions-input  table-lessons_subscriptions add-table-lessons_subscriptions hidden text" placeholder="Lesson" value="" />
<div class="form-group">
<label for="add_lessons_s_plan_id">Subscription Plans</label> 
			<select name="s_plan_id" id="add_lessons_subscriptions_s_plan_id" class="selectpicker form-control add_lessons_subscriptions_s_plan_id lessons_subscriptions-input  table-lessons_subscriptions add-table-lessons_subscriptions dropdown text dropdown-table" placeholder="Subscription Plans" data-live-search="true"  data-type="dropdown" data-label="Subscription Plans" data-field="s_plan_id" data-table="subscription_plans" data-key="s_plan_id" data-value="s_plan_name" data-filter="0" data-filter-key="" data-filter-value="" data-order="1" data-order-by="s_plan_name" data-order-sort="ASC">
			<option value="">- - Select Subscription Plans - -</option>
</select></div>
</div> <!-- .panel-body -->

<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="add returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="add-action-lessons_subscriptions">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm add-back-button" id="add-back-lessons_subscriptions">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-lessons_subscriptions -->
</div>
<?php } ?><?php if( isset($admin_access->controller_lessons_subscriptions->can_edit) && ($admin_access->controller_lessons_subscriptions->can_edit == 1) ) { ?>
		<div id="edit-view-lessons_subscriptions" style="display:none">
		
		<div class="panel panel-default edit-panel-lessons_subscriptions">
<div class="panel-heading">
	 <h3 class="panel-title">Edit Subscription Plan</h3>
	 <div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="lesson_id" id="edit_lessons_subscriptions_lesson_id" class="edit_lessons_subscriptions_lesson_id lessons_subscriptions-input  table-lessons_subscriptions edit-table-lessons_subscriptions hidden text" placeholder="Lesson" value="" />
<input data-type="hidden" type="hidden" name="id" id="edit_lessons_subscriptions_id" class="edit_lessons_subscriptions_id lessons_subscriptions-input  table-lessons_subscriptions edit-table-lessons_subscriptions hidden text" placeholder="ID" value="" />
<div class="form-group">
<label for="add_lessons_s_plan_id">Subscription Plans</label> 
			<select name="s_plan_id" id="edit_lessons_subscriptions_s_plan_id" class="selectpicker form-control edit_lessons_subscriptions_s_plan_id lessons_subscriptions-input  table-lessons_subscriptions edit-table-lessons_subscriptions dropdown text dropdown-table" placeholder="Subscription Plans" data-live-search="true"  data-type="dropdown" data-label="Subscription Plans" data-field="s_plan_id" data-table="subscription_plans" data-key="s_plan_id" data-value="s_plan_name" data-filter="0" data-filter-key="" data-filter-value="" data-order="1" data-order-by="s_plan_name" data-order-sort="ASC">
			<option value="">- - Select Subscription Plans - -</option>
</select></div>
</div> <!-- .panel-body -->
<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="update-action-lessons_subscriptions">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm update-back-lessons_subscriptions" id="update-back-lessons_subscriptions">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-lessons_subscriptions -->
</div>
<?php } ?></div><!-- .tab-content .tab-content-lessons_subscriptions -->
			<?php } ?>
			<?php if( isset($admin_access->controller_lessons_meta) ) { ?>
			<div class="tab-content tab-content-lessons_meta" style="display:none"><div id="list-view-lessons_meta" class="list-view">
<div class="panel panel-default panel-lessons_meta">
<div class="panel-heading">

			<?php if( isset($admin_access->controller_lessons_meta->can_add) && ($admin_access->controller_lessons_meta->can_add == 1) ) { ?>
			<a href="javascript:void(0);" class="btn btn-default btn-sm pull-right" id="add-button-lessons_meta">Add Meta</a>
			<?php } ?>
<div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">
<div class="table-responsive">
<table id="dataTables" class="table table-striped table-bordered table-hover">
<thead>
<tr>

<th width="">Meta Key<span  data-key="meta_key" data-table="lessons_meta" id="list_search_button_meta_key" class="btn btn-primary btn-xs pull-right btn-search list-search-lessons_meta" title="Search Meta Key">
		<i class="fa fa-search"></i></span></th><th width="">Meta Value<span  data-key="meta_value" data-table="lessons_meta" id="list_search_button_meta_value" class="btn btn-primary btn-xs pull-right btn-search list-search-lessons_meta" title="Search Meta Value">
		<i class="fa fa-search"></i></span></th><th width="130">Actions</th>
</tr>
</thead>
<tbody>
</tbody>
</table>
</div>
</div> <!-- .panel-body -->
</div> <!-- .panel .panel-lessons_meta -->
</div>
		<?php if( isset($admin_access->controller_lessons_meta->can_add) && ($admin_access->controller_lessons_meta->can_add == 1) ) { ?>
		<div id="add-view-lessons_meta" style="display:none">
<div class="panel panel-default add-panel-lessons_meta">
                        <div class="panel-heading"><h3 class="panel-title">Add Meta</h3><div class="clearfix"></div>
                        </div>
                        <!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="lesson_id" id="add_lessons_meta_lesson_id" class="add_lessons_meta_lesson_id lessons_meta-input  table-lessons_meta add-table-lessons_meta hidden text" placeholder="Lesson" value="" />
<div class="form-group">
<label for="add_lessons_meta_meta_key">Meta Key</label> 
<input data-type="text" type="text" name="meta_key" id="add_lessons_meta_meta_key" class="form-control add_lessons_meta_meta_key lessons_meta-input  table-lessons_meta add-table-lessons_meta text text" placeholder="Meta Key" value=""/>
</div>
<div class="form-group">
<label for="add_lessons_meta_meta_value">Meta Value</label>
<textarea rows="5" data-type="textarea" data-wysiwyg="0" type="text" name="meta_value" id="add_lessons_meta_meta_value" class="form-control add_lessons_meta_meta_value lessons_meta-input  table-lessons_meta add-table-lessons_meta textarea text" placeholder="Meta Value" value="" /></textarea>
</div>
</div> <!-- .panel-body -->

<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="add returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="add-action-lessons_meta">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm add-back-button" id="add-back-lessons_meta">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-lessons_meta -->
</div>
<?php } ?><?php if( isset($admin_access->controller_lessons_meta->can_edit) && ($admin_access->controller_lessons_meta->can_edit == 1) ) { ?>
		<div id="edit-view-lessons_meta" style="display:none">
		
		<div class="panel panel-default edit-panel-lessons_meta">
<div class="panel-heading">
	 <h3 class="panel-title">Edit Meta</h3>
	 <div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="lmeta_id" id="edit_lessons_meta_lmeta_id" class="edit_lessons_meta_lmeta_id lessons_meta-input  table-lessons_meta edit-table-lessons_meta hidden text" placeholder="Meta ID" value="" />
<div class="form-group change-deliberately button lessons_meta lesson_id">
<label>Lesson</label>
			<a href="javascript:void(0);" class="btn btn-xs btn-success pull-right update lessons_meta lesson_id" data-table="lessons_meta" data-field="lesson_id">Update Lesson</a>
			<p class="text-value lessons_meta lesson_id edit_lessons_meta_lesson_id lessons_meta-input  table-lessons_meta edit-table-lessons_meta hidden " data-type="hidden"></p>
			
<input data-type="hidden" type="hidden" name="lesson_id" id="edit_lessons_meta_lesson_id" class="edit_lessons_meta_lesson_id lessons_meta-input  table-lessons_meta edit-table-lessons_meta hidden " placeholder="Lesson" value="" /></div>
			<div class="panel panel-info change-deliberately form lessons_meta lesson_id" style="display:none;">
			<div class="panel-heading">
			<a href="javascript:void(0);" class="btn btn-xs btn-danger pull-right cancel lessons_meta lesson_id" data-table="lessons_meta" data-field="lesson_id">Cancel</a>
			Update Lesson</div>
			<div class="panel-body">
			
<input data-type="hidden" type="hidden" name="lesson_id" id="edit_lessons_meta_lesson_id" class="edit_lessons_meta_lesson_id lessons_meta-input  table-lessons_meta edit-table-lessons_meta hidden " placeholder="Lesson" value="" /></div>
</div>

<div class="form-group change-deliberately button lessons_meta meta_key">
<label>Meta Key</label>
			<a href="javascript:void(0);" class="btn btn-xs btn-success pull-right update lessons_meta meta_key" data-table="lessons_meta" data-field="meta_key">Update Meta Key</a>
			<p class="text-value lessons_meta meta_key edit_lessons_meta_meta_key lessons_meta-input  table-lessons_meta edit-table-lessons_meta text " data-type="text"></p>
			
<input data-type="text" type="hidden" name="meta_key" id="edit_lessons_meta_meta_key" class="edit_lessons_meta_meta_key lessons_meta-input  table-lessons_meta edit-table-lessons_meta text " placeholder="Meta Key" value="" /></div>
			<div class="panel panel-info change-deliberately form lessons_meta meta_key" style="display:none;">
			<div class="panel-heading">
			<a href="javascript:void(0);" class="btn btn-xs btn-danger pull-right cancel lessons_meta meta_key" data-table="lessons_meta" data-field="meta_key">Cancel</a>
			Update Meta Key</div>
			<div class="panel-body">
			
<div class="form-group">
<label for="edit_lessons_meta_meta_key">Meta Key</label> 
<input data-type="text" type="text" name="meta_key" id="edit_lessons_meta_meta_key" class="form-control edit_lessons_meta_meta_key lessons_meta-input  table-lessons_meta edit-table-lessons_meta text " placeholder="Meta Key" value=""/>
</div></div>
</div>

<div class="form-group">
<label for="edit_lessons_meta_meta_value">Meta Value</label>
<textarea rows="5" data-type="textarea" data-wysiwyg="0" type="text" name="meta_value" id="edit_lessons_meta_meta_value" class="form-control edit_lessons_meta_meta_value lessons_meta-input  table-lessons_meta edit-table-lessons_meta textarea text" placeholder="Meta Value" value="" /></textarea>
</div>
</div> <!-- .panel-body -->
<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="update-action-lessons_meta">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm update-back-lessons_meta" id="update-back-lessons_meta">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-lessons_meta -->
</div>
<?php } ?></div><!-- .tab-content .tab-content-lessons_meta -->
			<?php } ?>
			<?php if( isset($admin_access->controller_lessons_quiz) ) { ?>
			<div class="tab-content tab-content-lessons_quiz" style="display:none"><div id="list-view-lessons_quiz" class="list-view">
<div class="panel panel-default panel-lessons_quiz">
<div class="panel-heading">

			<?php if( isset($admin_access->controller_lessons_quiz->can_add) && ($admin_access->controller_lessons_quiz->can_add == 1) ) { ?>
			<a href="javascript:void(0);" class="btn btn-default btn-sm pull-right" id="add-button-lessons_quiz">Add Quiz</a>
			<?php } ?>
<div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">
<div class="table-responsive">
<table id="dataTables" class="table table-striped table-bordered table-hover">
<thead>
<tr>

<th width="None">Number</th><th width="None">Time</th><th width="">Question<span  data-key="quiz_question" data-table="lessons_quiz" id="list_search_button_quiz_question" class="btn btn-primary btn-xs pull-right btn-search list-search-lessons_quiz" title="Search Question">
		<i class="fa fa-search"></i></span></th><th width="130">Actions</th>
</tr>
</thead>
<tbody>
</tbody>
</table>
</div>
</div> <!-- .panel-body -->
</div> <!-- .panel .panel-lessons_quiz -->
</div>
		<?php if( isset($admin_access->controller_lessons_quiz->can_add) && ($admin_access->controller_lessons_quiz->can_add == 1) ) { ?>
		<div id="add-view-lessons_quiz" style="display:none">
<div class="panel panel-default add-panel-lessons_quiz">
                        <div class="panel-heading"><h3 class="panel-title">Add Quiz</h3><div class="clearfix"></div>
                        </div>
                        <!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="quiz_id" id="add_lessons_quiz_quiz_id" class="add_lessons_quiz_quiz_id lessons_quiz-input  table-lessons_quiz add-table-lessons_quiz hidden text" placeholder="ID" value="" />
<input data-type="hidden" type="hidden" name="lesson_id" id="add_lessons_quiz_lesson_id" class="add_lessons_quiz_lesson_id lessons_quiz-input  table-lessons_quiz add-table-lessons_quiz hidden text" placeholder="Lesson" value="" />
<div class="form-group">
<label for="add_lessons_quiz_quiz_number">Number</label> 
<input data-type="text" type="text" name="quiz_number" id="add_lessons_quiz_quiz_number" class="form-control add_lessons_quiz_quiz_number lessons_quiz-input  table-lessons_quiz add-table-lessons_quiz text text" placeholder="Number" value=""/>
</div>
<div class="form-group">
<label for="add_lessons_quiz_quiz_time">Time</label> 
<input data-type="text" type="text" name="quiz_time" id="add_lessons_quiz_quiz_time" class="form-control add_lessons_quiz_quiz_time lessons_quiz-input  table-lessons_quiz add-table-lessons_quiz text text" placeholder="Time" value=""/>
</div>
<div class="form-group">
<label for="add_lessons_quiz_quiz_question">Question</label> 
<input data-type="text" type="text" name="quiz_question" id="add_lessons_quiz_quiz_question" class="form-control add_lessons_quiz_quiz_question lessons_quiz-input  table-lessons_quiz add-table-lessons_quiz text text" placeholder="Question" value=""/>
</div>
<div class="form-group">
<label for="add_lessons_quiz_type">Type</label> 
			<select name="quiz_type" id="add_lessons_quiz_quiz_type" class="selectpicker form-control add_lessons_quiz_quiz_type lessons_quiz-input  table-lessons_quiz add-table-lessons_quiz dropdown text" placeholder="Type" data-live-search="true" >
			<option value="">- - Select Type - -</option>
<option value="multiple-choice">Multiple Choice</option>
</select></div>
<div class="form-group">
<label for="add_lessons_quiz_quiz_details">Details</label>
<textarea rows="5" data-type="textarea" data-wysiwyg="0" type="text" name="quiz_details" id="add_lessons_quiz_quiz_details" class="form-control add_lessons_quiz_quiz_details lessons_quiz-input  table-lessons_quiz add-table-lessons_quiz textarea text" placeholder="Details" value="" /></textarea>
</div>
<div class="form-group">
<label for="add_lessons_quiz_quiz_correct">Correct Answer</label> 
<input data-type="text" type="text" name="quiz_correct" id="add_lessons_quiz_quiz_correct" class="form-control add_lessons_quiz_quiz_correct lessons_quiz-input  table-lessons_quiz add-table-lessons_quiz text text" placeholder="Correct Answer" value=""/>
</div>
</div> <!-- .panel-body -->

<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="add returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="add-action-lessons_quiz">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm add-back-button" id="add-back-lessons_quiz">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-lessons_quiz -->
</div>
<?php } ?><?php if( isset($admin_access->controller_lessons_quiz->can_edit) && ($admin_access->controller_lessons_quiz->can_edit == 1) ) { ?>
		<div id="edit-view-lessons_quiz" style="display:none">
		
		<div class="panel panel-default edit-panel-lessons_quiz">
<div class="panel-heading">
	 <h3 class="panel-title">Edit Quiz</h3>
	 <div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="quiz_id" id="edit_lessons_quiz_quiz_id" class="edit_lessons_quiz_quiz_id lessons_quiz-input  table-lessons_quiz edit-table-lessons_quiz hidden text" placeholder="ID" value="" />
<input data-type="hidden" type="hidden" name="lesson_id" id="edit_lessons_quiz_lesson_id" class="edit_lessons_quiz_lesson_id lessons_quiz-input  table-lessons_quiz edit-table-lessons_quiz hidden text" placeholder="Lesson" value="" />
<div class="form-group">
<label for="edit_lessons_quiz_quiz_number">Number</label> 
<input data-type="text" type="text" name="quiz_number" id="edit_lessons_quiz_quiz_number" class="form-control edit_lessons_quiz_quiz_number lessons_quiz-input  table-lessons_quiz edit-table-lessons_quiz text text" placeholder="Number" value=""/>
</div>
<div class="form-group">
<label for="edit_lessons_quiz_quiz_time">Time</label> 
<input data-type="text" type="text" name="quiz_time" id="edit_lessons_quiz_quiz_time" class="form-control edit_lessons_quiz_quiz_time lessons_quiz-input  table-lessons_quiz edit-table-lessons_quiz text text" placeholder="Time" value=""/>
</div>
<div class="form-group">
<label for="edit_lessons_quiz_quiz_question">Question</label> 
<input data-type="text" type="text" name="quiz_question" id="edit_lessons_quiz_quiz_question" class="form-control edit_lessons_quiz_quiz_question lessons_quiz-input  table-lessons_quiz edit-table-lessons_quiz text text" placeholder="Question" value=""/>
</div>
<div class="form-group">
<label for="add_lessons_quiz_type">Type</label> 
			<select name="quiz_type" id="edit_lessons_quiz_quiz_type" class="selectpicker form-control edit_lessons_quiz_quiz_type lessons_quiz-input  table-lessons_quiz edit-table-lessons_quiz dropdown text" placeholder="Type" data-live-search="true" >
			<option value="">- - Select Type - -</option>
<option value="multiple-choice">Multiple Choice</option>
</select></div>
<div class="form-group">
<label for="edit_lessons_quiz_quiz_details">Details</label>
<textarea rows="5" data-type="textarea" data-wysiwyg="0" type="text" name="quiz_details" id="edit_lessons_quiz_quiz_details" class="form-control edit_lessons_quiz_quiz_details lessons_quiz-input  table-lessons_quiz edit-table-lessons_quiz textarea text" placeholder="Details" value="" /></textarea>
</div>
<div class="form-group">
<label for="edit_lessons_quiz_quiz_correct">Correct Answer</label> 
<input data-type="text" type="text" name="quiz_correct" id="edit_lessons_quiz_quiz_correct" class="form-control edit_lessons_quiz_quiz_correct lessons_quiz-input  table-lessons_quiz edit-table-lessons_quiz text text" placeholder="Correct Answer" value=""/>
</div>
</div> <!-- .panel-body -->
<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="update-action-lessons_quiz">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm update-back-lessons_quiz" id="update-back-lessons_quiz">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-lessons_quiz -->
</div>
<?php } ?></div><!-- .tab-content .tab-content-lessons_quiz -->
			<?php } ?></div>
<?php } ?>
	
	<script>
jQuery(document).ready(function($) {
	$(document).AdminActions({
		baseURL : '<?php echo base_url(); ?>',
		current_table : 'lessons',
		tables : { 
		<?php if( isset($admin_access->controller_lessons) ) { ?>
		
'lessons' : { label : 'Lesson',
fields : ["lesson_id","lesson_number","lesson_title","lesson_slug","lesson_level","lesson_chapter","lesson_video_url","lesson_video_duration","lesson_quiz","lesson_active"],
add_fields : ["lesson_id","lesson_number","lesson_title","lesson_level","lesson_chapter","lesson_video_url","lesson_video_duration","lesson_quiz","lesson_active"],
edit_fields : ["lesson_id","lesson_number","lesson_title","lesson_slug","lesson_level","lesson_chapter","lesson_video_url","lesson_video_duration","lesson_quiz","lesson_active"],
list_limit : 15,
list_fields : ["lesson_id","lesson_number","lesson_title","lesson_level","lesson_chapter"],
order_by : 'lesson_number',
order_sort : 'ASC',
filters : {"lesson_level":{"type":"table","anchor":0,"table":"tax_levels","key":"level_id","value":"level_name", "filter" : 0, "filter_key" : "", "filter_value" : "", "order" : 1, "order_by" : "level_name", "order_sort" : "ASC" },"lesson_chapter":{"type":"table","anchor":0,"table":"tax_chapters","key":"chapter_id","value":"chapter_name", "filter" : 0, "filter_key" : "", "filter_value" : "", "order" : 1, "order_by" : "chapter_name", "order_sort" : "ASC" }},
primary_key : 'lesson_id',
primary_title : 'lesson_title',
active_key : 'lesson_active',
actual_values : {"lesson_level" : "level_name_c","lesson_chapter" : "chapter_name_c"},
actions_edit : <?php echo ($admin_access->controller_lessons->can_edit) ? 1 : 0; ?>,
actions_delete : <?php echo ($admin_access->controller_lessons->can_delete) ? 1 : 0; ?> },

		<?php } ?>
		
		<?php if( isset($admin_access->controller_lessons_tags) ) { ?>
		
'lessons_tags' : { label : 'Lesson Tag',
fields : ["id","lesson_id","tag_id","active"],
add_fields : ["lesson_id","tag_id","active"],
edit_fields : ["id","lesson_id","tag_id","active"],
list_limit : 20,
list_fields : ["tag_name"],
order_by : 'id',
order_sort : 'DESC',
primary_key : 'id',
active_key : 'active',
actual_values : {"lesson_id" : "lesson_title","tag_id" : "tag_name"},
required_key : 'lesson_id',
required_value : 'lesson_id',
required_table : '',
actions_delete : <?php echo ($admin_access->controller_lessons_tags->can_delete) ? 1 : 0; ?> },

		<?php } ?>
		
		<?php if( isset($admin_access->controller_lessons_subscriptions) ) { ?>
		
'lessons_subscriptions' : { label : 'Lessons Subscription',
fields : ["lesson_id","id","s_plan_id"],
add_fields : ["lesson_id","s_plan_id"],
edit_fields : ["lesson_id","id","s_plan_id"],
list_limit : 20,
list_fields : ["s_plan_id"],
order_by : 'lesson_id',
order_sort : 'ASC',
filters : {"s_plan_id":{"type":"table","anchor":0,"table":"subscription_plans","key":"s_plan_id","value":"s_plan_name", "filter" : 0, "filter_key" : "", "filter_value" : "", "order" : 1, "order_by" : "s_plan_name", "order_sort" : "ASC" }},
primary_key : 'id',
actual_values : {"lesson_id" : "lesson_title_c","s_plan_id" : "s_plan_name_c"},
required_key : 'lesson_id',
required_value : 'lesson_id',
required_table : '',
actions_delete : <?php echo ($admin_access->controller_lessons_subscriptions->can_delete) ? 1 : 0; ?> },

		<?php } ?>
		
		<?php if( isset($admin_access->controller_lessons_meta) ) { ?>
		
'lessons_meta' : { label : 'Lesson Meta',
fields : ["lmeta_id","lesson_id","meta_key","meta_value"],
add_fields : ["lesson_id","meta_key","meta_value"],
edit_fields : ["lmeta_id","lesson_id","meta_key","meta_value"],
list_limit : 20,
list_fields : ["meta_key","meta_value"],
order_by : 'lmeta_id',
order_sort : 'DESC',
primary_key : 'lmeta_id',
primary_title : 'meta_key',
required_key : 'lesson_id',
required_value : 'lesson_id',
required_table : '',
actions_edit : <?php echo ($admin_access->controller_lessons_meta->can_edit) ? 1 : 0; ?>,
actions_delete : <?php echo ($admin_access->controller_lessons_meta->can_delete) ? 1 : 0; ?> },

		<?php } ?>
		
		<?php if( isset($admin_access->controller_lessons_quiz) ) { ?>
		
'lessons_quiz' : { label : 'Lesson Quiz',
fields : ["quiz_id","lesson_id","quiz_number","quiz_time","quiz_question","quiz_type","quiz_details","quiz_correct"],
add_fields : ["quiz_id","lesson_id","quiz_number","quiz_time","quiz_question","quiz_type","quiz_details","quiz_correct"],
edit_fields : ["quiz_id","lesson_id","quiz_number","quiz_time","quiz_question","quiz_type","quiz_details","quiz_correct"],
list_limit : 20,
list_fields : ["quiz_number","quiz_time","quiz_question"],
order_by : 'quiz_time',
order_sort : 'ASC',
primary_key : 'quiz_id',
primary_title : 'quiz_question',
actual_values : {"lesson_id" : "lesson_title"},
required_key : 'lesson_id',
required_value : 'lesson_id',
required_table : 'ci_lessons',
actions_edit : <?php echo ($admin_access->controller_lessons_quiz->can_edit) ? 1 : 0; ?>,
actions_delete : <?php echo ($admin_access->controller_lessons_quiz->can_delete) ? 1 : 0; ?> },

		<?php } ?>
		 },
		filters_data : {"active": {"1": "Active"}, "lesson_active": {"1": "Active"}, "quiz_type": {"multiple-choice": "Multiple Choice"}},
	});
});
</script>

</div></div><!-- .row . col-lg-12 -->
<?php $this->load->view('common_footer'); ?>