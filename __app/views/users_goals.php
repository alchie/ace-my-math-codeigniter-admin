<?php $this->load->view('common_header'); ?>
<div class="row"><div class="col-lg-12">
	<h1 class="page-header"><?php echo $page_title; ?></h1>
	
	<div id="list-view-users_goals" class="list-view">
<div class="panel panel-default panel-users_goals">
<div class="panel-heading">

			<?php if( isset($admin_access->controller_users_goals->can_add) && ($admin_access->controller_users_goals->can_add == 1) ) { ?>
			<a href="javascript:void(0);" class="btn btn-default btn-sm pull-right" id="add-button-users_goals">Add User Goal</a>
			<?php } ?>
<div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">
<div class="table-responsive">
<table id="dataTables" class="table table-striped table-bordered table-hover">
<thead>
<tr>

<th width="None">User</th><th width="None">Goal</th><th width="130">Actions</th>
</tr>
</thead>
<tbody>
</tbody>
</table>
</div>
</div> <!-- .panel-body -->
</div> <!-- .panel .panel-users_goals -->
</div>
		<?php if( isset($admin_access->controller_users_goals->can_add) && ($admin_access->controller_users_goals->can_add == 1) ) { ?>
		<div id="add-view-users_goals" style="display:none">
<div class="panel panel-default add-panel-users_goals">
                        <div class="panel-heading"><h3 class="panel-title">Add User Goal</h3><div class="clearfix"></div>
                        </div>
                        <!-- /.panel-heading -->
<div class="panel-body">

<div class="form-group">
<label for="add_users_goals_user_id">User</label> 
<input data-type="text" type="hidden" name="user_id" id="add_users_goals_user_id" class="form-control add_users_goals_user_id users_goals-input  table-users_goals add-table-users_goals text text text-searchable-key-user_id  add text-searchable-key" />
<a href="javascript:void(0)" data-field="user_id"  data-table="users" data-key="user_id" data-value="user_username" data-display="user_username" data-action="add"  class="text-searchable-list user_id" data-toggle="modal" data-target="#add-text-searchable-box-user_id"><span class="glyphicon glyphicon-list"></span></a>
				<input data-type="text" type="text" name="user_id" class="form-control add text-searchable user_id" placeholder="Search User" data-field="user_id"  data-table="users" data-key="user_id" data-value="user_username" data-display="user_username" data-action="add" />
				<div class="modal fade add" id="add-text-searchable-box-user_id" tabindex="-1" role="dialog" aria-labelledby="User" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">User List</h4>
      </div>
      <div class="modal-body">
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
				
</div>
<div class="form-group">
<label for="add_users_goals_goal_id">Goal</label> 
<input data-type="text" type="hidden" name="goal_id" id="add_users_goals_goal_id" class="form-control add_users_goals_goal_id users_goals-input  table-users_goals add-table-users_goals text text text-searchable-key-goal_id  add text-searchable-key" />
<a href="javascript:void(0)" data-field="goal_id"  data-table="goals" data-key="goal_id" data-value="goal_name" data-display="goal_name" data-action="add"  class="text-searchable-list goal_id" data-toggle="modal" data-target="#add-text-searchable-box-goal_id"><span class="glyphicon glyphicon-list"></span></a>
				<input data-type="text" type="text" name="goal_id" class="form-control add text-searchable goal_id" placeholder="Search Goal" data-field="goal_id"  data-table="goals" data-key="goal_id" data-value="goal_name" data-display="goal_name" data-action="add" />
				<div class="modal fade add" id="add-text-searchable-box-goal_id" tabindex="-1" role="dialog" aria-labelledby="Goal" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Goal List</h4>
      </div>
      <div class="modal-body">
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
				
</div>
</div> <!-- .panel-body -->

<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="add returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="add-action-users_goals">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm add-back-button" id="add-back-users_goals">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-users_goals -->
</div>
<?php } ?><?php if( isset($admin_access->controller_users_goals->can_edit) && ($admin_access->controller_users_goals->can_edit == 1) ) { ?>
		<div id="edit-view-users_goals" style="display:none">
		
		<div class="tab-content tab-content-users_goals parent active"><div class="panel panel-default edit-panel-users_goals">
<div class="panel-heading">
	 <h3 class="panel-title">Edit User Goal</h3>
	 <div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="ug_id" id="edit_users_goals_ug_id" class="edit_users_goals_ug_id users_goals-input  table-users_goals edit-table-users_goals hidden text" placeholder="ID" value="" />
<div class="form-group">
<label for="edit_users_goals_user_id">User</label> 
<input data-type="text" type="hidden" name="user_id" id="edit_users_goals_user_id" class="form-control edit_users_goals_user_id users_goals-input  table-users_goals edit-table-users_goals text text text-searchable-key-user_id  edit text-searchable-key" />
<a href="javascript:void(0)" data-field="user_id"  data-table="users" data-key="user_id" data-value="user_username" data-display="user_username" data-action="edit"  class="text-searchable-list user_id" data-toggle="modal" data-target="#edit-text-searchable-box-user_id"><span class="glyphicon glyphicon-list"></span></a>
				<input data-type="text" type="text" name="user_id" class="form-control edit text-searchable user_id" placeholder="Search User" data-field="user_id"  data-table="users" data-key="user_id" data-value="user_username" data-display="user_username" data-action="edit" />
				<div class="modal fade edit" id="edit-text-searchable-box-user_id" tabindex="-1" role="dialog" aria-labelledby="User" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">User List</h4>
      </div>
      <div class="modal-body">
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
				
</div>
<div class="form-group">
<label for="edit_users_goals_goal_id">Goal</label> 
<input data-type="text" type="hidden" name="goal_id" id="edit_users_goals_goal_id" class="form-control edit_users_goals_goal_id users_goals-input  table-users_goals edit-table-users_goals text text text-searchable-key-goal_id  edit text-searchable-key" />
<a href="javascript:void(0)" data-field="goal_id"  data-table="goals" data-key="goal_id" data-value="goal_name" data-display="goal_name" data-action="edit"  class="text-searchable-list goal_id" data-toggle="modal" data-target="#edit-text-searchable-box-goal_id"><span class="glyphicon glyphicon-list"></span></a>
				<input data-type="text" type="text" name="goal_id" class="form-control edit text-searchable goal_id" placeholder="Search Goal" data-field="goal_id"  data-table="goals" data-key="goal_id" data-value="goal_name" data-display="goal_name" data-action="edit" />
				<div class="modal fade edit" id="edit-text-searchable-box-goal_id" tabindex="-1" role="dialog" aria-labelledby="Goal" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Goal List</h4>
      </div>
      <div class="modal-body">
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
				
</div>
</div> <!-- .panel-body -->
<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="update-action-users_goals">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm update-back-users_goals" id="update-back-users_goals">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-users_goals -->
</div><!-- .tab-content .tab-content-users_goals --></div>
<?php } ?>
	
	<script>
jQuery(document).ready(function($) {
	$(document).AdminActions({
		baseURL : '<?php echo base_url(); ?>',
		current_table : 'users_goals',
		tables : { 
		<?php if( isset($admin_access->controller_users_goals) ) { ?>
		
'users_goals' : { label : 'User Goal',
fields : ["ug_id","user_id","goal_id"],
add_fields : ["user_id","goal_id"],
edit_fields : ["ug_id","user_id","goal_id"],
list_limit : 20,
list_fields : ["user_id","goal_id"],
order_by : 'goal_id',
order_sort : 'ASC',
primary_key : 'ug_id',
actions_edit : <?php echo ($admin_access->controller_users_goals->can_edit) ? 1 : 0; ?>,
actions_delete : <?php echo ($admin_access->controller_users_goals->can_delete) ? 1 : 0; ?> },

		<?php } ?>
		 },
		filters_data : {},
	});
});
</script>

</div></div><!-- .row . col-lg-12 -->
<?php $this->load->view('common_footer'); ?>