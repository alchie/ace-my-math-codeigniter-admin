<?php $this->load->view('common_header'); ?>
<div class="row"><div class="col-lg-12">
	<h1 class="page-header"><?php echo $page_title; ?></h1>
	
	<div id="list-view-tax_chapters" class="list-view">
<div class="panel panel-default panel-tax_chapters">
<div class="panel-heading">

			<?php if( isset($admin_access->controller_tax_chapters->can_add) && ($admin_access->controller_tax_chapters->can_add == 1) ) { ?>
			<a href="javascript:void(0);" class="btn btn-default btn-sm pull-right" id="add-button-tax_chapters">Add Lesson Chapter</a>
			<?php } ?>
<div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">
<div class="table-responsive">
<table id="dataTables" class="table table-striped table-bordered table-hover">
<thead>
<tr>
<th width='1%'></th>
<th width="50px">ID</th><th width="">Name<span  data-key="chapter_name" data-table="tax_chapters" id="list_search_button_chapter_name" class="btn btn-primary btn-xs pull-right btn-search list-search-tax_chapters" title="Search Name">
		<i class="fa fa-search"></i></span></th><th width="">Description<span  data-key="chapter_description" data-table="tax_chapters" id="list_search_button_chapter_description" class="btn btn-primary btn-xs pull-right btn-search list-search-tax_chapters" title="Search Description">
		<i class="fa fa-search"></i></span></th><th width="">Order</th><th width="130">Actions</th>
</tr>
</thead>
<tbody>
</tbody>
</table>
</div>
</div> <!-- .panel-body -->
</div> <!-- .panel .panel-tax_chapters -->
</div>
		<?php if( isset($admin_access->controller_tax_chapters->can_add) && ($admin_access->controller_tax_chapters->can_add == 1) ) { ?>
		<div id="add-view-tax_chapters" style="display:none">
<div class="panel panel-default add-panel-tax_chapters">
                        <div class="panel-heading"><h3 class="panel-title">Add Lesson Chapter</h3><div class="clearfix"></div>
                        </div>
                        <!-- /.panel-heading -->
<div class="panel-body">

<div class="form-group">
<label for="add_tax_chapters_chapter_name">Name</label> 
<input data-type="text" type="text" name="chapter_name" id="add_tax_chapters_chapter_name" class="form-control add_tax_chapters_chapter_name tax_chapters-input  table-tax_chapters add-table-tax_chapters text text" placeholder="Name" value=""/>
</div>
<div class="form-group">
<label for="add_tax_chapters_chapter_description">Description</label>
<textarea rows="5" data-type="textarea" data-wysiwyg="0" type="text" name="chapter_description" id="add_tax_chapters_chapter_description" class="form-control add_tax_chapters_chapter_description tax_chapters-input  table-tax_chapters add-table-tax_chapters textarea text" placeholder="Description" value="" /></textarea>
</div>
<div class="form-group">
<label for="add_tax_chapters_chapter_order">Order</label> 
<input data-type="text" type="text" name="chapter_order" id="add_tax_chapters_chapter_order" class="form-control add_tax_chapters_chapter_order tax_chapters-input  table-tax_chapters add-table-tax_chapters text text" placeholder="Order" value="0"/>
</div>
<div class="form-group"><strong>Active</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="chapter_active" id="add_tax_chapters_chapter_active" class="add_tax_chapters_chapter_active tax_chapters-input  table-tax_chapters add-table-tax_chapters checkbox text" placeholder="Active" value="1" />Active</label></div></div>
</div> <!-- .panel-body -->

<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="add returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="add-action-tax_chapters">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm add-back-button" id="add-back-tax_chapters">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-tax_chapters -->
</div>
<?php } ?><?php if( isset($admin_access->controller_tax_chapters->can_edit) && ($admin_access->controller_tax_chapters->can_edit == 1) ) { ?>
		<div id="edit-view-tax_chapters" style="display:none">
		<ul class="nav nav-tabs">
<li>
		<a href="javascript:void(0);" class="update-back-tax_chapters" id="update-back-tax_chapters">
		<span class="glyphicon glyphicon-arrow-left"></span>
		</a>
		</li><li class="parent-tab active">
		<a class="tab-item" href="javascript:void(0);" data-table="tax_chapters" data-child="0">Edit Lesson Chapter</a>
		</li>
			<?php if( isset($admin_access->controller_level_chapters) ) { ?>
			<li>
			<a class="tab-item" href="javascript:void(0);"  data-table="level_chapters" data-child="1">Chapter Titles</a>
			</li>
			<?php } ?>
</ul><br>
		<div class="tab-content tab-content-tax_chapters parent active"><div class="panel panel-default edit-panel-tax_chapters">
<div class="panel-heading">
	 <h3 class="panel-title">Edit Lesson Chapter</h3>
	 <div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="chapter_id" id="edit_tax_chapters_chapter_id" class="edit_tax_chapters_chapter_id tax_chapters-input  table-tax_chapters edit-table-tax_chapters hidden text" placeholder="ID" value="" />
<div class="form-group">
<label for="edit_tax_chapters_chapter_name">Name</label> 
<input data-type="text" type="text" name="chapter_name" id="edit_tax_chapters_chapter_name" class="form-control edit_tax_chapters_chapter_name tax_chapters-input  table-tax_chapters edit-table-tax_chapters text text" placeholder="Name" value=""/>
</div>
<div class="form-group">
<label for="edit_tax_chapters_chapter_slug">Slug</label> 
<input data-type="text" type="text" name="chapter_slug" id="edit_tax_chapters_chapter_slug" class="form-control edit_tax_chapters_chapter_slug tax_chapters-input  table-tax_chapters edit-table-tax_chapters text text" placeholder="Slug" value=""/>
</div>
<div class="form-group">
<label for="edit_tax_chapters_chapter_description">Description</label>
<textarea rows="5" data-type="textarea" data-wysiwyg="0" type="text" name="chapter_description" id="edit_tax_chapters_chapter_description" class="form-control edit_tax_chapters_chapter_description tax_chapters-input  table-tax_chapters edit-table-tax_chapters textarea text" placeholder="Description" value="" /></textarea>
</div>
<div class="form-group">
<label for="edit_tax_chapters_chapter_order">Order</label> 
<input data-type="text" type="text" name="chapter_order" id="edit_tax_chapters_chapter_order" class="form-control edit_tax_chapters_chapter_order tax_chapters-input  table-tax_chapters edit-table-tax_chapters text text" placeholder="Order" value="0"/>
</div>
<div class="form-group"><strong>Active</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="chapter_active" id="edit_tax_chapters_chapter_active" class="edit_tax_chapters_chapter_active tax_chapters-input  table-tax_chapters edit-table-tax_chapters checkbox text" placeholder="Active" value="1" />Active</label></div></div>
</div> <!-- .panel-body -->
<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="update-action-tax_chapters">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm update-back-tax_chapters" id="update-back-tax_chapters">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-tax_chapters -->
</div><!-- .tab-content .tab-content-tax_chapters -->
			<?php if( isset($admin_access->controller_level_chapters) ) { ?>
			<div class="tab-content tab-content-level_chapters" style="display:none"><div id="list-view-level_chapters" class="list-view">
<div class="panel panel-default panel-level_chapters">
<div class="panel-heading">

			<?php if( isset($admin_access->controller_level_chapters->can_add) && ($admin_access->controller_level_chapters->can_add == 1) ) { ?>
			<a href="javascript:void(0);" class="btn btn-default btn-sm pull-right" id="add-button-level_chapters">Add Title</a>
			<?php } ?>
<div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">
<div class="table-responsive">
<table id="dataTables" class="table table-striped table-bordered table-hover">
<thead>
<tr>

<th width=""><div class="dropdown-filter"><a href="javascript:void(0);" data-filter="level_id" data-table="level_chapters">Level <span class="glyphicon glyphicon-chevron-down pull-right"></span></a></div></th><th width="">Chapter Title<span  data-key="chapter_title" data-table="level_chapters" id="list_search_button_chapter_title" class="btn btn-primary btn-xs pull-right btn-search list-search-level_chapters" title="Search Chapter Title">
		<i class="fa fa-search"></i></span></th><th width="130">Actions</th>
</tr>
</thead>
<tbody>
</tbody>
</table>
</div>
</div> <!-- .panel-body -->
</div> <!-- .panel .panel-level_chapters -->
</div>
		<?php if( isset($admin_access->controller_level_chapters->can_add) && ($admin_access->controller_level_chapters->can_add == 1) ) { ?>
		<div id="add-view-level_chapters" style="display:none">
<div class="panel panel-default add-panel-level_chapters">
                        <div class="panel-heading"><h3 class="panel-title">Add Title</h3><div class="clearfix"></div>
                        </div>
                        <!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="chapter_id" id="add_level_chapters_chapter_id" class="add_level_chapters_chapter_id level_chapters-input  table-level_chapters add-table-level_chapters hidden text" placeholder="Chapter" value="" />
<div class="form-group">
<label for="add_lessons_level_id">Level</label> 
			<select name="level_id" id="add_level_chapters_level_id" class="selectpicker form-control add_level_chapters_level_id level_chapters-input  table-level_chapters add-table-level_chapters dropdown text dropdown-table" placeholder="Level" data-live-search="true"  data-type="dropdown" data-label="Level" data-field="level_id" data-table="tax_levels" data-key="level_id" data-value="level_name" data-filter="0" data-filter-key="" data-filter-value="" data-order="1" data-order-by="level_name" data-order-sort="ASC">
			<option value="">- - Select Level - -</option>
</select></div>
<div class="form-group">
<label for="add_level_chapters_chapter_title">Chapter Title</label> 
<input data-type="text" type="text" name="chapter_title" id="add_level_chapters_chapter_title" class="form-control add_level_chapters_chapter_title level_chapters-input  table-level_chapters add-table-level_chapters text text" placeholder="Chapter Title" value=""/>
</div>
</div> <!-- .panel-body -->

<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="add returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="add-action-level_chapters">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm add-back-button" id="add-back-level_chapters">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-level_chapters -->
</div>
<?php } ?><?php if( isset($admin_access->controller_level_chapters->can_edit) && ($admin_access->controller_level_chapters->can_edit == 1) ) { ?>
		<div id="edit-view-level_chapters" style="display:none">
		
		<div class="panel panel-default edit-panel-level_chapters">
<div class="panel-heading">
	 <h3 class="panel-title">Edit Title</h3>
	 <div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="chapter_id" id="edit_level_chapters_chapter_id" class="edit_level_chapters_chapter_id level_chapters-input  table-level_chapters edit-table-level_chapters hidden text" placeholder="Chapter" value="" />
<div class="form-group">
<label for="add_lessons_level_id">Level</label> 
			<select name="level_id" id="edit_level_chapters_level_id" class="selectpicker form-control edit_level_chapters_level_id level_chapters-input  table-level_chapters edit-table-level_chapters dropdown text dropdown-table" placeholder="Level" data-live-search="true"  data-type="dropdown" data-label="Level" data-field="level_id" data-table="tax_levels" data-key="level_id" data-value="level_name" data-filter="0" data-filter-key="" data-filter-value="" data-order="1" data-order-by="level_name" data-order-sort="ASC">
			<option value="">- - Select Level - -</option>
</select></div>
<div class="form-group">
<label for="edit_level_chapters_chapter_title">Chapter Title</label> 
<input data-type="text" type="text" name="chapter_title" id="edit_level_chapters_chapter_title" class="form-control edit_level_chapters_chapter_title level_chapters-input  table-level_chapters edit-table-level_chapters text text" placeholder="Chapter Title" value=""/>
</div>
</div> <!-- .panel-body -->
<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="update-action-level_chapters">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm update-back-level_chapters" id="update-back-level_chapters">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-level_chapters -->
</div>
<?php } ?></div><!-- .tab-content .tab-content-level_chapters -->
			<?php } ?></div>
<?php } ?>
	
	<script>
jQuery(document).ready(function($) {
	$(document).AdminActions({
		baseURL : '<?php echo base_url(); ?>',
		current_table : 'tax_chapters',
		tables : { 
		<?php if( isset($admin_access->controller_tax_chapters) ) { ?>
		
'tax_chapters' : { label : 'Lesson Chapter',
fields : ["chapter_id","chapter_name","chapter_slug","chapter_description","chapter_order","chapter_active"],
add_fields : ["chapter_name","chapter_description","chapter_order","chapter_active"],
edit_fields : ["chapter_id","chapter_name","chapter_slug","chapter_description","chapter_order","chapter_active"],
list_limit : 20,
list_fields : ["chapter_id","chapter_name","chapter_description","chapter_order"],
order_by : 'chapter_order',
order_sort : 'ASC',
primary_key : 'chapter_id',
primary_title : 'chapter_name',
active_key : 'chapter_active',
actions_edit : <?php echo ($admin_access->controller_tax_chapters->can_edit) ? 1 : 0; ?>,
actions_delete : <?php echo ($admin_access->controller_tax_chapters->can_delete) ? 1 : 0; ?> },

		<?php } ?>
		
		<?php if( isset($admin_access->controller_level_chapters) ) { ?>
		
'level_chapters' : { label : 'Level Chapter',
fields : ["level_chapter_id","chapter_id","level_id","chapter_title"],
add_fields : ["chapter_id","level_id","chapter_title"],
edit_fields : ["chapter_id","level_id","chapter_title"],
list_limit : 20,
list_fields : ["level_id","chapter_title"],
order_by : 'chapter_id',
order_sort : 'ASC',
filters : {"chapter_id":{"type":"table","anchor":0,"table":"tax_chapters","key":"chapter_id","value":"chapter_name", "filter" : 0, "filter_key" : "", "filter_value" : "", "order" : 1, "order_by" : "chapter_name", "order_sort" : "ASC" },"level_id":{"type":"table","anchor":0,"table":"tax_levels","key":"level_id","value":"level_name", "filter" : 0, "filter_key" : "", "filter_value" : "", "order" : 1, "order_by" : "level_name", "order_sort" : "ASC" }},
primary_key : 'level_chapter_id',
primary_title : 'chapter_title',
actual_values : {"chapter_id" : "chapter_name","level_id" : "level_name"},
required_key : 'chapter_id',
required_value : 'chapter_id',
actions_edit : <?php echo ($admin_access->controller_level_chapters->can_edit) ? 1 : 0; ?>,
actions_delete : <?php echo ($admin_access->controller_level_chapters->can_delete) ? 1 : 0; ?> },

		<?php } ?>
		 },
		filters_data : {"chapter_active": {"1": "Active"}},
	});
});
</script>

</div></div><!-- .row . col-lg-12 -->
<?php $this->load->view('common_footer'); ?>