<?php $this->load->view('common_header'); ?>
<div class="row"><div class="col-lg-12">
	<h1 class="page-header"><?php echo $page_title; ?></h1>
	
	<div id="list-view-attributes" class="list-view">
<div class="panel panel-default panel-attributes">
<div class="panel-heading">

			<?php if( isset($admin_access->controller_attributes->can_add) && ($admin_access->controller_attributes->can_add == 1) ) { ?>
			<a href="javascript:void(0);" class="btn btn-default btn-sm pull-right" id="add-button-attributes">Add Attribute</a>
			<?php } ?>
<div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">
<div class="table-responsive">
<table id="dataTables" class="table table-striped table-bordered table-hover">
<thead>
<tr>

<th width=""><div class="dropdown-filter"><a href="javascript:void(0);" data-filter="attr_controller" data-table="attributes">Controller <span class="glyphicon glyphicon-chevron-down pull-right"></span></a></div></th><th width="None">Name</th><th width="">Description<span  data-key="attr_description" data-table="attributes" id="list_search_button_attr_description" class="btn btn-primary btn-xs pull-right btn-search list-search-attributes" title="Search Description">
		<i class="fa fa-search"></i></span></th><th width="130">Actions</th>
</tr>
</thead>
<tbody>
</tbody>
</table>
</div>
</div> <!-- .panel-body -->
</div> <!-- .panel .panel-attributes -->
</div>
		<?php if( isset($admin_access->controller_attributes->can_add) && ($admin_access->controller_attributes->can_add == 1) ) { ?>
		<div id="add-view-attributes" style="display:none">
<div class="panel panel-default add-panel-attributes">
                        <div class="panel-heading"><h3 class="panel-title">Add Attribute</h3><div class="clearfix"></div>
                        </div>
                        <!-- /.panel-heading -->
<div class="panel-body">

<div class="form-group">
<label for="add_lessons_attr_controller">Controller</label> 
			<select name="attr_controller" id="add_attributes_attr_controller" class="selectpicker form-control add_attributes_attr_controller attributes-input  table-attributes add-table-attributes dropdown text" placeholder="Controller" data-live-search="true" >
			<option value="">- - Select Controller - -</option>
<option value="membership_plans">Membership Plans</option>
<option value="users_profiles">Users Profiles</option>
</select></div>
<div class="form-group">
<label for="add_attributes_attr_name">Name</label> 
<input data-type="text" type="text" name="attr_name" id="add_attributes_attr_name" class="form-control add_attributes_attr_name attributes-input  table-attributes add-table-attributes text text" placeholder="Name" value=""/>
</div>
<div class="form-group">
<label for="add_attributes_attr_label">Label</label> 
<input data-type="text" type="text" name="attr_label" id="add_attributes_attr_label" class="form-control add_attributes_attr_label attributes-input  table-attributes add-table-attributes text text" placeholder="Label" value=""/>
</div>
<div class="form-group">
<label for="add_attributes_attr_description">Description</label> 
<input data-type="text" type="text" name="attr_description" id="add_attributes_attr_description" class="form-control add_attributes_attr_description attributes-input  table-attributes add-table-attributes text text" placeholder="Description" value=""/>
</div>
</div> <!-- .panel-body -->

<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="add returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="add-action-attributes">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm add-back-button" id="add-back-attributes">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-attributes -->
</div>
<?php } ?><?php if( isset($admin_access->controller_attributes->can_edit) && ($admin_access->controller_attributes->can_edit == 1) ) { ?>
		<div id="edit-view-attributes" style="display:none">
		
		<div class="tab-content tab-content-attributes parent active"><div class="panel panel-default edit-panel-attributes">
<div class="panel-heading">
	 <h3 class="panel-title">Edit Attribute</h3>
	 <div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="attr_id" id="edit_attributes_attr_id" class="edit_attributes_attr_id attributes-input  table-attributes edit-table-attributes hidden text" placeholder="Attribute ID" value="" />
<div class="form-group">
<label for="add_lessons_attr_controller">Controller</label> 
			<select name="attr_controller" id="edit_attributes_attr_controller" class="selectpicker form-control edit_attributes_attr_controller attributes-input  table-attributes edit-table-attributes dropdown text" placeholder="Controller" data-live-search="true" >
			<option value="">- - Select Controller - -</option>
<option value="membership_plans">Membership Plans</option>
<option value="users_profiles">Users Profiles</option>
</select></div>
<div class="form-group">
<label for="edit_attributes_attr_name">Name</label> 
<input data-type="text" type="text" name="attr_name" id="edit_attributes_attr_name" class="form-control edit_attributes_attr_name attributes-input  table-attributes edit-table-attributes text text" placeholder="Name" value=""/>
</div>
<div class="form-group">
<label for="edit_attributes_attr_label">Label</label> 
<input data-type="text" type="text" name="attr_label" id="edit_attributes_attr_label" class="form-control edit_attributes_attr_label attributes-input  table-attributes edit-table-attributes text text" placeholder="Label" value=""/>
</div>
<div class="form-group">
<label for="edit_attributes_attr_description">Description</label> 
<input data-type="text" type="text" name="attr_description" id="edit_attributes_attr_description" class="form-control edit_attributes_attr_description attributes-input  table-attributes edit-table-attributes text text" placeholder="Description" value=""/>
</div>
</div> <!-- .panel-body -->
<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="update-action-attributes">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm update-back-attributes" id="update-back-attributes">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-attributes -->
</div><!-- .tab-content .tab-content-attributes --></div>
<?php } ?>
	
	<script>
jQuery(document).ready(function($) {
	$(document).AdminActions({
		baseURL : '<?php echo base_url(); ?>',
		current_table : 'attributes',
		tables : { 
		<?php if( isset($admin_access->controller_attributes) ) { ?>
		
'attributes' : { label : 'Attribute',
fields : ["attr_id","attr_controller","attr_name","attr_label","attr_description"],
add_fields : ["attr_controller","attr_name","attr_label","attr_description"],
edit_fields : ["attr_id","attr_controller","attr_name","attr_label","attr_description"],
list_limit : 20,
list_fields : ["attr_controller","attr_name","attr_description"],
order_by : 'attr_label',
order_sort : 'ASC',
filters : {"attr_controller":{"type":"manual","anchor":0}},
primary_key : 'attr_id',
primary_title : 'attr_name',
actions_edit : <?php echo ($admin_access->controller_attributes->can_edit) ? 1 : 0; ?>,
actions_delete : <?php echo ($admin_access->controller_attributes->can_delete) ? 1 : 0; ?> },

		<?php } ?>
		 },
		filters_data : {"attr_controller": {"membership_plans": "Membership Plans", "users_profiles": "Users Profiles"}},
	});
});
</script>

</div></div><!-- .row . col-lg-12 -->
<?php $this->load->view('common_footer'); ?>