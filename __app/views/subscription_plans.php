<?php $this->load->view('common_header'); ?>
<div class="row"><div class="col-lg-12">
	<h1 class="page-header"><?php echo $page_title; ?></h1>
	
	<div id="list-view-subscription_plans" class="list-view">
<div class="panel panel-default panel-subscription_plans">
<div class="panel-heading">

			<?php if( isset($admin_access->controller_subscription_plans->can_add) && ($admin_access->controller_subscription_plans->can_add == 1) ) { ?>
			<a href="javascript:void(0);" class="btn btn-default btn-sm pull-right" id="add-button-subscription_plans">Add Membership Plan</a>
			<?php } ?>
<div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">
<div class="table-responsive">
<table id="dataTables" class="table table-striped table-bordered table-hover">
<thead>
<tr>
<th width='1%'></th>
<th width="50px">ID</th><th width="">Plan Name<span  data-key="s_plan_name" data-table="subscription_plans" id="list_search_button_s_plan_name" class="btn btn-primary btn-xs pull-right btn-search list-search-subscription_plans" title="Search Plan Name">
		<i class="fa fa-search"></i></span></th><th width="10%">Price</th><th width="130">Actions</th>
</tr>
</thead>
<tbody>
</tbody>
</table>
</div>
</div> <!-- .panel-body -->
</div> <!-- .panel .panel-subscription_plans -->
</div>
		<?php if( isset($admin_access->controller_subscription_plans->can_add) && ($admin_access->controller_subscription_plans->can_add == 1) ) { ?>
		<div id="add-view-subscription_plans" style="display:none">
<div class="panel panel-default add-panel-subscription_plans">
                        <div class="panel-heading"><h3 class="panel-title">Add Membership Plan</h3><div class="clearfix"></div>
                        </div>
                        <!-- /.panel-heading -->
<div class="panel-body">

<div class="form-group">
<label for="add_subscription_plans_s_plan_name">Plan Name</label> 
<input data-type="text" type="text" name="s_plan_name" id="add_subscription_plans_s_plan_name" class="form-control add_subscription_plans_s_plan_name subscription_plans-input  table-subscription_plans add-table-subscription_plans text text" placeholder="Plan Name" value=""/>
</div>
<div class="form-group">
<label for="add_subscription_plans_s_plan_price">Price</label> 
<input data-type="text" type="text" name="s_plan_price" id="add_subscription_plans_s_plan_price" class="form-control add_subscription_plans_s_plan_price subscription_plans-input  table-subscription_plans add-table-subscription_plans text text" placeholder="Price" value=""/>
</div>
<div class="form-group">
<label for="add_subscription_plans_s_plan_order">Order</label> 
<input data-type="text" type="text" name="s_plan_order" id="add_subscription_plans_s_plan_order" class="form-control add_subscription_plans_s_plan_order subscription_plans-input  table-subscription_plans add-table-subscription_plans text text" placeholder="Order" value="0"/>
</div>
<div class="form-group"><strong>Active</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="s_plan_active" id="add_subscription_plans_s_plan_active" class="add_subscription_plans_s_plan_active subscription_plans-input  table-subscription_plans add-table-subscription_plans checkbox text" placeholder="Active" value="1" />Active</label></div></div>
</div> <!-- .panel-body -->

<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="add returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="add-action-subscription_plans">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm add-back-button" id="add-back-subscription_plans">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-subscription_plans -->
</div>
<?php } ?><?php if( isset($admin_access->controller_subscription_plans->can_edit) && ($admin_access->controller_subscription_plans->can_edit == 1) ) { ?>
		<div id="edit-view-subscription_plans" style="display:none">
		<ul class="nav nav-tabs">
<li>
		<a href="javascript:void(0);" class="update-back-subscription_plans" id="update-back-subscription_plans">
		<span class="glyphicon glyphicon-arrow-left"></span>
		</a>
		</li><li class="parent-tab active">
		<a class="tab-item" href="javascript:void(0);" data-table="subscription_plans" data-child="0">Edit Membership Plan</a>
		</li>
			<?php if( isset($admin_access->controller_subscription_attributes) ) { ?>
			<li>
			<a class="tab-item" href="javascript:void(0);"  data-table="subscription_attributes" data-child="1">Attributes</a>
			</li>
			<?php } ?>
</ul><br>
		<div class="tab-content tab-content-subscription_plans parent active"><div class="panel panel-default edit-panel-subscription_plans">
<div class="panel-heading">
	 <h3 class="panel-title">Edit Membership Plan</h3>
	 <div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="s_plan_id" id="edit_subscription_plans_s_plan_id" class="edit_subscription_plans_s_plan_id subscription_plans-input  table-subscription_plans edit-table-subscription_plans hidden text" placeholder="ID" value="" />
<div class="form-group">
<label for="edit_subscription_plans_s_plan_name">Plan Name</label> 
<input data-type="text" type="text" name="s_plan_name" id="edit_subscription_plans_s_plan_name" class="form-control edit_subscription_plans_s_plan_name subscription_plans-input  table-subscription_plans edit-table-subscription_plans text text" placeholder="Plan Name" value=""/>
</div>
<div class="form-group">
<label for="edit_subscription_plans_s_plan_price">Price</label> 
<input data-type="text" type="text" name="s_plan_price" id="edit_subscription_plans_s_plan_price" class="form-control edit_subscription_plans_s_plan_price subscription_plans-input  table-subscription_plans edit-table-subscription_plans text text" placeholder="Price" value=""/>
</div>
<div class="form-group">
<label for="edit_subscription_plans_s_plan_order">Order</label> 
<input data-type="text" type="text" name="s_plan_order" id="edit_subscription_plans_s_plan_order" class="form-control edit_subscription_plans_s_plan_order subscription_plans-input  table-subscription_plans edit-table-subscription_plans text text" placeholder="Order" value="0"/>
</div>
<div class="form-group"><strong>Active</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="s_plan_active" id="edit_subscription_plans_s_plan_active" class="edit_subscription_plans_s_plan_active subscription_plans-input  table-subscription_plans edit-table-subscription_plans checkbox text" placeholder="Active" value="1" />Active</label></div></div>
</div> <!-- .panel-body -->
<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="update-action-subscription_plans">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm update-back-subscription_plans" id="update-back-subscription_plans">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-subscription_plans -->
</div><!-- .tab-content .tab-content-subscription_plans -->
			<?php if( isset($admin_access->controller_subscription_attributes) ) { ?>
			<div class="tab-content tab-content-subscription_attributes" style="display:none"><div id="list-view-subscription_attributes" class="list-view">
<div class="panel panel-default panel-subscription_attributes">
<div class="panel-heading">

			<?php if( isset($admin_access->controller_subscription_attributes->can_add) && ($admin_access->controller_subscription_attributes->can_add == 1) ) { ?>
			<a href="javascript:void(0);" class="btn btn-default btn-sm pull-right" id="add-button-subscription_attributes">Add Attribute</a>
			<?php } ?>
<div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">
<div class="table-responsive">
<table id="dataTables" class="table table-striped table-bordered table-hover">
<thead>
<tr>

<th width="">Plan Attributes</th><th width="None">Value</th><th width="130">Actions</th>
</tr>
</thead>
<tbody>
</tbody>
</table>
</div>
</div> <!-- .panel-body -->
</div> <!-- .panel .panel-subscription_attributes -->
</div>
		<?php if( isset($admin_access->controller_subscription_attributes->can_add) && ($admin_access->controller_subscription_attributes->can_add == 1) ) { ?>
		<div id="add-view-subscription_attributes" style="display:none">
<div class="panel panel-default add-panel-subscription_attributes">
                        <div class="panel-heading"><h3 class="panel-title">Add Attribute</h3><div class="clearfix"></div>
                        </div>
                        <!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="s_plan_id" id="add_subscription_attributes_s_plan_id" class="add_subscription_attributes_s_plan_id subscription_attributes-input  table-subscription_attributes add-table-subscription_attributes hidden text" placeholder="Plan" value="" />
<div class="form-group">
<label for="add_lessons_attr_id">Plan Attributes</label> 
			<select name="attr_id" id="add_subscription_attributes_attr_id" class="selectpicker form-control add_subscription_attributes_attr_id subscription_attributes-input  table-subscription_attributes add-table-subscription_attributes dropdown text dropdown-table" placeholder="Plan Attributes" data-live-search="true"  data-type="dropdown" data-label="Plan Attributes" data-field="attr_id" data-table="attributes" data-key="attr_id" data-value="attr_name" data-filter="1" data-filter-key="attr_controller" data-filter-value="membership_plans" data-order="1" data-order-by="attr_name" data-order-sort="ASC">
			<option value="">- - Select Plan Attributes - -</option>
</select></div>
<div class="form-group">
<label for="add_subscription_attributes_s_attr_value">Value</label>
<textarea rows="5" data-type="textarea" data-wysiwyg="0" type="text" name="s_attr_value" id="add_subscription_attributes_s_attr_value" class="form-control add_subscription_attributes_s_attr_value subscription_attributes-input  table-subscription_attributes add-table-subscription_attributes textarea text" placeholder="Value" value="" /></textarea>
</div>
</div> <!-- .panel-body -->

<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="add returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="add-action-subscription_attributes">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm add-back-button" id="add-back-subscription_attributes">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-subscription_attributes -->
</div>
<?php } ?><?php if( isset($admin_access->controller_subscription_attributes->can_edit) && ($admin_access->controller_subscription_attributes->can_edit == 1) ) { ?>
		<div id="edit-view-subscription_attributes" style="display:none">
		
		<div class="panel panel-default edit-panel-subscription_attributes">
<div class="panel-heading">
	 <h3 class="panel-title">Edit Attribute</h3>
	 <div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="s_attr_id" id="edit_subscription_attributes_s_attr_id" class="edit_subscription_attributes_s_attr_id subscription_attributes-input  table-subscription_attributes edit-table-subscription_attributes hidden text" placeholder="Attribute ID" value="" />
<input data-type="hidden" type="hidden" name="s_plan_id" id="edit_subscription_attributes_s_plan_id" class="edit_subscription_attributes_s_plan_id subscription_attributes-input  table-subscription_attributes edit-table-subscription_attributes hidden text" placeholder="Plan" value="" />
<div class="form-group">
<label for="add_lessons_attr_id">Plan Attributes</label> 
			<select name="attr_id" id="edit_subscription_attributes_attr_id" class="selectpicker form-control edit_subscription_attributes_attr_id subscription_attributes-input  table-subscription_attributes edit-table-subscription_attributes dropdown text dropdown-table" placeholder="Plan Attributes" data-live-search="true"  data-type="dropdown" data-label="Plan Attributes" data-field="attr_id" data-table="attributes" data-key="attr_id" data-value="attr_name" data-filter="1" data-filter-key="attr_controller" data-filter-value="membership_plans" data-order="1" data-order-by="attr_name" data-order-sort="ASC">
			<option value="">- - Select Plan Attributes - -</option>
</select></div>
<div class="form-group">
<label for="edit_subscription_attributes_s_attr_value">Value</label>
<textarea rows="5" data-type="textarea" data-wysiwyg="0" type="text" name="s_attr_value" id="edit_subscription_attributes_s_attr_value" class="form-control edit_subscription_attributes_s_attr_value subscription_attributes-input  table-subscription_attributes edit-table-subscription_attributes textarea text" placeholder="Value" value="" /></textarea>
</div>
</div> <!-- .panel-body -->
<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="update-action-subscription_attributes">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm update-back-subscription_attributes" id="update-back-subscription_attributes">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-subscription_attributes -->
</div>
<?php } ?></div><!-- .tab-content .tab-content-subscription_attributes -->
			<?php } ?></div>
<?php } ?>
	
	<script>
jQuery(document).ready(function($) {
	$(document).AdminActions({
		baseURL : '<?php echo base_url(); ?>',
		current_table : 'subscription_plans',
		tables : { 
		<?php if( isset($admin_access->controller_subscription_plans) ) { ?>
		
'subscription_plans' : { label : 'Membership Plan',
fields : ["s_plan_id","s_plan_name","s_plan_price","s_plan_order","s_plan_active"],
add_fields : ["s_plan_name","s_plan_price","s_plan_order","s_plan_active"],
edit_fields : ["s_plan_id","s_plan_name","s_plan_price","s_plan_order","s_plan_active"],
list_limit : 20,
list_fields : ["s_plan_id","s_plan_name","s_plan_price"],
order_by : 's_plan_order',
order_sort : 'ASC',
primary_key : 's_plan_id',
primary_title : 's_plan_name',
active_key : 's_plan_active',
actions_edit : <?php echo ($admin_access->controller_subscription_plans->can_edit) ? 1 : 0; ?>,
actions_delete : <?php echo ($admin_access->controller_subscription_plans->can_delete) ? 1 : 0; ?> },

		<?php } ?>
		
		<?php if( isset($admin_access->controller_subscription_attributes) ) { ?>
		
'subscription_attributes' : { label : 'Plan Attribute',
fields : ["s_attr_id","s_plan_id","attr_id","s_attr_value"],
add_fields : ["s_plan_id","attr_id","s_attr_value"],
edit_fields : ["s_attr_id","s_plan_id","attr_id","s_attr_value"],
list_limit : 20,
list_fields : ["attr_name_c","s_attr_value"],
order_by : 'attr_id',
order_sort : 'ASC',
filters : {"attr_id":{"type":"table","anchor":0,"table":"attributes","key":"attr_id","value":"attr_name", "filter" : 1, "filter_key" : "attr_controller", "filter_value" : "membership_plans", "order" : 1, "order_by" : "attr_name", "order_sort" : "ASC" }},
primary_key : 's_attr_id',
primary_title : 's_attr_value',
actual_values : {"s_plan_id" : "s_plan_name_c","attr_id" : "attr_name_c"},
required_key : 's_plan_id',
required_value : 's_plan_id',
required_table : '',
actions_edit : <?php echo ($admin_access->controller_subscription_attributes->can_edit) ? 1 : 0; ?>,
actions_delete : <?php echo ($admin_access->controller_subscription_attributes->can_delete) ? 1 : 0; ?> },

		<?php } ?>
		 },
		filters_data : {"s_plan_active": {"1": "Active"}},
	});
});
</script>

</div></div><!-- .row . col-lg-12 -->
<?php $this->load->view('common_footer'); ?>