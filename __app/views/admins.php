<?php $this->load->view('common_header'); ?>
<div class="row"><div class="col-lg-12">
	<h1 class="page-header"><?php echo $page_title; ?></h1>
	
	<div id="list-view-admins" class="list-view">
<div class="panel panel-default panel-admins">
<div class="panel-heading">

			<?php if( isset($admin_access->controller_admins->can_add) && ($admin_access->controller_admins->can_add == 1) ) { ?>
			<a href="javascript:void(0);" class="btn btn-default btn-sm pull-right" id="add-button-admins">Add Admin</a>
			<?php } ?>
<div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">
<div class="table-responsive">
<table id="dataTables" class="table table-striped table-bordered table-hover">
<thead>
<tr>
<th width='1%'></th>
<th width="">Name<span  data-key="name" data-table="admins" id="list_search_button_name" class="btn btn-primary btn-xs pull-right btn-search list-search-admins" title="Search Name">
		<i class="fa fa-search"></i></span></th><th width="">Email<span  data-key="email" data-table="admins" id="list_search_button_email" class="btn btn-primary btn-xs pull-right btn-search list-search-admins" title="Search Email">
		<i class="fa fa-search"></i></span></th><th width="">Username<span  data-key="username" data-table="admins" id="list_search_button_username" class="btn btn-primary btn-xs pull-right btn-search list-search-admins" title="Search Username">
		<i class="fa fa-search"></i></span></th><th width="130">Actions</th>
</tr>
</thead>
<tbody>
</tbody>
</table>
</div>
</div> <!-- .panel-body -->
</div> <!-- .panel .panel-admins -->
</div>
		<?php if( isset($admin_access->controller_admins->can_add) && ($admin_access->controller_admins->can_add == 1) ) { ?>
		<div id="add-view-admins" style="display:none">
<div class="panel panel-default add-panel-admins">
                        <div class="panel-heading"><h3 class="panel-title">Add Admin</h3><div class="clearfix"></div>
                        </div>
                        <!-- /.panel-heading -->
<div class="panel-body">

<div class="form-group">
<label for="add_admins_name">Name</label> 
<input data-type="text" type="text" name="name" id="add_admins_name" class="form-control add_admins_name admins-input  table-admins add-table-admins text text" placeholder="Name" value=""/>
</div>
<div class="form-group">
<label for="add_admins_email">Email</label> 
<input data-type="text" type="text" name="email" id="add_admins_email" class="form-control add_admins_email admins-input  table-admins add-table-admins text text" placeholder="Email" value=""/>
</div>
<div class="form-group">
<label for="add_admins_username">Username</label> 
<input data-type="text" type="text" name="username" id="add_admins_username" class="form-control add_admins_username admins-input  table-admins add-table-admins text text" placeholder="Username" value=""/>
</div>
<div class="form-group">
<label for="add_admins_password">Password</label>

<input data-type="password" type="password" name="password" id="add_admins_password" class="form-control add_admins_password admins-input  table-admins add-table-admins password text" placeholder="Password" value="" />
</div>
<div class="form-group">
<label for="add_admins_confirm_password">Confirm Password</label>

<input data-type="password" type="password" name="confirm_password" id="add_admins_confirm_password" class="form-control add_admins_confirm_password admins-input  table-admins add-table-admins password text" placeholder="Confirm Password" value="" />
</div>
<div class="form-group"><strong>Active</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="active" id="add_admins_active" class="add_admins_active admins-input  table-admins add-table-admins checkbox text" placeholder="Active" value="1" />Active</label></div></div>
</div> <!-- .panel-body -->

<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="add returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="add-action-admins">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm add-back-button" id="add-back-admins">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-admins -->
</div>
<?php } ?><?php if( isset($admin_access->controller_admins->can_edit) && ($admin_access->controller_admins->can_edit == 1) ) { ?>
		<div id="edit-view-admins" style="display:none">
		<ul class="nav nav-tabs">
<li>
		<a href="javascript:void(0);" class="update-back-admins" id="update-back-admins">
		<span class="glyphicon glyphicon-arrow-left"></span>
		</a>
		</li><li class="parent-tab active">
		<a class="tab-item" href="javascript:void(0);" data-table="admins" data-child="0">Edit Admin</a>
		</li>
			<?php if( isset($admin_access->controller_admins_access) ) { ?>
			<li>
			<a class="tab-item" href="javascript:void(0);"  data-table="admins_access" data-child="1">Access Restrictions</a>
			</li>
			<?php } ?>
</ul><br>
		<div class="tab-content tab-content-admins parent active"><div class="panel panel-default edit-panel-admins">
<div class="panel-heading">
	 <h3 class="panel-title">Edit Admin</h3>
	 <div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="id" id="edit_admins_id" class="edit_admins_id admins-input  table-admins edit-table-admins hidden text" placeholder="Admin ID" value="" />
<div class="form-group">
<label for="edit_admins_name">Name</label> 
<input data-type="text" type="text" name="name" id="edit_admins_name" class="form-control edit_admins_name admins-input  table-admins edit-table-admins text text" placeholder="Name" value=""/>
</div>
<div class="form-group change-deliberately button admins email">
<label>Email</label>
			<a href="javascript:void(0);" class="btn btn-xs btn-success pull-right update admins email" data-table="admins" data-field="email">Update Email</a>
			<p class="text-value admins email edit_admins_email admins-input  table-admins edit-table-admins text " data-type="text"></p>
			
<input data-type="text" type="hidden" name="email" id="edit_admins_email" class="edit_admins_email admins-input  table-admins edit-table-admins text " placeholder="Email" value="" /></div>
			<div class="panel panel-info change-deliberately form admins email" style="display:none;">
			<div class="panel-heading">
			<a href="javascript:void(0);" class="btn btn-xs btn-danger pull-right cancel admins email" data-table="admins" data-field="email">Cancel</a>
			Update Email</div>
			<div class="panel-body">
			
<div class="form-group">
<label for="edit_admins_email">Email</label> 
<input data-type="text" type="text" name="email" id="edit_admins_email" class="form-control edit_admins_email admins-input  table-admins edit-table-admins text " placeholder="Email" value=""/>
</div></div>
</div>

<div class="form-group change-deliberately button admins username">
<label>Username</label>
			<a href="javascript:void(0);" class="btn btn-xs btn-success pull-right update admins username" data-table="admins" data-field="username">Update Username</a>
			<p class="text-value admins username edit_admins_username admins-input  table-admins edit-table-admins text " data-type="text"></p>
			
<input data-type="text" type="hidden" name="username" id="edit_admins_username" class="edit_admins_username admins-input  table-admins edit-table-admins text " placeholder="Username" value="" /></div>
			<div class="panel panel-info change-deliberately form admins username" style="display:none;">
			<div class="panel-heading">
			<a href="javascript:void(0);" class="btn btn-xs btn-danger pull-right cancel admins username" data-table="admins" data-field="username">Cancel</a>
			Update Username</div>
			<div class="panel-body">
			
<div class="form-group">
<label for="edit_admins_username">Username</label> 
<input data-type="text" type="text" name="username" id="edit_admins_username" class="form-control edit_admins_username admins-input  table-admins edit-table-admins text " placeholder="Username" value=""/>
</div></div>
</div>

<div class="form-group change-deliberately button admins password">
<label>Password</label>
			<a href="javascript:void(0);" class="btn btn-xs btn-success pull-right update admins password" data-table="admins" data-field="password|confirm_password">Update Password</a>
			<p class="text-value admins password edit_admins_password admins-input  table-admins edit-table-admins password " data-type="password"></p>
			</div>
			<div class="panel panel-info change-deliberately form admins password" style="display:none;">
			<div class="panel-heading">
			<a href="javascript:void(0);" class="btn btn-xs btn-danger pull-right cancel admins password" data-table="admins" data-field="password">Cancel</a>
			Update Password</div>
			<div class="panel-body">
			
<div class="form-group">
<label for="edit_admins_password">Password</label>

<input data-type="password" type="password" name="password" id="edit_admins_password" class="form-control edit_admins_password admins-input  table-admins edit-table-admins password " placeholder="Password" value="" />
</div>
<div class="form-group">
<label for="edit_admins_confirm_password">Confirm Password</label>

<input data-type="password" type="password" name="confirm_password" id="edit_admins_confirm_password" class="form-control edit_admins_confirm_password admins-input  table-admins edit-table-admins password " placeholder="Confirm Password" value="" />
</div></div>
</div>

<div class="form-group"><strong>Active</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="active" id="edit_admins_active" class="edit_admins_active admins-input  table-admins edit-table-admins checkbox text" placeholder="Active" value="1" />Active</label></div></div>
<div class="form-group">
<label for="edit_admins_date_created">Date Created</label> 
<p class="text-value admins date_created" data-type="view_only"></p>
</div>
<div class="form-group">
<label for="edit_admins_last_login">Last Login</label> 
<p class="text-value admins last_login" data-type="view_only"></p>
</div>
<div class="form-group">
<label for="edit_admins_last_login_ip">Last Login IP Address</label> 
<p class="text-value admins last_login_ip" data-type="view_only"></p>
</div>
</div> <!-- .panel-body -->
<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="update-action-admins">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm update-back-admins" id="update-back-admins">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-admins -->
</div><!-- .tab-content .tab-content-admins -->
			<?php if( isset($admin_access->controller_admins_access) ) { ?>
			<div class="tab-content tab-content-admins_access" style="display:none"><div id="list-view-admins_access" class="list-view">
<div class="panel panel-default panel-admins_access">
<div class="panel-heading">

			<?php if( isset($admin_access->controller_admins_access->can_add) && ($admin_access->controller_admins_access->can_add == 1) ) { ?>
			<a href="javascript:void(0);" class="btn btn-default btn-sm pull-right" id="add-button-admins_access">Add Restrictions</a>
			<?php } ?>
<div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">
<div class="table-responsive">
<table id="dataTables" class="table table-striped table-bordered table-hover">
<thead>
<tr>

<th width="">Controller</th><th width="50">Can Add</th><th width="50">Can Edit</th><th width="50">Can Delete</th><th width="130">Actions</th>
</tr>
</thead>
<tbody>
</tbody>
</table>
</div>
</div> <!-- .panel-body -->
</div> <!-- .panel .panel-admins_access -->
</div>
		<?php if( isset($admin_access->controller_admins_access->can_add) && ($admin_access->controller_admins_access->can_add == 1) ) { ?>
		<div id="add-view-admins_access" style="display:none">
<div class="panel panel-default add-panel-admins_access">
                        <div class="panel-heading"><h3 class="panel-title">Add Restrictions</h3><div class="clearfix"></div>
                        </div>
                        <!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="admin_id" id="add_admins_access_admin_id" class="add_admins_access_admin_id admins_access-input  table-admins_access add-table-admins_access hidden text" placeholder="Admin" value="" />
<div class="form-group">
<label for="add_lessons_controller">Controller</label> 
			<select name="controller" id="add_admins_access_controller" class="selectpicker form-control add_admins_access_controller admins_access-input  table-admins_access add-table-admins_access dropdown text" placeholder="Controller" data-live-search="true" >
			<option value="">- - Select Controller - -</option>
<option value="admins">Admins</option>
<option value="admins_access">Admins Access</option>
<option value="attributes">Attributes</option>
<option value="tax_chapters">Chapters</option>
<option value="goals">Goals</option>
<option value="tax_levels">Grade Levels</option>
<option value="lessons_subscriptions">Lesson Subscriptions</option>
<option value="lessons_tags">Lesson Topics</option>
<option value="lessons">Lessons</option>
<option value="lessons_meta">Lessons Meta</option>
<option value="lessons_quiz">Lessons Quiz</option>
<option value="level_chapters">Level Chapters</option>
<option value="media_uploads">Media Uploads</option>
<option value="subscription_attributes">Membership Plan Attributes</option>
<option value="subscription_plans">Membership Plans</option>
<option value="settings">Settings</option>
<option value="tax_tags">Topics</option>
<option value="users">Users</option>
<option value="users_logs">Users Activities</option>
<option value="users_goals">Users Goals</option>
<option value="users_meta">Users Meta</option>
<option value="users_profiles">Users Profiles</option>
<option value="users_sponsors">Users Sponsors</option>
</select></div>
<div class="form-group"><strong>Can Add</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="add" id="add_admins_access_add" class="add_admins_access_add admins_access-input  table-admins_access add-table-admins_access checkbox text" placeholder="Can Add" value="1" />Enabled</label></div></div>
<div class="form-group"><strong>Can Edit</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="edit" id="add_admins_access_edit" class="add_admins_access_edit admins_access-input  table-admins_access add-table-admins_access checkbox text" placeholder="Can Edit" value="1" />Enabled</label></div></div>
<div class="form-group"><strong>Can Delete</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="delete" id="add_admins_access_delete" class="add_admins_access_delete admins_access-input  table-admins_access add-table-admins_access checkbox text" placeholder="Can Delete" value="1" />Enabled</label></div></div>
</div> <!-- .panel-body -->

<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="add returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="add-action-admins_access">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm add-back-button" id="add-back-admins_access">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-admins_access -->
</div>
<?php } ?><?php if( isset($admin_access->controller_admins_access->can_edit) && ($admin_access->controller_admins_access->can_edit == 1) ) { ?>
		<div id="edit-view-admins_access" style="display:none">
		
		<div class="panel panel-default edit-panel-admins_access">
<div class="panel-heading">
	 <h3 class="panel-title">Edit Restrictions</h3>
	 <div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="id" id="edit_admins_access_id" class="edit_admins_access_id admins_access-input  table-admins_access edit-table-admins_access hidden text" placeholder="Access ID" value="" />
<input data-type="hidden" type="hidden" name="admin_id" id="edit_admins_access_admin_id" class="edit_admins_access_admin_id admins_access-input  table-admins_access edit-table-admins_access hidden text" placeholder="Admin" value="" />
<div class="form-group">
<label for="add_lessons_controller">Controller</label> 
			<select name="controller" id="edit_admins_access_controller" class="selectpicker form-control edit_admins_access_controller admins_access-input  table-admins_access edit-table-admins_access dropdown text" placeholder="Controller" data-live-search="true" >
			<option value="">- - Select Controller - -</option>
<option value="admins">Admins</option>
<option value="admins_access">Admins Access</option>
<option value="attributes">Attributes</option>
<option value="tax_chapters">Chapters</option>
<option value="goals">Goals</option>
<option value="tax_levels">Grade Levels</option>
<option value="lessons_subscriptions">Lesson Subscriptions</option>
<option value="lessons_tags">Lesson Topics</option>
<option value="lessons">Lessons</option>
<option value="lessons_meta">Lessons Meta</option>
<option value="lessons_quiz">Lessons Quiz</option>
<option value="level_chapters">Level Chapters</option>
<option value="media_uploads">Media Uploads</option>
<option value="subscription_attributes">Membership Plan Attributes</option>
<option value="subscription_plans">Membership Plans</option>
<option value="settings">Settings</option>
<option value="tax_tags">Topics</option>
<option value="users">Users</option>
<option value="users_logs">Users Activities</option>
<option value="users_goals">Users Goals</option>
<option value="users_meta">Users Meta</option>
<option value="users_profiles">Users Profiles</option>
<option value="users_sponsors">Users Sponsors</option>
</select></div>
<div class="form-group"><strong>Can Add</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="add" id="edit_admins_access_add" class="edit_admins_access_add admins_access-input  table-admins_access edit-table-admins_access checkbox text" placeholder="Can Add" value="1" />Enabled</label></div></div>
<div class="form-group"><strong>Can Edit</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="edit" id="edit_admins_access_edit" class="edit_admins_access_edit admins_access-input  table-admins_access edit-table-admins_access checkbox text" placeholder="Can Edit" value="1" />Enabled</label></div></div>
<div class="form-group"><strong>Can Delete</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="delete" id="edit_admins_access_delete" class="edit_admins_access_delete admins_access-input  table-admins_access edit-table-admins_access checkbox text" placeholder="Can Delete" value="1" />Enabled</label></div></div>
</div> <!-- .panel-body -->
<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="update-action-admins_access">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm update-back-admins_access" id="update-back-admins_access">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-admins_access -->
</div>
<?php } ?></div><!-- .tab-content .tab-content-admins_access -->
			<?php } ?></div>
<?php } ?>
	
	<script>
jQuery(document).ready(function($) {
	$(document).AdminActions({
		baseURL : '<?php echo base_url(); ?>',
		current_table : 'admins',
		tables : { 
		<?php if( isset($admin_access->controller_admins) ) { ?>
		
'admins' : { label : 'Admin',
fields : ["id","name","email","username","password","confirm_password","active","date_created","last_login","last_login_ip"],
add_fields : ["name","email","username","password","confirm_password","active"],
edit_fields : ["id","name","email","username","active"],
list_limit : 20,
list_fields : ["name","email","username"],
order_by : 'id',
order_sort : 'DESC',
primary_key : 'id',
primary_title : 'username',
active_key : 'active',
actions_edit : <?php echo ($admin_access->controller_admins->can_edit) ? 1 : 0; ?>,
actions_delete : <?php echo ($admin_access->controller_admins->can_delete) ? 1 : 0; ?> },

		<?php } ?>
		
		<?php if( isset($admin_access->controller_admins_access) ) { ?>
		
'admins_access' : { label : 'Admin Access',
fields : ["id","admin_id","controller","add","edit","delete"],
add_fields : ["admin_id","controller","add","edit","delete"],
edit_fields : ["id","admin_id","controller","add","edit","delete"],
list_limit : 20,
list_fields : ["controller","add","edit","delete"],
order_by : 'controller',
order_sort : 'ASC',
filters : {"controller":{"type":"manual","anchor":0}},
primary_key : 'id',
actual_values : {"admin_id" : "username_c"},
check_cross : ["add","edit","delete"],
required_key : 'id',
required_value : 'admin_id',
required_table : 'ci_admins_c',
actions_edit : <?php echo ($admin_access->controller_admins_access->can_edit) ? 1 : 0; ?>,
actions_delete : <?php echo ($admin_access->controller_admins_access->can_delete) ? 1 : 0; ?> },

		<?php } ?>
		 },
		filters_data : {"active": {"1": "Active"}, "edit": {"1": "Enabled"}, "controller": {"admins_access": "Admins Access", "users_logs": "Users Activities", "level_chapters": "Level Chapters", "lessons_quiz": "Lessons Quiz", "tax_levels": "Grade Levels", "users": "Users", "lessons": "Lessons", "tax_chapters": "Chapters", "users_meta": "Users Meta", "tax_tags": "Topics", "lessons_tags": "Lesson Topics", "subscription_attributes": "Membership Plan Attributes", "users_profiles": "Users Profiles", "users_sponsors": "Users Sponsors", "lessons_subscriptions": "Lesson Subscriptions", "lessons_meta": "Lessons Meta", "goals": "Goals", "media_uploads": "Media Uploads", "users_goals": "Users Goals", "settings": "Settings", "admins": "Admins", "subscription_plans": "Membership Plans", "attributes": "Attributes"}, "add": {"1": "Enabled"}, "delete": {"1": "Enabled"}},
	});
});
</script>

</div></div><!-- .row . col-lg-12 -->
<?php $this->load->view('common_footer'); ?>