<?php $this->load->view('common_header'); ?>
<div class="row"><div class="col-lg-12">
	<h1 class="page-header"><?php echo $page_title; ?></h1>
	
	<div id="list-view-lessons_quiz" class="list-view">
<div class="panel panel-default panel-lessons_quiz">
<div class="panel-heading">

			<?php if( isset($admin_access->controller_lessons_quiz->can_add) && ($admin_access->controller_lessons_quiz->can_add == 1) ) { ?>
			<a href="javascript:void(0);" class="btn btn-default btn-sm pull-right" id="add-button-lessons_quiz">Add Lesson Quiz</a>
			<?php } ?>
<div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">
<div class="table-responsive">
<table id="dataTables" class="table table-striped table-bordered table-hover">
<thead>
<tr>

<th width="">Lesson</th><th width="None">Number</th><th width="None">Time</th><th width="">Question<span  data-key="quiz_question" data-table="lessons_quiz" id="list_search_button_quiz_question" class="btn btn-primary btn-xs pull-right btn-search list-search-lessons_quiz" title="Search Question">
		<i class="fa fa-search"></i></span></th><th width="130">Actions</th>
</tr>
</thead>
<tbody>
</tbody>
</table>
</div>
</div> <!-- .panel-body -->
</div> <!-- .panel .panel-lessons_quiz -->
</div>
		<?php if( isset($admin_access->controller_lessons_quiz->can_add) && ($admin_access->controller_lessons_quiz->can_add == 1) ) { ?>
		<div id="add-view-lessons_quiz" style="display:none">
<div class="panel panel-default add-panel-lessons_quiz">
                        <div class="panel-heading"><h3 class="panel-title">Add Lesson Quiz</h3><div class="clearfix"></div>
                        </div>
                        <!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="quiz_id" id="add_lessons_quiz_quiz_id" class="add_lessons_quiz_quiz_id lessons_quiz-input  table-lessons_quiz add-table-lessons_quiz hidden text" placeholder="ID" value="" />
<div class="form-group">
<label for="add_lessons_quiz_lesson_id">Lesson</label> 
<input data-type="text" type="hidden" name="lesson_id" id="add_lessons_quiz_lesson_id" class="form-control add_lessons_quiz_lesson_id lessons_quiz-input  table-lessons_quiz add-table-lessons_quiz text text text-searchable-key-lesson_id  add text-searchable-key" />
<a href="javascript:void(0)" data-field="lesson_id"  data-table="lessons" data-key="lesson_id" data-value="lesson_title" data-display="lesson_title" data-action="add"  class="text-searchable-list lesson_id" data-toggle="modal" data-target="#add-text-searchable-box-lesson_id"><span class="glyphicon glyphicon-list"></span></a>
				<input data-type="text" type="text" name="lesson_id" class="form-control add text-searchable lesson_id" placeholder="Search Lesson" data-field="lesson_id"  data-table="lessons" data-key="lesson_id" data-value="lesson_title" data-display="lesson_title" data-action="add" />
				<div class="modal fade add" id="add-text-searchable-box-lesson_id" tabindex="-1" role="dialog" aria-labelledby="Lesson" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Lesson List</h4>
      </div>
      <div class="modal-body">
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
				
</div>
<div class="form-group">
<label for="add_lessons_quiz_quiz_number">Number</label> 
<input data-type="text" type="text" name="quiz_number" id="add_lessons_quiz_quiz_number" class="form-control add_lessons_quiz_quiz_number lessons_quiz-input  table-lessons_quiz add-table-lessons_quiz text text" placeholder="Number" value=""/>
</div>
<div class="form-group">
<label for="add_lessons_quiz_quiz_time">Time</label> 
<input data-type="text" type="text" name="quiz_time" id="add_lessons_quiz_quiz_time" class="form-control add_lessons_quiz_quiz_time lessons_quiz-input  table-lessons_quiz add-table-lessons_quiz text text" placeholder="Time" value=""/>
</div>
<div class="form-group">
<label for="add_lessons_quiz_quiz_question">Question</label> 
<input data-type="text" type="text" name="quiz_question" id="add_lessons_quiz_quiz_question" class="form-control add_lessons_quiz_quiz_question lessons_quiz-input  table-lessons_quiz add-table-lessons_quiz text text" placeholder="Question" value=""/>
</div>
<div class="form-group">
<label for="add_lessons_quiz_type">Type</label> 
			<select name="quiz_type" id="add_lessons_quiz_quiz_type" class="selectpicker form-control add_lessons_quiz_quiz_type lessons_quiz-input  table-lessons_quiz add-table-lessons_quiz dropdown text" placeholder="Type" data-live-search="true" >
			<option value="">- - Select Type - -</option>
<option value="multiple-choice">Multiple Choice</option>
</select></div>
<div class="form-group">
<label for="add_lessons_quiz_quiz_details">Details</label>
<textarea rows="5" data-type="textarea" data-wysiwyg="0" type="text" name="quiz_details" id="add_lessons_quiz_quiz_details" class="form-control add_lessons_quiz_quiz_details lessons_quiz-input  table-lessons_quiz add-table-lessons_quiz textarea text" placeholder="Details" value="" /></textarea>
</div>
<div class="form-group">
<label for="add_lessons_quiz_quiz_correct">Correct Answer</label> 
<input data-type="text" type="text" name="quiz_correct" id="add_lessons_quiz_quiz_correct" class="form-control add_lessons_quiz_quiz_correct lessons_quiz-input  table-lessons_quiz add-table-lessons_quiz text text" placeholder="Correct Answer" value=""/>
</div>
</div> <!-- .panel-body -->

<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="add returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="add-action-lessons_quiz">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm add-back-button" id="add-back-lessons_quiz">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-lessons_quiz -->
</div>
<?php } ?><?php if( isset($admin_access->controller_lessons_quiz->can_edit) && ($admin_access->controller_lessons_quiz->can_edit == 1) ) { ?>
		<div id="edit-view-lessons_quiz" style="display:none">
		
		<div class="tab-content tab-content-lessons_quiz parent active"><div class="panel panel-default edit-panel-lessons_quiz">
<div class="panel-heading">
	 <h3 class="panel-title">Edit Lesson Quiz</h3>
	 <div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="quiz_id" id="edit_lessons_quiz_quiz_id" class="edit_lessons_quiz_quiz_id lessons_quiz-input  table-lessons_quiz edit-table-lessons_quiz hidden text" placeholder="ID" value="" />
<div class="form-group">
<label for="edit_lessons_quiz_lesson_id">Lesson</label> 
<input data-type="text" type="hidden" name="lesson_id" id="edit_lessons_quiz_lesson_id" class="form-control edit_lessons_quiz_lesson_id lessons_quiz-input  table-lessons_quiz edit-table-lessons_quiz text text text-searchable-key-lesson_id  edit text-searchable-key" />
<a href="javascript:void(0)" data-field="lesson_id"  data-table="lessons" data-key="lesson_id" data-value="lesson_title" data-display="lesson_title" data-action="edit"  class="text-searchable-list lesson_id" data-toggle="modal" data-target="#edit-text-searchable-box-lesson_id"><span class="glyphicon glyphicon-list"></span></a>
				<input data-type="text" type="text" name="lesson_id" class="form-control edit text-searchable lesson_id" placeholder="Search Lesson" data-field="lesson_id"  data-table="lessons" data-key="lesson_id" data-value="lesson_title" data-display="lesson_title" data-action="edit" />
				<div class="modal fade edit" id="edit-text-searchable-box-lesson_id" tabindex="-1" role="dialog" aria-labelledby="Lesson" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Lesson List</h4>
      </div>
      <div class="modal-body">
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
				
</div>
<div class="form-group">
<label for="edit_lessons_quiz_quiz_number">Number</label> 
<input data-type="text" type="text" name="quiz_number" id="edit_lessons_quiz_quiz_number" class="form-control edit_lessons_quiz_quiz_number lessons_quiz-input  table-lessons_quiz edit-table-lessons_quiz text text" placeholder="Number" value=""/>
</div>
<div class="form-group">
<label for="edit_lessons_quiz_quiz_time">Time</label> 
<input data-type="text" type="text" name="quiz_time" id="edit_lessons_quiz_quiz_time" class="form-control edit_lessons_quiz_quiz_time lessons_quiz-input  table-lessons_quiz edit-table-lessons_quiz text text" placeholder="Time" value=""/>
</div>
<div class="form-group">
<label for="edit_lessons_quiz_quiz_question">Question</label> 
<input data-type="text" type="text" name="quiz_question" id="edit_lessons_quiz_quiz_question" class="form-control edit_lessons_quiz_quiz_question lessons_quiz-input  table-lessons_quiz edit-table-lessons_quiz text text" placeholder="Question" value=""/>
</div>
<div class="form-group">
<label for="add_lessons_quiz_type">Type</label> 
			<select name="quiz_type" id="edit_lessons_quiz_quiz_type" class="selectpicker form-control edit_lessons_quiz_quiz_type lessons_quiz-input  table-lessons_quiz edit-table-lessons_quiz dropdown text" placeholder="Type" data-live-search="true" >
			<option value="">- - Select Type - -</option>
<option value="multiple-choice">Multiple Choice</option>
</select></div>
<div class="form-group">
<label for="edit_lessons_quiz_quiz_details">Details</label>
<textarea rows="5" data-type="textarea" data-wysiwyg="0" type="text" name="quiz_details" id="edit_lessons_quiz_quiz_details" class="form-control edit_lessons_quiz_quiz_details lessons_quiz-input  table-lessons_quiz edit-table-lessons_quiz textarea text" placeholder="Details" value="" /></textarea>
</div>
<div class="form-group">
<label for="edit_lessons_quiz_quiz_correct">Correct Answer</label> 
<input data-type="text" type="text" name="quiz_correct" id="edit_lessons_quiz_quiz_correct" class="form-control edit_lessons_quiz_quiz_correct lessons_quiz-input  table-lessons_quiz edit-table-lessons_quiz text text" placeholder="Correct Answer" value=""/>
</div>
</div> <!-- .panel-body -->
<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="update-action-lessons_quiz">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm update-back-lessons_quiz" id="update-back-lessons_quiz">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-lessons_quiz -->
</div><!-- .tab-content .tab-content-lessons_quiz --></div>
<?php } ?>
	
	<script>
jQuery(document).ready(function($) {
	$(document).AdminActions({
		baseURL : '<?php echo base_url(); ?>',
		current_table : 'lessons_quiz',
		tables : { 
		<?php if( isset($admin_access->controller_lessons_quiz) ) { ?>
		
'lessons_quiz' : { label : 'Lesson Quiz',
fields : ["quiz_id","lesson_id","quiz_number","quiz_time","quiz_question","quiz_type","quiz_details","quiz_correct"],
add_fields : ["quiz_id","lesson_id","quiz_number","quiz_time","quiz_question","quiz_type","quiz_details","quiz_correct"],
edit_fields : ["quiz_id","lesson_id","quiz_number","quiz_time","quiz_question","quiz_type","quiz_details","quiz_correct"],
list_limit : 20,
list_fields : ["lesson_title","quiz_number","quiz_time","quiz_question"],
order_by : 'quiz_time',
order_sort : 'ASC',
primary_key : 'quiz_id',
primary_title : 'quiz_question',
actual_values : {"lesson_id" : "lesson_title"},
actions_edit : <?php echo ($admin_access->controller_lessons_quiz->can_edit) ? 1 : 0; ?>,
actions_delete : <?php echo ($admin_access->controller_lessons_quiz->can_delete) ? 1 : 0; ?> },

		<?php } ?>
		 },
		filters_data : {"quiz_type": {"multiple-choice": "Multiple Choice"}},
	});
});
</script>

</div></div><!-- .row . col-lg-12 -->
<?php $this->load->view('common_footer'); ?>