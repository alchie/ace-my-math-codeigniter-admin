<?php $this->load->view('common_header'); ?>
<div class="row"><div class="col-lg-12">
	<h1 class="page-header"><?php echo $page_title; ?></h1>
	
	<div id="list-view-tax_tags" class="list-view">
<div class="panel panel-default panel-tax_tags">
<div class="panel-heading">

			<?php if( isset($admin_access->controller_tax_tags->can_add) && ($admin_access->controller_tax_tags->can_add == 1) ) { ?>
			<a href="javascript:void(0);" class="btn btn-default btn-sm pull-right" id="add-button-tax_tags">Add Lesson Topic</a>
			<?php } ?>
<div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">
<div class="table-responsive">
<table id="dataTables" class="table table-striped table-bordered table-hover">
<thead>
<tr>
<th width='1%'></th>
<th width="50px">ID</th><th width="">Topic Name<span  data-key="tag_name" data-table="tax_tags" id="list_search_button_tag_name" class="btn btn-primary btn-xs pull-right btn-search list-search-tax_tags" title="Search Topic Name">
		<i class="fa fa-search"></i></span></th><th width="130">Actions</th>
</tr>
</thead>
<tbody>
</tbody>
</table>
</div>
</div> <!-- .panel-body -->
</div> <!-- .panel .panel-tax_tags -->
</div>
		<?php if( isset($admin_access->controller_tax_tags->can_add) && ($admin_access->controller_tax_tags->can_add == 1) ) { ?>
		<div id="add-view-tax_tags" style="display:none">
<div class="panel panel-default add-panel-tax_tags">
                        <div class="panel-heading"><h3 class="panel-title">Add Lesson Topic</h3><div class="clearfix"></div>
                        </div>
                        <!-- /.panel-heading -->
<div class="panel-body">

<div class="form-group">
<label for="add_tax_tags_tag_name">Topic Name</label> 
<input data-type="text" type="text" name="tag_name" id="add_tax_tags_tag_name" class="form-control add_tax_tags_tag_name tax_tags-input  table-tax_tags add-table-tax_tags text text" placeholder="Topic Name" value=""/>
</div>
<div class="form-group">
<label for="add_tax_tags_tag_count">Count</label> 
<input data-type="text" type="text" name="tag_count" id="add_tax_tags_tag_count" class="form-control add_tax_tags_tag_count tax_tags-input  table-tax_tags add-table-tax_tags text text" placeholder="Count" value=""/>
</div>
<div class="form-group"><strong>Active</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="tag_active" id="add_tax_tags_tag_active" class="add_tax_tags_tag_active tax_tags-input  table-tax_tags add-table-tax_tags checkbox text" placeholder="Active" value="1" />Active</label></div></div>
</div> <!-- .panel-body -->

<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="add returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="add-action-tax_tags">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm add-back-button" id="add-back-tax_tags">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-tax_tags -->
</div>
<?php } ?><?php if( isset($admin_access->controller_tax_tags->can_edit) && ($admin_access->controller_tax_tags->can_edit == 1) ) { ?>
		<div id="edit-view-tax_tags" style="display:none">
		<ul class="nav nav-tabs">
<li>
		<a href="javascript:void(0);" class="update-back-tax_tags" id="update-back-tax_tags">
		<span class="glyphicon glyphicon-arrow-left"></span>
		</a>
		</li><li class="parent-tab active">
		<a class="tab-item" href="javascript:void(0);" data-table="tax_tags" data-child="0">Edit Lesson Topic</a>
		</li>
			<?php if( isset($admin_access->controller_lessons_tags) ) { ?>
			<li>
			<a class="tab-item" href="javascript:void(0);"  data-table="lessons_tags" data-child="1">Lessons</a>
			</li>
			<?php } ?>
</ul><br>
		<div class="tab-content tab-content-tax_tags parent active"><div class="panel panel-default edit-panel-tax_tags">
<div class="panel-heading">
	 <h3 class="panel-title">Edit Lesson Topic</h3>
	 <div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="tag_id" id="edit_tax_tags_tag_id" class="edit_tax_tags_tag_id tax_tags-input  table-tax_tags edit-table-tax_tags hidden text" placeholder="ID" value="" />
<div class="form-group">
<label for="edit_tax_tags_tag_name">Topic Name</label> 
<input data-type="text" type="text" name="tag_name" id="edit_tax_tags_tag_name" class="form-control edit_tax_tags_tag_name tax_tags-input  table-tax_tags edit-table-tax_tags text text" placeholder="Topic Name" value=""/>
</div>
<div class="form-group">
<label for="edit_tax_tags_tag_slug">Slug</label> 
<input data-type="text" type="text" name="tag_slug" id="edit_tax_tags_tag_slug" class="form-control edit_tax_tags_tag_slug tax_tags-input  table-tax_tags edit-table-tax_tags text text" placeholder="Slug" value=""/>
</div>
<div class="form-group">
<label for="edit_tax_tags_tag_count">Count</label> 
<input data-type="text" type="text" name="tag_count" id="edit_tax_tags_tag_count" class="form-control edit_tax_tags_tag_count tax_tags-input  table-tax_tags edit-table-tax_tags text text" placeholder="Count" value=""/>
</div>
<div class="form-group"><strong>Active</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="tag_active" id="edit_tax_tags_tag_active" class="edit_tax_tags_tag_active tax_tags-input  table-tax_tags edit-table-tax_tags checkbox text" placeholder="Active" value="1" />Active</label></div></div>
</div> <!-- .panel-body -->
<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="update-action-tax_tags">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm update-back-tax_tags" id="update-back-tax_tags">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-tax_tags -->
</div><!-- .tab-content .tab-content-tax_tags -->
			<?php if( isset($admin_access->controller_lessons_tags) ) { ?>
			<div class="tab-content tab-content-lessons_tags" style="display:none"><div id="list-view-lessons_tags" class="list-view">
<div class="panel panel-default panel-lessons_tags">
<div class="panel-heading">

			<?php if( isset($admin_access->controller_lessons_tags->can_add) && ($admin_access->controller_lessons_tags->can_add == 1) ) { ?>
			<a href="javascript:void(0);" class="btn btn-default btn-sm pull-right" id="add-button-lessons_tags">Add Lesson</a>
			<?php } ?>
<div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">
<div class="table-responsive">
<table id="dataTables" class="table table-striped table-bordered table-hover">
<thead>
<tr>
<th width='1%'></th>
<th width="">Lesson<span data-linked='ci_lessons' data-key="lesson_title" data-table="lessons_tags" id="list_search_button_lesson_title" class="btn btn-primary btn-xs pull-right btn-search list-search-lessons_tags" title="Search Lesson">
		<i class="fa fa-search"></i></span></th><th width="66">Action</th>
</tr>
</thead>
<tbody>
</tbody>
</table>
</div>
</div> <!-- .panel-body -->
</div> <!-- .panel .panel-lessons_tags -->
</div>
		<?php if( isset($admin_access->controller_lessons_tags->can_add) && ($admin_access->controller_lessons_tags->can_add == 1) ) { ?>
		<div id="add-view-lessons_tags" style="display:none">
<div class="panel panel-default add-panel-lessons_tags">
                        <div class="panel-heading"><h3 class="panel-title">Add Lesson</h3><div class="clearfix"></div>
                        </div>
                        <!-- /.panel-heading -->
<div class="panel-body">

<div class="form-group">
<label for="add_lessons_tags_lesson_id">Lesson</label> 
<input data-type="text" type="hidden" name="lesson_id" id="add_lessons_tags_lesson_id" class="form-control add_lessons_tags_lesson_id lessons_tags-input  table-lessons_tags add-table-lessons_tags text text text-searchable-key-lesson_id  add text-searchable-key" />
<a href="javascript:void(0)" data-field="lesson_id"  data-table="lessons" data-key="lesson_id" data-value="lesson_title" data-display="lesson_title" data-action="add"  class="text-searchable-list lesson_id" data-toggle="modal" data-target="#add-text-searchable-box-lesson_id"><span class="glyphicon glyphicon-list"></span></a>
				<input data-type="text" type="text" name="lesson_id" class="form-control add text-searchable lesson_id" placeholder="Search Lesson" data-field="lesson_id"  data-table="lessons" data-key="lesson_id" data-value="lesson_title" data-display="lesson_title" data-action="add" />
				<div class="modal fade add" id="add-text-searchable-box-lesson_id" tabindex="-1" role="dialog" aria-labelledby="Lesson" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Lesson List</h4>
      </div>
      <div class="modal-body">
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
				
</div>
<input data-type="hidden" type="hidden" name="tag_id" id="add_lessons_tags_tag_id" class="add_lessons_tags_tag_id lessons_tags-input  table-lessons_tags add-table-lessons_tags hidden text" placeholder="Topic" value="" />
<div class="form-group"><strong>Active</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="active" id="add_lessons_tags_active" class="add_lessons_tags_active lessons_tags-input  table-lessons_tags add-table-lessons_tags checkbox text" placeholder="Active" value="1" />Active</label></div></div>
</div> <!-- .panel-body -->

<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="add returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="add-action-lessons_tags">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm add-back-button" id="add-back-lessons_tags">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-lessons_tags -->
</div>
<?php } ?><?php if( isset($admin_access->controller_lessons_tags->can_edit) && ($admin_access->controller_lessons_tags->can_edit == 1) ) { ?>
		<div id="edit-view-lessons_tags" style="display:none">
		
		<div class="panel panel-default edit-panel-lessons_tags">
<div class="panel-heading">
	 <h3 class="panel-title">Edit Lesson</h3>
	 <div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="id" id="edit_lessons_tags_id" class="edit_lessons_tags_id lessons_tags-input  table-lessons_tags edit-table-lessons_tags hidden text" placeholder="" value="" />
<div class="form-group">
<label for="edit_lessons_tags_lesson_id">Lesson</label> 
<input data-type="text" type="hidden" name="lesson_id" id="edit_lessons_tags_lesson_id" class="form-control edit_lessons_tags_lesson_id lessons_tags-input  table-lessons_tags edit-table-lessons_tags text text text-searchable-key-lesson_id  edit text-searchable-key" />
<a href="javascript:void(0)" data-field="lesson_id"  data-table="lessons" data-key="lesson_id" data-value="lesson_title" data-display="lesson_title" data-action="edit"  class="text-searchable-list lesson_id" data-toggle="modal" data-target="#edit-text-searchable-box-lesson_id"><span class="glyphicon glyphicon-list"></span></a>
				<input data-type="text" type="text" name="lesson_id" class="form-control edit text-searchable lesson_id" placeholder="Search Lesson" data-field="lesson_id"  data-table="lessons" data-key="lesson_id" data-value="lesson_title" data-display="lesson_title" data-action="edit" />
				<div class="modal fade edit" id="edit-text-searchable-box-lesson_id" tabindex="-1" role="dialog" aria-labelledby="Lesson" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Lesson List</h4>
      </div>
      <div class="modal-body">
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
				
</div>
<input data-type="hidden" type="hidden" name="tag_id" id="edit_lessons_tags_tag_id" class="edit_lessons_tags_tag_id lessons_tags-input  table-lessons_tags edit-table-lessons_tags hidden text" placeholder="Topic" value="" />
<div class="form-group"><strong>Active</strong>
<div class="checkbox">
<label>
<input data-type="checkbox" type="checkbox" name="active" id="edit_lessons_tags_active" class="edit_lessons_tags_active lessons_tags-input  table-lessons_tags edit-table-lessons_tags checkbox text" placeholder="Active" value="1" />Active</label></div></div>
</div> <!-- .panel-body -->
<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="update-action-lessons_tags">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm update-back-lessons_tags" id="update-back-lessons_tags">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-lessons_tags -->
</div>
<?php } ?></div><!-- .tab-content .tab-content-lessons_tags -->
			<?php } ?></div>
<?php } ?>
	
	<script>
jQuery(document).ready(function($) {
	$(document).AdminActions({
		baseURL : '<?php echo base_url(); ?>',
		current_table : 'tax_tags',
		tables : { 
		<?php if( isset($admin_access->controller_tax_tags) ) { ?>
		
'tax_tags' : { label : 'Lesson Topic',
fields : ["tag_id","tag_name","tag_slug","tag_count","tag_active"],
add_fields : ["tag_name","tag_count","tag_active"],
edit_fields : ["tag_id","tag_name","tag_slug","tag_count","tag_active"],
list_limit : 20,
list_fields : ["tag_id","tag_name"],
order_by : 'tag_name',
order_sort : 'ASC',
primary_key : 'tag_id',
primary_title : 'tag_name',
active_key : 'tag_active',
actions_edit : <?php echo ($admin_access->controller_tax_tags->can_edit) ? 1 : 0; ?>,
actions_delete : <?php echo ($admin_access->controller_tax_tags->can_delete) ? 1 : 0; ?> },

		<?php } ?>
		
		<?php if( isset($admin_access->controller_lessons_tags) ) { ?>
		
'lessons_tags' : { label : 'Lesson Tag',
fields : ["id","lesson_id","tag_id","active"],
add_fields : ["lesson_id","tag_id","active"],
edit_fields : ["id","lesson_id","tag_id","active"],
list_limit : 20,
list_fields : ["lesson_title"],
order_by : 'id',
order_sort : 'DESC',
primary_key : 'id',
active_key : 'active',
actual_values : {"lesson_id" : "lesson_title","tag_id" : "tag_name"},
required_key : 'tag_id',
required_value : 'tag_id',
actions_delete : <?php echo ($admin_access->controller_lessons_tags->can_delete) ? 1 : 0; ?> },

		<?php } ?>
		 },
		filters_data : {"active": {"1": "Active"}, "tag_active": {"1": "Active"}},
	});
});
</script>

</div></div><!-- .row . col-lg-12 -->
<?php $this->load->view('common_footer'); ?>