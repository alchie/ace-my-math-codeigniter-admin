<?php $this->load->view('common_header'); ?>
<div class="row"><div class="col-lg-12">
	<h1 class="page-header"><?php echo $page_title; ?></h1>
	
	<div id="list-view-settings" class="list-view">
<div class="panel panel-default panel-settings">
<div class="panel-heading">

			<?php if( isset($admin_access->controller_settings->can_add) && ($admin_access->controller_settings->can_add == 1) ) { ?>
			<a href="javascript:void(0);" class="btn btn-default btn-sm pull-right" id="add-button-settings">Add Setting</a>
			<?php } ?>
<div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">
<div class="table-responsive">
<table id="dataTables" class="table table-striped table-bordered table-hover">
<thead>
<tr>

<th width=""><div class="dropdown-filter"><a href="javascript:void(0);" data-filter="setting_controller" data-table="settings">Controller <span class="glyphicon glyphicon-chevron-down pull-right"></span></a></div></th><th width="">Name<span  data-key="setting_name" data-table="settings" id="list_search_button_setting_name" class="btn btn-primary btn-xs pull-right btn-search list-search-settings" title="Search Name">
		<i class="fa fa-search"></i></span></th><th width="">Description<span  data-key="setting_description" data-table="settings" id="list_search_button_setting_description" class="btn btn-primary btn-xs pull-right btn-search list-search-settings" title="Search Description">
		<i class="fa fa-search"></i></span></th><th width="">Value<span  data-key="setting_value" data-table="settings" id="list_search_button_setting_value" class="btn btn-primary btn-xs pull-right btn-search list-search-settings" title="Search Value">
		<i class="fa fa-search"></i></span></th><th width="130">Actions</th>
</tr>
</thead>
<tbody>
</tbody>
</table>
</div>
</div> <!-- .panel-body -->
</div> <!-- .panel .panel-settings -->
</div>
		<?php if( isset($admin_access->controller_settings->can_add) && ($admin_access->controller_settings->can_add == 1) ) { ?>
		<div id="add-view-settings" style="display:none">
<div class="panel panel-default add-panel-settings">
                        <div class="panel-heading"><h3 class="panel-title">Add Setting</h3><div class="clearfix"></div>
                        </div>
                        <!-- /.panel-heading -->
<div class="panel-body">

<div class="form-group">
<label for="add_lessons_setting_controller">Controller</label> 
			<select name="setting_controller" id="add_settings_setting_controller" class="selectpicker form-control add_settings_setting_controller settings-input  table-settings add-table-settings dropdown text" placeholder="Controller" data-live-search="true" >
			<option value="">- - Select Controller - -</option>
<option value="site">Website</option>
</select></div>
<div class="form-group">
<label for="add_settings_setting_name">Name</label> 
<input data-type="text" type="text" name="setting_name" id="add_settings_setting_name" class="form-control add_settings_setting_name settings-input  table-settings add-table-settings text text" placeholder="Name" value=""/>
</div>
<div class="form-group">
<label for="add_settings_setting_description">Description</label> 
<input data-type="text" type="text" name="setting_description" id="add_settings_setting_description" class="form-control add_settings_setting_description settings-input  table-settings add-table-settings text text" placeholder="Description" value=""/>
</div>
<div class="form-group">
<label for="add_settings_setting_value">Value</label>
<textarea rows="5" data-type="textarea" data-wysiwyg="0" type="text" name="setting_value" id="add_settings_setting_value" class="form-control add_settings_setting_value settings-input  table-settings add-table-settings textarea text" placeholder="Value" value="" /></textarea>
</div>
</div> <!-- .panel-body -->

<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="add returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="add-action-settings">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm add-back-button" id="add-back-settings">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-settings -->
</div>
<?php } ?><?php if( isset($admin_access->controller_settings->can_edit) && ($admin_access->controller_settings->can_edit == 1) ) { ?>
		<div id="edit-view-settings" style="display:none">
		
		<div class="tab-content tab-content-settings parent active"><div class="panel panel-default edit-panel-settings">
<div class="panel-heading">
	 <h3 class="panel-title">Edit Setting</h3>
	 <div class="clearfix"></div>
</div>
<!-- /.panel-heading -->
<div class="panel-body">

<input data-type="hidden" type="hidden" name="setting_id" id="edit_settings_setting_id" class="edit_settings_setting_id settings-input  table-settings edit-table-settings hidden text" placeholder="Setting ID" value="" />
<div class="form-group change-deliberately button settings setting_controller">
<label>Controller</label>
			<a href="javascript:void(0);" class="btn btn-xs btn-success pull-right update settings setting_controller" data-table="settings" data-field="setting_controller">Update Controller</a>
			<p class="text-value settings setting_controller edit_settings_setting_controller settings-input  table-settings edit-table-settings dropdown " data-type="dropdown"></p>
			
<input data-type="dropdown" type="hidden" name="setting_controller" id="edit_settings_setting_controller" class="edit_settings_setting_controller settings-input  table-settings edit-table-settings dropdown " placeholder="Controller" value="" /></div>
			<div class="panel panel-info change-deliberately form settings setting_controller" style="display:none;">
			<div class="panel-heading">
			<a href="javascript:void(0);" class="btn btn-xs btn-danger pull-right cancel settings setting_controller" data-table="settings" data-field="setting_controller">Cancel</a>
			Update Controller</div>
			<div class="panel-body">
			
<div class="form-group">
<label for="add_lessons_setting_controller">Controller</label> 
			<select name="setting_controller" id="edit_settings_setting_controller" class="selectpicker form-control edit_settings_setting_controller settings-input  table-settings edit-table-settings dropdown " placeholder="Controller" data-live-search="true" >
			<option value="">- - Select Controller - -</option>
<option value="site">Website</option>
</select></div></div>
</div>

<div class="form-group change-deliberately button settings setting_name">
<label>Name</label>
			<a href="javascript:void(0);" class="btn btn-xs btn-success pull-right update settings setting_name" data-table="settings" data-field="setting_name">Update Name</a>
			<p class="text-value settings setting_name edit_settings_setting_name settings-input  table-settings edit-table-settings text " data-type="text"></p>
			
<input data-type="text" type="hidden" name="setting_name" id="edit_settings_setting_name" class="edit_settings_setting_name settings-input  table-settings edit-table-settings text " placeholder="Name" value="" /></div>
			<div class="panel panel-info change-deliberately form settings setting_name" style="display:none;">
			<div class="panel-heading">
			<a href="javascript:void(0);" class="btn btn-xs btn-danger pull-right cancel settings setting_name" data-table="settings" data-field="setting_name">Cancel</a>
			Update Name</div>
			<div class="panel-body">
			
<div class="form-group">
<label for="edit_settings_setting_name">Name</label> 
<input data-type="text" type="text" name="setting_name" id="edit_settings_setting_name" class="form-control edit_settings_setting_name settings-input  table-settings edit-table-settings text " placeholder="Name" value=""/>
</div></div>
</div>

<div class="form-group change-deliberately button settings setting_description">
<label>Description</label>
			<a href="javascript:void(0);" class="btn btn-xs btn-success pull-right update settings setting_description" data-table="settings" data-field="setting_description">Update Description</a>
			<p class="text-value settings setting_description edit_settings_setting_description settings-input  table-settings edit-table-settings text " data-type="text"></p>
			</div>
			<div class="panel panel-info change-deliberately form settings setting_description" style="display:none;">
			<div class="panel-heading">
			<a href="javascript:void(0);" class="btn btn-xs btn-danger pull-right cancel settings setting_description" data-table="settings" data-field="setting_description">Cancel</a>
			Update Description</div>
			<div class="panel-body">
			
<div class="form-group">
<label for="edit_settings_setting_description">Description</label> 
<input data-type="text" type="text" name="setting_description" id="edit_settings_setting_description" class="form-control edit_settings_setting_description settings-input  table-settings edit-table-settings text " placeholder="Description" value=""/>
</div></div>
</div>

<div class="form-group">
<label for="edit_settings_setting_value">Value</label>
<textarea rows="5" data-type="textarea" data-wysiwyg="0" type="text" name="setting_value" id="edit_settings_setting_value" class="form-control edit_settings_setting_value settings-input  table-settings edit-table-settings textarea text" placeholder="Value" value="" /></textarea>
</div>
</div> <!-- .panel-body -->
<div class="panel-footer">
<label class="pull-right">
<input checked="" type="checkbox" class="returnToList"> On submit, return to list</label>
<button class="btn btn-success btn-sm action-button" id="update-action-settings">Submit</button>
<a href="javascript:void(0)" class="btn btn-danger btn-sm update-back-settings" id="update-back-settings">Back</a>
<div class="clearfix"></div>
</div> <!-- .panel-footer -->
</div><!-- .panel .panel-settings -->
</div><!-- .tab-content .tab-content-settings --></div>
<?php } ?>
	
	<script>
jQuery(document).ready(function($) {
	$(document).AdminActions({
		baseURL : '<?php echo base_url(); ?>',
		current_table : 'settings',
		tables : { 
		<?php if( isset($admin_access->controller_settings) ) { ?>
		
'settings' : { label : 'Setting',
fields : ["setting_id","setting_controller","setting_name","setting_description","setting_value"],
add_fields : ["setting_controller","setting_name","setting_description","setting_value"],
edit_fields : ["setting_id","setting_controller","setting_name","setting_value"],
list_limit : 20,
list_fields : ["setting_controller","setting_name","setting_description","setting_value"],
order_by : 'setting_name',
order_sort : 'ASC',
filters : {"setting_controller":{"type":"manual","anchor":0}},
primary_key : 'setting_id',
primary_title : 'setting_name',
actions_edit : <?php echo ($admin_access->controller_settings->can_edit) ? 1 : 0; ?>,
actions_delete : <?php echo ($admin_access->controller_settings->can_delete) ? 1 : 0; ?> },

		<?php } ?>
		 },
		filters_data : {"setting_controller": {"site": "Website"}},
	});
});
</script>

</div></div><!-- .row . col-lg-12 -->
<?php $this->load->view('common_footer'); ?>