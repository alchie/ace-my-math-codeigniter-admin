<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class CI_Template_data {
    
    var $CI;
    private $isLoggedIn = false;
    private $user_id = NULL;
    private $data = array();
    public function __construct()
    {
       $this->CI =& get_instance();
        $this->isLoggedIn = $this->CI->session->userdata('logged_in');
        $this->user_id = $this->CI->session->userdata('user_id');    
        $this->defaults();
    }
    
    function defaults() {
        $this->data = array(
                    /*******
                    * Header
                    ********/
                    'page_title' => 'Home',
                    'site_title' => 'Ace My Math, LLC',
                    'header_css' => '',
                    'header_js' => '',
                    'header' => '',
                    /**** 
                    URL 
                    *****/
                     'base_url' => base_url(),
                     'site_url' => base_url(),
                     'home_url' => base_url(),
                     'redirect_url' => site_url('my/account'),
                     /*******
                     Session
                     ********/
                     'isLoggedIn' => $this->isLoggedIn(),
                     'user_id' => $this->user_id,
                    /******* 
                     Alert
                    *******/
                     'alert' => FALSE,
                     'alert_status' => 'default',
                     'alert_message' => '',
                    /*******
                    * Footer
                    ********/
                     'footer' => '',
                     );
    }
    
    function isLoggedIn()
    {
        return $this->CI->session->userdata('logged_in');
    }
    
    function set($key, $value)
    {
        $this->data[$key] = $value; 
                               
    }
    
    function get($key=false)
    {
        if($key) {
            return $this->data[$key];
        } else {
            return $this->data;
        }
    }
    
    function prepend($key, $value)
    {
        $current_value = $this->get($key);
        $this->set($key, $value . $current_value );  
    }
    
    function append($key, $value)
    {
        $current_value = $this->get($key);
        $this->set($key, $current_value . $value );        
    }
    
    function alert($message='Alert!', $status='default') {
        $this->set('alert', TRUE );
        $this->set('alert_status', $status );       
        $this->set('alert_message', $message );
    }
    
    function alert_off()
    {
        $this->set('alert', FALSE );
    }
    
    function page_title($value='') {
        if($value == '') 
        {
            $value = $this->get('page_title');
        }
        $this->set('page_title', $value );
    }
    
    function opengraph( $var=array() ) {
        $defaults = array(
            'og:title'=> $this->get('page_title'),
            'og:site_name'=>'',
            'og:description'=>'',
            'og:url'=> site_url( trim( uri_string(), "/") ),
            'og:type'=>'article',
            'og:image'=>'/assets/images/logo.png',
            'og:image:width'=>'289',
            'og:image:height'=>'102',
            'fb:app_id'=>'',
        );
        
        $opengraph = array_merge( (array) $defaults, (array) $var );
        
        foreach( $opengraph as $tag=>$value ) {
            if( $value != '' ) {
                $meta .= "\n" . '<meta property="'.$tag.'" content="'.$value.'" />';               
            }
        }
        $this->set('opengraph', $meta );
    }
    
}

/* End of file Global_variables.php */
