<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Media_uploads_model Class
 *
 * Manipulates `ci_media_uploads` table on database

CREATE TABLE `ci_media_uploads` (
  `media_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `file_name` varchar(255) NOT NULL,
  `file_type` varchar(100) DEFAULT NULL,
  `file_path` varchar(255) DEFAULT NULL,
  `full_path` varchar(255) DEFAULT NULL,
  `raw_name` varchar(255) DEFAULT NULL,
  `orig_name` varchar(255) DEFAULT NULL,
  `client_name` varchar(255) DEFAULT NULL,
  `file_ext` varchar(100) DEFAULT NULL,
  `file_size` float DEFAULT NULL,
  `is_image` int(1) DEFAULT NULL,
  `image_width` int(10) DEFAULT NULL,
  `image_height` int(10) DEFAULT NULL,
  `image_type` varchar(100) DEFAULT NULL,
  `image_size_str` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`media_id`)
);

 * @package			Model
 * @project			Ace My Math, LLC
 * @project_link	http://www.acemymath.com/
 * @author			Chester Alan Tagudin
 * @author_link		http://www.chesteralan.com/
 */
 
class Media_uploads_model extends CI_Model {

	protected $media_id;
	protected $file_name;
	protected $file_type;
	protected $file_path;
	protected $full_path;
	protected $raw_name;
	protected $orig_name;
	protected $client_name;
	protected $file_ext;
	protected $file_size;
	protected $is_image = '0';
	protected $image_width;
	protected $image_height;
	protected $image_type;
	protected $image_size_str;
	protected $dataFields = array();
	protected $select = array();
	protected $join = array();
	protected $where = array();
	protected $where_or = array();
	protected $where_in = array();
	protected $where_in_or = array();
	protected $where_not_in = array();
	protected $where_not_in_or = array();
	protected $like = array();
	protected $like_or = array();
	protected $like_not = array();
	protected $like_not_or = array();
	protected $having = array();
	protected $having_or = array();
	protected $group_by = array();
	protected $filter = array();
	protected $order = array();
	protected $exclude = array();
	protected $required = array();
	protected $countField = '';
	protected $start = 0;
	protected $limit = 10;
	protected $results = FALSE;
	protected $distinct = FALSE;
	protected $cache_on = FALSE;
	protected $batch = array();

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct() {
		parent::__construct();
	}

	// --------------------------------------------------------------------


	// --------------------------------------------------------------------
	// Start Field: media_id
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `media_id` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setMediaId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->media_id = ( ($value == '') && ($this->media_id != '') ) ? '' : $value;

		if( $setWhere ) {
			$key = 'media_uploads.media_id';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}

			$this->__setCondition($underCondition, $key, $value, $priority);
			
		}
		
		if( $set_data_field ) {
			$this->dataFields[] = 'media_id';
		}
		return $this;
	}

	/** 
	* Get the value of `media_id` variable
	* @access public
	* @return String;
	*/

	public function getMediaId() {
		return $this->media_id;
	}

	/**
	* Get row by `media_id`
	* @param media_id
	* @return QueryResult
	**/

	public function getByMediaId() {
		if($this->media_id != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('media_uploads', array('media_uploads.media_id' => $this->media_id), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `media_id`
	**/

	public function updateByMediaId() {
		if($this->media_id != '') {
			$this->setExclude('media_id');
			if( $this->getData() ) {
				return $this->db->update('media_uploads', $this->getData(), array('media_uploads.media_id' => $this->media_id ) );
			}
		}
	}


	/**
	* Delete row by `media_id`
	**/

	public function deleteByMediaId() {
		if($this->media_id != '') {
			return $this->db->delete('media_uploads', array('media_uploads.media_id' => $this->media_id ) );
		}
	}

	/**
	* Increment row by `media_id`
	**/

	public function incrementByMediaId() {
		if($this->media_id != '' && $this->countField != '') {
			$this->db->where('media_id', $this->media_id);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('media_uploads');
		}
	}

	/**
	* Decrement row by `media_id`
	**/

	public function decrementByMediaId() {
		if($this->media_id != '' && $this->countField != '') {
			$this->db->where('media_id', $this->media_id);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('media_uploads');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: media_id
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: file_name
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `file_name` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setFileName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->file_name = ( ($value == '') && ($this->file_name != '') ) ? '' : $value;

		if( $setWhere ) {
			$key = 'media_uploads.file_name';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}

			$this->__setCondition($underCondition, $key, $value, $priority);
			
		}
		
		if( $set_data_field ) {
			$this->dataFields[] = 'file_name';
		}
		return $this;
	}

	/** 
	* Get the value of `file_name` variable
	* @access public
	* @return String;
	*/

	public function getFileName() {
		return $this->file_name;
	}

	/**
	* Get row by `file_name`
	* @param file_name
	* @return QueryResult
	**/

	public function getByFileName() {
		if($this->file_name != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('media_uploads', array('media_uploads.file_name' => $this->file_name), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `file_name`
	**/

	public function updateByFileName() {
		if($this->file_name != '') {
			$this->setExclude('file_name');
			if( $this->getData() ) {
				return $this->db->update('media_uploads', $this->getData(), array('media_uploads.file_name' => $this->file_name ) );
			}
		}
	}


	/**
	* Delete row by `file_name`
	**/

	public function deleteByFileName() {
		if($this->file_name != '') {
			return $this->db->delete('media_uploads', array('media_uploads.file_name' => $this->file_name ) );
		}
	}

	/**
	* Increment row by `file_name`
	**/

	public function incrementByFileName() {
		if($this->file_name != '' && $this->countField != '') {
			$this->db->where('file_name', $this->file_name);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('media_uploads');
		}
	}

	/**
	* Decrement row by `file_name`
	**/

	public function decrementByFileName() {
		if($this->file_name != '' && $this->countField != '') {
			$this->db->where('file_name', $this->file_name);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('media_uploads');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: file_name
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: file_type
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `file_type` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setFileType($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->file_type = ( ($value == '') && ($this->file_type != '') ) ? '' : $value;

		if( $setWhere ) {
			$key = 'media_uploads.file_type';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}

			$this->__setCondition($underCondition, $key, $value, $priority);
			
		}
		
		if( $set_data_field ) {
			$this->dataFields[] = 'file_type';
		}
		return $this;
	}

	/** 
	* Get the value of `file_type` variable
	* @access public
	* @return String;
	*/

	public function getFileType() {
		return $this->file_type;
	}

	/**
	* Get row by `file_type`
	* @param file_type
	* @return QueryResult
	**/

	public function getByFileType() {
		if($this->file_type != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('media_uploads', array('media_uploads.file_type' => $this->file_type), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `file_type`
	**/

	public function updateByFileType() {
		if($this->file_type != '') {
			$this->setExclude('file_type');
			if( $this->getData() ) {
				return $this->db->update('media_uploads', $this->getData(), array('media_uploads.file_type' => $this->file_type ) );
			}
		}
	}


	/**
	* Delete row by `file_type`
	**/

	public function deleteByFileType() {
		if($this->file_type != '') {
			return $this->db->delete('media_uploads', array('media_uploads.file_type' => $this->file_type ) );
		}
	}

	/**
	* Increment row by `file_type`
	**/

	public function incrementByFileType() {
		if($this->file_type != '' && $this->countField != '') {
			$this->db->where('file_type', $this->file_type);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('media_uploads');
		}
	}

	/**
	* Decrement row by `file_type`
	**/

	public function decrementByFileType() {
		if($this->file_type != '' && $this->countField != '') {
			$this->db->where('file_type', $this->file_type);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('media_uploads');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: file_type
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: file_path
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `file_path` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setFilePath($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->file_path = ( ($value == '') && ($this->file_path != '') ) ? '' : $value;

		if( $setWhere ) {
			$key = 'media_uploads.file_path';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}

			$this->__setCondition($underCondition, $key, $value, $priority);
			
		}
		
		if( $set_data_field ) {
			$this->dataFields[] = 'file_path';
		}
		return $this;
	}

	/** 
	* Get the value of `file_path` variable
	* @access public
	* @return String;
	*/

	public function getFilePath() {
		return $this->file_path;
	}

	/**
	* Get row by `file_path`
	* @param file_path
	* @return QueryResult
	**/

	public function getByFilePath() {
		if($this->file_path != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('media_uploads', array('media_uploads.file_path' => $this->file_path), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `file_path`
	**/

	public function updateByFilePath() {
		if($this->file_path != '') {
			$this->setExclude('file_path');
			if( $this->getData() ) {
				return $this->db->update('media_uploads', $this->getData(), array('media_uploads.file_path' => $this->file_path ) );
			}
		}
	}


	/**
	* Delete row by `file_path`
	**/

	public function deleteByFilePath() {
		if($this->file_path != '') {
			return $this->db->delete('media_uploads', array('media_uploads.file_path' => $this->file_path ) );
		}
	}

	/**
	* Increment row by `file_path`
	**/

	public function incrementByFilePath() {
		if($this->file_path != '' && $this->countField != '') {
			$this->db->where('file_path', $this->file_path);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('media_uploads');
		}
	}

	/**
	* Decrement row by `file_path`
	**/

	public function decrementByFilePath() {
		if($this->file_path != '' && $this->countField != '') {
			$this->db->where('file_path', $this->file_path);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('media_uploads');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: file_path
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: full_path
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `full_path` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setFullPath($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->full_path = ( ($value == '') && ($this->full_path != '') ) ? '' : $value;

		if( $setWhere ) {
			$key = 'media_uploads.full_path';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}

			$this->__setCondition($underCondition, $key, $value, $priority);
			
		}
		
		if( $set_data_field ) {
			$this->dataFields[] = 'full_path';
		}
		return $this;
	}

	/** 
	* Get the value of `full_path` variable
	* @access public
	* @return String;
	*/

	public function getFullPath() {
		return $this->full_path;
	}

	/**
	* Get row by `full_path`
	* @param full_path
	* @return QueryResult
	**/

	public function getByFullPath() {
		if($this->full_path != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('media_uploads', array('media_uploads.full_path' => $this->full_path), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `full_path`
	**/

	public function updateByFullPath() {
		if($this->full_path != '') {
			$this->setExclude('full_path');
			if( $this->getData() ) {
				return $this->db->update('media_uploads', $this->getData(), array('media_uploads.full_path' => $this->full_path ) );
			}
		}
	}


	/**
	* Delete row by `full_path`
	**/

	public function deleteByFullPath() {
		if($this->full_path != '') {
			return $this->db->delete('media_uploads', array('media_uploads.full_path' => $this->full_path ) );
		}
	}

	/**
	* Increment row by `full_path`
	**/

	public function incrementByFullPath() {
		if($this->full_path != '' && $this->countField != '') {
			$this->db->where('full_path', $this->full_path);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('media_uploads');
		}
	}

	/**
	* Decrement row by `full_path`
	**/

	public function decrementByFullPath() {
		if($this->full_path != '' && $this->countField != '') {
			$this->db->where('full_path', $this->full_path);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('media_uploads');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: full_path
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: raw_name
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `raw_name` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setRawName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->raw_name = ( ($value == '') && ($this->raw_name != '') ) ? '' : $value;

		if( $setWhere ) {
			$key = 'media_uploads.raw_name';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}

			$this->__setCondition($underCondition, $key, $value, $priority);
			
		}
		
		if( $set_data_field ) {
			$this->dataFields[] = 'raw_name';
		}
		return $this;
	}

	/** 
	* Get the value of `raw_name` variable
	* @access public
	* @return String;
	*/

	public function getRawName() {
		return $this->raw_name;
	}

	/**
	* Get row by `raw_name`
	* @param raw_name
	* @return QueryResult
	**/

	public function getByRawName() {
		if($this->raw_name != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('media_uploads', array('media_uploads.raw_name' => $this->raw_name), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `raw_name`
	**/

	public function updateByRawName() {
		if($this->raw_name != '') {
			$this->setExclude('raw_name');
			if( $this->getData() ) {
				return $this->db->update('media_uploads', $this->getData(), array('media_uploads.raw_name' => $this->raw_name ) );
			}
		}
	}


	/**
	* Delete row by `raw_name`
	**/

	public function deleteByRawName() {
		if($this->raw_name != '') {
			return $this->db->delete('media_uploads', array('media_uploads.raw_name' => $this->raw_name ) );
		}
	}

	/**
	* Increment row by `raw_name`
	**/

	public function incrementByRawName() {
		if($this->raw_name != '' && $this->countField != '') {
			$this->db->where('raw_name', $this->raw_name);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('media_uploads');
		}
	}

	/**
	* Decrement row by `raw_name`
	**/

	public function decrementByRawName() {
		if($this->raw_name != '' && $this->countField != '') {
			$this->db->where('raw_name', $this->raw_name);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('media_uploads');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: raw_name
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: orig_name
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `orig_name` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setOrigName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->orig_name = ( ($value == '') && ($this->orig_name != '') ) ? '' : $value;

		if( $setWhere ) {
			$key = 'media_uploads.orig_name';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}

			$this->__setCondition($underCondition, $key, $value, $priority);
			
		}
		
		if( $set_data_field ) {
			$this->dataFields[] = 'orig_name';
		}
		return $this;
	}

	/** 
	* Get the value of `orig_name` variable
	* @access public
	* @return String;
	*/

	public function getOrigName() {
		return $this->orig_name;
	}

	/**
	* Get row by `orig_name`
	* @param orig_name
	* @return QueryResult
	**/

	public function getByOrigName() {
		if($this->orig_name != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('media_uploads', array('media_uploads.orig_name' => $this->orig_name), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `orig_name`
	**/

	public function updateByOrigName() {
		if($this->orig_name != '') {
			$this->setExclude('orig_name');
			if( $this->getData() ) {
				return $this->db->update('media_uploads', $this->getData(), array('media_uploads.orig_name' => $this->orig_name ) );
			}
		}
	}


	/**
	* Delete row by `orig_name`
	**/

	public function deleteByOrigName() {
		if($this->orig_name != '') {
			return $this->db->delete('media_uploads', array('media_uploads.orig_name' => $this->orig_name ) );
		}
	}

	/**
	* Increment row by `orig_name`
	**/

	public function incrementByOrigName() {
		if($this->orig_name != '' && $this->countField != '') {
			$this->db->where('orig_name', $this->orig_name);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('media_uploads');
		}
	}

	/**
	* Decrement row by `orig_name`
	**/

	public function decrementByOrigName() {
		if($this->orig_name != '' && $this->countField != '') {
			$this->db->where('orig_name', $this->orig_name);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('media_uploads');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: orig_name
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: client_name
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `client_name` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setClientName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->client_name = ( ($value == '') && ($this->client_name != '') ) ? '' : $value;

		if( $setWhere ) {
			$key = 'media_uploads.client_name';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}

			$this->__setCondition($underCondition, $key, $value, $priority);
			
		}
		
		if( $set_data_field ) {
			$this->dataFields[] = 'client_name';
		}
		return $this;
	}

	/** 
	* Get the value of `client_name` variable
	* @access public
	* @return String;
	*/

	public function getClientName() {
		return $this->client_name;
	}

	/**
	* Get row by `client_name`
	* @param client_name
	* @return QueryResult
	**/

	public function getByClientName() {
		if($this->client_name != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('media_uploads', array('media_uploads.client_name' => $this->client_name), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `client_name`
	**/

	public function updateByClientName() {
		if($this->client_name != '') {
			$this->setExclude('client_name');
			if( $this->getData() ) {
				return $this->db->update('media_uploads', $this->getData(), array('media_uploads.client_name' => $this->client_name ) );
			}
		}
	}


	/**
	* Delete row by `client_name`
	**/

	public function deleteByClientName() {
		if($this->client_name != '') {
			return $this->db->delete('media_uploads', array('media_uploads.client_name' => $this->client_name ) );
		}
	}

	/**
	* Increment row by `client_name`
	**/

	public function incrementByClientName() {
		if($this->client_name != '' && $this->countField != '') {
			$this->db->where('client_name', $this->client_name);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('media_uploads');
		}
	}

	/**
	* Decrement row by `client_name`
	**/

	public function decrementByClientName() {
		if($this->client_name != '' && $this->countField != '') {
			$this->db->where('client_name', $this->client_name);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('media_uploads');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: client_name
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: file_ext
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `file_ext` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setFileExt($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->file_ext = ( ($value == '') && ($this->file_ext != '') ) ? '' : $value;

		if( $setWhere ) {
			$key = 'media_uploads.file_ext';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}

			$this->__setCondition($underCondition, $key, $value, $priority);
			
		}
		
		if( $set_data_field ) {
			$this->dataFields[] = 'file_ext';
		}
		return $this;
	}

	/** 
	* Get the value of `file_ext` variable
	* @access public
	* @return String;
	*/

	public function getFileExt() {
		return $this->file_ext;
	}

	/**
	* Get row by `file_ext`
	* @param file_ext
	* @return QueryResult
	**/

	public function getByFileExt() {
		if($this->file_ext != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('media_uploads', array('media_uploads.file_ext' => $this->file_ext), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `file_ext`
	**/

	public function updateByFileExt() {
		if($this->file_ext != '') {
			$this->setExclude('file_ext');
			if( $this->getData() ) {
				return $this->db->update('media_uploads', $this->getData(), array('media_uploads.file_ext' => $this->file_ext ) );
			}
		}
	}


	/**
	* Delete row by `file_ext`
	**/

	public function deleteByFileExt() {
		if($this->file_ext != '') {
			return $this->db->delete('media_uploads', array('media_uploads.file_ext' => $this->file_ext ) );
		}
	}

	/**
	* Increment row by `file_ext`
	**/

	public function incrementByFileExt() {
		if($this->file_ext != '' && $this->countField != '') {
			$this->db->where('file_ext', $this->file_ext);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('media_uploads');
		}
	}

	/**
	* Decrement row by `file_ext`
	**/

	public function decrementByFileExt() {
		if($this->file_ext != '' && $this->countField != '') {
			$this->db->where('file_ext', $this->file_ext);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('media_uploads');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: file_ext
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: file_size
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `file_size` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setFileSize($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->file_size = ( ($value == '') && ($this->file_size != '') ) ? '' : $value;

		if( $setWhere ) {
			$key = 'media_uploads.file_size';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}

			$this->__setCondition($underCondition, $key, $value, $priority);
			
		}
		
		if( $set_data_field ) {
			$this->dataFields[] = 'file_size';
		}
		return $this;
	}

	/** 
	* Get the value of `file_size` variable
	* @access public
	* @return String;
	*/

	public function getFileSize() {
		return $this->file_size;
	}

	/**
	* Get row by `file_size`
	* @param file_size
	* @return QueryResult
	**/

	public function getByFileSize() {
		if($this->file_size != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('media_uploads', array('media_uploads.file_size' => $this->file_size), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `file_size`
	**/

	public function updateByFileSize() {
		if($this->file_size != '') {
			$this->setExclude('file_size');
			if( $this->getData() ) {
				return $this->db->update('media_uploads', $this->getData(), array('media_uploads.file_size' => $this->file_size ) );
			}
		}
	}


	/**
	* Delete row by `file_size`
	**/

	public function deleteByFileSize() {
		if($this->file_size != '') {
			return $this->db->delete('media_uploads', array('media_uploads.file_size' => $this->file_size ) );
		}
	}

	/**
	* Increment row by `file_size`
	**/

	public function incrementByFileSize() {
		if($this->file_size != '' && $this->countField != '') {
			$this->db->where('file_size', $this->file_size);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('media_uploads');
		}
	}

	/**
	* Decrement row by `file_size`
	**/

	public function decrementByFileSize() {
		if($this->file_size != '' && $this->countField != '') {
			$this->db->where('file_size', $this->file_size);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('media_uploads');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: file_size
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: is_image
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `is_image` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setIsImage($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->is_image = ( ($value == '') && ($this->is_image != '') ) ? '' : $value;

		if( $setWhere ) {
			$key = 'media_uploads.is_image';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}

			$this->__setCondition($underCondition, $key, $value, $priority);
			
		}
		
		if( $set_data_field ) {
			$this->dataFields[] = 'is_image';
		}
		return $this;
	}

	/** 
	* Get the value of `is_image` variable
	* @access public
	* @return String;
	*/

	public function getIsImage() {
		return $this->is_image;
	}

	/**
	* Get row by `is_image`
	* @param is_image
	* @return QueryResult
	**/

	public function getByIsImage() {
		if($this->is_image != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('media_uploads', array('media_uploads.is_image' => $this->is_image), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `is_image`
	**/

	public function updateByIsImage() {
		if($this->is_image != '') {
			$this->setExclude('is_image');
			if( $this->getData() ) {
				return $this->db->update('media_uploads', $this->getData(), array('media_uploads.is_image' => $this->is_image ) );
			}
		}
	}


	/**
	* Delete row by `is_image`
	**/

	public function deleteByIsImage() {
		if($this->is_image != '') {
			return $this->db->delete('media_uploads', array('media_uploads.is_image' => $this->is_image ) );
		}
	}

	/**
	* Increment row by `is_image`
	**/

	public function incrementByIsImage() {
		if($this->is_image != '' && $this->countField != '') {
			$this->db->where('is_image', $this->is_image);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('media_uploads');
		}
	}

	/**
	* Decrement row by `is_image`
	**/

	public function decrementByIsImage() {
		if($this->is_image != '' && $this->countField != '') {
			$this->db->where('is_image', $this->is_image);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('media_uploads');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: is_image
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: image_width
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `image_width` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setImageWidth($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->image_width = ( ($value == '') && ($this->image_width != '') ) ? '' : $value;

		if( $setWhere ) {
			$key = 'media_uploads.image_width';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}

			$this->__setCondition($underCondition, $key, $value, $priority);
			
		}
		
		if( $set_data_field ) {
			$this->dataFields[] = 'image_width';
		}
		return $this;
	}

	/** 
	* Get the value of `image_width` variable
	* @access public
	* @return String;
	*/

	public function getImageWidth() {
		return $this->image_width;
	}

	/**
	* Get row by `image_width`
	* @param image_width
	* @return QueryResult
	**/

	public function getByImageWidth() {
		if($this->image_width != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('media_uploads', array('media_uploads.image_width' => $this->image_width), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `image_width`
	**/

	public function updateByImageWidth() {
		if($this->image_width != '') {
			$this->setExclude('image_width');
			if( $this->getData() ) {
				return $this->db->update('media_uploads', $this->getData(), array('media_uploads.image_width' => $this->image_width ) );
			}
		}
	}


	/**
	* Delete row by `image_width`
	**/

	public function deleteByImageWidth() {
		if($this->image_width != '') {
			return $this->db->delete('media_uploads', array('media_uploads.image_width' => $this->image_width ) );
		}
	}

	/**
	* Increment row by `image_width`
	**/

	public function incrementByImageWidth() {
		if($this->image_width != '' && $this->countField != '') {
			$this->db->where('image_width', $this->image_width);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('media_uploads');
		}
	}

	/**
	* Decrement row by `image_width`
	**/

	public function decrementByImageWidth() {
		if($this->image_width != '' && $this->countField != '') {
			$this->db->where('image_width', $this->image_width);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('media_uploads');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: image_width
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: image_height
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `image_height` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setImageHeight($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->image_height = ( ($value == '') && ($this->image_height != '') ) ? '' : $value;

		if( $setWhere ) {
			$key = 'media_uploads.image_height';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}

			$this->__setCondition($underCondition, $key, $value, $priority);
			
		}
		
		if( $set_data_field ) {
			$this->dataFields[] = 'image_height';
		}
		return $this;
	}

	/** 
	* Get the value of `image_height` variable
	* @access public
	* @return String;
	*/

	public function getImageHeight() {
		return $this->image_height;
	}

	/**
	* Get row by `image_height`
	* @param image_height
	* @return QueryResult
	**/

	public function getByImageHeight() {
		if($this->image_height != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('media_uploads', array('media_uploads.image_height' => $this->image_height), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `image_height`
	**/

	public function updateByImageHeight() {
		if($this->image_height != '') {
			$this->setExclude('image_height');
			if( $this->getData() ) {
				return $this->db->update('media_uploads', $this->getData(), array('media_uploads.image_height' => $this->image_height ) );
			}
		}
	}


	/**
	* Delete row by `image_height`
	**/

	public function deleteByImageHeight() {
		if($this->image_height != '') {
			return $this->db->delete('media_uploads', array('media_uploads.image_height' => $this->image_height ) );
		}
	}

	/**
	* Increment row by `image_height`
	**/

	public function incrementByImageHeight() {
		if($this->image_height != '' && $this->countField != '') {
			$this->db->where('image_height', $this->image_height);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('media_uploads');
		}
	}

	/**
	* Decrement row by `image_height`
	**/

	public function decrementByImageHeight() {
		if($this->image_height != '' && $this->countField != '') {
			$this->db->where('image_height', $this->image_height);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('media_uploads');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: image_height
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: image_type
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `image_type` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setImageType($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->image_type = ( ($value == '') && ($this->image_type != '') ) ? '' : $value;

		if( $setWhere ) {
			$key = 'media_uploads.image_type';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}

			$this->__setCondition($underCondition, $key, $value, $priority);
			
		}
		
		if( $set_data_field ) {
			$this->dataFields[] = 'image_type';
		}
		return $this;
	}

	/** 
	* Get the value of `image_type` variable
	* @access public
	* @return String;
	*/

	public function getImageType() {
		return $this->image_type;
	}

	/**
	* Get row by `image_type`
	* @param image_type
	* @return QueryResult
	**/

	public function getByImageType() {
		if($this->image_type != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('media_uploads', array('media_uploads.image_type' => $this->image_type), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `image_type`
	**/

	public function updateByImageType() {
		if($this->image_type != '') {
			$this->setExclude('image_type');
			if( $this->getData() ) {
				return $this->db->update('media_uploads', $this->getData(), array('media_uploads.image_type' => $this->image_type ) );
			}
		}
	}


	/**
	* Delete row by `image_type`
	**/

	public function deleteByImageType() {
		if($this->image_type != '') {
			return $this->db->delete('media_uploads', array('media_uploads.image_type' => $this->image_type ) );
		}
	}

	/**
	* Increment row by `image_type`
	**/

	public function incrementByImageType() {
		if($this->image_type != '' && $this->countField != '') {
			$this->db->where('image_type', $this->image_type);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('media_uploads');
		}
	}

	/**
	* Decrement row by `image_type`
	**/

	public function decrementByImageType() {
		if($this->image_type != '' && $this->countField != '') {
			$this->db->where('image_type', $this->image_type);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('media_uploads');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: image_type
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: image_size_str
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `image_size_str` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setImageSizeStr($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->image_size_str = ( ($value == '') && ($this->image_size_str != '') ) ? '' : $value;

		if( $setWhere ) {
			$key = 'media_uploads.image_size_str';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}

			$this->__setCondition($underCondition, $key, $value, $priority);
			
		}
		
		if( $set_data_field ) {
			$this->dataFields[] = 'image_size_str';
		}
		return $this;
	}

	/** 
	* Get the value of `image_size_str` variable
	* @access public
	* @return String;
	*/

	public function getImageSizeStr() {
		return $this->image_size_str;
	}

	/**
	* Get row by `image_size_str`
	* @param image_size_str
	* @return QueryResult
	**/

	public function getByImageSizeStr() {
		if($this->image_size_str != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('media_uploads', array('media_uploads.image_size_str' => $this->image_size_str), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `image_size_str`
	**/

	public function updateByImageSizeStr() {
		if($this->image_size_str != '') {
			$this->setExclude('image_size_str');
			if( $this->getData() ) {
				return $this->db->update('media_uploads', $this->getData(), array('media_uploads.image_size_str' => $this->image_size_str ) );
			}
		}
	}


	/**
	* Delete row by `image_size_str`
	**/

	public function deleteByImageSizeStr() {
		if($this->image_size_str != '') {
			return $this->db->delete('media_uploads', array('media_uploads.image_size_str' => $this->image_size_str ) );
		}
	}

	/**
	* Increment row by `image_size_str`
	**/

	public function incrementByImageSizeStr() {
		if($this->image_size_str != '' && $this->countField != '') {
			$this->db->where('image_size_str', $this->image_size_str);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('media_uploads');
		}
	}

	/**
	* Decrement row by `image_size_str`
	**/

	public function decrementByImageSizeStr() {
		if($this->image_size_str != '' && $this->countField != '') {
			$this->db->where('image_size_str', $this->image_size_str);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('media_uploads');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: image_size_str
	// --------------------------------------------------------------------


	// --------------------------------------------------------------------

	/**
	* Checks if result not empty 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function nonEmpty() {
		if ( $this->hasConditions() ) {
			
			$this->setupJoin();
			$this->setupConditions();
			$this->setupHaving();
			$this->setupGroupBy();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get('media_uploads', 1);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			$result = $query->result();
			if ( isset( $result[0] ) === true ) {
				$this->results = $result[0];
				return true;
			} else {
				return false;
			}
		}
	}


	/**
	* Get Results 
	* @return Mixed;
	*/

	public function getResults() {
		return $this->results;
	}


	// --------------------------------------------------------------------

	/**
	* Delete 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function delete() {
		if ( $this->hasConditions() ) {
				$this->setupConditions();
				return $this->db->delete('media_uploads');
		}
	}


	// --------------------------------------------------------------------

	/**
	* Limit Data Fields
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function limitDataFields($fields) {
		if($fields != '') {
			if( ! is_array($fields) ) {
				$this->dataFields = array($fields);
			} else {
				$this->dataFields = $fields;
			}
		}
	}

	// --------------------------------------------------------------------

	/**
	* Update 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function update() {
		if ( ( $this->getData() ) && ( $this->hasConditions() ) ) {
				$this->setupConditions();
				return $this->db->update('media_uploads', $this->getData() );
		}
	}


	// --------------------------------------------------------------------

	/**
	* Insert new row 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function insert() {
		if( $this->getData() ) {
			if( $this->db->insert('media_uploads', $this->getData() ) === TRUE ) {
				$this->media_id = $this->db->insert_id();
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}


	// --------------------------------------------------------------------

	/**
	* Replace new row 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function replace() {
		if( $this->getData() ) {
			if( $this->db->replace('media_uploads', $this->getData() ) === TRUE ) {
				$this->media_id = $this->db->insert_id();
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}
	
	// --------------------------------------------------------------------

	/**
	* Get First Data 
	* @access public
	* @return Object / False;
	*/

	public function get() {
		
		if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
		}
		
		$this->setupJoin();
		$this->setupConditions();
		$this->setupHaving();
		$this->setupGroupBy();
		
		if( $this->order ) {
			foreach( $this->order as $field=>$orientation ) {
				$this->db->order_by( $field, $orientation );
			}
		}
		
		$this->db->limit( 1, $this->start);
		
		if( $this->cache_on ) {
			$this->db->cache_on();
		}
		
		$query = $this->db->get('media_uploads');
		
		if( $this->cache_on ) {
			$this->db->cache_off();
		}
		
		$result = $query->result();
		
		if( isset($result[0]) ) {
			
			$this->results = $result[0];
			
			return $this->results;
			
		}
		
		return false;
	}
	// --------------------------------------------------------------------

	/**
	* Populate Data 
	* @access public
	* @param  String
	* @return Array;
	*/

	public function populate() {
	
		if( $this->distinct ) {
			$this->db->distinct();
		}
		
		if( $this->select ) {
				$this->db->select( implode(',' , $this->select ) );
		}
		
		$this->setupJoin();
		$this->setupConditions();
		$this->setupHaving();
		$this->setupGroupBy();
		
		if( $this->order ) {
			foreach( $this->order as $field=>$orientation ) {
				$this->db->order_by( $field, $orientation );
			}
		}
		
		if( $this->limit > 0 ) {
			$this->db->limit( $this->limit,$this->start);
		}
		
		if( $this->cache_on ) {
			$this->db->cache_on();
		}
		
		$query = $this->db->get('media_uploads');
		
		if( $this->cache_on ) {
			$this->db->cache_off();
		}
			
		$this->results = $query->result();
		
		return $this->results;
	}
	// --------------------------------------------------------------------
	/**
	* Recursive 
	* @access public
	* @param  String
	* @return Array;
	*/

	public function recursive($match, $find, $child='media_id', $level=10, $conn='children') {
			if( $level == 0 ) return;
			if( $this->limit > 0 ) {
				$this->db->limit( $this->limit,$this->start);
			}
			if( $this->select ) {
					$this->db->select( implode(',' , $this->select) );
			}
			
			$this->setupJoin();
			$this->setWhere($match, $find);
			$this->setupConditions();
			$this->setupHaving();
			$this->setupGroupBy();
			$this->clearWhere( $match );
			
			if( $this->order ) {
				foreach( $this->order as $field=>$orientation ) {
					$this->db->order_by( $field, $orientation );
				}
			}
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get('media_uploads');
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			$results=array();
			if( $query->result() ) {
				foreach( $query->result() as $qr ) {
					$children = $this->recursive($match, $qr->$child, $child, ($level-1), $conn ) ;
					$results[] = (object) array_merge( (array) $qr, array($conn=>$children) );
				}
			}
		return $results;
	}

// --------------------------------------------------------------------

	/**
	* Set Field Where Clause 
	* @access public
	* @param String / Array
	* @return Array;
	*/
	public function setFieldWhere($fields) {
		if($fields != '') {
			if( ! is_array($fields) ) {
				$this->where[] = array($fields => $this->$fields);
			} else {
				foreach($fields as $field) {
					if($w != '') {
						$this->where[] = array($field => $this->$field);
					}
				}
			}
		}
	}

// --------------------------------------------------------------------

	/**
	* Set Field Value Manually
	* @access public
	* @param Field Key ; Value
	* @return self;
	*/
	public function setFieldValue($field, $value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL) {
		if( isset( $this->$field ) ) {
			$this->$field = ( ($value == '') && ($this->$field != '') ) ? '' : $value;
			if( $setWhere ) {
				$key = 'media_uploads.'.$field;
				if ( $whereOperator != NULL && $whereOperator != '' ) {
					$key = $key . ' ' . $whereOperator;
				}
				//$this->where[$key] = $this->$field;
				$this->where[] = array( $key => $this->$field );
			}
			if( $set_data_field ) {
				$this->dataFields[] = $field;
			}
		}
		return $this;
	}
	
	// --------------------------------------------------------------------

	/**
	* Prepares data 
	* @access private
	* @return Array;
	*/
	public function getData($exclude=NULL) {
		$data = array();

		$fields = array("media_id","file_name","file_type","file_path","full_path","raw_name","orig_name","client_name","file_ext","file_size","is_image","image_width","image_height","image_type","image_size_str");
		if( count( $this->dataFields ) > 0 ) {
    		            $fields = $this->dataFields;
		}
		foreach( $fields as $field ) {
		    if( ( in_array( $field, $this->required ) ) 
		    && ($this->$field == '') 
		    && ( ! in_array( $field, $this->exclude ) ) 
		    ) {
        		return false;
    		}
    		if( ( in_array( $field, $this->required ) ) 
    		&& ($this->$field != '') 
    		&& ( ! in_array( $field, $this->exclude ) ) 
    		) {
		        $data[$field] = $this->$field;
		    }
		    if( ( ! in_array( $field, $this->required ) ) 
		    && ( ! in_array( $field, $this->exclude ) ) 
		    ) {
        		$data[$field] = $this->$field;
    		}  
		}
		return $data;   
	}


	// --------------------------------------------------------------------

	/**
	* Setup Conditional Clauses 
	* @access public
	* @return Null;
	*/

        protected function __setCondition($condition, $key, $value, $priority) {
		switch( $condition ) {
			case 'where_or':
				if( ! is_array( $value ) ) {
					$value = explode(',', $value );
				}
				$this->setWhereOr($key, $value, $priority);
			break;
			case 'where_in':
				if( ! is_array( $value ) ) {
					$value = explode(',', $value );
				}
				$this->setWhereIn($key, $value, $priority);
			break;
			case 'where_in_or':
				if( ! is_array( $value ) ) {
					$value = explode(',', $value );
				}
				$this->setWhereInOr($key, $value, $priority);
			break;
			case 'where_not_in':
				if( ! is_array( $value ) ) {
					$value = explode(',', $value );
				}
				$this->setWhereNotIn($key, $value, $priority);
			break;
			case 'where_not_in_or':
				if( ! is_array( $value ) ) {
					$value = explode(',', $value );
				}
				$this->setWhereNotInOr($key, $value, $priority);
			break;
			case 'like':
				if( ! is_array( $value ) ) {
					$value = explode(',', $value );
				}
				$this->setLike($key, $value, $priority);
			break;
			case 'like_or':
				if( ! is_array( $value ) ) {
					$value = explode(',', $value );
				}
				$this->setLikeOr($key, $value, $priority);
			break;
			case 'like_not':
				if( ! is_array( $value ) ) {
					$value = explode(',', $value );
				}
				$this->setLikeNot($key, $value, $priority);
			break;
			case 'like_not_or':
				if( ! is_array( $value ) ) {
					$value = explode(',', $value );
				}
				$this->setLikeNotOr($key, $value, $priority);
			break;
			default:
				$this->setWhere($key, $value, $priority);
			break;
		}
	}
	
	protected function __apply_condition($what, $condition) {
		if( is_array( $condition ) ) {
			foreach( $condition as $key => $value ) {
				$this->db->$what( $key , $value );
			}
		} else {
			$this->db->$what( $condition );
		}
	}
	
	private function setupConditions() {
		$return = FALSE;
		
		$conditions = array_merge(
                                $this->where,
                                $this->filter,
                                $this->where_or,
                                $this->where_in,
                                $this->where_in_or,
                                $this->where_not_in,
                                $this->where_not_in_or,
                                $this->like,
                                $this->like_or,
                                $this->like_not,
                                $this->like_not_or
                                );
		
		if( count( $conditions ) > 0 ) {
			$i = 0;
			$sorted = array();
			
			foreach( $conditions as $condition ) {
				foreach( $condition as $field=>$options ) {
					$priority = $options['priority'];
					unset( $options['priority'] );
					if( ! is_null( $priority ) ) {
						while( isset($sorted[$priority]) ) {
							$i++;
							$priority += $i;
						}
						$sorted[ $priority ] = $options;
					}
				}
			}
			
			foreach( $conditions as $condition ) {
				foreach( $condition as $field=>$options ) {
					$priority = $options['priority'];
					unset( $options['priority'] );
					if( is_null( $priority )  ) {
						while( isset($sorted[$i]) ) {
							$i++;
						}
						$sorted[$i] = $options;
					} 
				}
			}
			
			ksort( $sorted );
		
			foreach( $sorted as $sortd ) {
				$sortd_key = array_keys( $sortd );
				$sortd_value = array_values( $sortd );
				
				if( isset( $sortd_key[0] ) && $sortd_value[0] ) {
					switch( $sortd_key[0] ) {
						case 'where_or':
							$this->__apply_condition('or_where', $sortd_value[0]);
						break;
						case 'where_in':
							$this->__apply_condition('where_in', $sortd_value[0]);
						break;
						case 'where_in_or':
							$this->__apply_condition('or_where_in', $sortd_value[0]);
						break;
						case 'where_not_in':
							$this->__apply_condition('where_not_in', $sortd_value[0]);
						break;
						case 'where_not_in_or':
							$this->__apply_condition('or_where_not_in', $sortd_value[0]);
						break;
						case 'like':
							$this->__apply_condition('like', $sortd_value[0]);
						break;
						case 'like_or':
							$this->__apply_condition('or_like', $sortd_value[0]);
						break;
						case 'like_not':
							$this->__apply_condition('not_like', $sortd_value[0]);
						break;
						case 'like_not_or':
							$this->__apply_condition('or_not_like', $sortd_value[0]);
						break;
						default:
							$this->__apply_condition('where', $sortd_value[0]);
						break;
					}
				}
			}
			$return = TRUE;
		}
		
		return $return;
	}
	
	private function setupHaving() {
		
		$return = FALSE;
		
		$conditions = array_merge( $this->having, $this->having_or );
		
		if( count( $conditions ) > 0 ) {
			$i = 0;
			$sorted = array();
			
			foreach( $conditions as $condition ) {
				foreach( $condition as $field=>$options ) {
					$priority = $options['priority'];
					unset( $options['priority'] );
					if( ! is_null( $priority ) ) {
						while( isset($sorted[$priority]) ) {
							$i++;
							$priority += $i;
						}
						$sorted[ $priority ] = $options;
					}
				}
			}
			
			foreach( $conditions as $condition ) {
				foreach( $condition as $field=>$options ) {
					$priority = $options['priority'];
					unset( $options['priority'] );
					if( is_null( $priority )  ) {
						while( isset($sorted[$i]) ) {
							$i++;
						}
						$sorted[$i] = $options;
					} 
				}
			}
			
			ksort( $sorted );
		
			foreach( $sorted as $sortd ) {
				$sortd_key = array_keys( $sortd );
				$sortd_value = array_values( $sortd );
				
				if( isset( $sortd_key[0] ) && $sortd_value[0] ) {
					switch( $sortd_key[0] ) {
						case 'having':
							$this->__apply_condition('having', $sortd_value[0]);
						break;
						case 'having_or':
							$this->__apply_condition('or_having', $sortd_value[0]);
						break;
					}
				}
			}
			$return = TRUE;
		}
		
		return $return;
	}
	
	/**
	* Check Conditions Availability
	* @access public
	* @return Array;
	*/
	private function hasConditions() {
		$conditions = array_merge(
                                $this->where,
                                $this->filter,
                                $this->where_or,
                                $this->where_in,
                                $this->where_in_or,
                                $this->where_not_in,
                                $this->where_not_in_or,
                                $this->like,
                                $this->like_or,
                                $this->like_not,
                                $this->like_not_or,
                                $this->having,
                                $this->having_or
                        );
		if( count( $conditions ) > 0 ) {
			return true;
		}
		return false;
	}
	

	// --------------------------------------------------------------------
	/**
	* Set Join Clause 
	* @access public
	* @param String / Array
	* @return Array;
	*/
	public function setJoin($table, $connection, $option='left') {
		$this->join[] = array('table'=>$table, 'connection'=>$connection, 'option'=>$option );
	}

	private function setupJoin() {
		if( $this->join ) {
			foreach( $this->join as $join ) {
				$this->db->join( $join['table'], $join['connection'], $join['option'] );
			}
		}
	}
	
	// --------------------------------------------------------------------

	/**
	* Set Filter 
	* @access public
	* @return Array;
	*/
	public function setFilter($field, $value, $table=NULL, $priority=NULL, $underCondition='where') {
		$key = array();
		if( $table == NULL ) { 
			$table = 'media_uploads'; 
		} 
		if( $table != '' ) {
			$key[] = $table;
		}
		$key[] = $field;
		
		$newField = implode('.', $key);
		
		if( is_null( $value ) ) {
			$where = $newField;
		} else {
			$where = array( $newField => $value );
		}
		
		$this->filter[] = array( $newField => array( $underCondition => $where, 'priority'=>$priority ) );
	}


	public function clearFilter($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->filter);
			$this->filter = array();
		} else {
			$newfilter = array();
			foreach($this->filter as $filter ) {
				if( ! isset( $filter[$field] ) ) {
					$newfilter[] = $filter;
				}
			}
			$this->filter = $newfilter;
		}
	}

	// --------------------------------------------------------------------

	/**
	* Set Where 
	* @access public
	*/
	public function setWhere($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->where[] = array( $field => array( 'where' => $where, 'priority'=>$priority )); 
	}


	public function clearWhere($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->where);
			$this->where = array();
		} else {
			$newwhere = array();
			foreach($this->where as $where ) {
				if( ! isset( $where[$field] ) ) {
					$newwhere[] = $where;
				}
			}
			$this->where = $newwhere;
		}
	}
	
	/**
	* Set Or Where 
	* @access public
	*/
	public function setWhereOr($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->where_or[] = array( $field => array( 'where_or' => $where, 'priority'=>$priority ));  
	}
	
	public function clearWhereOr($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->where_or);
			$this->where_or = array();
		} else {
			$newwhere_or = array();
			foreach($this->where_or as $where_or ) {
				if( ! isset( $where_or[$field] ) ) {
					$newwhere_or[] = $where_or;
				}
			}
			$this->where_or = $newwhere_or;
		}
	}
	
	/**
	* Set Where In
	* @access public
	*/
	public function setWhereIn($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->where_in[] = array( $field => array( 'where_in' => $where, 'priority'=>$priority ));  
	}
	
	public function clearWhereIn($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->where_in);
			$this->where_in = array();
		} else {
			$newwhere_in = array();
			foreach($this->where_in as $where_in ) {
				if( ! isset( $where_in[$field] ) ) {
					$newwhere_in[] = $where_in;
				}
			}
			$this->where_in = $newwhere_in;
		}
	}
	
	/**
	* Set Or Where In
	* @access public
	*/
	public function setWhereInOr($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->where_in_or[] = array( $field => array( 'where_in_or' => $where, 'priority'=>$priority ));  
	}

	
	public function clearWhereInOr($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->where_in_or);
			$this->where_in_or = array();
		} else {
			$newwhere_in_or = array();
			foreach($this->where_in_or as $where_in_or ) {
				if( ! isset( $where_in_or[$field] ) ) {
					$newwhere_in_or[] = $where_in_or;
				}
			}
			$this->where_in_or = $newwhere_in_or;
		}
	}
	
	/**
	* Set Where Not In
	* @access public
	*/
	public function setWhereNotIn($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->where_not_in[] = array( $field => array( 'where_not_in' => $where, 'priority'=>$priority )); 
	}
	
	public function clearWhereNotIn($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->where_not_in);
			$this->where_not_in = array();
		} else {
			$newwhere_not_in = array();
			foreach($this->where_not_in as $where_not_in ) {
				if( ! isset( $where_not_in[$field] ) ) {
					$newwhere_not_in[] = $where_not_in;
				}
			}
			$this->where_not_in = $newwhere_not_in;
		}
	}
	
	/**
	* Set Or Where Not In
	* @access public
	*/
	public function setWhereNotInOr($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->where_not_in_or[] = array( $field => array( 'where_not_in_or' => $where, 'priority'=>$priority )); 
	}

	public function clearWhereNotInOr($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->where_not_in_or);
			$this->where_not_in_or = array();
		} else {
			$newwhere_not_in_or = array();
			foreach($this->where_not_in_or as $where_not_in_or ) {
				if( ! isset( $where_not_in_or[$field] ) ) {
					$newwhere_not_in_or[] = $where_not_in_or;
				}
			}
			$this->where_not_in_or = $newwhere_not_in_or;
		}
	}
	
	// --------------------------------------------------------------------

	/**
	* Set Like
	* @access public
	*/
	public function setLike($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->like[] = array( $field => array( 'like' => $where, 'priority'=>$priority )); 
	}

	public function clearLike($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->like);
			$this->like = array();
		} else {
			$newlike = array();
			foreach($this->like as $like ) {
				if( ! isset( $like[$field] ) ) {
					$newlike[] = $like;
				}
			}
			$this->like = $newlike;
		}
	}
	
	/**
	* Set Like
	* @access public
	*/
	public function setLikeOr($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->like_or[] = array( $field => array( 'like_or' => $where, 'priority'=>$priority ));  
	}

	public function clearLikeOr($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->like_or);
			$this->like_or = array();
		} else {
			$newlike_or = array();
			foreach($this->like_or as $like_or ) {
				if( ! isset( $like_or[$field] ) ) {
					$newlike_or[] = $like_or;
				}
			}
			$this->like_or = $newlike_or;
		}
	}
	
	/**
	* Set Like
	* @access public
	*/
	public function setLikeNot($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->like_not[] = array( $field => array( 'like_not' => $where, 'priority'=>$priority ));  
	}

	public function clearLikeNot($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->like_not);
			$this->like_not = array();
		} else {
			$newlike_not = array();
			foreach($this->like_not as $like_not ) {
				if( ! isset( $like_not[$field] ) ) {
					$newlike_not[] = $like_not;
				}
			}
			$this->like_not = $newlike_not;
		}
	}
	
	/**
	* Set Like
	* @access public
	*/
	public function setLikeNotOr($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->like_not_or[] = array( $field => array( 'like_not_or' => $where, 'priority'=>$priority ));  
	}

	public function clearLikeNotOr($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->like_not_or);
			$this->like_not_or = array();
		} else {
			$newlike_not_or = array();
			foreach($this->like_not_or as $like_not_or ) {
				if( ! isset( $like_not_or[$field] ) ) {
					$newlike_not_or[] = $like_not_or;
				}
			}
			$this->like_not_or = $newlike_not_or;
		}
	}
	
	// --------------------------------------------------------------------
	
	/**
	* Set Having 
	* @access public
	*/
	public function setHaving($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$having = $field;
		} else {
			$having = array( $field => $value );
		}
		$this->having[] = array( $field => array( 'having' => $having, 'priority'=>$priority )); 
	}
	
	public function clearHaving($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->having);
			$this->having = array();
		} else {
			$newhaving = array();
			foreach($this->having as $having ) {
				if( ! isset( $having[$field] ) ) {
					$newhaving[] = $having;
				}
			}
			$this->having = $newhaving;
		}
	}
	
	/**
	* Set Or Having 
	* @access public
	*/
	public function setHavingOr($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$having = $field;
		} else {
			$having = array( $field => $value );
		}
		$this->having_or[] = array( $field => array( 'having_or' => $having, 'priority'=>$priority ));  
	}

	public function clearHavingOr($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->having_or);
			$this->having_or = array();
		} else {
			$newhaving_or = array();
			foreach($this->having_or as $having_or ) {
				if( ! isset( $having_or[$field] ) ) {
					$newhaving_or[] = $having_or;
				}
			}
			$this->having_or = $newhaving_or;
		}
	}
	
	// --------------------------------------------------------------------

	/**
	* Set Group By
	* @access public
	*/
	public function setGroupBy($fields) {
		if( is_array( $fields ) ) { 
			$this->group_by = array_merge( $this->group_by, $fields );
		} else {
			$this->group_by[] = $fields;
		}
	}

	private function setupGroupBy() {
		if( $this->group_by ) {
			$this->db->group_by( $this->group_by ); 
		}
	}
	
	// --------------------------------------------------------------------
	
	/**
	* Set Start  
	* @access public
	*/
	public function setStart($value) {
		$this->start = $value;
		return $this;
	}

	/**
	* Set Limit  
	* @access public
	*/
	public function setLimit($value) {
		$this->limit = $value;
		return $this;
	}

	// --------------------------------------------------------------------

	/**
	* Set Order  
	* @access public
	*/

	public function setOrder($field, $orientation='asc') {
		$this->order[$field] = $orientation;
		return $this;
	}

	// --------------------------------------------------------------------

	/**
	* Set Select  
	* @access public
	*/

	public function setSelect($select, $index=NULL) {
		if( is_null( $index ) ) {
			$this->select[] = $select;
		} else {
			$this->select[$index] = $select;
		}
		return $this;
	}

	// --------------------------------------------------------------------

	/**
	* Set Exclude  
	* @access public
	*/

	public function setExclude($exclude) {
		$this->exclude[] = $exclude;
		return $this;
	}

	// --------------------------------------------------------------------

	/**
	* Set Exclude  
	* @access public
	*/

	public function isDistinct($distinct=TRUE) {
		$this->distinct = $distinct;
		return $this;
	}
	
	// --------------------------------------------------------------------

	/**
	* Count All  
	* @access public
	*/

	public function count_all() {
		return  $this->db->count_all('media_uploads');
	}

	// --------------------------------------------------------------------

	/**
	* Count All Results 
	* @access public
	*/

	public function count_all_results() {
		$this->setupConditions();
		$this->db->from('media_uploads');
		return  $this->db->count_all_results();
	}
	
	// --------------------------------------------------------------------

	/**
	* Cache Control
	* @access public
	*/
	public function cache_on() {
		$this->cache_on = TRUE;
	}
	
	/**
	* Batch Insert
	* @access public
	*/
	public function updateBatch() {
		$this->batch = array_merge( $this->batch, array($this->getData()) );
	}
	
	public function insert_batch() {
		if( is_array( $this->batch ) && count( $this->batch ) > 0 ) {
			if( $this->db->insert_batch('media_uploads', $this->batch ) === TRUE ) {
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}
}

/* End of file ci_media_uploads_model.php */
/* Location: ./application/models/ci_media_uploads_model.php */
