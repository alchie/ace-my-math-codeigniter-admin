<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Lessons_model Class
 *
 * Manipulates `ci_lessons` table on database

CREATE TABLE `ci_lessons` (
  `lesson_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `lesson_title` varchar(100) NOT NULL,
  `lesson_slug` varchar(100) NOT NULL,
  `lesson_level` bigint(20) NOT NULL,
  `lesson_chapter` bigint(20) NOT NULL,
  `lesson_video_url` text,
  `lesson_video_duration` varchar(10) DEFAULT NULL,
  `lesson_number` int(10) NOT NULL,
  `lesson_active` int(1) DEFAULT '0',
  `lesson_quiz` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`lesson_id`),
  UNIQUE KEY `lesson_id` (`lesson_id`)
);

 * @package			Model
 * @project			Ace My Math, LLC
 * @project_link	http://www.acemymath.com/
 * @author			Chester Alan Tagudin
 * @author_link		http://www.chesteralan.com/
 */
 
class Lessons_model extends CI_Model {

	protected $lesson_id;
	protected $lesson_title;
	protected $lesson_slug;
	protected $lesson_level;
	protected $lesson_chapter;
	protected $lesson_video_url;
	protected $lesson_video_duration;
	protected $lesson_number;
	protected $lesson_active = '0';
	protected $lesson_quiz;
	protected $dataFields = array();
	protected $select = array();
	protected $join = array();
	protected $where = array();
	protected $where_or = array();
	protected $where_in = array();
	protected $where_in_or = array();
	protected $where_not_in = array();
	protected $where_not_in_or = array();
	protected $like = array();
	protected $like_or = array();
	protected $like_not = array();
	protected $like_not_or = array();
	protected $having = array();
	protected $having_or = array();
	protected $group_by = array();
	protected $filter = array();
	protected $order = array();
	protected $exclude = array();
	protected $required = array("lesson_title","lesson_level","lesson_chapter","lesson_number");
	protected $countField = '';
	protected $start = 0;
	protected $limit = 10;
	protected $results = FALSE;
	protected $distinct = FALSE;
	protected $cache_on = FALSE;
	protected $batch = array();

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct() {
		parent::__construct();
	}

	// --------------------------------------------------------------------


	// --------------------------------------------------------------------
	// Start Field: lesson_id
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `lesson_id` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setLessonId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->lesson_id = ( ($value == '') && ($this->lesson_id != '') ) ? '' : $value;

		if( $setWhere ) {
			$key = 'lessons.lesson_id';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}

			$this->__setCondition($underCondition, $key, $value, $priority);
			
		}
		
		if( $set_data_field ) {
			$this->dataFields[] = 'lesson_id';
		}
		return $this;
	}

	/** 
	* Get the value of `lesson_id` variable
	* @access public
	* @return String;
	*/

	public function getLessonId() {
		return $this->lesson_id;
	}

	/**
	* Get row by `lesson_id`
	* @param lesson_id
	* @return QueryResult
	**/

	public function getByLessonId() {
		if($this->lesson_id != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('lessons', array('lessons.lesson_id' => $this->lesson_id), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `lesson_id`
	**/

	public function updateByLessonId() {
		if($this->lesson_id != '') {
			$this->setExclude('lesson_id');
			if( $this->getData() ) {
				return $this->db->update('lessons', $this->getData(), array('lessons.lesson_id' => $this->lesson_id ) );
			}
		}
	}


	/**
	* Delete row by `lesson_id`
	**/

	public function deleteByLessonId() {
		if($this->lesson_id != '') {
			return $this->db->delete('lessons', array('lessons.lesson_id' => $this->lesson_id ) );
		}
	}

	/**
	* Increment row by `lesson_id`
	**/

	public function incrementByLessonId() {
		if($this->lesson_id != '' && $this->countField != '') {
			$this->db->where('lesson_id', $this->lesson_id);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('lessons');
		}
	}

	/**
	* Decrement row by `lesson_id`
	**/

	public function decrementByLessonId() {
		if($this->lesson_id != '' && $this->countField != '') {
			$this->db->where('lesson_id', $this->lesson_id);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('lessons');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: lesson_id
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: lesson_title
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `lesson_title` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setLessonTitle($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->lesson_title = ( ($value == '') && ($this->lesson_title != '') ) ? '' : $value;

		if( $setWhere ) {
			$key = 'lessons.lesson_title';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}

			$this->__setCondition($underCondition, $key, $value, $priority);
			
		}
		
		if( $set_data_field ) {
			$this->dataFields[] = 'lesson_title';
		}
		return $this;
	}

	/** 
	* Get the value of `lesson_title` variable
	* @access public
	* @return String;
	*/

	public function getLessonTitle() {
		return $this->lesson_title;
	}

	/**
	* Get row by `lesson_title`
	* @param lesson_title
	* @return QueryResult
	**/

	public function getByLessonTitle() {
		if($this->lesson_title != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('lessons', array('lessons.lesson_title' => $this->lesson_title), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `lesson_title`
	**/

	public function updateByLessonTitle() {
		if($this->lesson_title != '') {
			$this->setExclude('lesson_title');
			if( $this->getData() ) {
				return $this->db->update('lessons', $this->getData(), array('lessons.lesson_title' => $this->lesson_title ) );
			}
		}
	}


	/**
	* Delete row by `lesson_title`
	**/

	public function deleteByLessonTitle() {
		if($this->lesson_title != '') {
			return $this->db->delete('lessons', array('lessons.lesson_title' => $this->lesson_title ) );
		}
	}

	/**
	* Increment row by `lesson_title`
	**/

	public function incrementByLessonTitle() {
		if($this->lesson_title != '' && $this->countField != '') {
			$this->db->where('lesson_title', $this->lesson_title);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('lessons');
		}
	}

	/**
	* Decrement row by `lesson_title`
	**/

	public function decrementByLessonTitle() {
		if($this->lesson_title != '' && $this->countField != '') {
			$this->db->where('lesson_title', $this->lesson_title);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('lessons');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: lesson_title
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: lesson_slug
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `lesson_slug` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setLessonSlug($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->lesson_slug = ( ($value == '') && ($this->lesson_slug != '') ) ? '' : $value;

		if( $setWhere ) {
			$key = 'lessons.lesson_slug';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}

			$this->__setCondition($underCondition, $key, $value, $priority);
			
		}
		
		if( $set_data_field ) {
			$this->dataFields[] = 'lesson_slug';
		}
		return $this;
	}

	/** 
	* Get the value of `lesson_slug` variable
	* @access public
	* @return String;
	*/

	public function getLessonSlug() {
		return $this->lesson_slug;
	}

	/**
	* Get row by `lesson_slug`
	* @param lesson_slug
	* @return QueryResult
	**/

	public function getByLessonSlug() {
		if($this->lesson_slug != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('lessons', array('lessons.lesson_slug' => $this->lesson_slug), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `lesson_slug`
	**/

	public function updateByLessonSlug() {
		if($this->lesson_slug != '') {
			$this->setExclude('lesson_slug');
			if( $this->getData() ) {
				return $this->db->update('lessons', $this->getData(), array('lessons.lesson_slug' => $this->lesson_slug ) );
			}
		}
	}


	/**
	* Delete row by `lesson_slug`
	**/

	public function deleteByLessonSlug() {
		if($this->lesson_slug != '') {
			return $this->db->delete('lessons', array('lessons.lesson_slug' => $this->lesson_slug ) );
		}
	}

	/**
	* Increment row by `lesson_slug`
	**/

	public function incrementByLessonSlug() {
		if($this->lesson_slug != '' && $this->countField != '') {
			$this->db->where('lesson_slug', $this->lesson_slug);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('lessons');
		}
	}

	/**
	* Decrement row by `lesson_slug`
	**/

	public function decrementByLessonSlug() {
		if($this->lesson_slug != '' && $this->countField != '') {
			$this->db->where('lesson_slug', $this->lesson_slug);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('lessons');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: lesson_slug
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: lesson_level
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `lesson_level` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setLessonLevel($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->lesson_level = ( ($value == '') && ($this->lesson_level != '') ) ? '' : $value;

		if( $setWhere ) {
			$key = 'lessons.lesson_level';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}

			$this->__setCondition($underCondition, $key, $value, $priority);
			
		}
		
		if( $set_data_field ) {
			$this->dataFields[] = 'lesson_level';
		}
		return $this;
	}

	/** 
	* Get the value of `lesson_level` variable
	* @access public
	* @return String;
	*/

	public function getLessonLevel() {
		return $this->lesson_level;
	}

	/**
	* Get row by `lesson_level`
	* @param lesson_level
	* @return QueryResult
	**/

	public function getByLessonLevel() {
		if($this->lesson_level != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('lessons', array('lessons.lesson_level' => $this->lesson_level), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `lesson_level`
	**/

	public function updateByLessonLevel() {
		if($this->lesson_level != '') {
			$this->setExclude('lesson_level');
			if( $this->getData() ) {
				return $this->db->update('lessons', $this->getData(), array('lessons.lesson_level' => $this->lesson_level ) );
			}
		}
	}


	/**
	* Delete row by `lesson_level`
	**/

	public function deleteByLessonLevel() {
		if($this->lesson_level != '') {
			return $this->db->delete('lessons', array('lessons.lesson_level' => $this->lesson_level ) );
		}
	}

	/**
	* Increment row by `lesson_level`
	**/

	public function incrementByLessonLevel() {
		if($this->lesson_level != '' && $this->countField != '') {
			$this->db->where('lesson_level', $this->lesson_level);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('lessons');
		}
	}

	/**
	* Decrement row by `lesson_level`
	**/

	public function decrementByLessonLevel() {
		if($this->lesson_level != '' && $this->countField != '') {
			$this->db->where('lesson_level', $this->lesson_level);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('lessons');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: lesson_level
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: lesson_chapter
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `lesson_chapter` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setLessonChapter($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->lesson_chapter = ( ($value == '') && ($this->lesson_chapter != '') ) ? '' : $value;

		if( $setWhere ) {
			$key = 'lessons.lesson_chapter';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}

			$this->__setCondition($underCondition, $key, $value, $priority);
			
		}
		
		if( $set_data_field ) {
			$this->dataFields[] = 'lesson_chapter';
		}
		return $this;
	}

	/** 
	* Get the value of `lesson_chapter` variable
	* @access public
	* @return String;
	*/

	public function getLessonChapter() {
		return $this->lesson_chapter;
	}

	/**
	* Get row by `lesson_chapter`
	* @param lesson_chapter
	* @return QueryResult
	**/

	public function getByLessonChapter() {
		if($this->lesson_chapter != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('lessons', array('lessons.lesson_chapter' => $this->lesson_chapter), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `lesson_chapter`
	**/

	public function updateByLessonChapter() {
		if($this->lesson_chapter != '') {
			$this->setExclude('lesson_chapter');
			if( $this->getData() ) {
				return $this->db->update('lessons', $this->getData(), array('lessons.lesson_chapter' => $this->lesson_chapter ) );
			}
		}
	}


	/**
	* Delete row by `lesson_chapter`
	**/

	public function deleteByLessonChapter() {
		if($this->lesson_chapter != '') {
			return $this->db->delete('lessons', array('lessons.lesson_chapter' => $this->lesson_chapter ) );
		}
	}

	/**
	* Increment row by `lesson_chapter`
	**/

	public function incrementByLessonChapter() {
		if($this->lesson_chapter != '' && $this->countField != '') {
			$this->db->where('lesson_chapter', $this->lesson_chapter);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('lessons');
		}
	}

	/**
	* Decrement row by `lesson_chapter`
	**/

	public function decrementByLessonChapter() {
		if($this->lesson_chapter != '' && $this->countField != '') {
			$this->db->where('lesson_chapter', $this->lesson_chapter);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('lessons');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: lesson_chapter
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: lesson_video_url
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `lesson_video_url` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setLessonVideoUrl($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->lesson_video_url = ( ($value == '') && ($this->lesson_video_url != '') ) ? '' : $value;

		if( $setWhere ) {
			$key = 'lessons.lesson_video_url';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}

			$this->__setCondition($underCondition, $key, $value, $priority);
			
		}
		
		if( $set_data_field ) {
			$this->dataFields[] = 'lesson_video_url';
		}
		return $this;
	}

	/** 
	* Get the value of `lesson_video_url` variable
	* @access public
	* @return String;
	*/

	public function getLessonVideoUrl() {
		return $this->lesson_video_url;
	}

	/**
	* Get row by `lesson_video_url`
	* @param lesson_video_url
	* @return QueryResult
	**/

	public function getByLessonVideoUrl() {
		if($this->lesson_video_url != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('lessons', array('lessons.lesson_video_url' => $this->lesson_video_url), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `lesson_video_url`
	**/

	public function updateByLessonVideoUrl() {
		if($this->lesson_video_url != '') {
			$this->setExclude('lesson_video_url');
			if( $this->getData() ) {
				return $this->db->update('lessons', $this->getData(), array('lessons.lesson_video_url' => $this->lesson_video_url ) );
			}
		}
	}


	/**
	* Delete row by `lesson_video_url`
	**/

	public function deleteByLessonVideoUrl() {
		if($this->lesson_video_url != '') {
			return $this->db->delete('lessons', array('lessons.lesson_video_url' => $this->lesson_video_url ) );
		}
	}

	/**
	* Increment row by `lesson_video_url`
	**/

	public function incrementByLessonVideoUrl() {
		if($this->lesson_video_url != '' && $this->countField != '') {
			$this->db->where('lesson_video_url', $this->lesson_video_url);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('lessons');
		}
	}

	/**
	* Decrement row by `lesson_video_url`
	**/

	public function decrementByLessonVideoUrl() {
		if($this->lesson_video_url != '' && $this->countField != '') {
			$this->db->where('lesson_video_url', $this->lesson_video_url);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('lessons');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: lesson_video_url
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: lesson_video_duration
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `lesson_video_duration` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setLessonVideoDuration($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->lesson_video_duration = ( ($value == '') && ($this->lesson_video_duration != '') ) ? '' : $value;

		if( $setWhere ) {
			$key = 'lessons.lesson_video_duration';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}

			$this->__setCondition($underCondition, $key, $value, $priority);
			
		}
		
		if( $set_data_field ) {
			$this->dataFields[] = 'lesson_video_duration';
		}
		return $this;
	}

	/** 
	* Get the value of `lesson_video_duration` variable
	* @access public
	* @return String;
	*/

	public function getLessonVideoDuration() {
		return $this->lesson_video_duration;
	}

	/**
	* Get row by `lesson_video_duration`
	* @param lesson_video_duration
	* @return QueryResult
	**/

	public function getByLessonVideoDuration() {
		if($this->lesson_video_duration != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('lessons', array('lessons.lesson_video_duration' => $this->lesson_video_duration), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `lesson_video_duration`
	**/

	public function updateByLessonVideoDuration() {
		if($this->lesson_video_duration != '') {
			$this->setExclude('lesson_video_duration');
			if( $this->getData() ) {
				return $this->db->update('lessons', $this->getData(), array('lessons.lesson_video_duration' => $this->lesson_video_duration ) );
			}
		}
	}


	/**
	* Delete row by `lesson_video_duration`
	**/

	public function deleteByLessonVideoDuration() {
		if($this->lesson_video_duration != '') {
			return $this->db->delete('lessons', array('lessons.lesson_video_duration' => $this->lesson_video_duration ) );
		}
	}

	/**
	* Increment row by `lesson_video_duration`
	**/

	public function incrementByLessonVideoDuration() {
		if($this->lesson_video_duration != '' && $this->countField != '') {
			$this->db->where('lesson_video_duration', $this->lesson_video_duration);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('lessons');
		}
	}

	/**
	* Decrement row by `lesson_video_duration`
	**/

	public function decrementByLessonVideoDuration() {
		if($this->lesson_video_duration != '' && $this->countField != '') {
			$this->db->where('lesson_video_duration', $this->lesson_video_duration);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('lessons');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: lesson_video_duration
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: lesson_number
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `lesson_number` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setLessonNumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->lesson_number = ( ($value == '') && ($this->lesson_number != '') ) ? '' : $value;

		if( $setWhere ) {
			$key = 'lessons.lesson_number';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}

			$this->__setCondition($underCondition, $key, $value, $priority);
			
		}
		
		if( $set_data_field ) {
			$this->dataFields[] = 'lesson_number';
		}
		return $this;
	}

	/** 
	* Get the value of `lesson_number` variable
	* @access public
	* @return String;
	*/

	public function getLessonNumber() {
		return $this->lesson_number;
	}

	/**
	* Get row by `lesson_number`
	* @param lesson_number
	* @return QueryResult
	**/

	public function getByLessonNumber() {
		if($this->lesson_number != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('lessons', array('lessons.lesson_number' => $this->lesson_number), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `lesson_number`
	**/

	public function updateByLessonNumber() {
		if($this->lesson_number != '') {
			$this->setExclude('lesson_number');
			if( $this->getData() ) {
				return $this->db->update('lessons', $this->getData(), array('lessons.lesson_number' => $this->lesson_number ) );
			}
		}
	}


	/**
	* Delete row by `lesson_number`
	**/

	public function deleteByLessonNumber() {
		if($this->lesson_number != '') {
			return $this->db->delete('lessons', array('lessons.lesson_number' => $this->lesson_number ) );
		}
	}

	/**
	* Increment row by `lesson_number`
	**/

	public function incrementByLessonNumber() {
		if($this->lesson_number != '' && $this->countField != '') {
			$this->db->where('lesson_number', $this->lesson_number);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('lessons');
		}
	}

	/**
	* Decrement row by `lesson_number`
	**/

	public function decrementByLessonNumber() {
		if($this->lesson_number != '' && $this->countField != '') {
			$this->db->where('lesson_number', $this->lesson_number);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('lessons');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: lesson_number
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: lesson_active
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `lesson_active` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setLessonActive($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->lesson_active = ( ($value == '') && ($this->lesson_active != '') ) ? '' : $value;

		if( $setWhere ) {
			$key = 'lessons.lesson_active';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}

			$this->__setCondition($underCondition, $key, $value, $priority);
			
		}
		
		if( $set_data_field ) {
			$this->dataFields[] = 'lesson_active';
		}
		return $this;
	}

	/** 
	* Get the value of `lesson_active` variable
	* @access public
	* @return String;
	*/

	public function getLessonActive() {
		return $this->lesson_active;
	}

	/**
	* Get row by `lesson_active`
	* @param lesson_active
	* @return QueryResult
	**/

	public function getByLessonActive() {
		if($this->lesson_active != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('lessons', array('lessons.lesson_active' => $this->lesson_active), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `lesson_active`
	**/

	public function updateByLessonActive() {
		if($this->lesson_active != '') {
			$this->setExclude('lesson_active');
			if( $this->getData() ) {
				return $this->db->update('lessons', $this->getData(), array('lessons.lesson_active' => $this->lesson_active ) );
			}
		}
	}


	/**
	* Delete row by `lesson_active`
	**/

	public function deleteByLessonActive() {
		if($this->lesson_active != '') {
			return $this->db->delete('lessons', array('lessons.lesson_active' => $this->lesson_active ) );
		}
	}

	/**
	* Increment row by `lesson_active`
	**/

	public function incrementByLessonActive() {
		if($this->lesson_active != '' && $this->countField != '') {
			$this->db->where('lesson_active', $this->lesson_active);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('lessons');
		}
	}

	/**
	* Decrement row by `lesson_active`
	**/

	public function decrementByLessonActive() {
		if($this->lesson_active != '' && $this->countField != '') {
			$this->db->where('lesson_active', $this->lesson_active);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('lessons');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: lesson_active
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: lesson_quiz
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `lesson_quiz` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setLessonQuiz($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->lesson_quiz = ( ($value == '') && ($this->lesson_quiz != '') ) ? '' : $value;

		if( $setWhere ) {
			$key = 'lessons.lesson_quiz';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}

			$this->__setCondition($underCondition, $key, $value, $priority);
			
		}
		
		if( $set_data_field ) {
			$this->dataFields[] = 'lesson_quiz';
		}
		return $this;
	}

	/** 
	* Get the value of `lesson_quiz` variable
	* @access public
	* @return String;
	*/

	public function getLessonQuiz() {
		return $this->lesson_quiz;
	}

	/**
	* Get row by `lesson_quiz`
	* @param lesson_quiz
	* @return QueryResult
	**/

	public function getByLessonQuiz() {
		if($this->lesson_quiz != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('lessons', array('lessons.lesson_quiz' => $this->lesson_quiz), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `lesson_quiz`
	**/

	public function updateByLessonQuiz() {
		if($this->lesson_quiz != '') {
			$this->setExclude('lesson_quiz');
			if( $this->getData() ) {
				return $this->db->update('lessons', $this->getData(), array('lessons.lesson_quiz' => $this->lesson_quiz ) );
			}
		}
	}


	/**
	* Delete row by `lesson_quiz`
	**/

	public function deleteByLessonQuiz() {
		if($this->lesson_quiz != '') {
			return $this->db->delete('lessons', array('lessons.lesson_quiz' => $this->lesson_quiz ) );
		}
	}

	/**
	* Increment row by `lesson_quiz`
	**/

	public function incrementByLessonQuiz() {
		if($this->lesson_quiz != '' && $this->countField != '') {
			$this->db->where('lesson_quiz', $this->lesson_quiz);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('lessons');
		}
	}

	/**
	* Decrement row by `lesson_quiz`
	**/

	public function decrementByLessonQuiz() {
		if($this->lesson_quiz != '' && $this->countField != '') {
			$this->db->where('lesson_quiz', $this->lesson_quiz);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('lessons');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: lesson_quiz
	// --------------------------------------------------------------------


	// --------------------------------------------------------------------

	/**
	* Checks if result not empty 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function nonEmpty() {
		if ( $this->hasConditions() ) {
			
			$this->setupJoin();
			$this->setupConditions();
			$this->setupHaving();
			$this->setupGroupBy();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get('lessons', 1);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			$result = $query->result();
			if ( isset( $result[0] ) === true ) {
				$this->results = $result[0];
				return true;
			} else {
				return false;
			}
		}
	}


	/**
	* Get Results 
	* @return Mixed;
	*/

	public function getResults() {
		return $this->results;
	}


	// --------------------------------------------------------------------

	/**
	* Delete 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function delete() {
		if ( $this->hasConditions() ) {
				$this->setupConditions();
				return $this->db->delete('lessons');
		}
	}


	// --------------------------------------------------------------------

	/**
	* Limit Data Fields
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function limitDataFields($fields) {
		if($fields != '') {
			if( ! is_array($fields) ) {
				$this->dataFields = array($fields);
			} else {
				$this->dataFields = $fields;
			}
		}
	}

	// --------------------------------------------------------------------

	/**
	* Update 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function update() {
		if ( ( $this->getData() ) && ( $this->hasConditions() ) ) {
				$this->setupConditions();
				return $this->db->update('lessons', $this->getData() );
		}
	}


	// --------------------------------------------------------------------

	/**
	* Insert new row 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function insert() {
		if( $this->getData() ) {
			if( $this->db->insert('lessons', $this->getData() ) === TRUE ) {
				$this->lesson_id = $this->db->insert_id();
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}


	// --------------------------------------------------------------------

	/**
	* Replace new row 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function replace() {
		if( $this->getData() ) {
			if( $this->db->replace('lessons', $this->getData() ) === TRUE ) {
				$this->lesson_id = $this->db->insert_id();
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}
	
	// --------------------------------------------------------------------

	/**
	* Get First Data 
	* @access public
	* @return Object / False;
	*/

	public function get() {
		
		if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
		}
		
		$this->setupJoin();
		$this->setupConditions();
		$this->setupHaving();
		$this->setupGroupBy();
		
		if( $this->order ) {
			foreach( $this->order as $field=>$orientation ) {
				$this->db->order_by( $field, $orientation );
			}
		}
		
		$this->db->limit( 1, $this->start);
		
		if( $this->cache_on ) {
			$this->db->cache_on();
		}
		
		$query = $this->db->get('lessons');
		
		if( $this->cache_on ) {
			$this->db->cache_off();
		}
		
		$result = $query->result();
		
		if( isset($result[0]) ) {
			
			$this->results = $result[0];
			
			return $this->results;
			
		}
		
		return false;
	}
	// --------------------------------------------------------------------

	/**
	* Populate Data 
	* @access public
	* @param  String
	* @return Array;
	*/

	public function populate() {
	
		if( $this->distinct ) {
			$this->db->distinct();
		}
		
		if( $this->select ) {
				$this->db->select( implode(',' , $this->select ) );
		}
		
		$this->setupJoin();
		$this->setupConditions();
		$this->setupHaving();
		$this->setupGroupBy();
		
		if( $this->order ) {
			foreach( $this->order as $field=>$orientation ) {
				$this->db->order_by( $field, $orientation );
			}
		}
		
		if( $this->limit > 0 ) {
			$this->db->limit( $this->limit,$this->start);
		}
		
		if( $this->cache_on ) {
			$this->db->cache_on();
		}
		
		$query = $this->db->get('lessons');
		
		if( $this->cache_on ) {
			$this->db->cache_off();
		}
			
		$this->results = $query->result();
		
		return $this->results;
	}
	// --------------------------------------------------------------------
	/**
	* Recursive 
	* @access public
	* @param  String
	* @return Array;
	*/

	public function recursive($match, $find, $child='lesson_id', $level=10, $conn='children') {
			if( $level == 0 ) return;
			if( $this->limit > 0 ) {
				$this->db->limit( $this->limit,$this->start);
			}
			if( $this->select ) {
					$this->db->select( implode(',' , $this->select) );
			}
			
			$this->setupJoin();
			$this->setWhere($match, $find);
			$this->setupConditions();
			$this->setupHaving();
			$this->setupGroupBy();
			$this->clearWhere( $match );
			
			if( $this->order ) {
				foreach( $this->order as $field=>$orientation ) {
					$this->db->order_by( $field, $orientation );
				}
			}
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get('lessons');
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			$results=array();
			if( $query->result() ) {
				foreach( $query->result() as $qr ) {
					$children = $this->recursive($match, $qr->$child, $child, ($level-1), $conn ) ;
					$results[] = (object) array_merge( (array) $qr, array($conn=>$children) );
				}
			}
		return $results;
	}

// --------------------------------------------------------------------

	/**
	* Set Field Where Clause 
	* @access public
	* @param String / Array
	* @return Array;
	*/
	public function setFieldWhere($fields) {
		if($fields != '') {
			if( ! is_array($fields) ) {
				$this->where[] = array($fields => $this->$fields);
			} else {
				foreach($fields as $field) {
					if($w != '') {
						$this->where[] = array($field => $this->$field);
					}
				}
			}
		}
	}

// --------------------------------------------------------------------

	/**
	* Set Field Value Manually
	* @access public
	* @param Field Key ; Value
	* @return self;
	*/
	public function setFieldValue($field, $value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL) {
		if( isset( $this->$field ) ) {
			$this->$field = ( ($value == '') && ($this->$field != '') ) ? '' : $value;
			if( $setWhere ) {
				$key = 'lessons.'.$field;
				if ( $whereOperator != NULL && $whereOperator != '' ) {
					$key = $key . ' ' . $whereOperator;
				}
				//$this->where[$key] = $this->$field;
				$this->where[] = array( $key => $this->$field );
			}
			if( $set_data_field ) {
				$this->dataFields[] = $field;
			}
		}
		return $this;
	}
	
	// --------------------------------------------------------------------

	/**
	* Prepares data 
	* @access private
	* @return Array;
	*/
	public function getData($exclude=NULL) {
		$data = array();

		$fields = array("lesson_id","lesson_title","lesson_slug","lesson_level","lesson_chapter","lesson_video_url","lesson_video_duration","lesson_number","lesson_active","lesson_quiz");
		if( count( $this->dataFields ) > 0 ) {
    		            $fields = $this->dataFields;
		}
		foreach( $fields as $field ) {
		    if( ( in_array( $field, $this->required ) ) 
		    && ($this->$field == '') 
		    && ( ! in_array( $field, $this->exclude ) ) 
		    ) {
        		return false;
    		}
    		if( ( in_array( $field, $this->required ) ) 
    		&& ($this->$field != '') 
    		&& ( ! in_array( $field, $this->exclude ) ) 
    		) {
		        $data[$field] = $this->$field;
		    }
		    if( ( ! in_array( $field, $this->required ) ) 
		    && ( ! in_array( $field, $this->exclude ) ) 
		    ) {
        		$data[$field] = $this->$field;
    		}  
		}
		return $data;   
	}


	// --------------------------------------------------------------------

	/**
	* Setup Conditional Clauses 
	* @access public
	* @return Null;
	*/

        protected function __setCondition($condition, $key, $value, $priority) {
		switch( $condition ) {
			case 'where_or':
				if( ! is_array( $value ) ) {
					$value = explode(',', $value );
				}
				$this->setWhereOr($key, $value, $priority);
			break;
			case 'where_in':
				if( ! is_array( $value ) ) {
					$value = explode(',', $value );
				}
				$this->setWhereIn($key, $value, $priority);
			break;
			case 'where_in_or':
				if( ! is_array( $value ) ) {
					$value = explode(',', $value );
				}
				$this->setWhereInOr($key, $value, $priority);
			break;
			case 'where_not_in':
				if( ! is_array( $value ) ) {
					$value = explode(',', $value );
				}
				$this->setWhereNotIn($key, $value, $priority);
			break;
			case 'where_not_in_or':
				if( ! is_array( $value ) ) {
					$value = explode(',', $value );
				}
				$this->setWhereNotInOr($key, $value, $priority);
			break;
			case 'like':
				if( ! is_array( $value ) ) {
					$value = explode(',', $value );
				}
				$this->setLike($key, $value, $priority);
			break;
			case 'like_or':
				if( ! is_array( $value ) ) {
					$value = explode(',', $value );
				}
				$this->setLikeOr($key, $value, $priority);
			break;
			case 'like_not':
				if( ! is_array( $value ) ) {
					$value = explode(',', $value );
				}
				$this->setLikeNot($key, $value, $priority);
			break;
			case 'like_not_or':
				if( ! is_array( $value ) ) {
					$value = explode(',', $value );
				}
				$this->setLikeNotOr($key, $value, $priority);
			break;
			default:
				$this->setWhere($key, $value, $priority);
			break;
		}
	}
	
	protected function __apply_condition($what, $condition) {
		if( is_array( $condition ) ) {
			foreach( $condition as $key => $value ) {
				$this->db->$what( $key , $value );
			}
		} else {
			$this->db->$what( $condition );
		}
	}
	
	private function setupConditions() {
		$return = FALSE;
		
		$conditions = array_merge(
                                $this->where,
                                $this->filter,
                                $this->where_or,
                                $this->where_in,
                                $this->where_in_or,
                                $this->where_not_in,
                                $this->where_not_in_or,
                                $this->like,
                                $this->like_or,
                                $this->like_not,
                                $this->like_not_or
                                );
		
		if( count( $conditions ) > 0 ) {
			$i = 0;
			$sorted = array();
			
			foreach( $conditions as $condition ) {
				foreach( $condition as $field=>$options ) {
					$priority = $options['priority'];
					unset( $options['priority'] );
					if( ! is_null( $priority ) ) {
						while( isset($sorted[$priority]) ) {
							$i++;
							$priority += $i;
						}
						$sorted[ $priority ] = $options;
					}
				}
			}
			
			foreach( $conditions as $condition ) {
				foreach( $condition as $field=>$options ) {
					$priority = $options['priority'];
					unset( $options['priority'] );
					if( is_null( $priority )  ) {
						while( isset($sorted[$i]) ) {
							$i++;
						}
						$sorted[$i] = $options;
					} 
				}
			}
			
			ksort( $sorted );
		
			foreach( $sorted as $sortd ) {
				$sortd_key = array_keys( $sortd );
				$sortd_value = array_values( $sortd );
				
				if( isset( $sortd_key[0] ) && $sortd_value[0] ) {
					switch( $sortd_key[0] ) {
						case 'where_or':
							$this->__apply_condition('or_where', $sortd_value[0]);
						break;
						case 'where_in':
							$this->__apply_condition('where_in', $sortd_value[0]);
						break;
						case 'where_in_or':
							$this->__apply_condition('or_where_in', $sortd_value[0]);
						break;
						case 'where_not_in':
							$this->__apply_condition('where_not_in', $sortd_value[0]);
						break;
						case 'where_not_in_or':
							$this->__apply_condition('or_where_not_in', $sortd_value[0]);
						break;
						case 'like':
							$this->__apply_condition('like', $sortd_value[0]);
						break;
						case 'like_or':
							$this->__apply_condition('or_like', $sortd_value[0]);
						break;
						case 'like_not':
							$this->__apply_condition('not_like', $sortd_value[0]);
						break;
						case 'like_not_or':
							$this->__apply_condition('or_not_like', $sortd_value[0]);
						break;
						default:
							$this->__apply_condition('where', $sortd_value[0]);
						break;
					}
				}
			}
			$return = TRUE;
		}
		
		return $return;
	}
	
	private function setupHaving() {
		
		$return = FALSE;
		
		$conditions = array_merge( $this->having, $this->having_or );
		
		if( count( $conditions ) > 0 ) {
			$i = 0;
			$sorted = array();
			
			foreach( $conditions as $condition ) {
				foreach( $condition as $field=>$options ) {
					$priority = $options['priority'];
					unset( $options['priority'] );
					if( ! is_null( $priority ) ) {
						while( isset($sorted[$priority]) ) {
							$i++;
							$priority += $i;
						}
						$sorted[ $priority ] = $options;
					}
				}
			}
			
			foreach( $conditions as $condition ) {
				foreach( $condition as $field=>$options ) {
					$priority = $options['priority'];
					unset( $options['priority'] );
					if( is_null( $priority )  ) {
						while( isset($sorted[$i]) ) {
							$i++;
						}
						$sorted[$i] = $options;
					} 
				}
			}
			
			ksort( $sorted );
		
			foreach( $sorted as $sortd ) {
				$sortd_key = array_keys( $sortd );
				$sortd_value = array_values( $sortd );
				
				if( isset( $sortd_key[0] ) && $sortd_value[0] ) {
					switch( $sortd_key[0] ) {
						case 'having':
							$this->__apply_condition('having', $sortd_value[0]);
						break;
						case 'having_or':
							$this->__apply_condition('or_having', $sortd_value[0]);
						break;
					}
				}
			}
			$return = TRUE;
		}
		
		return $return;
	}
	
	/**
	* Check Conditions Availability
	* @access public
	* @return Array;
	*/
	private function hasConditions() {
		$conditions = array_merge(
                                $this->where,
                                $this->filter,
                                $this->where_or,
                                $this->where_in,
                                $this->where_in_or,
                                $this->where_not_in,
                                $this->where_not_in_or,
                                $this->like,
                                $this->like_or,
                                $this->like_not,
                                $this->like_not_or,
                                $this->having,
                                $this->having_or
                        );
		if( count( $conditions ) > 0 ) {
			return true;
		}
		return false;
	}
	

	// --------------------------------------------------------------------
	/**
	* Set Join Clause 
	* @access public
	* @param String / Array
	* @return Array;
	*/
	public function setJoin($table, $connection, $option='left') {
		$this->join[] = array('table'=>$table, 'connection'=>$connection, 'option'=>$option );
	}

	private function setupJoin() {
		if( $this->join ) {
			foreach( $this->join as $join ) {
				$this->db->join( $join['table'], $join['connection'], $join['option'] );
			}
		}
	}
	
	// --------------------------------------------------------------------

	/**
	* Set Filter 
	* @access public
	* @return Array;
	*/
	public function setFilter($field, $value, $table=NULL, $priority=NULL, $underCondition='where') {
		$key = array();
		if( $table == NULL ) { 
			$table = 'lessons'; 
		} 
		if( $table != '' ) {
			$key[] = $table;
		}
		$key[] = $field;
		
		$newField = implode('.', $key);
		
		if( is_null( $value ) ) {
			$where = $newField;
		} else {
			$where = array( $newField => $value );
		}
		
		$this->filter[] = array( $newField => array( $underCondition => $where, 'priority'=>$priority ) );
	}


	public function clearFilter($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->filter);
			$this->filter = array();
		} else {
			$newfilter = array();
			foreach($this->filter as $filter ) {
				if( ! isset( $filter[$field] ) ) {
					$newfilter[] = $filter;
				}
			}
			$this->filter = $newfilter;
		}
	}

	// --------------------------------------------------------------------

	/**
	* Set Where 
	* @access public
	*/
	public function setWhere($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->where[] = array( $field => array( 'where' => $where, 'priority'=>$priority )); 
	}


	public function clearWhere($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->where);
			$this->where = array();
		} else {
			$newwhere = array();
			foreach($this->where as $where ) {
				if( ! isset( $where[$field] ) ) {
					$newwhere[] = $where;
				}
			}
			$this->where = $newwhere;
		}
	}
	
	/**
	* Set Or Where 
	* @access public
	*/
	public function setWhereOr($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->where_or[] = array( $field => array( 'where_or' => $where, 'priority'=>$priority ));  
	}
	
	public function clearWhereOr($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->where_or);
			$this->where_or = array();
		} else {
			$newwhere_or = array();
			foreach($this->where_or as $where_or ) {
				if( ! isset( $where_or[$field] ) ) {
					$newwhere_or[] = $where_or;
				}
			}
			$this->where_or = $newwhere_or;
		}
	}
	
	/**
	* Set Where In
	* @access public
	*/
	public function setWhereIn($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->where_in[] = array( $field => array( 'where_in' => $where, 'priority'=>$priority ));  
	}
	
	public function clearWhereIn($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->where_in);
			$this->where_in = array();
		} else {
			$newwhere_in = array();
			foreach($this->where_in as $where_in ) {
				if( ! isset( $where_in[$field] ) ) {
					$newwhere_in[] = $where_in;
				}
			}
			$this->where_in = $newwhere_in;
		}
	}
	
	/**
	* Set Or Where In
	* @access public
	*/
	public function setWhereInOr($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->where_in_or[] = array( $field => array( 'where_in_or' => $where, 'priority'=>$priority ));  
	}

	
	public function clearWhereInOr($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->where_in_or);
			$this->where_in_or = array();
		} else {
			$newwhere_in_or = array();
			foreach($this->where_in_or as $where_in_or ) {
				if( ! isset( $where_in_or[$field] ) ) {
					$newwhere_in_or[] = $where_in_or;
				}
			}
			$this->where_in_or = $newwhere_in_or;
		}
	}
	
	/**
	* Set Where Not In
	* @access public
	*/
	public function setWhereNotIn($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->where_not_in[] = array( $field => array( 'where_not_in' => $where, 'priority'=>$priority )); 
	}
	
	public function clearWhereNotIn($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->where_not_in);
			$this->where_not_in = array();
		} else {
			$newwhere_not_in = array();
			foreach($this->where_not_in as $where_not_in ) {
				if( ! isset( $where_not_in[$field] ) ) {
					$newwhere_not_in[] = $where_not_in;
				}
			}
			$this->where_not_in = $newwhere_not_in;
		}
	}
	
	/**
	* Set Or Where Not In
	* @access public
	*/
	public function setWhereNotInOr($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->where_not_in_or[] = array( $field => array( 'where_not_in_or' => $where, 'priority'=>$priority )); 
	}

	public function clearWhereNotInOr($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->where_not_in_or);
			$this->where_not_in_or = array();
		} else {
			$newwhere_not_in_or = array();
			foreach($this->where_not_in_or as $where_not_in_or ) {
				if( ! isset( $where_not_in_or[$field] ) ) {
					$newwhere_not_in_or[] = $where_not_in_or;
				}
			}
			$this->where_not_in_or = $newwhere_not_in_or;
		}
	}
	
	// --------------------------------------------------------------------

	/**
	* Set Like
	* @access public
	*/
	public function setLike($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->like[] = array( $field => array( 'like' => $where, 'priority'=>$priority )); 
	}

	public function clearLike($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->like);
			$this->like = array();
		} else {
			$newlike = array();
			foreach($this->like as $like ) {
				if( ! isset( $like[$field] ) ) {
					$newlike[] = $like;
				}
			}
			$this->like = $newlike;
		}
	}
	
	/**
	* Set Like
	* @access public
	*/
	public function setLikeOr($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->like_or[] = array( $field => array( 'like_or' => $where, 'priority'=>$priority ));  
	}

	public function clearLikeOr($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->like_or);
			$this->like_or = array();
		} else {
			$newlike_or = array();
			foreach($this->like_or as $like_or ) {
				if( ! isset( $like_or[$field] ) ) {
					$newlike_or[] = $like_or;
				}
			}
			$this->like_or = $newlike_or;
		}
	}
	
	/**
	* Set Like
	* @access public
	*/
	public function setLikeNot($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->like_not[] = array( $field => array( 'like_not' => $where, 'priority'=>$priority ));  
	}

	public function clearLikeNot($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->like_not);
			$this->like_not = array();
		} else {
			$newlike_not = array();
			foreach($this->like_not as $like_not ) {
				if( ! isset( $like_not[$field] ) ) {
					$newlike_not[] = $like_not;
				}
			}
			$this->like_not = $newlike_not;
		}
	}
	
	/**
	* Set Like
	* @access public
	*/
	public function setLikeNotOr($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->like_not_or[] = array( $field => array( 'like_not_or' => $where, 'priority'=>$priority ));  
	}

	public function clearLikeNotOr($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->like_not_or);
			$this->like_not_or = array();
		} else {
			$newlike_not_or = array();
			foreach($this->like_not_or as $like_not_or ) {
				if( ! isset( $like_not_or[$field] ) ) {
					$newlike_not_or[] = $like_not_or;
				}
			}
			$this->like_not_or = $newlike_not_or;
		}
	}
	
	// --------------------------------------------------------------------
	
	/**
	* Set Having 
	* @access public
	*/
	public function setHaving($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$having = $field;
		} else {
			$having = array( $field => $value );
		}
		$this->having[] = array( $field => array( 'having' => $having, 'priority'=>$priority )); 
	}
	
	public function clearHaving($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->having);
			$this->having = array();
		} else {
			$newhaving = array();
			foreach($this->having as $having ) {
				if( ! isset( $having[$field] ) ) {
					$newhaving[] = $having;
				}
			}
			$this->having = $newhaving;
		}
	}
	
	/**
	* Set Or Having 
	* @access public
	*/
	public function setHavingOr($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$having = $field;
		} else {
			$having = array( $field => $value );
		}
		$this->having_or[] = array( $field => array( 'having_or' => $having, 'priority'=>$priority ));  
	}

	public function clearHavingOr($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->having_or);
			$this->having_or = array();
		} else {
			$newhaving_or = array();
			foreach($this->having_or as $having_or ) {
				if( ! isset( $having_or[$field] ) ) {
					$newhaving_or[] = $having_or;
				}
			}
			$this->having_or = $newhaving_or;
		}
	}
	
	// --------------------------------------------------------------------

	/**
	* Set Group By
	* @access public
	*/
	public function setGroupBy($fields) {
		if( is_array( $fields ) ) { 
			$this->group_by = array_merge( $this->group_by, $fields );
		} else {
			$this->group_by[] = $fields;
		}
	}

	private function setupGroupBy() {
		if( $this->group_by ) {
			$this->db->group_by( $this->group_by ); 
		}
	}
	
	// --------------------------------------------------------------------
	
	/**
	* Set Start  
	* @access public
	*/
	public function setStart($value) {
		$this->start = $value;
		return $this;
	}

	/**
	* Set Limit  
	* @access public
	*/
	public function setLimit($value) {
		$this->limit = $value;
		return $this;
	}

	// --------------------------------------------------------------------

	/**
	* Set Order  
	* @access public
	*/

	public function setOrder($field, $orientation='asc') {
		$this->order[$field] = $orientation;
		return $this;
	}

	// --------------------------------------------------------------------

	/**
	* Set Select  
	* @access public
	*/

	public function setSelect($select, $index=NULL) {
		if( is_null( $index ) ) {
			$this->select[] = $select;
		} else {
			$this->select[$index] = $select;
		}
		return $this;
	}

	// --------------------------------------------------------------------

	/**
	* Set Exclude  
	* @access public
	*/

	public function setExclude($exclude) {
		$this->exclude[] = $exclude;
		return $this;
	}

	// --------------------------------------------------------------------

	/**
	* Set Exclude  
	* @access public
	*/

	public function isDistinct($distinct=TRUE) {
		$this->distinct = $distinct;
		return $this;
	}
	
	// --------------------------------------------------------------------

	/**
	* Count All  
	* @access public
	*/

	public function count_all() {
		return  $this->db->count_all('lessons');
	}

	// --------------------------------------------------------------------

	/**
	* Count All Results 
	* @access public
	*/

	public function count_all_results() {
		$this->setupConditions();
		$this->db->from('lessons');
		return  $this->db->count_all_results();
	}
	
	// --------------------------------------------------------------------

	/**
	* Cache Control
	* @access public
	*/
	public function cache_on() {
		$this->cache_on = TRUE;
	}
	
	/**
	* Batch Insert
	* @access public
	*/
	public function updateBatch() {
		$this->batch = array_merge( $this->batch, array($this->getData()) );
	}
	
	public function insert_batch() {
		if( is_array( $this->batch ) && count( $this->batch ) > 0 ) {
			if( $this->db->insert_batch('lessons', $this->batch ) === TRUE ) {
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}
}

/* End of file ci_lessons_model.php */
/* Location: ./application/models/ci_lessons_model.php */
