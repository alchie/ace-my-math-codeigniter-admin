<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller {
    
    function __construct() 
    {
        parent::__construct();
                
        if ( !  $this->session->userdata('admin_logged_in') ) {
            $this->session->sess_destroy();
            if( $this->input->post('action') != '') {
				echo json_encode(array('not_logged_in' => true));
			} else {
				redirect('login', 'location', 301);
			}
            exit;
        }        
                
        $this->template_data->set('active_session', $this->session->all_userdata() );
        $this->template_data->set('admin_access', json_decode( base64_decode( $this->session->userdata('admin_access') ) ) );
        $this->template_data->set('main_page', 'dashboard' );
        $this->template_data->set('sub_page', 'dashboard' ); 
        
        //$this->output->enable_profiler(TRUE);       

    }

}

