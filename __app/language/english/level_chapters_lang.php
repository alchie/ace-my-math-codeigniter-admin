<?php

// level_chapters

$lang['level_chapters_level_chapter_id'] = 'ID';
$lang['level_chapters_level_id'] = 'Level';
$lang['level_chapters_chapter_id'] = 'Chapter';
$lang['level_chapters_chapter_title'] = 'Chapter Title';

/* End of file level_chapters_lang.php */

/* Location: ./application/language/english/level_chapters_lang.php */
