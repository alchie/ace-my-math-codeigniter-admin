<?php

// users_logs

$lang['users_logs_log_id'] = 'ID';
$lang['users_logs_log_date'] = 'Date';
$lang['users_logs_user_id'] = 'User';
$lang['users_logs_log_code'] = 'Code';
$lang['users_logs_log_msg'] = 'Content';

/* End of file users_logs_lang.php */

/* Location: ./application/language/english/users_logs_lang.php */
