<?php

// subscription_plans

$lang['subscription_plans_s_plan_id'] = 'ID';
$lang['subscription_plans_s_plan_name'] = 'Plan Name';
$lang['subscription_plans_s_plan_price'] = 'Price';
$lang['subscription_plans_s_plan_active'] = 'Active';
$lang['subscription_plans_s_plan_order'] = 'Order';

/* End of file subscription_plans_lang.php */

/* Location: ./application/language/english/subscription_plans_lang.php */
