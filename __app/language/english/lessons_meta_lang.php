<?php

// lessons_meta

$lang['lessons_meta_lmeta_id'] = 'Meta ID';
$lang['lessons_meta_lesson_id'] = 'Lesson';
$lang['lessons_meta_meta_key'] = 'Meta Key';
$lang['lessons_meta_meta_value'] = 'Meta Value';

/* End of file lessons_meta_lang.php */

/* Location: ./application/language/english/lessons_meta_lang.php */
