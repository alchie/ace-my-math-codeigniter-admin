<?php

// subscription_attributes

$lang['subscription_attributes_s_attr_id'] = 'Attribute ID';
$lang['subscription_attributes_s_plan_id'] = 'Plan';
$lang['subscription_attributes_attr_id'] = 'Plan Attributes';
$lang['subscription_attributes_s_attr_value'] = 'Value';

/* End of file subscription_attributes_lang.php */

/* Location: ./application/language/english/subscription_attributes_lang.php */
