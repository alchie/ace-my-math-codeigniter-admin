<?php

// lessons_quiz

$lang['lessons_quiz_quiz_id'] = 'ID';
$lang['lessons_quiz_lesson_id'] = 'Lesson';
$lang['lessons_quiz_quiz_number'] = 'Number';
$lang['lessons_quiz_quiz_time'] = 'Time';
$lang['lessons_quiz_quiz_question'] = 'Question';
$lang['lessons_quiz_quiz_type'] = 'Type';
$lang['lessons_quiz_quiz_details'] = 'Details';
$lang['lessons_quiz_quiz_correct'] = 'Correct Answer';

/* End of file lessons_quiz_lang.php */

/* Location: ./application/language/english/lessons_quiz_lang.php */
