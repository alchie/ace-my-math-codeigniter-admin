<?php

// lessons

$lang['lessons_lesson_id'] = 'ID';
$lang['lessons_lesson_title'] = 'Title';
$lang['lessons_lesson_slug'] = 'Slug';
$lang['lessons_lesson_level'] = 'Grade Level';
$lang['lessons_lesson_chapter'] = 'Chapter';
$lang['lessons_lesson_video_url'] = 'Video URL';
$lang['lessons_lesson_video_duration'] = 'Video Duration';
$lang['lessons_lesson_number'] = 'Number';
$lang['lessons_lesson_active'] = 'Active';
$lang['lessons_lesson_quiz'] = 'Quiz JS File';

/* End of file lessons_lang.php */

/* Location: ./application/language/english/lessons_lang.php */
