<?php

// users_profiles

$lang['users_profiles_profile_id'] = 'Profile ID';
$lang['users_profiles_user_id'] = 'User';
$lang['users_profiles_attr_id'] = 'Profile Attribute';
$lang['users_profiles_profile_value'] = 'Value';

/* End of file users_profiles_lang.php */

/* Location: ./application/language/english/users_profiles_lang.php */
