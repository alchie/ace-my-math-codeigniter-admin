<?php

// users

$lang['users_user_id'] = 'ID';
$lang['users_user_username'] = 'Username';
$lang['users_user_password'] = 'Password';
$lang['users_user_type'] = 'User Type';
$lang['users_user_level'] = 'Grade Level';
$lang['users_user_firstname'] = 'Firstname';
$lang['users_user_lastname'] = 'Lastname';
$lang['users_user_email'] = 'Email';
$lang['users_user_plan'] = 'Membership Plan';
$lang['users_user_created'] = 'Created';
$lang['users_user_expiry'] = 'Expiry';
$lang['users_user_active'] = 'Active';
$lang['users_last_login'] = 'Last Login Date';
$lang['users_last_login_ip'] = 'Last Login IP';

/* End of file users_lang.php */

/* Location: ./application/language/english/users_lang.php */
