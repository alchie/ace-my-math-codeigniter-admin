<?php

// lessons_tags

$lang['lessons_tags_id'] = 'Id';
$lang['lessons_tags_tag_id'] = 'Topic';
$lang['lessons_tags_lesson_id'] = 'Lesson';
$lang['lessons_tags_active'] = 'Active';

/* End of file lessons_tags_lang.php */

/* Location: ./application/language/english/lessons_tags_lang.php */
