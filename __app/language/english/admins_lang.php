<?php

// admins

$lang['admins_id'] = 'Admin ID';
$lang['admins_name'] = 'Name';
$lang['admins_email'] = 'Email';
$lang['admins_password'] = 'Password';
$lang['admins_username'] = 'Username';
$lang['admins_active'] = 'Active';
$lang['admins_date_created'] = 'Date Created';
$lang['admins_last_login'] = 'Last Login';
$lang['admins_last_login_ip'] = 'Last Login IP Address';

/* End of file admins_lang.php */

/* Location: ./application/language/english/admins_lang.php */
