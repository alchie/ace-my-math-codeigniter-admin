<?php

// tax_chapters

$lang['tax_chapters_chapter_id'] = 'ID';
$lang['tax_chapters_chapter_name'] = 'Name';
$lang['tax_chapters_chapter_slug'] = 'Slug';
$lang['tax_chapters_chapter_description'] = 'Description';
$lang['tax_chapters_chapter_active'] = 'Active';
$lang['tax_chapters_chapter_order'] = 'Order';

/* End of file tax_chapters_lang.php */

/* Location: ./application/language/english/tax_chapters_lang.php */
