<?php

// settings

$lang['settings_setting_id'] = 'Setting ID';
$lang['settings_setting_controller'] = 'Controller';
$lang['settings_setting_name'] = 'Name';
$lang['settings_setting_description'] = 'Description';
$lang['settings_setting_value'] = 'Value';

/* End of file settings_lang.php */

/* Location: ./application/language/english/settings_lang.php */
