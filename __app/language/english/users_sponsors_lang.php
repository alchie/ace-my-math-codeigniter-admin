<?php

// users_sponsors

$lang['users_sponsors_sponsorship_id'] = 'Sponsorship ID';
$lang['users_sponsors_user_id'] = 'User ID';
$lang['users_sponsors_sponsor_id'] = 'Sponsor';
$lang['users_sponsors_date_sponsored'] = 'Date Sponsored';
$lang['users_sponsors_active'] = 'Active';

/* End of file users_sponsors_lang.php */

/* Location: ./application/language/english/users_sponsors_lang.php */
