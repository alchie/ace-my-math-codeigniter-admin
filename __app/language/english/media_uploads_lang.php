<?php

// media_uploads

$lang['media_uploads_media_id'] = 'Media ID';
$lang['media_uploads_file_name'] = 'Filename';
$lang['media_uploads_file_type'] = 'File Type';
$lang['media_uploads_file_path'] = 'File Path';
$lang['media_uploads_full_path'] = 'Full Path';
$lang['media_uploads_raw_name'] = 'Raw Name';
$lang['media_uploads_orig_name'] = 'Original Name';
$lang['media_uploads_client_name'] = 'Client Name';
$lang['media_uploads_file_ext'] = 'File Extension';
$lang['media_uploads_file_size'] = 'File Size';
$lang['media_uploads_is_image'] = 'Is Image';
$lang['media_uploads_image_width'] = 'Image Width';
$lang['media_uploads_image_height'] = 'Image Height';
$lang['media_uploads_image_type'] = 'Image Type';
$lang['media_uploads_image_size_str'] = 'Image Size String';

/* End of file media_uploads_lang.php */

/* Location: ./application/language/english/media_uploads_lang.php */
