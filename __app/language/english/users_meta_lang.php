<?php

// users_meta

$lang['users_meta_umeta_id'] = 'Meta ID';
$lang['users_meta_user_id'] = 'User';
$lang['users_meta_meta_key'] = 'Meta Key';
$lang['users_meta_meta_value'] = 'Meta Value';

/* End of file users_meta_lang.php */

/* Location: ./application/language/english/users_meta_lang.php */
