<?php

// admins_access

$lang['admins_access_id'] = 'Access ID';
$lang['admins_access_admin_id'] = 'Admin';
$lang['admins_access_controller'] = 'Controller';
$lang['admins_access_add'] = 'Can Add';
$lang['admins_access_edit'] = 'Can Edit';
$lang['admins_access_delete'] = 'Can Delete';

/* End of file admins_access_lang.php */

/* Location: ./application/language/english/admins_access_lang.php */
