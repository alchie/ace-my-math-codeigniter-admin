<?php

// goals

$lang['goals_goal_id'] = 'ID';
$lang['goals_goal_name'] = 'Name';
$lang['goals_goal_slug'] = 'Slug';
$lang['goals_grade_level'] = 'Grade Level';
$lang['goals_goal_public'] = 'Public';
$lang['goals_creator'] = 'Created By';

/* End of file goals_lang.php */

/* Location: ./application/language/english/goals_lang.php */
