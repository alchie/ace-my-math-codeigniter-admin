<?php

// tax_levels

$lang['tax_levels_level_id'] = 'ID';
$lang['tax_levels_level_name'] = 'Name';
$lang['tax_levels_level_slug'] = 'Slug';
$lang['tax_levels_level_description'] = 'Description';
$lang['tax_levels_level_active'] = 'Active';
$lang['tax_levels_level_order'] = 'Order';

/* End of file tax_levels_lang.php */

/* Location: ./application/language/english/tax_levels_lang.php */
