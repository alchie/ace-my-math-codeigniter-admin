<?php

// attributes

$lang['attributes_attr_id'] = 'Attribute ID';
$lang['attributes_attr_controller'] = 'Controller';
$lang['attributes_attr_name'] = 'Name';
$lang['attributes_attr_label'] = 'Label';
$lang['attributes_attr_description'] = 'Description';

/* End of file attributes_lang.php */

/* Location: ./application/language/english/attributes_lang.php */
