<?php

// tax_tags

$lang['tax_tags_tag_id'] = 'ID';
$lang['tax_tags_tag_name'] = 'Topic Name';
$lang['tax_tags_tag_slug'] = 'Slug';
$lang['tax_tags_tag_count'] = 'Count';
$lang['tax_tags_tag_active'] = 'Active';

/* End of file tax_tags_lang.php */

/* Location: ./application/language/english/tax_tags_lang.php */
