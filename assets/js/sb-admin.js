(function($) {
	
$(function() {
	if( $('#side-menu').length != 0 ) {
		$('#side-menu').metisMenu();
	}
	if( $('.datetimepicker').length != 0 ) {
		$('.datetimepicker').datetimepicker();
	}
});

//Loads the correct sidebar on window load,
//collapses the sidebar on window resize.
$(function() {
    $(window).bind("load resize", function() {
        if ($(this).width() < 768) {
            $('div.sidebar-collapse').addClass('collapse')
        } else {
            $('div.sidebar-collapse').removeClass('collapse')
        }
    })
});

$(document).ready(function() {
        $('.btn-delete').click(function() {
            var conf = confirm('Are you sure?');
            if(conf) {
                return true;
            }
            return false;
        });
});
})(jQuery);

